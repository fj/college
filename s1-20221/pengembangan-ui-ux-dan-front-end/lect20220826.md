---
author: Faiz Unisa Jazadi
date: '2022-08-26'
numbersections: true
title: Konsep User Experience
---

# Preamble

User experience berlaku pada semua produk. User experience adalah rasa, emosi,
pengalaman, dan dampak aksi pada seorang pengguna sebagai hasil dari
persinggungan aktivitas pengguna produk tersebut dengan suatu produk.

# Ada apa dengan UX?

-   Semua produk menghasilkan: pengalaman, emosi, rasa

-   Di bidang software development: aplikasi dan web

-   Aplikasi komputer dengan desain bagus **tidak berarti** experience bagus.
    Kemudian, jika experience bagus, maka desain bagus.

# Posisi UX (UX - UI)

-   On-stage: UI seperti content, layout, interactions, colors, typography,
    features, icons, speed

-   Back-stage: UX seperti persona, analytics insight, journey maps, sketches,
    wirefames, heatmaps, testing, accessibility

## Bidang UX

-   Experience strategy
-   User research
-   Information architecture
-   Interaction design
-   Usability research
-   Accessibility
-   Visual design
-   Content

UI designer adalah staff UX designer sehingga UX designer merasa hanya perlu
fokus ke UX design. Namun, jika tidak ada staff, maka UX designer juga perlu
menguasai pembagian kerja staff.

## Proses Desain

Beberapa pendekatan sebagai berikut.

## User-centered Thinking

Lebih mendekati sistem agile dan lifecycle.

1.  Lakukan kajian as a user
2.  Lakukan proses berulang-ulang
3.  Mencoba sekali belum tentu sukses: ajukan banyak alternatif desain
4.  Cari kesan mendalam dalam setiap kajian

## 10 Usability Heuristics (Prinsip Jacob Nielson)

### Visibility of system status

-   Provide confidence: sebagus apapun websitenya, harus memberi keyakinan pada
    user (misalnya selalu menampilkan logo UGM dan tidak mengubah logo secara
    tiba-tiba)

-   Keep users informed: cegah user mengalami kebingungan dengan selalu
    menginformasikan apa yang terjadi (misal dengan sistem navigasi, atau dengan
    alert jika proses gagal/berhasil)

-   Error prevention: cegah agar user tidak melakukan kesalahan secara tidak
    sengaja (mis. salah format DOB)

-   Situational resilience: pastikan aplikasi tetap bisa digunakan pada kondisi
    apapun (mis. desain yang nyaman pada siang dan malam hari)

### Match between system and the real world

-   Familiar language: misalnya menampilkan suhu dalam derajat celcius karena
    aplikasi digunakan di Indonesia

-   Match real-world items misalnya logo home yang melambangkan logo rumah

-   Logical order: paging, column, form, menu

-   Coordination

-   Plain language: hindari jargon dan bahasa yang terlalu formal (hindari juga
    beberapa istilah seperti galat dan suka)

### User control and freedom

-   Supporting confidence

-   Allow user to go back, usahakan back jangan meragukan, jangan melompat, dan
    tepat ke state sebelumnya

-   Confirmation and undo: konfirmasi menghindari insiden

-   Easily identifiable exists: hargai usaha user menemukan

### Consistency and standard

Perhatikan standar dan harus **konsisten**.

-   Visual: icon, posisi, warna, size, tipografi, style (mis. konsisten dengan
    istilah back atau kembali)

-   Convention: istilah, symbol, format, masking

-   Action: click, double click, right click

-   Breaking: sela alur

### Error prevention

-   To error is human

-   Slips vs mistakes

-   Choosing good defaults

-   Forgiving formatting

-   Showing previews

-   Affordances

Contoh: tanggal lahir tidak boleh salah format (berikan input masking, berikan
juga contoh)

### Recognition rather than recall

-   Recognition: user mengenali apa yang harus dilakukan

-   Recall: user harus mengingat kembali

-   Integrating recall into an interface

### Flexibility and efficiency of use

-   Same tasks, different paths: misal beli produk promo dan produk biasa
    alurnya sama dan konsisten

-   Give experienced users superpowers

-   Customizing interface

### Aesthetic and minimalist design

-   Competing information: jangan sampai design malah melawan informasi yang
    disampaikan (mis. design yang sangat heboh hingga mengalihkan perhatian user
    dan membuat informasi tidak bisa tersampaikan dengan baik)

-   Minimal tidak berarti **bare**

-   Communicate, not decorate

### Help users recognize, diagnose and recover from errors

-   Anticipate errors
-   Language mtters
-   Constructive advice
-   Ensure work is kept

Pastikan ada panduan atau jalan keluar ketika kita masuk ke dalam suatu kondisi
yang tidak diharapkan.

### Help and documentation

Ada panduan dan dokumentasi.

-   Proactive help

-   Reactive help

# Tugas Mandiri

**Deadline: 2 September 2022 (1 minggu)**

Sebelum mendesain suatu UX product, seorang desainer harus bisa menjelaskan dan
melaporkan experience-nya pada suatu product (user-centered thinking)

-   Tujuan: mahasiswa mampu melakukan user-research

-   Project:

    -   Kajian UX satu atau beberapa aplikasi (desktop, mobile, atau web)

    -   Kajian bisa mengacu pada 10 prinsip Jacob Nielson

    -   Kajian paling tidak memuat persepsi sebagai user, kritik, dan koreksi
        aspek UX pda produk yang dikaji

    -   Hasil kajian berupa **poster ilmiah** (1 halaman), berukuran bebas (bisa
        A4, folio), bisa digital
