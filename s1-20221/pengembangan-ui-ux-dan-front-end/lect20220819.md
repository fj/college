---
author: Faiz Unisa Jazadi
date: '2022-08-19'
numbersections: true
title: 'Pengembangan UI/UX dan Front-end (Intro)'
---

# Overview

Mata kuliah ini dilaksanakan secara hybrid: luring 50% dan daring 50%.

Selama kuliah luring, tidak ada sesi tatap maya.

Pastikan jaga kondusifitas kelas.

Digunakan beberapa utility/online tools seperti Figma dan GitHub.

Sebenarnya pengembangan web dibagi menjadi front-end dan back-end. Namun, u
back-end tidak dibahas di mata kuliah ini melainkan mempunyai mata kuliahnya
sendiri.

# UI/UX

User interface, user experience. Terdapat keterkaitan antara keduanya.

User interface mengacu pada bagian yang berinteraksi atau merupakan antarmuka
untuk pengguna. Sedangkan user experience adalah pengalaman yang dihasilkan
ketika pengguna menggunakan interface.

UI didesain sedemikian rupa sehingga UX-nya mantap.

UI adalah bagaimana user bisa mendapatkan sisi fungsionalitas dari produk.

## Proses

Sebelum production, tahapan yang dilakukan oleh designer diringkas dalam
beberapa tahap berikut:

### Case study: Bisnis bubble tea (boba)

-   Riset: minuman berfungsi menghilangkan haus, oleh karena itu ditambah
    *bubble* (dilihat dari segi komposisi)

-   User flow: cara mengonsumsi, metode pengemasan

-   Prototyping & arsitektur: calon minuman sudah jadi, sudah bisa dicoba

-   Visual design/interface: kemasan disiapkan ketika sudah mau dirilis (kemasan
    boba adalah gelas kemudian disegel plastik).

-   Usability: jika dibayangkan tanpa sedotan (tanpa interface yang
    memfasilitasi), maka pengalaman (UX) akan menjadi buruk dan produk menjadi
    kehilangan fungsi utamanya. Agar UX-nya bagus, harus ada cara sehingga
    produk bisa fungsional: misalnya dengan memberikan cara untuk meminum tanpa
    minumannya tumpah atau berserakan (dengan menggunakan sedotan atau suatu
    lubang yang bisa dibuka dengan mudah).

### Case-study: Game balap

Interface tidak selalu tampak. Game balap yang memanfaatkan gyroscope adalah
salah satu contohnya. Dalam hal ini, gyroscope termasuk interface. Keyboard juga
termasuk interface.

# UX dan UX Designer

-   Experience: pengalaman

-   Pengalaman user (setelah menggunakan aplikasi) yang diharapkan:

    -   Nyaman (minim keluhan)
    -   Suka (tidak anti)
    -   Bangga (promosi)

-   UX designer mendesain sesuatu yang belum ada (bagaimana bisa ada pengalaman
    jika produknya belum ada?)

## Pendekatan Designer

Menggiring atau membuat skenario (mengkondisikan). Kita mencoba menciptakan
pengalaman user.

**Contoh** Ketika mendesain aplikasi media sosial, harus diputuskan apakah yang
tampil terlebih dahulu adalah gambar atau komentar. Oleh karena itu, berdasarkan
pengalaman serupa sebelumnya (atau mendapat pengalaman dari orang yang pernah),
dapat diputuskan bahwa menampilkan gambar terlebih dahulu kemudian diikuti
komentar adalah hal yang tepat.

## Pengalaman

Didapatkan dari sumber keluhan/opini/review:

-   Interaksi
-   Navigasi
-   Command
-   Komposisi
-   Arsitekur
-   User flow

Didapatkan juga dari apa yang disukai:

-   Grafis
-   Tata warna
-   Tipografi
-   Layout

Yang membuat bangga:

-   Branding
-   Konsistensi
-   Trend

# UI dan UI Designer

-   **Inter**face: antarmuka, tidak sekedar visual

-   Komponen yang **menjembatani** interaksi user dengan aplikasi

-   Faktor awal:

    -   Usability
    -   User friendly
    -   Command simplicity
    -   Perkakas

## Pergeseran Ruang Lingkup UI

-   Tools (misal mouse, touch screen, keyboard, radio button)
-   Arsitektur
-   Komposisi (warna, bentuk)
-   Ergonomi (effort yang dibutuhkan)
-   Usability (kenyamanan untuk digunakan)
-   Estetika (nyaman dilihat, termasuk dari segi warna)

Digeser menjadi...

-   Komposisi
-   Arsitektur
-   Estetika

Sisanya menjadi ruang lingkup UX.

## Tren Desain UI

-   Visual: menarik, unik, ikonik
-   Trend: platform, framework, branding
-   Ergonomis

# Buku Acuan

Tidak bajakan.
