---
author: 'Faiz Unisa Jazadi -- `21/475298/PA/20563`'
date: '2022-09-30'
numbersections: true
title: Manajemen State
---

# Apa itu state?

-   Kondisi atau situasi pada aplikasi

-   Representasi dari suatu sistem dalam waktu tertentu

        .-------.                   .-------.
        | x = 2 | user interaction  | x = 4 |
        | y = 3 | ----------------> | y = 5 |
        '-------'                   '-------'
         State A                     State B

# State Management

... adalah metode mengelola state (aplikasi).

-   Aksi mendifinisikan sekumpulan bentuk data dan operasi terhadapnya
-   Sekumpulan teknik yang menangani perubahan
-   Kompleksitas berbanding lurus dengan ukuran aplikasi dan waktu

# Tipe State

-   Apps domain
-   View state
-   Network state
-   Communication state
-   Navigation state

# Kenapa Perlu State Management

-   Semakin banyak fitur, semakin banyak state
-   Semakin banyak state, semakin kompleks
-   Rantai state bisa membentuk jejaring
-   Pemisahan frontend-backend mengarah pada akumulasi state di frontend

# Framework Manajemen State

## Redux

-   1st state management framework
-   State container

Prinsip-prinsip:

-   Satu sumber
    -   State dikelola terpusat
    -   Satu wadah
-   Immutable
    -   State bersifat read only
    -   View hanya bisa update state lewat action
    -   Perubahan hanya dengan Reducer
        -   Fungsi yang akan mengambil state
        -   Menghasilkan state baru

Aliran data di Redux:

Inisialisasi:

-   Store dibuat
-   Inisial state: store call root reducer
-   view akses state di store

Update:

-   ada event
-   event dispatch action
-   store call reducer
-   reducer return state baru
-   store notifikasi ke view
-   view akses state baru di store

## Mobx

-   Simple
-   Scalable
-   Reactive
-   Aplikasi = spreadsheet

# Todo (liburan MID)

-   Perhatikan kegiatan transaksi di kantin/cafe FMIPA termasuk situasi dan
    kondisi kantin dan pengunjungnya.
-   Explore aplikasi kiosk order (pemesanan menu secara mandiri lewat komputer
    layar sentuh), contoh di outlet McD.
-   Explore pembayaran mandiri dengan QRIS di beberapa outlet.
-   pelajari tentang teknologi web dasar, DOM, dan web component.
