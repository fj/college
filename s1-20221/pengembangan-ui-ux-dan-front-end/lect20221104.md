---
author: Faiz Jazadi
date: '2022-11-04'
numbersections: true
title: Persona
---

# User Persona

Persona adalah bagaimana menggambarkan user, tidak hanya tentang produk yang
dibuat, tapi juga menggambarkan secara lengkap tentang posisi atau keadaan atau
karakteristik dari pengguna.
