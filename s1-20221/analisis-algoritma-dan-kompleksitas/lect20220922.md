---
author: Faiz Unisa Jazadi
date: '2022-09-22'
numbersections: true
title: Divide and Conquer (cont.)
---

# Matrix Multiplication

Digunakan dalam ilustrasi:

Input adalah matrix $A$ dan $B$.

Output: $C = [c_{ij}] = A \cdot B$.

## Algoritma Standar

Nested loop: $i$, $j$, $k$.

## Algoritma Divide-and-Conquer

Matrix berukuran $m\ \times\ n$ dijadikan matriks $2\ \times\ 2$ dari submatriks
berukuran $(n/2)\ \times\ (n/2)$.

Pada akhirnya, metode divide-and-conquer bedanya tidak jauh dengan metode
standar.

### Ide Strassen

Re-use operasi yang sudah dilakukan (mengurangi panggilan rekursif).

1.  Divide: partisi $A$ dan $B$ menjadi $(n/2)\times(n/2)$ submatriks. Bentuk
    term untuk dikalikan menggunakan $+$ dan $-$.
2.  Conquer: lakukan tujuh multiplikasi....

#### Analisis Strassen

$$ T(n) = 7 T(n/2) + \Theta(n^2) $$

# Kesimpulan

Divide-and-conquer adalah satu dari beberapa metode yang powerful untuk
mendesain algoritma.

Divide-and-conquer dapat dianalisa dengan menggunakan rekurensi dan master
method.

Strategi divide-and-conquer seringkali mengarah pada algoritma yang efisien.
