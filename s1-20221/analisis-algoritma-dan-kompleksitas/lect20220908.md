---
author: Faiz Unisa Jazadi
date: '2022-09-08'
numbersections: true
title: 'Pertemuan Minggu Ke-4'
---

**Catatan ini hanya berisi kata kunci materi tatap muka (tidak komprehensif).**

# Recursion-tree Method

-   Bisa tidak reliabel
-   Mengandalkan intuisi
-   Bagus untuk mendapatkan gambaran tebakan untuk metode substitusi

# The master method

Berlaku untuk rekurensi dalam bentuk

$$ T(n) = a\ T(n/b) + f(n) $$

dengan $a \geq 1, b > 1$ dan $f$ bernilai positif secara asimtotik.

Terdapat tiga kasus umum untuk master method.

![](img/w4-master-method.png){height="150px"}

Ide dari master theorem berasal dari visualisasi rekurensi melalui pohon
rekursi.

# Data structures

Definisi-definisi untuk istilah umum:

-   Static problems: input $\rightarrow$ output

-   Dynamic problems: sequence of operations (one at a time) $\rightarrow$
    sequence of outputs

-   Algorithm: step-by-step procedure to solve a problem

-   Data structure: way to store and organize data

Goal struktur data adalah mendapatkan kompleksitas $O(1)$ untuk operasi-operasi:

-   `INIT(A, N)`: kembalikan instance struktur data dengan ukuran `N`
-   `READ(A, i)`: nilai pada struktur data untuk indeks `i`
-   `WRITE(A, i, value)`: simpan nilai `value` pada indeks `i`

## Amortized Analysis

Worst-case analysis untuk menentukan running time terburuk dari suatu operasi
struktur data sebagai fungsi dengan ukuran input $n$.

Amortized analysis adalah menentukan kasus running time terburuk untuk suatu
barisan $n$ operasi pada struktur data. Contoh: stack kosong diimplementasikan
dengan dynmaic table, maka barisan operasi $n$ push dan pop memakan waktu $O(n)$
(worst case).

Bebrapa metode:

-   Aggregation
-   Accounting/banker's
