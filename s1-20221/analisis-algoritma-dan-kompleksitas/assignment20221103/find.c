#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int find_index(int v, int w, int arr[], size_t N)
{
        /*
           Given an array of size N, we want to find the index
           of an element in the array with value v and followed by w.

           X = banyak operasi perbandingan
           E(X) = 
         */
        for (int i = 0; i < N - 1; i++) {
                if (arr[i] == v && arr[i + 1] == w)
                        return i;
        }
        return -1;
}

int main(int argc, char* argv[])
{
        int* arr = calloc(argc - 3, sizeof(int));
        int v = atoi(argv[1]);
        int w = atoi(argv[2]);
        for (int i = 3; i < argc; i++) {
                arr[i - 3] = atoi(argv[i]);
        }
        printf("%d\n", find_index(v, w, arr, argc - 3));
        return 0;
}
