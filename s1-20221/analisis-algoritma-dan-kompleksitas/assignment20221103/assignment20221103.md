---
author: Faiz Jazadi
date: '2022-11-09'
numbersections: true
subtitle: Analisis Algoritma dan Kompleksitas
title: 'Tugas: Analisis Probabilistik'
geometry: margin=1.5cm
---

Diberikan array $A$ dengan panjang $N$, kita ingin mencari index $i$ pada array
sedemikian sehingga $A[i] = k$ dan $A[i + 1] = v$.

# Pseudocode

``` {.numberLines}
fun find_couple(k, v, A[])
{
    for i = 1 ... (arr.length - 1) {
        if (A[i] == k and A[i + 1] == v) {
            return i;
        }
        return -1;
    }
}
```

# Complexity Analysis

Worst-case: $O(n)$

Untuk menghitung average-case complexity, perlu dilakukan analisis probabilitas.

```{=tex}
\begin{align*}
X &= \text{banyak iterasi yang dilakukan} \\
p(X = x) &= \text{probabilitas dilakukan sebanyak}\ x\ \text{iterasi}
\end{align*}
```

Dengan asumsi bahwa variabel random $X$ terdistribusi secara uniform, maka:

$$p(X = x) = \frac{1}{n - 1}$$

Sehingga,

```{=tex}
\begin{align*}
E[X] &= \sum_{x = 1}^{n - 1} x\ p(x) \\
     &= \sum_{x = 1}^{n - 1} x\ \frac{1}{n - 1}
      = \frac{1}{n - 1} \sum_{x = 1}^{n - 1} x \\
     &= \frac{1}{n - 1} \frac{(n - 1)(n - 1 + 1)}{2} \\
     &= \frac{n}{2} = O(n)
\end{align*}
```
