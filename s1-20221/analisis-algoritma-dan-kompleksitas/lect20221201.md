---
author: Faiz Jazadi
date: '2022-12-01'
numbersections: true
title: Topik UAS
---

1.  Probability analysis
2.  Memoization
3.  Growth of function
