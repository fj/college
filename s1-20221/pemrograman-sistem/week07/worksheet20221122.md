---
author: 'Faiz Unisa Jazadi -- `faiz.uni2003@mail.ugm.ac.id`'
date: '2022-11-22'
geometry: margin=2cm
subtitle: CSCM603127 System Programming
title: 'Pretest 7 - Scripting'
---

# Answers

1.  Write a bash script that read a JSON file and beautify it into formatted
    JSON. The new beautified JSON is stored as a new file. You also must provide
    an explanation of how your bash script works!

    Notes:

    -   You can use this JSON file as an input example.
    -   Use as many resources as you can get.
    -   Don't hardcode the path of the unformatted JSON file! Use argument
        instead.

    **Answer:**

    ``` {.sh}
    #!/bin/bash

    if [ $# -lt 2 ]
    then
            PROG_NAME=$(basename "$0")
            echo "usage: $PROG_NAME <source.json> <dest.json>";
            exit 1
    fi

    python -m json.tool "$1" > "$2"
    ```

    The bash script works by utilizing a Python module `json.tool`. It takes two
    arguments: source and destination. The output of the Python prettifier is
    redirected to the destination file.

2.  Structure of file system consists of directory and file. Make a bash script
    that represents your file system structure as a tree. Here is the example of
    the structure (not included). You also must provide an explanation of how
    your bash script works!

    **Answer:**

    ``` {.sh}
    #!/bin/bash
    IDIR=$(pwd)
    if [ $# -ge 1 ]
    then
            IDIR=$1
    fi
    find "$IDIR" -print | sed -e "s;$IDIR;\.;g;s;[^/]*\/;|__;g;s;__|; |;g"
    # credits: https://gist.github.com/pravj/d39fb4149638c183b922
    ```

    It works by using the `find` tool and substituting some patterns using sed
    to create a tree-like visual structure.

3.  Create a bash script that can change your terminal as displayed in the
    following picture (not included). Make sure that this script changes your
    terminal permanently (meaning that if the terminal is restarted, it would
    still look the same). You also must provide an explanation of how your bash
    script works!

    **Answer:**

    ``` {.sh}
    #!/bin/bash
    PS1='\[\e[36m\][\D{%A, %d-%m-%Y}\[\e[0m\]'
    PS1+='||\[\e[31m\]\@ \D{%Z}]\[\e[0m\][\[\e[34m\]'
    PS1+='\H \[\e[33m\]of \[\e[32m\]\u:\[\e[35m\]'
    PS1+='\W\[\e[0m\]]\[\e[31m\]\$ \[\e[0m\]'
    echo "PS1=\"$PS1\"" >> "$HOME/.bashrc"
    ```

    The script is made by first crafting the appropriate value of the `PS1`
    shell variable (primary prompt). Then, the crafted `PS1` value is written in
    the `~/.bashrc` file (loaded before bash starts).

4.  Suppose that we have this bash script

    ``` {.sh}
    while read LINE
    do
        echo  $LINE
    done < `tail -f /var/log/messages`
    ```

    Unfortunately, the above code block hangs and does nothing useful. Fix this
    so it does work. You also must provide an explanation of how your bash
    script works! (Hint: rather than redirecting the stdin of the loop, try a
    pipe.)

    **Answer:**

    ``` {.sh}
    #!/bin/bash
    while read -r LINE
    do
        echo  "$LINE"
    done | tail -f /var/log/messages
    ```

    The above bash script works by piping the output of
    `tail -f /var/log/messages` to the while loop block (reading and echoing
    each line of the piped stdout).

5.  Create a bash script to create a .ssh folder for every user that has a HOME
    directory if it does not exist. Otherwise, print the full path of that user
    .ssh folder using format "{USER} .ssh folder in {PATH\_TO\_FOLDER}" where:

    -   {USER}: Replace it with name of the usr
    -   {PATH\_TO\_FOLDER}: Replace it with the path to .ssh folder

    You also must provide an explanation of how your bash script works!

    **Answer:**

    ``` {.sh}
    #!/bin/bash
    for home_dir in /home/*; do
            username=$(basename "$home_dir")
            ssh_dir="$home_dir"/.ssh
            if [ -e "$ssh_dir" ]
            then
                    echo "$username .ssh folder in $ssh_dir";
            else
                    mkdir "$ssh_dir"
            fi
    done
    ```

    The script works by enumerating all home directories, then checking whether
    the `.ssh` directory exists. If it does not exist, echo the directory path,
    else, create the directory.

6.  Create a bash script that receives a path to a certain folder as a
    parameter. Change the ownership of every file and folder inside that path to
    the user that executed this script. You also must provide an explanation of
    how your bash script works!

    **Answer:**

    ``` {.sh}
    #!/bin/bash

    if [ $# -lt 1 ]
    then
            echo "usage: $0 <path>"
            exit 1
    fi

    if ! [ -d "$1" ]
    then
            echo "invalid path (not exist or not a directory)"
            exit 2
    fi

    chown -R "$USER":"$USER" "$1"
    ```

    The script works by using `$USER` to determine the current user and using
    `chown` to change the owner of the directory recursively (with `-R` flag).

7.  Create a bash script that can compress each user home folder into .tar.gz
    file. The result will be stored in /var/backup folder. You also must provide
    an explanation of how your bash script works!

    **Answer:**

    ``` {.sh}
    #!/bin/bash
    dest_dir=/var/backup
    for home_dir in /home/*
    do
            temp=$(mktemp).tar.gz
            echo "$home_dir"
            tar czf "$temp" -P "$home_dir"
            mv "$temp" "$dest_dir"/"$USER".tar.gz
    done
    ```

    The script works by traversing all home directories and running the `tar`
    command to each of them. It first creates the archive in a temporary file.
    Once the archiving process is complete, the temporary file is then moved to
    the destination folder (`/var/backup`)
