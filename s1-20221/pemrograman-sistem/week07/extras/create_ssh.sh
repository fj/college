#!/bin/bash
for home_dir in /home/*; do
        username=$(basename "$home_dir")
        ssh_dir="$home_dir"/.ssh
        if [ -e "$ssh_dir" ]
        then
                echo "$username .ssh folder in $ssh_dir";
        else
                mkdir "$ssh_dir"
        fi
done
