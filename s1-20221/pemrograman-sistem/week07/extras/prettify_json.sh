#!/bin/bash

if [ $# -lt 2 ]
then
        prog_name=$(basename "$0")
        echo "usage: $prog_name <source.json> <dest.json>";
        exit 1
fi

python -m json.tool "$1" > "$2"
