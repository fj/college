#!/bin/bash
dest_dir=/var/backup
for home_dir in /home/*
do
        temp=$(mktemp).tar.gz
        echo "$home_dir"
        tar czf "$temp" -P "$home_dir"
        mv "$temp" "$dest_dir"/"$USER".tar.gz
done
