#!/bin/bash
idir=$(pwd)
if [ $# -ge 1 ]
then
        idir=$1
fi
find "$idir" -print | sed -e "s;$idir;\.;g;s;[^/]*\/;|__;g;s;__|; |;g"
