#!/bin/bash

if [ $# -lt 1 ]
then
        echo "usage: $0 <path>"
        exit 1
fi

if ! [ -d "$1" ]
then
        echo "invalid path (not exist or not a directory)"
        exit 2
fi

chown -R "$USER":"$USER" "$1"
