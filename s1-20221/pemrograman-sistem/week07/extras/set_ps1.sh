#!/bin/bash
PS1='\[\e[36m\][\D{%A, %d-%m-%Y}\[\e[0m\]'
PS1+='||\[\e[31m\]\@ \D{%Z}]\[\e[0m\][\[\e[34m\]'
PS1+='\H \[\e[33m\]of \[\e[32m\]\u:\[\e[35m\]'
PS1+='\W\[\e[0m\]]\[\e[31m\]\$ \[\e[0m\]'
echo "PS1=\"$PS1\"" >> "$HOME/.bashrc"
