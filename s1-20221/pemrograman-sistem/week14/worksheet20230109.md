---
author: 'Faiz Unisa Jazadi -- `faiz.uni2003@mail.ugm.ac.id`'
date: '2023-01-09'
documentclass: report
subtitle: CSCM603127 System Programming
title: 'Worksheet 14 - Kernel Optimization and Device Driver'
---

# Task 1 (75 points)

**Objective:** Explore the capability of the device driver currently installed
in your system.

The device I currently want to observe is the fan speed controller on a ThinkPad
laptop. The device driver related to the fan speed controller on a ThinkPad
laptop is the `thinkpad_acpi` driver (I use a ThinkPad T470 laptop).

Using the `lsmod` command, I can successfully identify the presence of the
`thinkpad_acpi` module.

![](img/ws-lsmod-tpacpi.png)

The `thinkpad_acpi` driver is a Linux kernel module that provides access to the
hardware features of IBM/Lenovo ThinkPad laptops. It allows the user to control
various features of the laptop, such as the fan speed, the brightness of the
display, and the status of the battery. The driver also provides the ability to
read the temperature of various parts of the laptop, such as the CPU and GPU.

Below is the output of the `modinfo thinkpad_acpi | grep fan` command.

![](img/ws-tpacpi-fan.png) I want to observe the fan controller device that is
provided by `thinkpad_acpi` driver/kernel module. I want to adjust the
`fan_control` parameter and try to adjust the fan speed.

From the above output, one of the capabilities that the module provides is to
enable/disable fan control. The `fan_control` parameter, when set to "true",
enables the user to set various parameters related to the fan, such as its speed
or temperature thresholds. The `fan` parameter simulates the "thinkpad-acpi"
command in the `procfs` file system and allows the user to access the fan status
and control it from user space.

# Task 2 (25 points)

**Objective:** Modify and control the device driver to adjust it to your current
need

I want to observe the fan controller device that is provided by `thinkpad_acpi`
driver/kernel module. I want to adjust the `fan_control` parameter and try to
adjust the fan speed.

By default, fan control is disabled. We can verify this by reading the `fan`
file from the `thinkpad_acpi` module provided `procfs` using the following
command.

    cat /proc/acpi/ibm/fan

![](img/ws-task-2-fan-control-disabled.png)

As mentioned in the previous task, the module provides the `fan_control`
parameter to change this default behavior. To control fan speed, we first need
to set `fan_control` to true. One of the ways to accomplish this is by unloading
then loading `thinkpad_acpi` with the desired parameter using `modprobe`.

    sudo modprobe -r thinkpad_acpi \
        && sudo modprobe thinkpad_acpi fan_control=1

We can verify that fan control is enabled by once again reading
`/proc/acpi/ibm/fan`.

![](img/ws-task-2-fan-control-enabled.png)

From the above screenshot, we can see that there are some new lines that include
commands to enable/disable, adjust the speed, or adjust the watchdog timeout of
the fan.

As an example, we can run the following command to make the fan run on full
speed.

    echo level full-speed | sudo tee /proc/acpi/ibm/fan

![](img/ws-task-2-fan-speed-example.png)

We can also enable automatic fan setting by sending `level auto` command.

# References

1.  <https://www.thinkwiki.org/wiki/How_to_control_fan_speed>
2.  <https://github.com/torvalds/linux/blob/master/drivers/platform/x86/thinkpad_acpi.c>
3.  <https://bbs.archlinux.org/viewtopic.php?id=154957>
