---
author: 'Faiz Unisa Jazadi -- `faiz.uni2003@mail.ugm.ac.id`'
date: '2022-10-08'
subtitle: CSCM603127 System Programming
title: 'Worksheet 1 - Introduction'
---

# Answers

1.  By using Proc File System (/proc), mention the command to obtain the
    following information:

    a.  Information about the processor

        **Answer:**

        `cat /proc/cpuinfo`

        ![](img/ws1-proc-cpuinfo.png)

    b.  Information about memory usage, both physical and swap

        **Answer:**

        `cat /proc/meminfo`

        ![](img/ws1-proc-meminfo.png)

    c.  Information about how long the system has been up

        **Answer:**

        `cat /proc/uptime`

        ![](img/ws1-proc-uptime.png)

    d.  Information about kernel version currently used

        **Answer:**

        `cat /proc/version`

        ![](img/ws1-proc-version.png)

    e.  Information about kernel modules currently loaded

        **Answer:**

        `cat /proc/modules`

        ![](img/ws1-proc-modules.png)

2.  Answer these following questions:

    a.  In a Unix system such as GNU/Linux, file type could be recognized even
        without extension as in Microsoft Windows. For example, /bin/bash is an
        executable file, and file /dev/sda is a block device. Mention a tool
        that will help you recognize file type in GNU/Linux system!

        **Answer:** One of the tools that can be used to identify file type is
        the `file` command. Quoting from the `file`(1) manual page, the `file`
        command is used to determine file type.

    b.  Mention the differences between a relative path and absolute path?

        **Answer:** A relative path refers to a path of a file relative to
        another path (usually the current working directory). An absolute path
        is a full path that locates a specific file without any relative
        reference.

    c.  Suppose we have a relative path illustrated in the following picture.
        State the absolute path of `pg_hba.conf` file!

        **Answer**:

        -   Current working directory: `/etc/network`
        -   Relative path: `../postgresql/11/main/conf.d/../pg_hba.conf`
        -   **Absolute path**: `/etc/postgresql/11/main/pg_hba.conf`

3.  File and Directory in GNU/Linux arranged based on a hierarchy tree. What is
    the standard name used by this hierarchy? Explain your answer!

    **Answer:** The standard name is **Filesystem Hierarchy Standard (FHS)**.
    The standard defines what certain directories should contain and how they
    structure (e.g. `/bin` for essential binaries and `/dev` for device files).

4.  Nida found a file named /proc/partitions in /proc directory. When Nida read
    its content using `cat` command, it showed the following results. What kind
    of information that Nida got from /proc/partitions content?

    **Answer:** According to the `proc`(5) manual page, the `/proc/partitions`
    file contains the major and minor numbers of each partition as well as the
    number of 1024-byte blocks and the partition name.

5.  Based on each software below, identify which software produced using
    application programming and system programming and give your reasoning!

    a.  Creating a Web Service API

        **Answer:** A web service API is most likely produced using
        **application programming** because web service APIs don't typically
        interact with hardware.

    b.  Creating a USB Device Driver

        **Answer:** Creating a USB device driver requires a degree of hardware
        awareness. USB device driver provides a platform for other software.
        Therefore, a USB device driver is produced using **systems
        programming**.

    c.  Creating software to automatically deploy a web application

        **Answer:** On most cases, deploying a web application is usually a
        high-level operation and don't necessarily require a tight coupling with
        the hardware level details. Therefore, the automatic deployment software
        is likely produced using **application programming**.

# References

1.  <https://en.wikipedia.org/wiki/Filesystem_Hierarchy_Standard>
2.  Linux Manual Pages (version 5.10)
