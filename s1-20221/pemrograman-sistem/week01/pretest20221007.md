---
author: 'Faiz Unisa Jazadi -- `faiz.uni2003@mail.ugm.ac.id`'
date: '2022-10-07'
subtitle: CSCM603127 System Programming
title: 'Pretest 1 - Introduction'
---

# Answers

1.  What is the difference between system programming and application
    programming? Give a brief example using 7 sentences at most.

    **Answer:** Systems programming is an activity of developing a software that
    provides an interface to other software (e.g. driver, kernel). Application
    programming is an activity of developing a software that is meant to be used
    directly by the end-user (e.g. web API, music player).

2.  Explain the difference between scripting and programming language using 7
    sentences at most.

    **Answer:** Scripting languages are typically not compiled and require a
    specific runtime (e.g. Java requires JVM, Python requires Python runtime).
    Scripting languages are usually used to develop application software,
    whereas programming languages are usually used to develop Systems
    programming languages are typically compiled to native binary executable and
    usually doesn't depend on a specific platform (independent).

3.  Answer these following questions about kernel

    a.  What is a kernel and how to distinguish it from an operating system?

        **Answer:** Kernel is the core part of an operating system. Kernel is a
        system software that provides an interface for user programs to
        communicate with the hardware. An operating system is a fully working
        software system that contains a kernel and user programs.

    b.  Modern processor architectures typically allow the CPU to operate in at
        least two different modes: user mode and kernel mode. Explain why we
        need both of those modes.

        **Answer:** Both of those modes are required to separate the area of
        concern when running programs. User programs are run in user mode
        whereas system programs are run in kernel mode. User programs may switch
        temporarily to kernel mode (e.g. to get input from the keyboard) by
        doing a system call. Then, the kernel will review and decide whether to
        accept/reject the call. This way, user programs are isolated from system
        programs (if a user program crashes, the whole OS won't likely be
        affected).

4.  Use a Linux terminal to answer these following questions

    a.  Explain the purpose of man command in the Linux terminal and try to
        access 3 man pages. Mention the commands in the answer sheet

        **Answer:**

        The `man` command is used as an interface to the system reference manual
        pages.

        Command examples:

        1.  `man man`
        2.  `man groups`
        3.  `man passwd`

    b.  Suppose we have a user named badak in our Ubuntu/Debian OS, please
        mention one or more commands to

        i.  Display a group which this user belongs

            **Answer:** `groups badak`

        ii. Change the password of this user

            **Answer:** `passwd badak`

5.  Answer these following question about programs and processes

    a.  Explain the relation between programs and processes using 5 sentences at
        most.

        **Answer:** Processes are programs that are running (in execution). A
        program is a file that contains instructions. When a program is being
        run, it is turned into a process. When a program is being run, it is
        turned into a process.

    b.  A process memory is logically divided into several parts known as
        segments. Mention all those parts and their functionality.

        **Answer:**

        A process is logically divided into the following segments.

        -   Text: contains the program code/instructions loaded from the disk
        -   Data: contains information about static and global variable
            allocations
        -   Heap: for dynamic memory allocations (e.g. using `malloc()` and
            `free()`)
        -   Stack: for storing local variables and function arguments (stack
            management depends on the language being used)

# References

1.  <https://www.geeksforgeeks.org/difference-between-operating-system-and-kernel/>
2.  <https://www.baeldung.com/cs/user-kernel-modes>
3.  <https://www.cs.uic.edu/~jbell/CourseNotes/OperatingSystems/3_Processes.html>
