#!/bin/bash
echo "Creating a background process with nohup"
bash -c 'nohup sleep 1999 > /dev/null &'
pid=$(pgrep -f 1999)
echo "Sending SIGHUP to $pid"
kill -HUP "$pid"
pid=$(pgrep -f 1999)
echo "Process still runs with pid $pid"
kill "$pid"

echo "Creating a background process with disown"
sleep 2999 & disown
pid=$(pgrep -f 2999)
echo "Process is running with pid $pid"
echo "Sending SIGHUP to $pid"
kill -HUP "$pid"
echo "Pgrep result (should be not found): $(pgrep -f 2999)"

echo "Creating a background process by appending ampersand"
sleep 3999 &
pid=$(pgrep -f 3999)
echo "Process is running with pid $pid"
echo "Should be shown as one of the shell's background job"
echo "Background job pids: $(jobs -p)"
echo "Sending SIGHUP"
kill -HUP "$pid"
echo "Pgrep result (should be not found): $(pgrep -f 3999)"
