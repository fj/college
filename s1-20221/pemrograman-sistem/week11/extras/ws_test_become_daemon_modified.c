/**********************************************************************\
*                Copyright (C) Michael Kerrisk, 2010.                  *
*                                                                      *
* This program is free software. You may use, modify, and redistribute *
* it under the terms of the GNU Affero General Public License as       *
* published by the Free Software Foundation, either version 3 or (at   *
* your option) any later version. This program is distributed without  *
* any warranty. See the file COPYING for details.                      *
\**********************************************************************/

/* test_become_daemon.c

   Test our becomeDaemon() function.
*/
#include "become_daemon.h"
#include "tlpi_hdr.h"
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

void
sighup_handler(int sig)
{
        int fd = open("/tmp/daemon_test.log", O_CREAT|O_WRONLY|O_APPEND, 0644);
        write(fd, "Restarted test_daemon process\n", 30);
        close(fd);
}

int
main(int argc, char *argv[])
{
    struct sigaction sa;
    becomeDaemon(0);

    sa.sa_handler = sighup_handler;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;
    sigaction(SIGHUP, &sa, NULL);

    for(;;)  /* Infinite loop to simulate a daemon process */
            sleep((argc > 1) ? getInt(argv[1], GN_GT_0, "sleep-time") : 20);

    exit(EXIT_SUCCESS);
}

