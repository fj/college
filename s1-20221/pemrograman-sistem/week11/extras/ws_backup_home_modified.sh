#!/bin/bash
dest_dir=/var/backup

for home_dir in /home/*
do
        temp=$(mktemp).tar.gz
        echo "$home_dir"
        
        if ! tar czf "$temp" -P "$home_dir"; then
                logger Failed to backup "$home_dir"
        else
                mv "$temp" "$dest_dir"/"$USER".tar.gz
                logger Successfully backed up "$home_dir"
        fi
done
