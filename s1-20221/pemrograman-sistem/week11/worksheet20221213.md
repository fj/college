---
author: 'Faiz Unisa Jazadi -- `faiz.uni2003@mail.ugm.ac.id`'
date: '2022-12-13'
subtitle: CSCM603127 System Programming
title: 'Worksheet 9 - Daemon and Background Process'
geometry: margin=2cm
---

# Answers

1.  Explain the differences between:

    a.  `nohup`
    b.  `disown`
    c.  Executing a command in terminal while adding `&` at the end of the
        command

    **Answer:**

    Three of the above commands can be used to run a command in "background".
    The `nohup` program is used to run a program that ignores the `SIGHUP`
    signal. This will make the resulting process to be immune to anything that
    sends the `SIGHUP` signal (i.e. logging out, closing the terminal window).
    Meanwhile, `disown` is used to disown a job in a shell's job control. A
    disowned job will not be controlled by the shell (it will still run after
    the terminal is closed), but they are not immune to other events that may
    send the `SIGHUP` signal. On the other side, appending `&` to a command in a
    shell will cause it to run in the background but will still be controlled by
    the terminal.

    The simulation script is named `ws_p1_sim.sh`. The script will try to create
    three background processes using `nohup`, `disown`, and `&`. It will also
    show how each processes react to `SIGHUP`.

2.  Answer the following questions regarding how to daemonize a process:

    a.  What is a Process Group? Provide a simulation to help your explanation!

        **Answer:**

        Process groups is a way for the OS to manage and coordinate the
        execution of multiple processes. Each process in a process group has a
        different PID and same Group ID.

        A simple simulation can be done by running a command in a terminal.

            bash -c 'sleep 111; sleep 10 &' &

        ![](img/ws-p2a-sim.png)

        From the above output, it can be shown that a command has their own
        process group. If we send a signal to a process group, the signal will
        also be sent to all processes in the group.

    b.  What is the relation between Process Group and to daemonize a process?

        **Answer:**

        Daemonizing a process involves putting the process into its own process
        group.

    c.  Why does the child process that will become a daemon need to call
        `setsid()`?

        **Answer:**

        It is required for a process that will become a daemon because it allows
        the process to operate independently of the parent process and the
        terminal that the parent process was running in. A daemon process should
        not depend on the current parent process/terminal.

    d.  If you go to `setsid()`, you will find that this command is creating a
        new "session". Provide an explanation about the meaning of session in
        Linux! Is there any relation between session and process group?

        **Answer:**

        A session in Linux is a collection of process groups. It can either be
        attached to a terminal or not. Usually, session is used for job control
        by shells (by selecting one process group as foreground). There is
        another relation between session and process group. That is, when a
        session created, a new process group is also created (the process that
        created the session is the group leader).

3.  Can we differentiate between a normal process and a daemon process without
    looking at the source code of the program? Provide a simulation (if any) to
    further support your answer!

    **Answer:**

    A normal process and a daemon process can be differentiated without looking
    at their source code. This can be done by checking the parent process of the
    corresponding process. If the parent is the `init` init, it is most likely a
    daemon. The process of daemonizing a process involves re-parenting, usually
    to the `init` process. Another characteristics of a daemon process can also
    be used as a differentiating factor, such as whether or not the process has
    TTY attached, managed by a service, writing to a log, etc. Therefore, we
    cannot perfectly differentiate without looking at its source code.

4.  To work on this question, please use your bash script that you previously
    implemented in Worksheet 7.

    a.  Use a script to compress each user home folder into ".tar.gz" file. The
        result will be stored in `/var/backup` folder as illustrated in the
        following screenshot (not included).

    b.  Using the `logger` command in your bash script, create a log file to
        indicate that the backup process for each home folder is successful.

    c.  Use Cron Job to make sure that this script is executed everyday at 01.00
        AM refers to your Local Computer Time.

    Provide an explanation on how to solve each task in this question!

    **Answer:**

    1.  To implement the `logger` command in the bash script, the following
        lines are added.

        ``` {.sh}
        if ! tar czf "$temp" -P "$home_dir"; then
            logger Failed to backup "$home_dir"
        else
            mv "$temp" "$dest_dir"/"$USER".tar.gz
            logger Successfully backed up "$home_dir"
        fi
        ```

    2.  The cron job is installed by appending the following lines to the cron
        file (edited using `crontab -e` command)

            0  1    * * *   root    /bin/bash /opt/sysprog/backup_home.sh

5.  Since a daemon process has no controlling terminal, the kernel will never
    generate SIGHUP signal for a daemon process. As specified at the Linux
    Programming Interface Book Section 37.4, we can use `SIGHUP` signal to
    reinitialize daemon process. Your task here is to simulate this mechanism.
    Below are step-by-step of what you must done:

    -   Please find a bash script simulating a daemon process in
        `tlpi-dist/daemons/test_become_daemon.c`
    -   Edit this script to create a handler for `SIGHUP` signal.
    -   Make sure that each time a `SIGHUP` signal is sent to the process
        created by this script, it will restart the process as a result.

    You also must provide an explanation to the modified script and to the
    simulation!

    Hint: Add additional functionality (e.g. writing a debugging message to
    another file) to make the simulation easier to explain!

    **Answer:**

    The file `tlpi-dist/daemons/test_become_daemon.c` is not a bash script. The
    modified version of the program is attached as
    `ws_test_become_daemon_modified.c`. Signal handler is created using the
    `sigaction()` system call.

    ``` {.c}
    ...
    sa.sa_handler = sighup_handler;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;
    sigaction(SIGHUP, &sa, NULL);
    ...
    ```

    The handler is set to `sighup_handler`, a function that will write
    "Restarted test\_daemon process" to `/tmp/daemon_test.log`. The simulation
    is done by executing the modified program, then send multiple `SIGHUP`
    signal using `pkill -HUP -f test_become_daemon`. After sending the signal,
    we can see that there are several lines written in the
    `/tmp/daemon_test.log` file.

# References

1.  Linux man-pages (release 5.10)
2.  The Linux Programming Interface, Michael Kerrisk, 2010
