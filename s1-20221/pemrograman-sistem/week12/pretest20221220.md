---
author: 'Faiz Unisa Jazadi -- `faiz.uni2003@mail.ugm.ac.id`'
date: '2022-12-13'
subtitle: CSCM603127 System Programming
title: 'Pretest 12 - Boot Sequence and Kernel Compilation'
---

# Answers

1.  Complete the following tables (not included here) regarding Monolithic
    Kernel, Micro-Kernel, Hybrid Kernel Architecture

    **Answer:**

    a)  Monolithic kernel

        -   Characteristic: kernel services and functions run in one large
            process
        -   Advantage: efficient because all kernel functionalities can com-
            municate easily with each other
        -   Disadvantage: hard to extend and maintain because everything is
            contained in single entity

    b)  Micro-kernel

        -   Characteristic: only essential kernel functions run in the kernel
            space while other functionalities run in user space as separate
            processes
        -   Advantage: more modular, less coupling, easier to extend
        -   Disadvantage: less efficient in resource because kernel functions
            need to communicate using IPC

    c)  Hybrid kernel

        -   Characteristic: combination of monolithic and micro-kernel (some
            functions run in kernel space, some run in user space)
        -   Advantage: have both micro-kernel and monolithic kernel advan- tages
            while still having enough extensibility
        -   Disadvantage: more complex because it combines both mono- lithic and
            micro-kernel architectures

2.  Answer the following questions regarding UEFI and GPT

    a.  Why UEFI can replace BIOS functionality? Give several reasons to support
        your explanations!

        **Answer:**

        UEFI is a successor of BIOS. It is a specification for the interface
        between a computer's firmware and operating system. The following are
        reasons why UEFI can replace BIOS functionality.

        1.  UEFI provides security features that can be used to protect against
            untrusted firmware (secure boot)
        2.  UEFI has a better support for modern hardware
        3.  UEFI has enhanced power management

    b.  What is the advantage of using GPT when partitioning a Drive?

        **Answer:**

        GPT is compatible with modern hardware, including UEFI-based systems.
        GPT also allows for a larger partition size (up to 9.4 ZB). On a single
        drive, GPT allows for the creation of up to 128 partitions. GPT also
        stores multiple copy of the partition table which makes it more
        reliable.

3.  Explain a booting process of a computer using the following pictures (not
    included)!

    **Answer:**

    1.  When computer is powered on, it first loads the BIOS.
    2.  BIOS then loads stage 1 Grub from the MBR. If it fails, then BIOS will
        report "Missing operating system".
    3.  BIOS proceeds to load stage 1.5 then stage 2 of Grub. If it fails to
        load the stages, only "GRUB" is displayed.
    4.  After loaded, Grub will read menu.lst file. If it fails, it will fall
        back into the Grub command line.
    5.  Entries from the menu.lst file is presented as boot-time menu.
    6.  After user interacts with the menu, Grup will proceed by loading the
        kernel image and initial RAM disk. If those required files can't be
        found, Grub will report "Error 15: File not found".
    7.  After kernel is loaded, it will mount the root file system. When failed,
        kernel will panic or freeze.
    8.  Kernel runs init program. In this phase, kernel may panic or drop user
        into a root shell.
    9.  After init is started, it will start user-level services
    10. Create a systemd Init Script that can automatically run a Django web
        application! (You may use the runserver command from the `manage.py` to
        run the application). Provide a screenshot to support your answer!

4.  Create a systemd Init Script that can automatically run a Django web
    application! (You may use the runserver command from the `manage.py` to run
    the application). Provide a screenshot to support your answer!

    **Answer:**

        [Unit]
        Description=Unit for running a Django web application
        [Service]
        Restart=on-failure
        WorkingDirectory=/opt/django_app
        ExecStart=/opt/django_app/.venv/bin/python manage.py runserver
        [Install]
        WantedBy=multi-user.target

    ![](init-script-django.png)

# References

1.  Linux man-pages (release 5.10)
2.  The Linux Programming Interface, Michael Kerrisk, 2010
