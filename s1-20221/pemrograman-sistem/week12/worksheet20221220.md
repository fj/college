---
author: 'Faiz Unisa Jazadi -- `faiz.uni2003@mail.ugm.ac.id`'
date: '2022-12-20'
geometry: 'margin=1.5cm,bottom=3cm'
subtitle: CSCM603127 System Programming
title: 'Worksheet 12 - Boot Sequence and Kernel Compilation'
---

# Mandatory Task

The task is to compile our own kernel with NPM/name as part of the kernel
version.

1.  Download the kernel using `wget`, then extract.

        wget http://kambing.ui.ac.id/linux/v4.x/linux-4.15.1.tar.xz
        tar -xvJf linux-4.15.1.tar.xz
        cd linux-4.15.1

    ![](img/ws-step-1.png)

2.  Prepare kernel configuration file by copying it from the already existing
    config in `/boot` directory.

        cp /boot/config-4.15.0-196-generic .config

    ![](img/ws-step-2.png)

3.  Configure the desired modules using `make menuconfig` (and load the
    previously copied config file). Also, customize the kernel version by going
    to "General setup -\> Local version - append to kernel release".

    ![](img/ws-step-3.png)

    ![](img/ws-step-3-kernel-append.png)

4.  I figured that signature checking configuration is also necessary. I decided
    to just disable the checking by adjusting the value of
    `CONFIG_SYSTEM_TRUSTED_KEYS` in `.config`.

        CONFIG_SYSTEM_TRUSTED_KEYS=""

5.  Start building the kernel by running `make`.

    ![](img/ws-step-4.png)

6.  Install the kernel modules by running the following command.

        make modules_install

    ![](img/ws-step-6.png)

7.  After the kernel modules have been installed, we proceed by installing the
    kernel.

        make install

    ![](img/ws-step-7.png)

8.  For some reason, the above method (step 5 - 6) does not work. Instead, I
    tried to rebuild the kernel using `make-kpkg`.

        fakeroot make-kpkg --append-to-version -faizunisajazadi --revision 1 --initird kernel-image

    ![](img/ws-step-8.png)

9.  Next, I installed the kernel using `dpkg`.

        sudo dpkg -i linux-image-4.15.1-faizunisajazadifaizunisajazadi_2_amd64.deb

10. Reboot, then verify using `uname -a`.

    ![](img/ws-step-9.png)

# References

1.  Lecture slides and video
2.  https://www.linux.com/topic/desktop/how-compile-linux-kernel-0/
3.  https://askubuntu.com/questions/1329538/compiling-the-kernel-5-11-11
4.  https://debian-handbook.info/browse/squeeze/sect.kernel-compilation.html
