---
author: 'Faiz Unisa Jazadi -- `faiz.uni2003@mail.ugm.ac.id`'
date: '2022-11-14'
geometry: 'margin=1.5cm,bottom=2.5cm'
subtitle: CSCM603127 System Programming
title: 'Worksheet 6 - Make, Stdio, and Buffering'
---

# Answers

1.  Open `Makefile` file inside `tlpi-dist/fileio` folder to answer the
    following questions:

    a.  Mention all targets specified in the file!

        **Answer:**

        The targets are `all`, `exe`, `allgen`, `clean`, `showall`, and
        `${EXE}`.

    b.  Mention the default target specified in the file!

        **Answer:**

        The default target is the first target in the `Makefile` which is the
        `all` target.

    c.  When we run `make` inside `tlpi-dist/fileio` folder, our terminal will
        show several outputs as illustrated in the following picture (not
        included). Mention which files describing the commands executed by
        `Makefile` and why it is executed!

        **Answer:**

        The files that describe the commands are the `Makefile` file and the
        `../Makefile.inc` file (included by `Makefile`). The C compilation
        commands are implicit (part of GNU Make's implicit rules). The options
        (flags) are specified inside the `../Makefile.inc` file.

2.  You are asked to create a `Makefile` as a management script of a Django
    Framework web application. To solve this question, you must follow these
    procedures:

    a.  ~~Use your web programming course (PPW) project to create your Makefile
        management script.~~

    b.  If you don't have any, use the following procedures:

        i.  Perform Fork on the following Gitlab Repository
            https://gitlab.com/spectrum71/Repo-PPW-Tutorial

        ii. Clone the forked repository so you can use the source code to create
            a Makefile management script

        Make sure your Makefile comply with the following specification:

        -   Create a virtual environment and install all dependencies
        -   Static file collection
        -   Database preparations which consist of:
            -   Make sure all migrations file is up-to-date
            -   Migrate the migration files
        -   Clean up .pyc file
        -   Run development server (Using the runserver command in manage.py
            file)
        -   Execute deployment preparation which consists of:
            -   Static file collection
            -   Database preparations \[Default Target\]
        -   Perform deployment which consists of:
            -   Cleanup .pyc file
            -   Create a virtual environment and install all dependencies
            -   Execute deployment preparation
            -   Run the development server

    **Answer:**

3.  Observe the output produced as a result of executing the following commands:

            time `dd if=/dev/urandom of=big1 bs=2048 count=25000`
            time `dd if=/dev/zero of=big2 bs=1024 count=50000`

    ![](img/dd-output.png)

    a.  Explain the output of those commands!

        **Answer:**

        The first command tries to write random $25000\times2048$ bytes to the
        `big1` file. The first command tries to write random $50000\times1024$
        bytes to the `big2` file.

    b.  Mention the similarity and differences of those commands!

        **Answer:**

        Both command tries to write the same amount of bytes with different
        block size (first command is 2048, second command is 1024). The first
        command writes random bytes (from `/dev/urandom`) and the second command
        writes zeros (from `/dev/zero`).

    c.  What can you summarize after observing the output of those commands?

        **Answer:**

        Even with larger block size, the first command runs slower than the
        second command. It shows that the overhead of generating random bytes
        also matters.

4.  Explain the produces effects if we execute the following statements:

            fflush(fp);
            fsync(fileno(fp));

    **Answer:**

    The first line will flush the `stdio` (user-space) buffered data to kernel
    buffer. The second line will flush the kernel-space cache (in-core storage
    data) to disk.

5.  Observe the following steps to simulate the usage of buffer in `write`() and
    `read`() system call:

    a.  Use `mystery.c` program provided in Scele and explain what is the action
        being performed by the program! Explain how the program can achieve its
        purpose!

        **Answer:**

        The program reads the last $N$ lines from a file. It does that by
        searching for the offset of the last $n$ lines from the file, seek the
        file starting from that offset, and then read until EOF.

    b.  Modify the file to print the first $N$ lines of a file! Explain how the
        modified program can achieve its purpose!

        **Answer:**

        The program achieves its purpose by searching for the offset of the end
        of the $n$-th line from the file, seek from the start of the file, and
        read until the previously found offset.

6.  Create a C program that executes these 2 statements:

    ``` {.c}
    int main(int argc, char** argv)
    {
        printf("If I had more time\n");
        write(STDOUT_FILENO, "I would have written you a letter\n", 43);
    }
    ```

    Perform the following simulations:

    -   Execute the program and observe its output in the terminal.
    -   Execute the program and perform output redirection to a file (e.g.
        `output.txt`).

    Explain why the output of those simulations is different!

    **Answer:**

    When redirecting standard output to a file, writing directly to
    `STDOUT_FILENO` will bypass the `stdio` buffer thus making the output
    different.

# References

1.  Linux man-pages (release 5.10)
