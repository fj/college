---
author: 'Faiz Unisa Jazadi -- `faiz.uni2003@mail.ugm.ac.id`'
date: '2022-11-14'
subtitle: CSCM603127 System Programming
title: 'Pretest 6 - Make, Stdio, and Buffering'
---

# Answers

1.  What is the effect of buffer size in several I/O system call such as
    `read()`, and `write()`?

    **Answer:** the larger the buffer size, the more memory is needed to
    allocate the buffer. By writing/reading larger chunks at a time, there will
    be less system call invocation which makes the total execution time faster.

2.  Time the operation of the `filebuff/write_bytes.c` program for various
    buffer sizes with the same data size (use 5 examples)! What can you
    summarize from this simulation?

    **Answer:**

    ![](img/write-bytes-output.png)

    From the time measurements, we can see that the time shrinks as the buffer
    size increases. This may be due to the less invocation to the `write()`
    system call.

3.  Explain how I/O Buffering works using the following picture (not included)
    to guide your explanation!

    **Answer:**

    I/O operations against the user data is done using the `stdio` library
    functions. From the user space, a buffer (`stdio` buffer) is allocated to
    store the operated data in the user memory before passing it to/from the
    kernel using I/O system calls. Buffers will be flushed if they met a certain
    condition (full) or by explicitly calling `fflush()`. The `stdio` buffer can
    also be skipped by setting the buffer to null using `setbuf(stream, NULL)`.
    After a buffer is flushed, the data is put inside the kernel cache buffer on
    the kernel memory. Then, I/O operations on the kernel space is syncronized
    (e.g. with `fsync()`) before they are actually written on the disk.

4.  Use `getconf` command to retrieve the following information:

    a.  The maximum length of a login name, including the terminating null byte.

        **Answer:**

        Command: `getconf LOGIN_NAME_MAX`

        ![](img/getconf-max-login-name.png)

    b.  The maximum number of files that a process can have open at any time

        **Answer:**

        Command: `getconf OPEN_MAX`

        ![](img/img/getconf-open-max.png)

    c.  The maximum length of a hostname, not including the terminating null
        byte

        **Answer:**

        Command: `getconf HOST_NAME_MAX`

        ![](img/getconf-hostname-max.png)

# References

1.  Linux man-pages (release 5.10)
