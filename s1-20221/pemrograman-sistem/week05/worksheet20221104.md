---
author: 'Faiz Unisa Jazadi -- `faiz.uni2003@mail.ugm.ac.id`'
date: '2022-11-04'
geometry: 'margin=1.5cm,bottom=2cm'
subtitle: CSCM603127 System Programming
title: 'Worksheet 5 - Memory'
---

# Answers

1.  Answer the following questions about virtual memory:

    a.  Why physical memory is not enough so that virtual memory is still used
        in operating systems today?

        **Answer:**

        Virtual memory is necessary to make an efficient use of the CPU and
        memory. Without virtual memory, memory management may become more
        inefficient. Many things benefit from virtual memory, including process
        isolation and memory sharing (by pointing to the same page). Virtual
        memory is also a convenient abstraction for developers (they do not have
        to care about the physical memory structure). Finally, the virtual
        memory size can also exceed the size of the physical memory.

    b.  What is the advantage and disadvantage of using virtual memory?

        **Answer:**

        The main advantage is being able to utilize the memory and CPU more
        efficiently. This is possible because virtual memory can reduce a
        process' memory footprint by residing only the needed part on the
        memory. Another advantages include process isolation, memory sharing,
        abstraction, and easier implementation of memory protection schemes.

        On the other side, using virtual memory means having another overhead.
        Swapping in and out from the disk can make the overall system slower.

2.  Answer the following questions about memory leak:

    a.  Explain the meaning of memory leak

        **Answer:**

        Memory leak means a failure in memory management, particularly by
        continuously allocating memory and not deallocating them after use.

    b.  Explain several events that may cause a memory leak to happen

        **Answer:**

        1.  Bad error handling: when an error happened, the part of the program
            that is supposed to free a segment of the memory is not executed,
            thus leaking the memory.
        2.  Relying on process termination memory cleanup: this will cause
            allocated memory to build up and will eventually leak over time.

3.  Explain how `malloc()` is implemented referring to:

    a.  The mechanism on how `malloc()` allocate memory to a program

        **Answer:**

        First, call to `malloc(size)` scans the free list (doubly linked list of
        memory segments freed using `free()`) to find a free memory that can be
        allocated with size equal to or greater than `size`. If none found, it
        then adjusts the program break using `sbrk()` system call.

    b.  How `malloc()` manage free memory that can be allocated

        **Answer:**

        Once a free memory block is found and the size is equal to `size`, a
        pointer to it will be directly returned. If the size is greater than
        `size`, the block is then split into the appropriate size, and the rest
        stays in the free list.

4.  To answer the following questions, you must use `/proc/longjmp.c` program:

    a.  Explain the program flow according to the following input and output:

                 $ ./longjmp
                 Calling f1() after initial setjmp()
                 We jumped back from f1()
                 $ ./longjmp 1 2 3
                 Calling f1() after initial setjmp()
                 We jumped back from f2()

        **Answer:**

        In the first command, the `longjmp` program is executed without any
        additional arguments (making `argc` equal to 1). First, the program
        calls `setjmp(env)` to set the jump target for later `longjmp()` calls.
        Initial return value of the `setjmp()` call is 0 which prints the first
        line in the output and calls `f1(argc)`. Because `argc` is 1, a call to
        `longjmp(env, 1)` is made which makes the execution of the program jump
        to the point that was initially set (by `setjmp()`). This `longjmp()`
        call fakes the return value of `setjmp()` to `1`. This causes the
        program to print a line to the standard output: "We jumped back from
        f1()".

        In the second command, the case is almost the same as the first command
        except now the `argc` is not equal to 1 (indeed it is 4). This makes the
        call to `f1()` not followed by `longjmp(env, 1)` call. Instead, it calls
        `f2()` which then calls `longjmp(env, 2)`. This makes the program jump
        back from `f2()`.

    b.  Suppose `/proc/longjmp.c` program is modified as specified below (not
        included). Provide an explanation for the following output as a result
        of `/proc/longjmp.c` modification!

                 $ ./modified_longjmp
                 case 0
                 case 1
                 segmentation fault

        **Answer:**

        First, the call to `func(1)` calls `setjump(env)` which saves the jump
        target for the next `longjmp()` calls. But, the call to `func(1)`
        returns and its stack frame is destroyed. Then, a `longjmp()` is called
        from the `main()` function using a target set in `env` earlier using
        `setjmp()` from the already returned `func(1)` call. This `longjmp()`
        call tries to unwind the stack frames which no longer exist which end up
        causing the segmentation fault.

5.  To answer the following questions, you must use `/memalloc/free_and_sbrk.c`
    program:

    a.  Modify the program `free_and_sbrk.c` to print out the current value of
        the program break after each execution of `malloc()`.

        **Answer:**

        The modified program is the Appendix section.

    b.  Use the modified program to simulate that `malloc()` doesn't employ
        `sbrk()` to adjust the program break on each call. Provide explanations
        of the simulation. Hint: Suppose the program name is
        `modified_free_and_sbrk.c` then you can use the following command as
        simulation:

             $ ./modified_free_and_sbrk_7_1 40 25000

        **Answer:**

        ![](img/output-of-modified-free-and-sbrk.png)

        Based on the program execution output above, the program break after the
        first call to `malloc()` is unchanged (`sbrk()` is not employed). This
        continues until the 6th `malloc()` call, where the program break is
        changed from 0x561e0a7ac000 to 0x561e0a7d1000 (incremented by
        0x25000). A pattern is then observed: every $n$-th call to `malloc()`,
        where $n$ is a multiple of 6, the program break is changed. This makes
        sense because $25000_{16} / 25000_{10} \approx 6$ (recall that the block
        size is 25000 bytes and the program break is incremented by 0x25000
        bytes).

# References

1.  Linux man-pages (release 5.10)
2.  The Linux Programming Interface, Michael Kerrisk, 2010
