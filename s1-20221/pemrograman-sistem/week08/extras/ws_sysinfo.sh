#!/bin/bash

menu() {
        while true; do
                clear
                date
                echo Main menu:
                echo 1. Operating system info
                echo 2. Hardware list
                echo 3. Free and used memory
                echo 4. Hardware details
                echo 5. Exit
                echo -ne 'Choose 1-5: '
                read -r choice
                clear
                case $choice in
                        1) os_info;;
                        2) hardware_list;;
                        3) memory;;
                        4) hw_details_menu;;
                        5) echo Bye bye..; exit;;
                esac
        done
}

hw_details_menu() {
        while true; do
                echo 1. CPU
                echo 2. Block devices
                echo 3. Back
                echo -n 'Choose 1-3: '
                read -r choice
                case $choice in
                        1) cpu_details;;
                        2) block_devices_details;;
                        3) break;;
                esac
        done
}

cpu_details() {
       lscpu | awk '/(Model name|MHz|cache:)/'
       pause
}

block_devices_details() {
        lsblk
        pause
}

pause() {
        echo Press [Enter] to continue...
        read -r
}

os_info() {
        echo System Status
        echo "Username: $USER"
        echo "OS: $(uname -sr)"
        echo "Uptime: $(uptime -p | cut -c 4-)"
        echo "IP: $(hostname -I)"
        pause
}

hardware_list() {
        echo "Machine hardware: $(uname -m)"
        lshw -short
        pause
}

memory() {
        free | awk '/Mem/ { OFS="\n"; print "Size: "$2, "Free: "$4 }'
        vmstat
        pause
}

menu
