#!/bin/bash
for i in $(seq 0 8); do
        for j in $(seq 0 8); do
                if [ "$i" == "$j" ] || [ "$j" == $((8-i)) ]; then
                        echo -n '█'
                else
                        echo -n ' '
                fi
        done
        echo
done
