#!/bin/bash

handle_sigusr1() {
        echo "Handler: SIGUSR1 signal received"
        sleep 1
}

handle_sigusr2() {
        echo "Handler: SIGUSR2 signal received"
        sleep 1
}

main() {
        trap handle_sigusr1 SIGUSR1
        trap handle_sigusr2 SIGUSR2
        echo "Main: running with PID $BASHPID"
        sleep 5
}

killer() {
        echo "Killer: running with PID $BASHPID"
        echo "Killer: killing $1"
        (kill -USR1 "$1") &
        (kill -USR1 "$1") &
        (kill -USR1 "$1") &
        (kill -USR2 "$1") &
        (kill -USR2 "$1") &
        (kill -USR2 "$1") &
}

main &
main_pid=$!
echo "Parent: Main's PID is $main_pid, sending signals"
killer "$main_pid"
sleep 6
