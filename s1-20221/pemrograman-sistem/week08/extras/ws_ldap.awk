#!/usr/bin/awk -f

BEGIN {
        FS=":"
        OFS="\n"
        ORS="\n\n"
}

/^[^#]/ && !visited[$0]++ {
        print(sprintf("dn: uid=%s dc=example, dc=com", $1),
              sprintf("cn: %s%s", $2, $3),
              sprintf("sn: %s", $3),
              sprintf("telephoneNumber: %s\n", $4));
}
