#!/bin/bash
pattern="^[1-7]"

ps -xo pid,exe | while read -r LINE
do
        if [[ $LINE =~ $pattern ]]
        then
                echo "$LINE"
        fi
done

