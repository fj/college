#!/bin/bash

signal_received=0
trap "signal_received=1" USR1

wait_turn() {
        while true; do
                if [[ "$signal_received" == 1 ]]; then
                        signal_received=0
                        break
                fi
        done
}

main() {
        echo Waiting for the other vocalist
        while true; do
                if vpid=$(pgrep vocalist2.sh); then
                        break
                fi
        done

        while read -r line; do
                if [[ "$line" != *#* ]]; then
                        echo "$line"
                elif [[ "$line" == 1#* ]]; then
                        cut -d '#' -f 2 <<< "$line"
                        kill -USR1 "$vpid"
                else
                        echo "...";
                        wait_turn
                fi
                sleep 0.2
        done < lyrics.txt
}

main
