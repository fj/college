---
author: 'Faiz Unisa Jazadi -- `faiz.uni2003@mail.ugm.ac.id`'
date: '2022-11-29'
geometry: margin=2.5cm
subtitle: CSCM603127 System Programming
title: 'Worksheet 8 - Scripting (II) dan Signal'
---

# Answers

1.  Create a bash script named `draw_x.sh` to produce the following output (not
    included). Provide an explanation to your answer.

    **Answer:**

    The script is attached as `ws_draw_x.sh`.

    The script works by doing two `for` loops. The outer loop is used to print
    the lines, and the inner loop is used to print the columns. For each line
    and column, a conditional echo is executed (depends on the value of `i` and
    `j`).

2.  Suppose that you have the following `profile_user.txt` (not included). Using
    `awk`, parse each user data in `profile_user.txt` into LDAP record, but keep
    in mind that each user data must be created only once. Below is the sample
    output. Provide an explanation to your answer!

    **Answer:**

    The script is attached as `ws_ldap.awk`.

    The `awk` script works by utilizing two pattern and action explained below.

    1.  `BEGIN`: used to set the `IFS`, `OFS`, and the `ORS` separators
        accordingly.
    2.  `/^[^#]/ && !visited[$0]++` (matches when a line is not a comment and no
        similar line has not been matched before): used to print the desired
        output format.

3.  Create a script that displays 5 menu choices regarding system information on
    your Linux OS. The conditions are as follows:

    -   The first option will display the username, operating system used,
        uptime along with how many users, IPs, and hostnames.
    -   The Second Option will briefly display all your hardware information.
    -   The third option will display memory size and free memory, statistical
        system memory information, including the list of applications that use
        the largest CPU performance.
    -   The fourth option, create a submenu that contains 2 menus, namely:
        -   CPU: displays details about your cpu.
        -   Block Device: displays the storage devices on your system
    -   The fifth option, exit from script

    Name the script `sysinfo.sh` and provide an explanation to your program in
    the PDF document file!

    **Answer:**

    The script is attached as `ws_sysinfo.sh`.

    The script works by utilizing the utilities mentioned in the hint. There is
    also additional text processing using `cut` and `awk`.

4.  Is there a way to trap `SIGKILL` signal? Provide an explanation to your
    answer!

    **Answer:**

    According to the *signals*(9), there is no way to trap the `SIGKILL` signal
    from a user program. The signal is designed with the capability to forcely
    terminate a process. If it can be caught, blocked, or ignored, then it would
    lose its purpose.

5.  Create a simulation using bash script to prove that if we send the same
    signal multiple times, it will only be sent once by the system! Provide an
    explanation to your simulation!

    **Answer:**

    Source code attached as `merged_signal.sh`.

    ![](img/merged-signal-sim.png)

    The simulation is done by creating a bash script that basically consists of
    three parts: parent, main, and killer. The main part is a child that only
    sleeps and has signal `SIGUSR1` and `SIGUSR2` trap set. The killer part is a
    child that sends three `SIGUSR1` and three `SIGUSR2` signals to the main
    process. The parent part is the parent process that calls the main part and
    the killer part to run as a separate process in the background. Based on the
    output above, even though the signal was sent 6 times, it was only detected
    as if only one `SIGUSR1` and one `SIGUSR2` was sent.

6.  Have you ever heard about a song called Kill Dash Nine?! In this section,
    you are challenged to create a duet by writing 2 shell scripts (because you
    are a programmer) that sings Kill Dash Nine consecutively and harmoniously
    according to the lyrics provided at this link...

    If you have succeeded in creating both scripts, please answer the following
    questions.

    ![](img/kill-dash-nine-demo.png)

    a.  Please explain briefly how the scripts work!

        **Answer:**

        The scripts work by sequentially sending the `SIGUSR1` signal to each
        other to indicate that if one has finished it's turn.

    b.  Is there any algorithm that you use? What are them, and why do you use
        them in this assignment?

        **Answer:**

        At first, the script will wait each other to start by continuously
        searching for the corresponding PID using `pgrep`. Once found, the
        script will iterate through the `lyrics.txt` file by line. If a line is
        not the script's turn, it will wait for the `SIGUSR1` signal (sent by
        the other vocalist process). If the line is the script's turn, it will
        just print the lyric line.

        I use the above algorithm because of its simplicity and its ease of
        implementation.

    c.  What are the signals that you use to complete the scripts? Please
        elaborate the reasons why you use them.

        **Answer:**

        The signal that is sent by a vocalist process to another process is the
        `SIGUSR1` signal.

    d.  Please explain what happens if you do `SIGTERM` and `SIGKILL` when both
        scripts are running.

        **Answer:**

        When sent to one of the two running processes of the script, the process
        will be terminated and the duet will be out of sync. In both scripts,
        there is no handler for `SIGTERM` signal and `SIGKILL` can't be caught.

    e.  Please explain what happens if you do `SIGQUIT` when both scripts are
        running. What makes it different with case number 4?

        **Answer:**

        Both scripts continue to run as expected. This is the opposite of what
        happened with case number 4. This is caused by a non-standard behavior
        of bash that ignores `SIGQUIT`.

# References

1.  Linux man-pages (release 5.10)
