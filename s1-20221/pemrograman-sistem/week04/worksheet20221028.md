---
author: 'Faiz Unisa Jazadi -- `faiz.uni2003@mail.ugm.ac.id`'
date: '2022-10-29'
subtitle: CSCM603127 System Programming
title: 'Worksheet 4 - Processes'
---

# Answers

1.  Answer the following questions about process and program:

    a.  What will happen if we execute the `fork()` system call to an existing
        process?

        **Answer:** The existing process will create a child process running the
        same program.

    b.  Explain the meaning of copy-on-write and its relation with the `fork()`
        system call!

        **Answer:** The implementation of fork on Linux uses copy-on-write
        pages. This means that creating a child process on Linux will not copy
        the entire pages of the parent process at once. Rather, it will only be
        copied when there is a write operation done to a specific page. Thus,
        the only penalty that `fork()` incurs is the time and memory needed to
        duplicate parent's page tables, and to create a unique task structure
        for the child.

    c.  What is the difference between `getpid(void)` and `getppid(void)`? Why
        do we need to know about both `ppid` and `pid`?

        **Answer:** The `getpid()` system call is used to get the process ID of
        the calling process, whereas `getppid()` is used to get the parent
        process ID of the calling process. Both `ppid` and `pid` are necessary
        to perform many operations that require them (e.g. killing a process,
        creating unique temporary files).

2.  To answer these following questions, use the `ps` command on Linux/UNIX
    operating system. You must understand how to use this command using the man
    page (`man ps`) or any other sources:

    a.  Mention three `ps` options along with its explanation to help a user to
        manage a process. Provide an explanation and a case where this option
        might help the user.

        **Answer:**

        -   `ps -e` is used to display every process on the system using
            standard syntax. This is helpful when the user wants to see every
            process on the system (not only the ones with the same user and
            terminal).
        -   `ps -ejH` is used to print a process tree. This is useful when the
            user wants to see the hierarchical structure of the running
            processes.
        -   `ps -u username` is used to display processes running as `username`
            (by effective user ID). This is helpful when the user wants to only
            see a specific user's processes (e.g. processes run by `www-data`).

    b.  Mention `ps` options that can be used to obtain information about when a
        process is started! Provide an example on how to use it and screenshot
        the result!

        **Answer:**

        To view all processes on the system with an added column of when the
        process is started, the `-O lstart` option can be used
        (e.g. `ps -a -O lstart`).

        ![](img/ws4-ps-lstart.png)

    c.  Mention various types of process states on Linux/UNIX operating systems!

        **Answer:**

        According to the `ps`(1) man page, a process can be described in the
        following states.

        -   `D`: uninterruptible sleep (usually IO)
        -   `I`: Idle kernel thread
        -   `R`: running or runnable (on run queue)
        -   `S`: interruptible sleep (waiting for an event to complete)
        -   `T`: stopped by job control signal
        -   `t`: stopped by debugger during the tracing
        -   `W`: paging (not valid since the 2.6.xx kernel)
        -   `X`: dead (should never be seen)
        -   `Z`: defunct ("zombie") process, terminated but not reaped by its
            parent

    d.  Explain the information provided in `PID`, `USER`, `PR`, `NI`, `VIRT`,
        `RSS`, `S`, `%CPU`, `%MEM`, `TIME+`, and `COMMAND` field when using the
        `ps` command!

        -   `PID`: ID of the process
        -   `USER`: effective username
        -   `PR`: priority value
        -   `NI`: niceness value
        -   `VIRT`: amount of virtual memory used
        -   `RSS`: non-swapped physical memory used
        -   `S`: current state
        -   `%CPU`: CPU utilization in percent (CPU time/real time ratio)
        -   `%MEM`: ratio of the process's resident set size to the physical
            memory in percent
        -   `TIME+`: cumulative CPU time
        -   `COMMAND`: the command of the process with all its arguments as a
            string

3.  Answer the following questions about the process life cycle:

    a.  Explain the meaning of a zombie process, orphaned process, and daemon
        process! Mention the differences between them!

        **Answer:**

        A zombie process is a partly dead process, usually a child process that
        has not been reaped by its parent. An orphaned process is a process
        whose parent has terminated earlier than the child, usually making the
        child process re-parented to the `init` process. A daemon process is a
        non-interactive process that runs in the background, usually made by
        intentionally making an orphaned process and closing open file
        descriptors.

        A zombie process is partly dead and waiting to be reaped by its parent,
        whereas an orphaned or daemon process is still alive. A daemon process
        is an intentionally orphaned process, whereas an orphaned process may
        not be intentionally made as a daemon process.

    b.  What can we do to prevent the creation of a zombie process and mention
        (if any) a method to remove a zombie process!

        **Answer:** The creation of a zombie process can be prevented by quickly
        reaping them after they finished their execution. The `wait()` system
        call can be used to remove or to reap zombie processes from the
        execution table.

4.  Mention step-by-step on how to set an environment variable in Linux
    permanently (if you restart the system, the environment variable will
    persist)!

    **Answer:**

    One of the ways to accomplish this is by modifying the `/etc/environment`
    file. These are the required steps.

    1)  With `root` user, open the `/etc/environment` file with a text editor.
    2)  Go to the last line, and create a new line after it.
    3)  Write the desired new environment variable in that last line with
        `KEY=VALUE` format.
    4)  Save the file. The changes will take effect on the next login.

5.  Explain the differences between `_exit()` and `exit()`!

    **Answer:**

    `_exit()` is a system call that is used to terminate the calling process
    "immediately". `exit()` is a library function that is used to cause a normal
    termination to a process. By normal, it means that `exit()` will call
    functions registered with `atexit()` and `on_exit()` prior to the
    termination (usually for cleaning resources).

# References

1.  Linux man-pages (release 5.10)
