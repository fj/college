#include <asm/uaccess.h>
#include <linux/device.h>
#include <linux/fs.h>
#include <linux/init.h>
#include <linux/cdev.h>
#include <linux/kernel.h>
#include <linux/module.h>

#define DEVICE_NAME "fujcdev"
#define CLASS_NAME "fuj"

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Faiz Unisa Jazadi");
MODULE_DESCRIPTION("Simple character device driver");
MODULE_VERSION("0.1");

static dev_t dev;
static struct cdev fuj_cdev;
static unsigned int count = 5;
static char message[256] = { 0 };
static short size_of_message;
static int number_opens = 0;
static struct class *classptr = NULL;
static struct device *dvcptr = NULL;

// The prototype functions for the character driver
static int dev_open(struct inode *, struct file *);
static int dev_release(struct inode *, struct file *);
static ssize_t dev_read(struct file *, char *, size_t, loff_t *);
static ssize_t dev_write(struct file *, const char *, size_t, loff_t *);

static struct file_operations fops = {
	.open = dev_open,
	.read = dev_read,
	.write = dev_write,
	.release = dev_release,
};

static int dev_open(struct inode *inodep, struct file *filep)
{
	number_opens++;
	printk(KERN_INFO "fujcdev: Device has been opened %d time(s)\n",
	       number_opens);
	return 0;
}

static int dev_release(struct inode *inodep, struct file *filep)
{
	printk(KERN_INFO "fujcdev: Device successfully closed\n");
	return 0;
}

static int __init fujdrv_init(void)
{
	int err;

	err = alloc_chrdev_region(&dev, 0, count, DEVICE_NAME);
	if (err < 0)
		return err;

	printk(KERN_INFO "fujcdev: Registered major number %d\n", MAJOR(dev));

	cdev_init(&fuj_cdev, &fops);
	fuj_cdev.owner = THIS_MODULE;
	err = cdev_add(&fuj_cdev, dev, count);
	if (err < 0)
		return err;

	// Register the device class
	classptr = class_create(THIS_MODULE, CLASS_NAME);
	if (IS_ERR(classptr)) {
		unregister_chrdev_region(dev, count);
		return PTR_ERR(classptr);
	}

	// Register the device driver
	dvcptr = device_create(classptr, NULL, dev, NULL, DEVICE_NAME);
	if (IS_ERR(dvcptr)) {
		class_destroy(classptr);
		unregister_chrdev_region(dev, count);
		return PTR_ERR(dvcptr);
	}
	printk(KERN_INFO "fujcdev: device class created correctly\n");
	return 0;
}

static void __exit fujdrv_exit(void)
{
	device_destroy(classptr, dev);
	class_unregister(classptr);
	class_destroy(classptr);
	unregister_chrdev_region(dev, count);
	printk(KERN_INFO "fujcdev: Goodbye from the LKM!\n");
}

static ssize_t dev_read(struct file *filep, char *buffer, size_t len,
			loff_t *offset)
{
	// precaution
	size_t bytes_to_copy = len >= size_of_message ? size_of_message : len;
	size_t bytes_not_copied = 0;
	if (!bytes_to_copy)
		return 0;

	bytes_not_copied = raw_copy_to_user(buffer, message, bytes_to_copy);
	if (bytes_not_copied) {
		return -EFAULT;
	}
	size_of_message = 0;
	return bytes_to_copy;
}

static ssize_t dev_write(struct file *filep, const char *buffer, size_t len,
			 loff_t *offset)
{
	const size_t max_len = 256 - 1;
	size_t bytes_to_copy = len >= max_len ? max_len : len;
	size_t bytes_not_copied = 0;

	bytes_not_copied = raw_copy_from_user(message, buffer, bytes_to_copy);
	size_of_message = bytes_to_copy - bytes_not_copied;

	if (bytes_not_copied)
		return -EFAULT;
	return bytes_to_copy;
}

module_init(fujdrv_init);
module_exit(fujdrv_exit);
