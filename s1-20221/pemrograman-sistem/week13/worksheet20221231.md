---
author: 'Faiz Unisa Jazadi -- `faiz.uni2003@mail.ugm.ac.id`'
date: '2022-12-31'
documentclass: report
geometry: 'left=2cm,right=2cm'
subtitle: CSCM603127 System Programming
title: 'Worksheet 13 - Custom Device Driver and Kernel Module'
---

# Basic Task (75 points)

We are asked to perform the tutorial as displayed in the provided hands-on video
(to create a simple character device).

## Steps

1.  Clone the driver repository mentioned in the video and adjust the source
    code accordingly.

    ![](img/ws-basic-step-1.png)

2.  Install build dependencies. Since I use Alpine Linux (`virt`), I run the
    following command.

        apk add --update alpine-sdk linux-headers linux-virt-dev kmod

    ![](img/ws-basic-step-2.png)

3.  Compile the module and test program using `make`.

    ![](img/ws-basic-step-3.png)

4.  Insert the compiled kernel module using `insmod` and verify using `dmesg`.

    ![](img/ws-basic-step-4.png)

5.  Run the test program and verify if the created device driver works properly.

    ![](img/ws-basic-step-5.png)

    As shown in the image, the compiled device driver works properly.

# Advanced Task (25 points)

The task is to modify the source code of the previous task to replace
`register_chrdev()` with `alloc_chrdev_region()`.

## Steps

Here are the steps I use to adjust the source code.

1.  Add `linux/cdev.h` to the includes part on top of the file. The header is
    used to provide `add_cdev()` function that will be used in the following
    steps.

    ``` {.c}
    #include <linux/cdev.h>
    ```

2.  Add three new static global variables and remove `major_number` variable.

    ``` {.c}
    static dev_t dev;
    static struct cdev fuj_cdev;
    static unsigned int count = 5;
    ```

    The `dev` variable is used to store the device number (`dev_t`), `fuj_cdev`
    is used to store the `cdev` struct, and `count` is used to state the desired
    minor numbers to be allocated.

3.  Modify the `fujdrv_init()` function (brief explanations provided as
    comments).

    ``` {.c}
    static int __init fujdrv_init(void)
    {
            int err; // Used to store error

            // Replace `register_chrdev()` to `alloc_chrdev_region()`
            // The allocated major and minor number pair is stored in `dev`
            err = alloc_chrdev_region(&dev, 0, count, DEVICE_NAME);
            if (err < 0)
                    return err;

            printk(KERN_INFO "fujcdev: Registered major number %d\n", MAJOR(dev));

            // Initialize the `fuj_cdev` struct with specified file operations
            cdev_init(&fuj_cdev, &fops);
            fuj_cdev.owner = THIS_MODULE; // This needs to be manually assigned
            // Register the character device
            err = cdev_add(&fuj_cdev, dev, count);
            if (err < 0)
                    return err;

            // Register the device class
            classptr = class_create(THIS_MODULE, CLASS_NAME);
            if (IS_ERR(classptr)) {
                    // Uses `unregister_chrdev_region()` instead of
                    // unregister_chrdev()`
                    unregister_chrdev_region(dev, count);
                    return PTR_ERR(classptr);
            }

            // Register the device driver
            dvcptr = device_create(classptr, NULL, dev, NULL, DEVICE_NAME);
            if (IS_ERR(dvcptr)) {
                    class_destroy(classptr);
                    unregister_chrdev_region(dev, count);
                    return PTR_ERR(dvcptr);
            }
            printk(KERN_INFO "fujcdev: device class created correctly\n");
            return 0;
    }
    ```

4.  Modify the `fujdrv_exit()` function.

    ``` {.c}
    static void __exit fujdrv_exit(void)
    {
            // Uses dev instead of MKDEV(major_number, 0)
            device_destroy(classptr, dev);
            class_unregister(classptr);
            class_destroy(classptr);
            // Uses `unregister_chrdev_region()` instead of `unregister_chrdev()`
            unregister_chrdev_region(dev, count);
            printk(KERN_INFO "fujcdev: Goodbye from the LKM!\n");
    }
    ```

5.  Compile and verify using the test program.

    ![](img/ws-advanced-testing.png)

    Based on the output of the testing program, the compiled driver works
    properly.

# Bonus Task (20 points)

The task is to create a device driver for determining if a number is prime or
not. The task is done by adjusting the source code of the previous task.

## Steps

My intention is to keep it simple. Hence, the adjustment is only done on the
`dev_write()` file operation function.

1.  Modify the `dev_write()` function as follows (explanations are given in the
    form of comments in the code).

    ``` {.c}
    static ssize_t dev_write(struct file *filep, const char *buffer, size_t len,
                             loff_t *offset)
    {
            // Initialize a temporary buffer that will later be parsed as int
            char tmpbuf[256] = { 0 };
            const size_t max_len = 256 - 1;
            size_t bytes_to_copy = len >= max_len ? max_len : len;
            size_t bytes_not_copied = 0;
            int d; // Used to store the value after int parsing
            int i; // Used to store iteration in for-loop

            // Instead of copying to `message`, copy to `tmpbuf` instead
            bytes_not_copied = raw_copy_from_user(tmpbuf, buffer, bytes_to_copy);

            if (bytes_not_copied) {
                    printk(KERN_ERR "fujcdev: Failed to copy to tmpbuf!\n");
                    return -EFAULT;
            }

            // Try parsing `tmpbuf` as int to `d`, return -EINVAL if failed
            if (kstrtoint(tmpbuf, 10, &d) < 0) {
                    printk(KERN_ERR "fujcdev: Failed to parse tmpbuf!\n");
                    return -EINVAL;
            }

            // Assume that `d` is prime
            sprintf(message, "%d is a prime number", d);
            // Naive method to determine if a number is prime
            for (i = 2; (i < d || d == 1); i++) {
                    if (d % i == 0 || d == 1) {
                            // Break and update `message` if `d` is not prime
                            sprintf(message, "%d is not a prime number", d);
                            break;
                    }
            }

            // Determine the `size_of_message` using `strlen()`
            size_of_message = strlen(message);

            return bytes_to_copy;
    }
    ```

2.  Compile, then verify using the test program.

    ![](img/ws-bonus-testing.png)

    Based on the image, the compiled driver works as expected.

# References

Below are the references used to complete the tasks.

1.  <https://github.com/arizbw/sysprog/blob/master/driver/Makefile>
2.  <https://sysprog21.github.io/lkmpg/>
3.  <https://rockyhotas.github.io/linux,/kernel,/modules,/device/2021/11/16/linux-char-devices.html>
4.  <https://www.kernel.org/doc/htmldocs/kernel-api>
5.  <https://unix.stackexchange.com/questions/606073/how-to-build-kernel-modules-in-alpine-3-12>
