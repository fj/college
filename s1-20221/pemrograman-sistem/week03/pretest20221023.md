---
author: 'Faiz Unisa Jazadi -- `faiz.uni2003@mail.ugm.ac.id`'
date: '2022-10-23'
subtitle: CSCM603127 System Programming
title: 'Pretest 3 - System Calls'
---

# Answers

1.  To retrieve detailed information about a file (permissions, size, last
    modified file's last timestamp), do we need a system call? Explain using max
    5 sentences!

    **Answer:**

    A system call is needed to retrieve information about a file because
    accessing a file means accessing the disk. Accessing a disk requires a
    system call. The system call that is commonly used to access file
    information is `stat()` system call.

2.  What is a `stat` structure and what kind of information can be stored in the
    stat structure?

    **Answer:**

    A `stat` structure is a C struct, usually stored in
    `/usr/include/linux/stat.h` header file. It is a struct that contains
    information about a file including its name, type, inode number, etc.

    ``` {.c}
    struct stat {
        dev_t     st_dev;         /* ID of device containing file */
        ino_t     st_ino;         /* Inode number */
        mode_t    st_mode;        /* File type and mode */
        nlink_t   st_nlink;       /* Number of hard links */
        uid_t     st_uid;         /* User ID of owner */
        gid_t     st_gid;         /* Group ID of owner */
        dev_t     st_rdev;        /* Device ID (if special file) */
        off_t     st_size;        /* Total size, in bytes */
        blksize_t st_blksize;     /* Block size for filesystem I/O */
        blkcnt_t  st_blocks;      /* Number of 512B blocks allocated */
        ... stripped
    ```

3.  Explain briefly each following terminology:

    a.  File descriptor
    b.  Inode table

    **Answer:**

    a.  File descriptor is an unsigned integer used to identify each open files
        on a process.
    b.  Inode table is a table stored in the logic disk block that contains all
        inode numbers for a filesystem.

4.  Is it possible to use `close()` a standard file descriptor (`stdin`,
    `stdout`, or `stderr`)? What is a side effect if we manage to do that?

    **Answer:**

    Standard file descriptor like `stdin` is usually opened for the process by
    the OS. The standard file descriptors will also be closed by the OS upon
    process termination. Intentionally closing one or more of them in a program
    can cause undefined behavior.

# References

1.  Linux man-pages (release 5.10)
