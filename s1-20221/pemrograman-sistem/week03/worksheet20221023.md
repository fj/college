---
author: 'Faiz Unisa Jazadi -- `faiz.uni2003@mail.ugm.ac.id`'
date: '2022-10-23'
subtitle: CSCM603127 System Programming
title: 'Worksheet 3 - Stat System Calls'
---

# Answers

1.  Explain the differences in:

    a.  `stat()` and `lstat()` command

        **Answer:**

        `stat()` system call follows symbolic links, whereas `lstat()` system
        call will not follow symbolic links.

    b.  File descriptor and file description

        **Answer:**

        File descriptor is an unsigned integer that refers to a file description
        in the kernel. File description is a description about an open file on
        the kernel including the current offset, blocking/non-blocking, etc.

2.  Are there any processes that don't have a file descriptor? Explain Why?

    **Answer:**

    Daemon processes is a type of process that usually don't have a file
    descriptor. They are processes that are designed to run in the background
    (instead of being interactive), usually as a service and suffixed with 'd'
    (e.g. `sshd`, `httpd`). These type of processes close any of their open file
    descriptors to make sure that there is no hanging resources left when they
    are terminated.

3.  Explain why these following commands are equal!

    -   `creat(pathname, mode)`
    -   `open(pathname, O_WRONLY|O_CREAT|O_TRUNC, mode)`

    **Answer:**

    In the `open`(2) man-page, it is mentioned that the call to
    `creat(pathname, mode)` is equivalent to calling
    `open(pathname, O_WRONLY|O_CREAT|O_TRUNC, mode)`. In the
    [`fs/open.c`](https://lxr.linux.no/linux+v5.17.4/fs/open.c#L1300) file of
    the Linux source code, `creat()` is defined as a wrapper to `open()`.

    ``` {.c}
    SYSCALL_DEFINE2(creat, const char __user *, pathname, umode_t, mode)
    {
        int flags = O_CREAT | O_WRONLY | O_TRUNC;

        if (force_o_largefile())
                flags |= O_LARGEFILE;
        return do_sys_open(AT_FDCWD, pathname, flags, mode);
    }
    ```

4.  What are the consequences (if any) when we opening a file (using system call
    `open()`) and never close it (using system call `close()`)?

    **Answer:**

    If the program keeps running and keeps opening files without closing them,
    file handles will continuously be created. Eventually, the program will
    eventually the process will run out of file descriptors and subsequent calls
    to `open()` will fail.

5.  Use [`seek_io.c`](https://man7.org/tlpi/code/online/dist/fileio/seek_io.c)
    to answer the following questions:

    a.  When you execute these following commands:

             $ touch dummy_file
             $ ./seek_io dummy_file s30 wabcde
             $ ./seek_io dummy_file s32 r5

        Explain why the output of the last command is

             s32: seek succeeded
             r5: cde

        **Answer:**

        1.  `touch dummy_file`: creates a new (if not exists) called
            `dummy_file`
        2.  `./seek_io dummy_file s30 wabcde`: opens `dummy_file`, set the file
            offset to 30 (setting offset beyond EOF if possible; if there is a
            write operation, previously non-existent bytes will be filled with
            null bytes), and writes "abcde".
        3.  `./seek_io dummy_file s32 r5`: opens `dummy_file`, set the file
            offset to 32 (`...ab|c|de` or the file offset is at "c"), and
            attempts read 5 bytes (`cde`).

    b.  As a continuation from previous questions, Describe the relationship
        between the output of the command below and the term File Holes:

            $ ./seek_io dummy_file s20 R5

        **Answer:**

        The above command tries to read 5 bytes from the `dummy_file` file
        starting from the 20th byte. It shows the following output.

            s20: seek succeeded
            R5: 00 00 00 00 00

        In the previous question, "abcde" was written to `dummy_file`
        (previously empty) at offset 30. Being previously empty, the expected
        behavior of `lseek()` (used in the program) when a file is written after
        setting offset beyond EOF is to fill the "hole" (gap between EOF and
        offset) with null bytes. The above command shows that `dummy_file` has
        null bytes or "file holes" in the offset 20-25. Actually, the content of
        the file starting from offset 0 to 30 is null bytes (file hole).

# References

1.  Linux man-pages (release 5.10)
