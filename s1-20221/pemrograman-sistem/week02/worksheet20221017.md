---
author: 'Faiz Unisa Jazadi -- `faiz.uni2003@mail.ugm.ac.id`'
date: '2022-10-17'
subtitle: CSCM603127 System Programming
title: 'Worksheet 2 - System Calls'
---

# Answers

1.  Can you guess whether this activity needs a system call or not? Provide your
    answer with enough reasoning:

    a.  Create a program that can list all directories and files in a certain
        path

        **Answer:** Listing directories and files in a certain path requires
        access to the disk. To access the disk, **system call is needed**.

    b.  Changing value inside a variable

        **Answer:** Changing a variable means changing the value of a memory
        address. Each program has its own memory space that is directly
        accessible by the program itself. Thus, changing something inside the
        program's memory space will **not require system call**.

    c.  Print "Hello World" in a terminal/command prompt

        **Answer:** In Linux, printing something in a terminal usually means
        writing to the standard output. Standard output is a file. Thus,
        printing something in a terminal **requires system call**.

    d.  Perform sorting for lists/arrays

        **Answer:** Depends on the sorting algorithm. Most sorting algorithm
        requires dynamic memory allocation (which **requires system call**).
        But, the sorting algorithm itself does not usually require system call.

    e.  We have a Java program that continuously creating a static variable

        **Answer:** A static variable is a variable that is only initialized
        once (when a control passes through it) through the lifetime of a
        program. Static variables are stored in either the `.data` or `.bss`
        segment of a program. Trying to continuously creating a static variable
        at runtime is actually not creating them, it may just be accessing or
        modifying the already initialized variable. Static variables are not
        re-initialized, thus it **does not require system calls** to
        continuously try to create a static variable.

2.  Look at the following picture (not included). Explain step by step that
    occurs from user space to kernel space when performing a system call based
    on the previous picture.

    **Answer:**

    1)  The program calls a `glibc` system call wrapper function `execve()`
    2)  The `execve()` wrapper function then issues a `trap` instruction with
        arguments (including `0x80` as the system call number) stored in some
        predefined register.
    3)  The processor issues a software trap exception and passes control to the
        trap exception handler (switches to kernel mode)
    4)  The trap exception handler on the OS then looks for the corresponding
        function handler for the system call number (`0x80`) in the
        `sys_call_table`. Then, it passes control to the corresponding function
        (`sys_execve()`).
    5)  The `sys_execve()` parses the arguments given from the call (loading
        `path` to memory, etc.), and then return the error code. Control then
        returns back to the trap exception handler.
    6)  The trap exception handler reads the returned `error` value and sets a
        certain register to the `error` value before giving control back to the
        user program.
    7)  After the trap exception is handled, execution continues in the user
        mode. The return value of the system call is read from a predefined
        register. Then, the `errno` value is set according to the return value.
        Control is returned back to the application program.
    8)  The application program continues its execution.

3.  Answer these following questions related to handling errors from the system
    call

    a.  Find out which command or file that can provide a collection of error
        numbers that will be used to indicate an error when performing system
        calls! Provide several examples of error numbers that you can find!

        **Answer:**

        The `errno` command can be used to look for a collection of error
        numbers and its description. The error numbers is stored in the
        `/usr/include/errno.h` header file. Error numbers examples:

        -   `EPERM` 1 Operation not permitted
        -   `ENOENT` 2 No such file or directory
        -   `ESRCH` 3 No such process
        -   `EINTR` 4 Interrupted system call
        -   `EIO` 5 Input/output error
        -   `ENXIO` 6 No such device or address

    b.  Several system calls may return -1 even though it is a successful
        process. Based on this fact, how can we determine if there are errors
        that occurred when performing a system call?

        **Answer:**

        There are some exceptions (and they are documented in the man-pages). If
        a system call returns -1 even though no error happened (unreliable),
        usually the `errno` is set accordingly. The value of `errno` can be used
        to determine whether an error occurred or not.

4.  Answer these following questions regarding system call:

    a.  Why it is preferable to use Library Functions to perform system calls?
        Explain using 5 sentences at most.

        **Answer:**

        Library functions (system call wrappers) provide abstraction for system
        calls, application programs can rely on the corresponding library
        function when trying to invoke a system call. Using the library
        functions is preferable because it provides lower level independence
        (application programs do not need to care about the lower level system
        call invocation details), thus making the application program more
        manageable and reliable.

    b.  Find out which system call is executed when using the following Library
        Functions

        i.  `fopen()`

            **Answer:** The function calls `open()`

        ii. `malloc()`

            **Answer:** The function calls `sbrk()`, `brk()`, and `mmap()`

        iii. `printf()`

             **Answer:** The function calls `write()` (to write something to the
             standard output)

5.  Suppose we have these following C programs (not included): `time.c` and
    `checkusage.c`. Based on the previous C programs:

    -   Can you find system calls from the above programs and explain its
        usability?

        **Answer:**

        -   `gettimeofday()`: called by the `time.c` program, is used to get the
            current time
        -   `getrusage()`: called by the `checkusage.c` program, is used to get
            resource usage statistics from the kernel
        -   `write()`: used by `printf()` calls in both programs to show output
            (printing to standard output)

    -   What is the output for each program if it compiled?

        **Answer:**

        -   Output of compiled `time.c`

            ![](img/output-of-time-c.png)

        -   Output of compiled `checkusage.c`

            ![](img/output-of-checkusage-c.png)

# References

1.  Linux man-pages (release 5.10)
