---
author: Faiz Unisa Jazadi
date: '2022-09-02'
numbersections: true
title: Intro to Relational Model
---

# Structure of Relational Databases

## Atribut

-   Suatu entitas memiliki atribut. Semua nilai yang mungkin untuk suatu atribut
    disebut sebagai domain atribut.
-   Atribut umumnya bersifat **atomik**, artinya sudah tidak dapat dipecah lagi
-   Terdapat suatu nilai spesial `null` yang merupakan anggota dari seluruh
    domain atribut. Nilai yang diindikasikan adalah "tidak diketahui".
-   Sering kali, penggunaan nilai `null` menyebabkan komplikasi definisi
    terhadap banyak operasi

## Relasi yang Tidak Berurut

Urutan baris maupun urutan pada tabel tidak mempengaruhi relasi.

# Database Schema

-   Skema database adalah struktur *logical* dari database
-   Instance database adalah suatu *snapshot* dari data di database pada suatu
    waktu

Contoh skema: `instructor (ID, name, dept_name, salary)`

# Keys

-   Let $K \subseteq R$
-   $K$ adalah **superkey** dari $R$ jika semua nilai $K$ cukup untuk
    mengidentifikasi secara unik pasangan tuple dari setiap kemungkinan relasi
    $r(R)$
-   Superkey $K$ adalah suatu **candidate key** jika $K$ bersifat minimal
    (minimal = efisien ruang?)
-   Hanya satu dari key-key yang merupakan kandidat yang dipilih
-   Foreign key constraint: nilai pada suatu relasi harus muncul pada yang
    lainnya
    -   Referencing relation
    -   Referenced relation

# Schema Diagrams

![](img/schema-diagram-example.png)

# Relational Query Languages

-   Banyak jenisnya: procedural, non-procedural, declarative
-   Bahasa yang "murni": relational algebra, tuple relational calculus, domain
    relational calculus (mirip dari segi computing power)
-   Relational algebra: tidak turing-machine equivalent dan terdiri dari 6
    operasi dasar

# The Relational Algebra

... adalah suatu bahasa prosedural yang terdiri dari beberapa operasi yang
memiliki satu ata dua relasi sebagai input dan menghasilkan suatu relasi yang
baru sebagai hasilnya.

Enam operator dasar:

-   select: $\sigma$
-   project: $\Pi$
-   union: $\cup$
-   set difference: $-$
-   Cartesian product: $x$
-   rename: $\rho$

## Operasi Select

-   Menyeleksi tuple yang memenuhi predikat tertentu.
-   Notasi: $\sigma_{p}(r)$ dengan $p$ sebagai **selection predicate**

Contoh: query untuk mencari semua tuple pada relasi `instructor` dengan nama
departemen `Physics` adalah:

$$ \sigma_{\text{dept\_name = "Physics"}}(\text{instructor}) $$

Pada predikat, selain perbandingan dengan $=$, perbandingan juga mungkin dengan
$\neq, >, \geq, <, \leq$.

Tiap predikat dapat dikombinasikan dengan koneksi: $\wedge$ (and), $\vee$ (or),
$\neg$ (not).}

Predikat juga dapat mengandung perbandingan antara dua atribut.

## Operasi Project

... adalah operasi unary yang mengembalikan relasi pada argumen-nya, dengan
mengeliminasi beberapa atribut.

Notasi:

$$ \Pi_{A_1, A_2, A_3, \dots, A_k} (r) $$

Hasil operasi didefinisikan sebagai relasi dari sebanyak $k$ kolom yang didapat
dengan menghapus kolom lain yang tidak dituliskan. Kemudian, baris yang
terduplikasi dihapus, karena relasi adalah himpunan.

## Komposisi Operasi Relasional

Pada aljabar relasional, hasil operasi adalah relasi. Maka, operasi-operasi
aljabar relasional dapat disusun menjadi suatu ekspresi aljabar relasional.

## Operasi Cartesian-Product

Operasi ini digunakan untuk mendapatkan semua baris-baris pasangan yang mungkin
antara dua relasi.

Misalnya untuk menggabungkan produk Cartesian dari relasi `instructor` dan
`teaches`, ditulis sebagai berikut.

$$ \text{instructor}\ \times\ \text{teaches} $$

![](img/cartesian-prod-example.png)

## Operasi Join

Sebelumnya, produk Cartesian digunakan untuk menghubungkan semua tuple (baris)
dari relasi `instructor` dengan semua baris dari relasi `teaches`. **Namun**,
sebagian besar dari baris-baris hasil operasi memiliki informasi tentang
instruktur yang bahkan tidak mengajar course yang tercantum.

Maka, untuk mendapatkan baris-baris atau tuple-tuple dari hasil "instructor X
teaches" yang menunjukkan dengan benar informasi yang benar, digunakan kombinasi
operasi select dan operasi produk Cartesian seperti berikut.

$$ \sigma_{\text{instructor.id = teaches.id} = \text{(instructor)\ \times\
\text{teaches}}} $$

Operasi join memungkinkan kita untuk mengkombinasikan kedua operasi di atas.

Asumsikan ada dua relasi $r(R)$ dan $s(S)$. Kemudian, "theta" adalah predikat
terhadap atribut pada skema $R$ "union" $S$. Maka, operasi join $r\
\bowtie_\theta\ s$ didefinisikan sebagai berikut.

$$ r\ \bowtie_\theta\ s = \sigma_\theta(r \times s) $$

Maka, untuk kasus sebelumnya (gabungan select dan produk kartesian) dapat
ditulis secara ekuivalen sebagai:

$$ \text{instructor} \bowtie_{\text{instructor.id = teaches.id}}\ \text{teaches}
$$

## Operasi Union

-   Digunakan untuk mengkombinasikan dua relasi.
-   Notasi: $r \cup s$
-   Agar $r \cup s$ valid, syaratnya:
    -   $r$, $s$ harus memiliki jumlah atribut sama (*same arity*)
    -   Domain atribut harus **kompatibel**

## Operasi Set-Intersection

-   Digunakan untuk menemukan baris-baris atau tuple-tuple yang ada di kedua
    relasi pada input.
-   Notasi: $r \cap s$
-   Asumsi:
    -   $r$, $s$ mempunyai aritas sama
    -   atribut-atribut pada $r$ dan $s$ kompatibel

## Operasi Set Difference

-   Mirip dengan operasi set-intersection, namun digunakan untuk menemukan
    tuple-tuple yang ada pada satu relasi namun tidak pada yang lainnya.

-   Notasi: $r - s$

-   Syarat:

    -   $r$, $s$ mempunyai aritas sama
    -   atribut-atribut pada $r$ dan $s$ kompatibel

## Operasi Assignment

-   Terkadang, lebih mudah untuk menulis ekspresi aljabar relasional dengan
    memecah ekspresi tersebut menjadi beberapa variabel (yang mewakilkan suatu
    bagian).

-   Operasi assignment dinotasikan dengan $\leftarrow$ dan mirip dengan operator
    assignment pada banyak bahasa pemrograman

Contoh:

```{=tex}
\begin{align*}
\text{Physics} \leftarrow\ r \\
\text{Music} \leftarrow\ s \\
\text{Phsyics}\ \cup\ \text{Music}
\end{align*}
```
Dalam contoh tersebut, relasi $r$ di-assign ke variabel `Phsyics` dan relasi $s$
di-assign ke variabel `Music`.

## Operasi Rename

Hasil dari suatu ekspresi aljabar relasional tidak memiliki nama, sehingga tidak
memiliki *reference*. Maka, untuk tujuan tersebut, digunakan operator rename
($\rho$).

Ekspresi $\rho_x(E)$ akan mengembalikan hasil ekspresi $E$ ke dalam nama
(variabel?) $x$.

Bentuk lain operasi rename adalah dengan mengkhususkan beberapa kolom tertentu
seperti berikut.

$$ \rho_{x(A_1, A_2, \dots, A_n)}(E) $$

## Equivalent Queries

Ada banyak cara untuk menulis suatu query dalam aljabar relasional.
