---
author: Faiz Jazadi
date: '2022-10-23'
subtitle: Basis Data
title: 'Tugas 3: ER Diagram dengan ERDPLUS'
---

# Soal A

Gambar ER diagram pada Gambar 1 menggunakan tool ERDPlus.


1.  Sebutkan entitas dalam ERD dan jenis relationship (one-to-one, one-to-many,
    atau many-to-many) antar entitasnya.

    **Jawaban:**

    Entitas-entitas: `BAND`, `SHOW`, `REPAIR TECHNICIAN`, `SHOW`, `INSTRUMENT`

      Entitas A      Entitas B             Jenis Relasi
      -------------- --------------------- --------------
      `BAND`         `SHOW`                many-to-many
      `BAND`         `INSTRUMENT`          one-to-many
      `INSTRUMENT`   `REPAIR TECHNICIAN`   many-to-many

2.  Mengapa atribut 'BandPhone' ditempatkan dalam elip yang garisnya dobel?

    **Jawaban:**

    Atribut `BandPhone` adalah bersifat *multivalued* atau bisa memiliki lebih
    dari satu nilai. Artinya, satu tuple pada `BAND` bisa memiliki lebih dari
    satu nilai `BandPhone`.

3.  Mengapa atribut 'ShowID' dibagi menjadi 2 atribut?

    **Jawaban:**

    Atribut `ShowID` adalah atribut *composite* atau atribut yang merupakan
    gabungan dari dua atau lebih atribut lainnya. Desainer database
    mengasumsikan bahwa setiap `SHOW` direpresentasikan (dengan key `ShowID`).

# Soal B

Dari ER diagram yang anda buat di (1), konversikan menjadi skema basisdata.


1.  Ada berapa tabel dalam skema basisdata yang dihasilkan?

    **Jawaban:**

    Ada 8 tabel dalam skema basisdata.

2.  Mengapa jumlah entitias dalam ERD tidak sama dengan jumlah tabel dalam skema
    basisdata?

    **Jawaban:**

    Dalam ERD, ada beberapa relasi yang sifatnya many-to-many. Dalam
    realisasinya, agar efisien, biasanya relasi many-to-many dipetakan dalam
    basis data menggunakan suatu tabel penghubung (biasanya hanya berisi dua
    kolom foreign key). Kolom yang bersifat *multivalued* juga dipetakan dengan
    menambahkan tabel baru.

# Soal C

Dari skema basisdata pada (2), konversikan menjadi SQL.

1.  Apakah tipe atribut dalam SQL menurut Anda sudah sesuai untuk setiap
    atribut?

    **Jawaban:**

    Belum, semua atribut pada hasil konversi SQL diasumsikan bertipe `INT`
    sedangkan tidak semua atribut sesuai dengan tipe `INT`.

2.  Jika ada yang belum sesuai, sarankan tipe atribut yang sesuai.

    **Jawaban:**

      Nama Atribut    Saran Tipe Atribut
      --------------- --------------------
      `BandName`      `VARCHAR(100)`
      `BandAddress`   `TEXT`
      `BandContact`   `VARCHAR(50)`
      `ShowName`      `VARCHAR(150)`
      `ShowType`      `VARCHAR(100)`
      `ShowVenue`     `VARCHAR(150)`
      `ShowDate`      `DATE`
      `InsBrand`      `VARCHAR(100)`
      `InsModel`      `VARCHAR(100)`
      `InsSerialNo`   `VARCHAR(150)`
      `RTSSN`         `VARCHAR(100)`
      `RTName`        `VARCHAR(100)`

# Lampiran

## Entity Relationship Diagram (ERD)

![](img/a3-erd.png)

## Relational Schema

![](img/a3-relschema.png)

## SQL

``` {.sql}
CREATE TABLE BAND
(
  BandName INT NOT NULL,
  BandID INT NOT NULL,
  BandAddress INT NOT NULL,
  BandContact INT NOT NULL,
  PRIMARY KEY (BandID),
  UNIQUE (BandName)
);

CREATE TABLE SHOW
(
  ShowName INT,
  ShowType INT NOT NULL,
  ShowVenue INT NOT NULL,
  ShowDate INT NOT NULL,
  PRIMARY KEY (ShowVenue, ShowDate)
);

CREATE TABLE INSTRUMENT
(
  InsYearMade INT NOT NULL,
  InsBrand INT NOT NULL,
  InsModel INT NOT NULL,
  InsSerialNo INT NOT NULL,
  BandID INT,
  PRIMARY KEY (InsSerialNo),
  FOREIGN KEY (BandID) REFERENCES BAND(BandID)
);

CREATE TABLE REPAIR_TECHNICIAN
(
  RTSSN INT NOT NULL,
  RTName INT NOT NULL,
  RTAddress INT NOT NULL,
  PRIMARY KEY (RTSSN)
);

CREATE TABLE PerformsIn
(
  BandID INT NOT NULL,
  ShowVenue INT NOT NULL,
  ShowDate INT NOT NULL,
  PRIMARY KEY (BandID, ShowVenue, ShowDate),
  FOREIGN KEY (BandID) REFERENCES BAND(BandID),
  FOREIGN KEY (ShowVenue, ShowDate) REFERENCES SHOW(ShowVenue, ShowDate)
);

CREATE TABLE Repairs
(
  RTSSN INT NOT NULL,
  InsSerialNo INT NOT NULL,
  PRIMARY KEY (RTSSN, InsSerialNo),
  FOREIGN KEY (RTSSN) REFERENCES REPAIR_TECHNICIAN(RTSSN),
  FOREIGN KEY (InsSerialNo) REFERENCES INSTRUMENT(InsSerialNo)
);

CREATE TABLE BAND_BandPhone
(
  BandPhone INT NOT NULL,
  BandID INT NOT NULL,
  PRIMARY KEY (BandPhone, BandID),
  FOREIGN KEY (BandID) REFERENCES BAND(BandID)
);

CREATE TABLE REPAIR_TECHNICIAN_RTPhone
(
  RTPhone INT NOT NULL,
  RTSSN INT NOT NULL,
  PRIMARY KEY (RTPhone, RTSSN),
  FOREIGN KEY (RTSSN) REFERENCES REPAIR_TECHNICIAN(RTSSN)
);
```
