---
author: Faiz Unisa Jazadi
date: '2022-09-15'
numbersections: true
title: 'Pertemuan Minggu ke-5'
---

# Info

Pertemuan minggu selanjutnya akan disertai kuis tentang relational model dan
relational algebra.

Tugas kelompok dikumpulkan hari Sabtu (soal relational algebra disertai
screenshot output dari RelaX).

# Aljabar Relasional: Operasi Tambahan

Operasi tambahan tidak menambahkan kekuatan apapun ke aljabar relasional, namun
bisa menyederhanakan query-query yang sering digunakan.

## Set-Intersection Operator

-   Notasi: $r \cap s$.
-   Sama saja dengan $r - (r - s)$.

## Natural-Join Operator

Menggabungkan kolom yang sama yang ada pada dua tabel pada baris-baris yang
nilai pada kolom tersebut sama.

Notasi: $r \bowtie s$

## Left (Outer) Join Operator

-   Notasi: $r \\leftouterjoin s$
-   Extension dari join yang mencegah loss of information
-   Outer join mirip dengan natural join, namun kolom dari $r$ yang tidak punya
    pasangan nilainya dianggap `null`.

## Right (Outer) Join Operator

-   Notasi: $r \\rightouterjoin s$
-   Sama seperti left outer join, namun di sebelah kanan.

## Full Join Operator

-   Notasi: $r \\fullouterjoin s$
-   Gabungan left join dan right join (tidak ada informasi yang hilang)

## Division Operator

-   Untuk menjawab query yang "for all"
-   Notasi: $r \div s$
-   $r$ bisa dibagi dengan $s$ jika atribut dari $s$ adalah subset dari $r$.

## Generalized Projection

Operator proyeksi bisa ditambahkan operasi aritmetika.

## Aggregate Functions

Fungsi yang bekerja terhadap banyak nilai dan mengembalikan satu nilai hasil.
