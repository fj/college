USE cafe;

/* 1. Menampilkan data nomer pesanan ,nama pemesan dan deskripsi produk yang
dipesan beserta jumlahnya untuk pemesanan yang dilakukan setelah tahun 2018. */

/* 2. Menampilkan jumlah barang yang dipesan dan Nama pemesan yang tinggal di
daerah "Ringroad" */

/* 3. Buatlah prosedur/fungsi untuk Mengupdate Harga Product:

  a. Yang finish-nya mengandung "natural" naik 5 %

  b.  Yang finishnya leather naik 3 %

  c. Yang finishnya warna "red" naik 2 %

  d. lainnya naik 1 %*/

/* 4. Menampilkan data order (nomer order, nama pemesan dan alamatnya) yang
memiliki total nilai pesanan diatas rata-rata total pesanan semua order. */

/* 5. Menampilkan nama semua barang yang dipesan customer dan diurutkan
berdasarkan jumlah pesanan per barangnya (kecil-besar) */

/* 6. Menampilkan data yang terdiri dari : Tanggal Pesan, Nama Customer, Alamat
Customer- Nomer Pesan . Urutkan berdasarkan tanggal nya , dari pesanan yang
paling baru */

/* 7. Buatlah Trigger untuk mencatat perubahan harga(unit_price) pada tabel
produk. Tunjukkan data pencatatan setelah harga diupdate yang menunjukkan bahwa
trigger berfungsi. */

/* 8. Tampilkan data Produk (Nama dan Harganya) yang belum pernah dipesan oleh
customer! */

/* 9. Tampilkan data pesanan (nama produk dan nama pemesan) 1 bulan terakhir. */

/* 10. Tampilkan Data Pemesan (Nama, Alamat) yang memesan produk yang memiliki
finish "leather" , "Yellow"  atau "Natural Ash" */
