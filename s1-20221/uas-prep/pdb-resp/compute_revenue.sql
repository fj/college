USE cafe;

DROP FUNCTION IF EXISTS compute_revenue;

DELIMITER $$

CREATE FUNCTION compute_revenue(search_date VARCHAR(100))
RETURNS DECIMAL(10, 2)
BEGIN
        DECLARE revenue DECIMAL(10, 2);

        SELECT SUM(Price)
        INTO revenue
        FROM order_recap o
        JOIN menu m
                ON o.Item = m.Menu_Name
        WHERE Order_Date = search_date;

        RETURN revenue;
END$$

DELIMITER ;

-- testing
SELECT compute_revenue('2020-02-08');
