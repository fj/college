/*
SQLyog Community v12.4.3 (64 bit)
MySQL - 10.4.11-MariaDB : Database - cafe
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`cafe` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `cafe`;

/*Table structure for table `customer` */

CREATE TABLE `customer` (
  `ID_Customer` varchar(100) DEFAULT NULL,
  `First_Name` varchar(100) DEFAULT NULL,
  `Last_Name` varchar(100) DEFAULT NULL,
  `Addres` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `customer` */

insert  into `customer`(`ID_Customer`,`First_Name`,`Last_Name`,`Addres`) values 
('F10028801','Deft','Mc Kelly','St Avenue'),
('F01117801','Matilda','Grey','Glasgow City Centre'),
('M31079301','Kevin','Steward','Stracthclyde S12'),
('F24069901','Ashley','Simpson','Edinburgh G7'),
('F10028802','Katty','Perry','St Petersburg'),
('F01117802','Billie','Eilish','Edinburgh G7'),
('M31079302','Mark','Petersburg','Glasgow City Centre'),
('F24069902','Betty','La Fea','St Avenue'),
('F10028802','Elsa','Petersburg','Glasgow City Centre'),
('F01117803','Anna','Petersburg','Stracthclyde J8'),
('M31079303','Bon','Jovi','Stracthclyde S12'),
('F24069903','Anne','Mc Kelly','St Maria'),
('M13129501','Edmond','Harris','St Maria'),
('M28129001','Jack','Schlumber','Glasgow City Centre'),
('M16079101','Simmon','Simpson','Glasgow City Centre'),
('F10028801','Deft','Mc Kelly','St Avenue'),
('F01117801','Matilda','Grey','Glasgow City Centre'),
('M31079301','Kevin','Steward','Stracthclyde S12'),
('F24069901','Ashley','Simpson','Edinburgh G7'),
('F10028802','Katty','Perry','St Petersburg'),
('F01117802','Billie','Eilish','Edinburgh G7'),
('M31079302','Mark','Petersburg','Glasgow City Centre'),
('F24069902','Betty','La Fea','St Avenue'),
('F10028802','Elsa','Petersburg','Glasgow City Centre'),
('F01117803','Anna','Petersburg','Stracthclyde J8'),
('M31079303','Bon','Jovi','Stracthclyde S12'),
('F24069903','Anne','Mc Kelly','St Maria'),
('M13129501','Edmond','Harris','St Maria'),
('M28129001','Jack','Schlumber','Glasgow City Centre'),
('M16079101','Simmon','Simpson','Glasgow City Centre');

/*Table structure for table `menu` */

CREATE TABLE `menu` (
  `Menu_Name` varchar(100) DEFAULT NULL,
  `Price` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `menu` */

insert  into `menu`(`Menu_Name`,`Price`) values 
('Affogato',15.00),
('Americano',13.00),
('Beef Burger',14.00),
('Cheese Burger',13.00),
('Crispy Onion Ring',3.00),
('Garlic Bread',6.00),
('Ice Green Tea',3.00),
('Lasagna',7.00),
('Latte',10.00),
('Machiato',12.00),
('Mocacino',13.00),
('Pasta',5.00),
('Sandwich',4.00),
('French Fries',6.00);

/*Table structure for table `order_recap` */

CREATE TABLE `order_recap` (
  `Order_ID` int(11) DEFAULT NULL,
  `Order_Date` varchar(100) DEFAULT NULL,
  `Customer_Number` varchar(100) DEFAULT NULL,
  `Item` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `order_recap` */

insert  into `order_recap`(`Order_ID`,`Order_Date`,`Customer_Number`,`Item`) values 
(1028,'2020-01-18','F10028801','Affogato'),
(1270,'2020-01-18','F01117801','Sandwich'),
(6729,'2020-01-18','M31079301','Americano'),
(6251,'2020-01-18','F24069901','Cheese Burger'),
(1927,'2020-01-18','F10028802','Beef Burger'),
(7262,'2020-01-18','F01117802','Latte'),
(3425,'2020-01-19','M31079302','Ice Green Tea'),
(1279,'2020-01-19','F24069902','Lasagna'),
(2829,'2020-01-19','F10028802','Garlic Bread'),
(9820,'2020-01-20','F01117803','Affogato'),
(8272,'2020-01-20','M31079303','Affogato'),
(1281,'2020-01-20','F24069903','Americano'),
(1001,'2020-01-21','M13129501','Cheese Burger'),
(8383,'2020-01-21','M28129001','Beef Burger'),
(1819,'2020-01-22','M16079101','Garlic Bread'),
(9898,'2020-01-23','M31079302','Crispy Onion Ring'),
(1113,'2020-01-23','F24069902','Pasta'),
(1110,'2020-01-23','F10028802','Mocacino'),
(1028,'2020-01-23','F01117803','Latte'),
(1270,'2020-01-24','M31079303','Affogato'),
(1927,'2020-01-24','F24069903','Garlic Bread'),
(7262,'2020-01-24','M13129501','Pasta'),
(3425,'2020-01-24','F01117801','Americano'),
(1279,'2020-01-24','M31079301','Affogato'),
(2829,'2020-01-24','F24069901','Beef Burger'),
(9820,'2020-01-25','F10028802','Cheese Burger'),
(8272,'2020-01-25','F01117802','Affogato'),
(1281,'2020-01-25','M31079302','Affogato'),
(1001,'2020-01-25','F24069902','Pasta'),
(8272,'2020-01-25','F10028802','Latte'),
(1281,'2020-01-25','F01117803','Garlic Bread'),
(1001,'2020-01-25','M31079303','Americano'),
(8383,'2020-01-25','F24069903','Ice Green Tea'),
(1819,'2020-01-25','M13129501','Sandwich'),
(9898,'2020-01-26','M28129001','Sandwich'),
(1113,'2020-01-26','M16079101','Cheese Burger'),
(1110,'2020-01-26','M31079302','Latte'),
(1028,'2020-01-27','F24069902','Lasagna'),
(1270,'2020-01-28','F10028802','Crispy Onion Ring'),
(1927,'2020-01-28','F01117803','Latte'),
(7262,'2020-01-28','M31079303','Cheese Burger'),
(3425,'2020-01-29','F24069903','Beef Burger'),
(8272,'2020-01-29','F10028802','Garlic Bread'),
(1281,'2020-01-29','F01117802','Garlic Bread'),
(1001,'2020-01-29','M31079302','Beef Burger'),
(8383,'2020-01-30','F24069902','Latte'),
(1819,'2020-01-30','F10028802','Affogato'),
(9898,'2020-01-30','F01117803','Machiato'),
(1113,'2020-01-30','M31079303','Machiato'),
(1110,'2020-01-30','F24069903','Ice Green Tea'),
(1028,'2020-01-30','M13129501','Americano'),
(1270,'2020-01-31','M28129001','Americano'),
(1927,'2020-01-31','F01117803','Affogato'),
(7262,'2020-01-31','M31079303','Crispy Onion Ring'),
(3425,'2020-01-31','F24069903','Beef Burger'),
(8272,'2020-01-31','M13129501','Crispy Onion Ring'),
(1281,'2020-01-31','F01117801','Affogato'),
(1001,'2020-01-31','M31079301','Americano'),
(8383,'2020-01-31','F24069901','Cheese Burger'),
(1819,'2020-02-01','F10028802','Cheese Burger'),
(9898,'2020-02-01','F01117802','Americano'),
(1113,'2020-02-01','M31079302','Sandwich'),
(1110,'2020-02-01','F24069902','Latte'),
(1028,'2020-02-01','F10028802','Americano'),
(1270,'2020-02-01','F01117803','Lasagna'),
(1927,'2020-02-01','M31079303','Machiato'),
(7262,'2020-02-01','F24069903','Affogato'),
(3425,'2020-02-01','M13129501','Cheese Burger'),
(1028,'2020-02-01','M28129001','Latte'),
(1270,'2020-02-02','M16079101','Affogato'),
(6729,'2020-02-02','M31079302','Crispy Onion Ring'),
(6251,'2020-02-02','F24069902','Lasagna'),
(1927,'2020-02-02','F10028802','Sandwich'),
(7262,'2020-02-03','F01117803','Lasagna'),
(3425,'2020-02-03','M31079303','Machiato'),
(1279,'2020-02-03','F01117801','Latte'),
(2829,'2020-02-04','M31079301','Affogato'),
(9820,'2020-02-04','F24069901','Ice Green Tea'),
(1028,'2020-02-04','F10028802','Ice Green Tea'),
(1270,'2020-02-04','F01117802','Latte'),
(6729,'2020-02-05','M31079302','Machiato'),
(6251,'2020-02-05','F24069902','Cheese Burger'),
(1927,'2020-02-05','F10028802','Cheese Burger'),
(7262,'2020-02-06','F01117803','Garlic Bread'),
(3425,'2020-02-06','M31079303','Beef Burger'),
(1279,'2020-02-06','F24069903','Mocacino'),
(2829,'2020-02-06','M13129501','Sandwich'),
(9820,'2020-02-06','F10028801','Mocacino'),
(1777,'2020-02-06','F01117801','Affogato'),
(1028,'2020-02-07','M31079301','Garlic Bread'),
(1270,'2020-02-07','F24069901','Mocacino'),
(6729,'2020-02-07','F10028802','Affogato'),
(6251,'2020-02-07','F01117802','Affogato'),
(1927,'2020-02-07','M31079302','Machiato'),
(7262,'2020-02-07','F24069902','Affogato'),
(3425,'2020-02-08','F10028802','Affogato'),
(1279,'2020-02-08','F01117803','Mocacino'),
(2829,'2020-02-08','M31079303','Lasagna'),
(9820,'2020-02-08','F24069903','Affogato');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
