---
author: Faiz Unisa Jazadi
date: '2022-08-15'
numbersections: true
title: Introduction to Artificial Intelligence
---

# Preamble

Sesuatu dikatakan cerdas jika memiliki **pengetahuan**.

Artificial intelligence $\rightarrow$ upaya menanamkan kecerdasan pada komputer
sedemikian hingga dapat tampil seperti intelegensia manusia.

Contoh aplikasi AI: Google Maps, games, etc.

Salah satu contoh aplikasi AI yang paling sukses adalah sistem pakar (misalnya
Halodoc).

# Konsep Dasar

## Definisi AI

-   AI adalah cabang Ilmu Komputer yang berkaitan dengan otomatisasi perilaku
    cerdas (Luger et al.).

-   AI adalah membuat komputer melakukan hal yang benar berdasarkan keadaan dan
    apa yang diketahui. Hal yang benar tidak terbatas pada bagaimana orang
    melakukannya. Evaluasi beradasarkan kinerja bukan bagaimana tugas dilakukan
    (Kerangka kerja kita).

-   AI adalah sain dan teknik untuk membuat mesin cerdas, khususnya program
    komputer cerdas. Terkait dengan tugas serupa menggunakan komputer untuk
    memahami kecerdasan manusia (John McCarthy, 56).

-   AI adalah otomatisasi aktivitias yang dikaitkan dengan pemikiran manusia,
    aktivitas seperti pengambilan keputusan, pemecahan masalah, pembelajaran
    (Bellman, 78).

## Paradigma AI

-   Thinking humanly: mengambil keputusan, mencari solusi, belajar

-   Acting humanly: melakukan pekerjaan

-   Thinking rationally: penilaian dan penalaran yang rasional

-   Acting rationally: melakukan tindakan yang benar

## Area Terapan AI

Permainan, NLP, penalaran ahli, perencanaan dan penjadwalan, computer vision,
robotika.

### Permainan Catur

-   Lingkungan: papan catur

-   Tindakan: aturan-aturan langkah (pion, patih, menteri, dll.)

-   Pekerjaan yang benar: gerakan yang mengarah pada kemenangan

## Komponen Arsitektur Sistem KA

-   Ruang keadaan untuk pencarian

-   Representasi pengetahuan

-   Logika penalaran

-   Penalaran dengan ketidakpastian

-   Pembelajaran

-   ... (see slides)
