---
author: 'Faiz Unisa Jazadi -- `21/475298/PA/20563`'
date: '2022-11-14'
numbersections: true
subtitle: Kecerdasan Artifisial
title: Natural Language Processing (NLP)
---

# Definisi

Natural language processing (NLP) adalah salah satu cabang ilmu komputer yang
memungkinkan komputer untuk berkomunikasi dengan manusia dalam bahasa
sehari-hari. NLP juga disebut sebagai computational linguistics. Tujuan NLP
adalah untuk komunikasi.

Beberapa area studi yang terkait dengan NLP adalah kecerdasan artifisial, teori
bahasa otomata, pembelajaran mesin, linguistik, psikolinguistik, dan filosofi
bahasa.

NLP dibagi menjadi dua kategori yaitu understanding dan generation.

# Komunikasi

Secara umum, komunikasi dibagi menjadi dua, yaitu speaker dan listener. Sisi
speaker terdiri dari tiga komponen: intention, generation, dan synthesis. Sisi
listener terdiri dari dua komponen: perception dan analysis.

# Pemahaman Modular

## Sintaks

Syntax adalah komponen bahasa yang bertujuan untuk menjelaskan bentuk dari
bahasa berdasarkan urutan kata-kata yang tepat dan pengaruhnya terhadap makna.

## Semantika

... merupakan makna harfiah dari suatu kata, frasa, dan kalimat dalam bahasa.
Semantik digunakan untuk membangun sistem NLP dengan representasi yang
sederhana.

## Pragmatika

Pragmatika adalah konteks komunikatif dan sosial secara keseluruhan dan
pengaruhnya terhadap interpretasi. Pada analisis ini, agen harus
mempertimbangkan lebih dari hanya sekedar kalimat, namun juga harus melihat
lebih ke dalam konteks kalimat tersebut.

# Syntactic Tasks

## Word Segmentation

Segmentasi kata adalah tindakan untuk memecah string kalimat menjadi
substring-substring yang berisi kata-kata penyusunnya (umumnya dipisahkan dengan
spasi).

## Part of Speech (POS Tagging)

POS tagging adalah proses menambahkan anotasi pada setiap kata dalam kalimat
(misalnya menandai kata 'aku' dalam kalimat 'Aku sayang kamu.' sebagai subjek.

## Phrase Chunking

Phrase chunking adalah proses dalam NLP yang digunakan untuk mengidentifikasi
part of speech dan menemukan frasa-frasa kata benda dan frasa-frasa kata kerja
yang bersifat non-recursive.

## Syntactic Parsing

Syntactic parsing adalah proses dalam NLP yang digunakan untuk memahami kalimat
secara sintaksis. Aturan yang digunakan adalah context-free grammar atau tata
bahasa yang tujuannya sama dengan tata bahasa reguler namun keberlakuannya tidak
terikat pada suatu konteks tertentu.

# Semantic Tasks

## Word Sense Disambiguation (WSD)

Proses ini digunakan untuk memperjelas makna kata yang bermakna ganda sesuai
dengan posisinya dalam kalimat.

## Semantic Role Labeling

Proses ini digunakan untuk menjelaskan kata kerja pada setiap klausa.

## Semantic Parsing

Proses ini digunakan untuk memetakan kalimat natural language ke representasi
semantik yang komprehensif.

## Textual Entailment

Proses ini digunakan untuk memetakan hubungan terarah antar fragmen teks.

# Pragmatics Tasks

## Anaphora Resolution

Proses ini digunakan untuk menentukan hubungan antara frasa dan entitas (apakah
suatu frasa merujuk pada entitas yang sama?).

## Ellipsis Resolution

Proses ini digunakan untuk menghilangkan sebagian kata atau frasa yang tidak
perlu (karena kesimpulan sudah bisa didapatkan dari teks tersebut).

# Other Tasks

## Information Extraction

Information extraction adalah proses mengidentifikasi frasa-frasa yang merujuk
pada entitas tertentu dalam teks. Berikut adalah contoh.

        (John Doe) (is the Vice Director of) (Tokopedia)
        People                               Company

## Question Answering

Proses menjawab pertanyaan berdasarkan sajian informasi pada kumpulan tulisan.

## Text Summarization

Proses mengubah dokumen yang panjang menjadi bentuk yang lebih ringkas.

## Machine Translation

Proses menerjemahkan suatu teks dari suatu bahasa natural ke bahasa natural lain
(misalnya dari Bahasa Inggris ke Bahasa Indonesia).

# Tantangan NLP

## Resolusi Ambiguitas dalam Penerjemahan

Suatu kalimat bisa dimaknai dengan berbagai cara sehingga menentukan makna yang
sesuai (intended) adalah suatu tantangan. Kesalahan dalam memaknai konteks akan
dapat mengubah makna yang sebenarnya.

## Akuisisi Pengetahuan yang Manual

NLP membutuhkan peran manusia yang ahli untuk menentukan dan memformalkan
pengetahuan yang dibutuhkan. NLP bersifat manual, sulit, memakan waktu, dan
rawan keliru. Aturan-aturan yang dibuat dalam suatu bahasa memiliki banyak kasus
khusus seperti pengecualian dan penyimpangan. Sehingga, mengembangkan sistem
secara manual seperti ini memiliki biaya yang mahal untuk dikembangkan.

Salah satu pendekatan yang bisa digunakan untuk masalah ini adalah dengan
menerapkan pendekatan automatic learning. Metode machine learning bisa digunakan
untuk memperoleh pengetahuan yang dibutuhkan secara otomatis dari sekumpulan
teks yang sudah dianotasikan dengan tepat (disebut juga sebagai pendekatan
berbasis corpus). Kelebihan dari pendekatan ini: banyaknya data teks digital
yang tersedia, memberikan keterangan lebih mudah dan murah, algoritma telah
berkembang sehingga lebih efektif.
