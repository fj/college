---
author: 'Faiz Jazadi -- `21/475298/PA/20563`'
date: '2022-10-25'
geometry: margin=2cm
subtitle: Kecerdasan Artifisial
title: 'Tugas: Sistem Pakar'
---

Paper yang dipilih adalah paper berikut.

> Teo, Yong. (1994). **An expert system for performance evaluation of Unix-based
> computer systems**. 250 - 254. 10.1109/ICESD.1994.302272.

# Abstrak

Perkembangan sistem komputer yang semakin pesat mendesak perlunya metode
evaluasi performa yang efektif dan murah. Terdapat sedikit pakar yang
benar-benar bisa melakukan evaluasi terhadap performa sistem komputer secara
komprehensif (tidak sebanding dengan perkembangan). Teknologi sistem pakar
adalah teknologi yang menawarkan replikasi pakar sehingga berjumlah tidak
terbatas dan murah. Sistem pakar adalah salah satu pendekatan yang dapat
dilakukan dalam melakukan evaluasi performa sistem komputer. Dalam paper ini,
dirancang suatu sistem pakar konsultasi performa sistem komputer berbasis aturan
yang disebut Computer Performance Evaluation Expert System (CPEES).

# Pembahasan

Evaluasi performa dilakukan secara off-line melalui data hasil rekaman performa
sistem yang disebut System Activity Record (SAR). Evaluasi dikhususkan pada
pendeteksian *bottleneck* (komponen yang membatasi performa sistem secara
keseluruhan) pada sistem komputer.

Dalam paper ini, objek evaluasi adalah sistem komputer berbasis Unix dengan
empat processor yang berkomunikasi melalui sistem *packet bus* berkecepatan
tinggi. Setiap processor memiliki fungsi masing-masing: master processor,
computational server, file server, communications server. Terdapat
load-balancing pada sistem: suatu job akan diberikan ke processor dengan load
terendah.

## Jenis Sistem Pakar

Interpretasi, prediksi, dan diagnosis.

## Manfaat

1.  Membantu analis sistem komputer dalam mengevaluasi performa sistem komputer
    sehingga dapat fokus pada isu-isu yang belum dianalisis.
2.  Menjadi sumber belajar bagi analis sistem komputer yang masih baru.
3.  Evaluasi performa bisa dilakukan dengan murah.

## Metodologi

CPEES bekerja berbasis aturan (*rule-based*). Desain CPEES dilakukan dengan
mikrokomputer sistem pakar IBM bernama Personal Consultant Plus (PC Plus).
Aturan-aturan ditulis menggunakan bahasa LISP atau bahasa Abbreviated Rule
Language (ARL). PC Plus menyediakan mekanisme forward-chaining dan
backward-chaining serta mendukung representasi pengetahuan berbasis frame.

Sistem komputer terdiri dari tiga komponen utama: CPU, memory, I/O. Ketika salah
satu komponen membatasi performa, maka terjadi performance bottleneck. Sehingga,
untuk tiap processor, satu dari tiga komponen tersebut bisa dipilih untuk
dievaluasi. Aturan-aturan juga dibuat dengan satu global frame yang berisi tiga
subframe yang mewakili tiga komponen tersebut. Masing-masing subframe memiliki
tiga fasilitas konsultasi: *review*, *analyse*, *forecast*. Frame global
berfungsi untuk memanggil fungsi-fungsi yang menginisialisasi subframe-subframe
untuk melakukan analisis. Frame global juga akan mengkonsolidasikan hasil
analisis subframe-subframe menjadi suatu kesimpulan/rekomendasi.

CPEES menyediakan dua mode operasi: konsultasi dan pengembangan. Mode konsultasi
menyediakan fasilitas konsultasi dan akan berkomunikasi dengan user untuk
menyediakan temuan, rekomendasi, dan penjelasan dari hasil evaluasi. Mode
pengembangan digunakan untuk perawatan knowledge base.

Berikut adalah diagram yang menunjukkan struktur eksternal dan struktur internal
pada CPEES.

![](img/struktur-external.png){height="300px"}
![](img/struktur-internal.png){height="300px"}

## Jumlah Rule

CPEES tersusun atas 200 aturan. Aturan-aturan ini terdiri dari 50 aturan global
(menganalisa keseluruhan sistem), 60 aturan CPU, 40 aturan memory, dan 50 aturan
I/O.

### Contoh Rule

        RULE010
        SUBJECT:: EVAL-CPU-80-RULES
        DESCRIPTION:: (Excessive system calls occuring)
        IF:: (((SCALL/S-I-80+SCALL/S-O-80)+SCALL/S-L-80)>300)
        THEN::
        (ADVICE-C.P.U.-80=((SCALL/S-I-80+SCALL/S-O-80)+SCALL/S-L-80))

### Contoh Konsultasi

        Analysis:
        The processor may be CPU limited. More than 90% of the time, the run queue
        had at least one process ready to run but was waiting for the CPU.

        Figures:
        % of the time the run queue waited for CPU (%runocc): 92%

        Recommendations:
        Reschedule jobs to non-prime-time
