---
author: 'Faiz Unisa Jazadi -- `21/475298/PA/20563`'
date: '2022-09-10'
subtitle: Tugas Kecerdasan Artifisial
theme: Warsaw
title: 'Contoh Implementasi Algoritma Dijkstra dan A\*'
---

# Algoritma Dijkstra

-   Sebuah algoritma untuk memecahkan masalah jarak terpendek (*shortest path
    problem*) untuk sebuah graf berarah (*directed graph*).
-   Algoritma Dijkstra membuat sebuah tree jarak terpendek dari suatu node pada
    graf ke semua node lainnya.
-   Keseluruhan shortest-path tree dibuat $\rightarrow$ semua vertex adalah goal

# Ilustrasi Algoritma Dijkstra

![](img/dijkstra-alg.png)

# Implementasi Algoritma Dijkstra

-   Masalah jarak terpendek adalah salah satu masalah yang **sering**
-   Salah satu contohnya adalah permasalahan *routing* dalam jaringan internet

# Internet Routing

![](img/internet-routing.png)

# Algoritma A\* Search

-   Sebuah algoritma *graph traversal* dan pencarian path
-   Ekstensi dari algoritma Dijkstra
    -   Menggunakan heuristic function
    -   $f(x) = g(x) + h(x)$
-   Dibandingkan dengan algoritma Dijkstra, algoritma A\* hanya menemukan jarak
    terpendek dari satu vertex spesifik menuju ke suatu tujuan tertentu (tidak
    semua).
-   Hanya ada satu vertex goal

# Implementasi Algoritma A\*

Menemukan jarak antara dua titik pada gambar

![](img/image-two-point-dist.png)

# Referensi

1.  https://mti.binus.ac.id/2017/11/28/algoritma-dijkstra/
2.  https://en.wikipedia.org/wiki/A\*\_search\_algorithm
