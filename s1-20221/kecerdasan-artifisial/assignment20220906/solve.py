class Node:
    childs = []

    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y

    def set_childs(self, childs):
        self.childs = childs

    def get_childs(self):
        return self.childs

    def is_leaf(self) -> bool:
        return len(self.childs) == 0

    def tup(self):
        return (self.x, self.y)

    def __repr__(self):
        return str(self.tup())


def enumerate_childs(n: Node):
    childs = []

    if n.y < 3:
        childs.append(Node(n.x, 3))
    if n.x < 4:
        childs.append(Node(4, n.y))
    if n.y != 0:
        childs.append(Node(n.x, 0))
    if n.x != 0:
        childs.append(Node(0, n.y))
    if n.x == 0 and n.y != 0:
        childs.append(Node(n.y, 0))
    if n.x == 0 and n.y == 2:
        childs.append(Node(2, 0))
    if n.y == 2:
        childs.append(Node(0, 2))

    return childs


def dfs():
    root = Node()
    visited = []

    def dfs(it):
        for n in it:
            if n.tup() in visited:
                print(f'skipped {n.tup()}')
                continue
            visited.append(n.tup())
            if n.tup() == (0, 2):
                print("GOAL!")
                return
            n.set_childs(enumerate_childs(n))
            return dfs(n.get_childs())

    dfs([root])


if __name__ == '__main__':
    dfs()
