---
author: Faiz Jazadi
date: '2022-10-19'
numbersections: true
title: 'Pertemuan ke-8'
---

# Ringkasan

Pertemuan ini tidak dicatat karena kendala keyboard. Pokok bahasan pada
pertemuan ini adalah deadlock yang sebagian besar konsepnya sudah pernah dibahas
di beberapa pertemuan sebelumnya.
