---
author: 'Faiz Unisa Jazadi -- `21/475298/PA/20563`'
date: '2022-09-28'
numbersections: true
title: Konkurensi Proses
---

# Concurrency

Proses-proses dalam status yang sama (run) memperebutkan suatu resource.
Misalkan $P_0$, $P_1$, dan $P_2$ sama-sama ingin mengakses resource $R$.
Bagaimana sistem operasi mengalokasikan resource $R$ sehingga semua proses bisa
selesai?

Beberapa contoh concurrency: multitasking, multiprocess

# Inter Process Communication

-   Independent (contoh: batch)
-   Indirect dependent (cooperating, contoh: melalui mailbox/buffer)
-   Direct dependent (berkomunikasi)

## Benefits of Cooperating Processes

1.  Information sharing
2.  Modularity
3.  Computation speed-up
4.  Convenience

# Kapan Konkurensi?

-   Banyak program aplikasi
-   Struktur program aplikasi
-   Struktur sistem operasi

# Mutual Exclusion

-   Sumber daya kritis (critical resource) tidak bisa dipakai secara bersama
    pada satu saat (critical region)
-   Jaminan bahwa hanya sebuah proses yang menggunakan satu sumber daya pada
    interval waktu tertentu disebut sebagai mutual exclusion
-   Sistem operasi bertugas memastikan tidak boleh ada proses yang akan masuk
    critical region (program) yang sudah ada proses lain dengan mekanisme
    tertentu.

## Menjaga Mutex

Ada dua pendekatan. Pendekatan software:

-   Algoritma Dekker
-   Algoritma Peterson

Pendekatan hardware:

-   Interrupt disabling
-   Intruksi mesin khusus (test and set)

## Mutex dengan Lock

Kunci lock sebelum mengakses resource dan akan membuka lock ketika sudah selesai
mengakses resource. Proses lain yang akan mengakses resource akan menunggu
hingga lock terbuka.

## Kriteria Penyelesaian

-   Mutual exclusion harus dijamin
-   Proses yang ada pada non-critical section dilarang mem-block proses lain
    yang ingin memasuki critical section
-   Dijamin tidak ada deadlock dan starvation
-   Jika critical section kosong maka proses lain boleh masuk tanpa waktu tunggu

## Algoritma Dekker

Proses digambarkan sebagai manusia salju.

Proses 0:

        while turn != 0 {
            // do nothing
        }
        // critical section
        turn = 1

Proses 1:

        while turn != 1 {
            // do nothing
        }
        // critical section
        turn = 0

# Deadlock

Deadlock terjadi ketika dua proses saling menunggu untuk melepaskan lock.

# Algoritma Banker

![](https://selaluwanien.files.wordpress.com/2011/12/banker.png)
