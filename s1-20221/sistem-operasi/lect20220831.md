---
author: Faiz Unisa Jazadi
date: '2022-08-31'
numbersections: true
title: Process Management
---

# Review

Fungsi OS adalah:

-   Process manager
-   File manager
-   Resource allocator
-   I/O manager

# Konsep Proses

Proses adalah program yang dieksekusi.

Sebuah proses memiliki:

1.  program counter,
2.  stack, dan
3.  data section.

Eksekusi program dilakukan secara sekuensial.

Proses eksekusi oleh OS:

1.  Instruksi pada program di-load ke memori (di antara stack dan heap)
2.  PC diatur ke awal instruksi program di memori
3.  Ketika process berjalan, statusnya bisa berubah, beberapa status: new,
    running, waiting, ready, terminated

# Process Control Block (PCB)

Informasi terkait masing-masing proses.

-   Process state
-   Program counter
-   CPU register
-   CPU scheduling information
-   Memory-management information
-   Accounting information
-   I/O status information

# Multitasking

Process dijalankan dan saling berganti dengan selalu menyimpan state ke PCB.

# Process Scheduling Queues

-   Job queue: semua proses pada sistem
-   Ready queue: semua proses pada memori yang sudah ready dan siap dieksekusi
-   Device queue: semua proses yang menunggu I/O device

**Processes migrate among the various queues**

## Alokasi Resource

Ketika satu resource diminta oleh banyak proses, maka akan dibuat linked list of
PCB pada tiap anggota queue device, berdasarkan prioritas. Setelah suatu proses
selesai menggunakan resource, tail diubah ke proses berikutnya.

Terdapat juga ready queue, nutuk memilih proses untuk mendapatkan CPU time.

Permintaan/alokasi I/O bisa diinterupsi jika *time slice*-nya habis/expire, atau
terdapat sinyal interrupt.

## Schedulers

-   Long-term scheduler (job scheduler): menyeleksi proses mana yang dimasukkan
    ke dalam ready queue
-   Short-term scheduler (CPU scheduler): memilih proses mana yang seharusnya
    dieksekusi selanjutnya dan mengalokasikan CPU

Antara long-term dan short-term scheduler, ada **process pertengahan**, yang
berada di-tengah-tengah. Misalnya proses di-pause, sehingga kemudian di-swap-out
ke memori sekunder. Kemudian, ketika dilanjutkan, proses dimasukkan kembali ke
dalam ready queue.

Scheduler short-term lebih sering di-invoke (harus cepat). Sedangkan scheduler
long-term dipanggil sangat tidak sering (memakan detik hingga menit), cukup
lama. Long-term scheduler mengontrol *degree of multiprogramming*.

Suatu process bisa dideskripsikan dengan salah satunya: - I/O-bound process:
memakan waktu lebih lama pada I/O dibandingkan banyaknya CPU bursts - CPU-bound
process: menghabiskan waktu lebih lama pada komputasi dibandingkan I/O

# Context Switch

Ketika CPU berganti ke proses lain, sistem akan menyimpan state status lama ke
PCB. Ketika kembali ke proses tersebut, state yang disimpan dimuat kembali.

Waktu untuk context-switch adalah suatu overhead. Sistem tidak melakukan apapun
ketika dalam proses ini. Sehingga, context-switch memakan waktu tergantung
dukungan hardware.

# Process Creation

Suatu proses bisa membuat proses lain (child process). Process child juga bisa
membuat anaknya sendiri. Pada akhirnya, struktur proses akan menyerupai tree.

Antara parent dan child bisa berbagi resource (semua, parsial, atau tidak sama
sekali).

Eksekusi bisa dilakukan bersamaan ataupun parent bisa menunggu hingga child
process berakhir.

# Process Termination

-   Process menjalankan statement terakhir dan meminta sistem operasi untuk
    menghapusnya
-   Resource untuk proses tersebut di-dealokasikan oleh OS

# Hubungan Antarproses (Cooperating Process)

-   Independent: tidak terpengaruh oleh proses lain
-   Cooperating: terpengaruh oleh proses lain

Benefits:

-   Information sharing
-   Computation speed-up
-   Modularity
-   Convenience

## Paradigma

Salah satunya adalah paradigma producer-consumer. Salah satu proses bertugas
sebagai producer dan salah satunya sebagai consumer.

Antara producer dan consumer, terdapat buffer dengan salah satu tipe:

-   unbounded: tidak terbatas, atau
-   bounded: terbatas (fixed size).

## Model Hubungan

-   Direct: sharing memori
-   Indirect: menggunakan suatu medium/mailbox (misalnya file)

## Synchronization

-   Blocking, dipertimbangkan sebagai sinkronus: pengirim diblok hingga pesan
    diterima
-   Non-blocking, dipertimbangkan sebagai asinkronus: pengirim terus mengirim
    pesan dan penerima terus menerima pesan bersamaan

## Buffering

Queue message yang terhubung ke link, diimplementasikan dengan:

1.  Zero capacity: langsung, blocking karena sender harus menunggu receiver
2.  Bounded capacity: fixed size of n messages (sender harus menunggu jika link
    full)
3.  Unbounded capacity: infinite length, sender tidak pernah menunggu

# List Pertanyaan

1.  ~~Bagaimana jika program memiliki instruksi jump menuju area memori di luar
    yang sudah dialokasikan untuk instruksi program tersebut?~~ Jawaban: 
    terdapat virtual memory, sehingga jika merefer ke area di luar virtual 
    memory tersebut, terjadi fault (dibahas pada beberapa pertemuan berikutnya).
2.  Siapa yang mengatur state proses? Apakah dari programnya secara eksplisit
    meminta status? 3. PCB disimpan di mana?
3.  ~~Apakah register `PC` yang disimpan pada PCB adalah instruksi terakhir yang
    dijalankan pada suatu proses? Atau, apakah setiap program memiliki register
    yang berbeda-beda?~~ Jawaban: ketika berganti proses, state (termasuk isi
    register `PC`) disimpan ke PCB.
