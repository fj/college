---
author: Faiz Unisa Jazadi
date: '2022-09-07'
numbersections: true
title: CPU Scheduler
---

# Basic Concepts

-   Maximum CPU utilization obtained with multiprogramming

-   CPU--I/O Burst Cycle: Process execution consists of a cycle of CPU execution
    and I/O wait

-   CPU burst distribution

-   CPU burst: peningkatan frekuensi dalam durasi yang sangat singkat

-   I/O burst: peningkatan utilisasi I/O dalam durasi yang sangat singkat

-   CPU dan I/O burst saling bergantian

-   CPU Scheduler: memilih dari proses pada memory yang mana yang siap
    dieksekusi dan mengalokasikan CPU untuk proses tersebut

-   Modul dispatcher memberikan kontrol CPU terhadap proses yang dipilih oleh
    short-term scheduler di antaranya: mengganti konteks, switching ke mode
    user, jump ke suatu lokasi program untuk merestart program

-   Dispatch latency: waktu yang dibutuhkan dispatcher untuk menghentikan suatu
    proses dan memulai proses lain

# Scheduling Criteria

-   CPU utilization: buat CPU sesibuk mungkin
-   Throughput: banyak proses yang menyelesaikan eksekusinya per satuan waktu
-   Turnaround time: waktu yang dibutuhkan untuk mengeksekusi suatu proses
-   Waiting time: waktu yang dibutuhkan ketika suatu proses sudah menunggu pada
    ready queue
-   Response time: waktu yang dibutuhkan dari request dikirim hingga respon
    pertama dibuat (dalam lingkungan time-sharing)

# Scheduling Algorithms

## Criterias

-   Max CPU utilization
-   Max throughput
-   Min turnaround
-   Min waiting time
-   Min response time

## Algoritma

-   First-Come, First-Served (FCFS) Scheduling: proses yang datang pertama
    dieksekusi pertama.
-   Shortest-Job-First (SJF) Scheduling: mengasosiasikan setiap oroses dengan
    panjang CPU burst berikutnya.
    -   Kelebihan: optimal karena memberikan rata-rata waiting time yang kecil
    -   Kekurangan: sulit mengetahui panjang CPU request setelahnya, biasanya
        ditentukan dengan *exponential averaging*)
-   Priority Scheduling: sebuah nomor prioritas diasosiasikan dengan setiap
    proses (integer terkecil $\rightarrow$ prioritas tertinggi).
    -   SJF adalah priority scheduling dengan CPU burst time berikutnya sebagai
        prioritas
    -   Masalah: starvation -- proses dengan prioritas rendah tidak pernah
        dijalankan
    -   Solusi: aging -- semakin lama waktu proses, tingkatkan prioritasnya
-   Round Robin (RR)
    -   Setiap proses mendapat sedikit CPU time (*time quantum*), biasanya
        10-100 ms.
    -   Jika ada $n$ proses pada ready queue dan time quantum adalah $q$, maka
        setiap proses mendapat $1/n$ dari CPU time dalam chunk-chunk dengan
        paling lama $q$ satuan waktu
    -   Tidak ada proses yang menunggu lebih dari $(n - 1)q$ satuan waktu.
    -   Performa: FCFS jika $q$ besar, dan jika $q$ kecil maka $q$ harus besar
        sebanding dengan context switch (jika tidak, maka overhead terlalu
        tinggi)

# Tugas

**Deadline:** Sebelum UTS

Buatlah sebuah tulisan mengenai thread (apa itu thread, bagaimana mekanismenya,
algoritma yang ada seperti apa, dan lain-lain).
