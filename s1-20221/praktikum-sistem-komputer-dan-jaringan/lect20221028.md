---
author: Faiz Jazadi
date: '2022-10-28'
numbersections: true
title: Threads
---

# Summary

Thread adalah unit eksekusi terkecil dari suatu proses. Suatu proses bisa
mempunyai banyak threads. Untuk membuat suatu program C dengan memanfaatkan
thread pada sistem berbasis Unix, bisa digunakan library `pthread` (POSIX
thread).
