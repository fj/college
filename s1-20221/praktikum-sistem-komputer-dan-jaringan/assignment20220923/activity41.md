---
author: 'Faiz Unisa Jazadi -- `21/475298/PA/20563`'
date: '2022-09-23'
subtitle: Praktikum Sistem Komputer dan Jaringan
title: Activity 4.1
---

# Soal dan Jawaban

1.  Create a sleep process and send a `SIGSTOP` signal to put this process into
    stopped state. Take a screenshot that shows that this process is now in the
    stopped state (T). Hint: Use --help to see how to use the kill command.

    ![](img/a41-sleep-stopped.png)

2.  Send a `SIGCONT` signal to the stopped sleep process. Take a screenshot
    that shows that this process is no longer in the stopped state.

	![](img/a41-sleep-continued.png)
