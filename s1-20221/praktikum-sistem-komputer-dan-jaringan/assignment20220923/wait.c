#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

int main(void)
{
        pid_t p;
        p = fork();
        printf("Starting the fork...\n");
        if (p == 0) {
                printf("I am a child process: %d\n", getpid());
                printf("My parent id is %d\n", getppid());
        } else {
                printf("I am a parent process: %d\n", getpid());
                printf("My child id is %d\n", p);
                sleep(15);
                wait(NULL);
        }
        return 0;
}
