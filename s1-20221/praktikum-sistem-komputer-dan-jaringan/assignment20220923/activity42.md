---
author: 'Faiz Unisa Jazadi -- `21/475298/PA/20563`'
date: '2022-09-23'
subtitle: Praktikum Sistem Komputer dan Jaringan
title: Activity 4.2
---

# Soal dan Jawaban

1.  Modify the `wait.c` by putting the `wait()` function after the `sleep()`
    function. Compile and run the result in the background. Take some
    screenshots.

    ![](img/a42-compile-and-execute.png)

2.  Take a screenshot of the process table. What happens with the child process?

    ![](img/a42-process-table.png)

    Berdasarkan screenshot tabel proses di atas, dapat dilihat bahwa child
    process berada pada state zombie (Z). Pada soal, diketahui bahwa `wait()`
    dieksekusi setelah `sleep()` (pada parent process). Akibatnya, ketika parent
    process sedang melakukan sleep, child process (yang merupakan zombie) masih
    belum dibersihkan dari tabel process. Sesaat setelah `sleep()` selesai,
    child process baru kemudian akan dibersihkan dari tabel proses dan
    resource-nya di-dealokasikan.
