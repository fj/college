#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

int main(void)
{
        int p = fork();
        if (p == 0) {
                sleep(5);
                printf("child: my pid is %d\n", getpid());
                printf("child: my parent pid is %d\n", getppid());
        } else {
                printf("parent: my pid is %d\n", getpid());
                printf("parent: my parent pid is %d\n", p);
                sleep(3);
        }
}
