---
author: 'Faiz Unisa Jazadi -- `21/475298/PA/20563`'
date: '2022-09-21'
subtitle: Praktikum Sistem Komputer dan Jaringan
title: Assignment 4
---

# Soal 1

Buatlah sebuah program X dengan perulangan dengan jumlah perulangan bebas namun
cukup besar. Jalankan program tersebut dan cetak tabel proses menggunakan ps.
Apakah status dari proses X setelah dijalankan? Kirimkan sinyal berhenti dan
lanjutkan ke proses X. Bagaimanakah status dari proses X berubah? Jelaskan
memberikan beberapa tangkapan layar dari proses ini.

## Jawaban

### Program

``` {.c}
#include <stdio.h>
#include <unistd.h>

int main(void)
{
        long arr[1000000];
        while (arr[0] != 10000) {
                for (int i = 0; i < 1000000; i++) {
                        arr[i] += 1;
                }
        }
        printf("process exited\n");
        return 0;
}
```

### Perubahan State

![](img/p1-state-change.png)

Ketika awal dijalankan, proses ada pada status runnable/running (R). Sinyal
`SIGSTOP` kemudian dikirimkan ke proses sehingga status berubah menjadi stopped
(T). Setelah diberikan sinyal `SIGCONT`, eksekusi proses kemudian berlanjut
(status berubah menjadi runnable/running (R)). Proses kemudian berakhir dan
dihapus dari tabel proses.

# Soal 2

Buatlah sebuah program yang membuat parent process A dan child process B. Di
program ini, buatlah sehingga proses B membutuhkan waktu yang lebih lama untuk
selesai daripada proses A dan proses A tidak memiliki wait system call.
Tampilkan id proses A dan B di masing-masing blok kode mereka. Jalankan program
tersebut dan ambil beberapa tangkapan layar. Apakah id proses dari parent
process di blok kode proses B? Mengapa bisa seperti itu?

## Jawaban

![](img/p2-execution.png)

Berdasarkan output program yang ditunjukkan oleh tangkapan layar di atas, PID
parent process di blok kode proses B adalah `1`. Sebelumnya, diketahui bahwa
proses B sengaja dibuat agar berjalan lebih lama dibanding proses A. Akibatnya,
proses A berakhir ketika proses B masih berjalan. Karena proses B adalah child
dari proses A yang telah berhenti, maka proses B kini merupakan status yang
*orphan* (tidak memiliki parent).

Pada sistem operasi Unix-like, aksi yang dilakukan terhadap proses yang *orphan*
berbeda-berbeda pada setiap implementasi (implementation-defined). Aksi yang
umum dilakukan (termasuk dalam kasus ini) adalah melakukan re-parenting proses
yang *orphan* dengan menyesuaikan parent-nya menjadi proses `init`. Proses
`init` adalah proses yang umumnya dijalankan pertama kali pada sistem operasi
Linux dan memiliki PID 1.
