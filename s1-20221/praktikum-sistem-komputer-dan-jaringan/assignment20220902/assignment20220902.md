---
author: 'Faiz Unisa Jazadi -- `475298`'
date: '2022-09-02'
subtitle: Praktikum Sistem Komputer dan Jaringan
title: Tugas 1
---

# Instalasi Wireshark

Wireshark diinstall dengan menggunakan bantuan package manager `apt`. Bukti
instalasi Wireshark dapat dilihat pada gambar berikut.

![](img/wireshark-proof.png)

# Catatan

Dalam jawaban-jawaban atas beberapa soal berikut, saya mengasumsikan bahwa nomor
paket dan beberapa parameter lain yang dispesifikasikan dalam soal adalah
terkait dengan file `intro-wireshark-trace1.pcap`. File tersebut disediakan oleh
link pada modul
(<http://gaia.cs.umass.edu/wireshark-labs/wireshark-traces-8.1.zip>).

# Soal 1

Manakah dari protokol berikut yang ditampilkan (yaitu, tercantum dalam kolom
"protokol" Wireshark) di file jejak Anda: TCP, QUIC, HTTP, DNS, UDP, TLSv1.2?

## Jawaban

Ditemukan semua protokol yang disebutkan soal.

![](img/p1-tcp.png)

![](img/p1-quic.png)

![](img/p1-http.png)

![](img/p1-dns.png)

![](img/p1-udp.png)

![](img/p1-tlsv1.2.png)

# Soal 2

Berapa lama waktu yang dibutuhkan sejak pesan HTTP GET dikirim hingga balasan
HTTP OK?

## Jawaban

Paket dicari dengan menggunakan filter `http`.

![](img/p2-http-packets.png)

Untuk menghitung berapa lama waktu yang dibutuhkan, maka nilai pada kolom Time
pada paket nomor 288 dikurangi dengan nilai kolom Time pada paket nomor 286.

```{=tex}
\begin{align*}
T &= 8.501613 - 8.472728 \\
  &= 0.028885
\end{align*}
```
Maka, waktu yang dibutuhkan sejak pesan HTTP GET dikirim hingga balasan HTTP OK
adalah sebesar $0.028885$ detik.

# Soal 3

Apa alamat Internet dari `gaia.cs.umass.edu` (juga dikenal sebagai
`www-net.cs.umass.edu`)? Apa alamat Internet komputer Anda atau (jika Anda
menggunakan file jejak) komputer yang mengirim pesan HTTP GET?

## Jawaban

Paket dengan nomor 286 berisi permintaan HTTP GET dari client ke server.

![](img/p3-packet286.png)

Dengan melihat kolom Source dan Destination, maka diketahui bahwa:

1.  Pengirim/client mempunyai alamat IP `10.0.0.44`
2.  Penerima/server mempunyai alamat IP `128.119.245.12`

# Soal 4

Perluas informasi tentang pesan HTTP di jendela Wireshark "Details of selected
packet" (lihat Gambar 5 pada modul) sehingga Anda dapat melihat kolom di pesan
permintaan HTTP GET. Jenis browser Web apa yang mengeluarkan permintaan HTTP?
Jawabannya ditampilkan di ujung kanan informasi setelah kolom "User-Agent:" di
tampilan pesan HTTP yang diperluas.

## Jawaban

Berikut adalah isi segmen HTTP pada jendela "Details of selected packet"
terhadap paket 286.

![](img/p4-packet286-details.png)

Dapat dilihat bahwa isi header `User-Agent` adalah
`Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:84.0) Gecko/20100101 Firefox/84.0`.
Dengan kata lain, pengirim menggunakan web browser Mozilla Firefox versi 84.

# Soal 5

Perluas informasi tentang Transmission Control Protocol untuk paket ini di
jendela Wireshark "Details of selected packet" (lihat Gambar 5 pada modul)
sehingga Anda dapat melihat bidang di segmen TCP yang membawa pesan HTTP. Berapa
nomor port tujuan (nomor yang mengikuti "Destination port:" untuk segmen TCP
yang berisi permintaan HTTP) yang menjadi tujuan pengiriman permintaan HTTP ini?

## Jawaban

Berikut adalah isi segmen TCP pada jendela "Details of selected packet" terhadap
paket 286.

![](img/p5-packet286-tcp-details.png)

Dapat dilihat pada kolom destination port bahwa nomor port tujuan paket TCP ini
adalah 80 (nomor port umum untuk layanan HTTP).
