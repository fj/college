---
author: 'Faiz Jazadi -- `21/475298/PA/20563`'
date: '2022-11-11'
subtitle: Praktikum Sistem Komputer dan Jaringan
title: Assignment 8
geometry: margin=1.5cm
---

# Screenshot

![](img/a8-screenshot.png)

# Source Code

## `EchoServer.java`

``` {.java}
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class EchoServer {
    public static void main(String[] args)
    {
        try {
            System.out.println("Waiting for clients...");
            ServerSocket ss = new ServerSocket(9806);
            Socket soc = ss.accept();
            System.out.println("Connection established");
            BufferedReader in = new BufferedReader(new InputStreamReader(soc.getInputStream()));
            String str = in.readLine();
            PrintWriter out = new PrintWriter(soc.getOutputStream(), true);
            out.println("Hello " + str);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
```

## `EchoServer.java`

``` {.java}
import java.io.*;
import java.net.Socket;

public class EchoClient {
    public static void main(String[] args)
    {
        try {
            System.out.println("Client started");
            Socket soc = new Socket("localhost", 9806);
            BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Enter your name:");
            String str = userInput.readLine();
            PrintWriter out = new PrintWriter(soc.getOutputStream(), true);
            out.println(str);
            BufferedReader in = new BufferedReader(new InputStreamReader(soc.getInputStream()));
            System.out.println(in.readLine());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
```
