---
author: Faiz Unisa Jazadi
date: '2022-09-02'
numbersections: true
title: Sistem Komputer dan Jaringan (Intro)
---

# Prologue

Praktikum SKJ menggabungkan materi OS dan jaringan komputer.

Jaringan komputer: ekstensi OS agar komputer bisa berinteraksi.

## Format Penugasan

Tugas untuk periode sebelum ada eLOK dikirim ke `nia.gella.a@ugm.ac.id` dengan
subjek "Pertemuan 1 SKJ".

File penugasan diberi nama `[Nama]_[NIU].pdf`

## Materi

Secara mingguan:

1.  Pengenalan Wireshark
2.  HTTP
3.  Pengenalan Linux dan system call
4.  Process dan manajemen process
5.  DNS
6.  TCP dan kendali congestion
7.  Thread dan OpenMP
8.  Socket programming
9.  Deadlock dan sync
10. ARP dan ethernet
11. Manajemen disk

## Kegiatan Mingguan

1.  Penjelasan Praktikum: 15 menit
2.  Aktifitas praktikum: 75 menit
3.  Pengumpulan laporan aktifitas: 10 menit

Total: 100 menit

## Bobot Penilaian

-   Aktifitas: 2%
-   Tugas: 4%
-   Responsi: 40%

# Layering and Abstraction

Layering jaringan adalah salah satu bentuk abstraksi yang memudahkan proses
desain suatu protokol.

# Wireshark

Wireshark adalah aplikasi yang digunakan *packet sniffing* atau dalam bahasa
Indonesia adalah mengendus paket. Wireshark digunakan untuk melihat aktifitas
jaringan (paket-paket yang ditransmisikan).
