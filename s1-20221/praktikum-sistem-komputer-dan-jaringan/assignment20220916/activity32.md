---
author: 'Faiz Unisa Jazadi -- `21/475298/PA/20563`'
date: '2022-09-16'
subtitle: Praktikum Sistem Komputer dan Jaringan
title: Activity 3.2
---

# Soal

1.  Save the trace of a command `echo hello` to a file entitled `echo.log`.
2.  Filter the `echo.log` file to find the text "hello" being mentioned. Screen
    capture the result.
3.  Explain what the system call that related to the text in question number 2
    is doing based on the manual page of the system call.

# Jawaban

Screenshot:

![](img/a32-echo-strace.png)

Dapat dilihat pada screenshot, beberapa invokasi system call yang
menarik/relevan adalah `execve()` dan `write()`.

1.  `execve("/usr/bin/echo", ["echo", "hello"], 0x7ffe3d9d9698)  = 0`

    Signature:

    ``` {.c}
    int execve(const char *pathname, char *const argv[],
               char *const envp[]);
    ```

    System call `execve()` berfungsi untuk mengeksekusi program yang diacu
    sebagai `pathname` (dalam hal ini, `pathname` adalah command `echo` yang
    berada di `/usr/bin/echo`). Akibatnya, program yang sedang dijalankan oleh
    program pemanggil akan digantikan oleh program baru, dengan nilai stack,
    heap dan segmen data (initialized dan uninitialized) yang baru
    diinisialisasi.

    Argumen kedua pada fungsi berisi nilai argumen-argumen yang diberikan ke
    program yang akan dieksekusi. Argumen ketiga berisi nilai-nilai environment
    variable yang diberikan kepada program yang akan dieksekusi.

2.  `write(1, "hello\n", 6)  = 6`

    Signature:

    ``` {.c}
    ssize_t write(int fd, const void *buf, size_t count);
    ```

    System call `write()` digunakan untuk menulis sebanyak `count` bytes dimulai
    dari `buf` (pointer ke byte pertama pada buffer) ke suatu file descriptor
    `fd`.

    Dalam kasus ini, system call `write()` digunakan untuk menulis sebanyak 6
    bytes dari buffer (didapatkan dari argumen `echo`) ke file descriptor untuk
    standard output.
