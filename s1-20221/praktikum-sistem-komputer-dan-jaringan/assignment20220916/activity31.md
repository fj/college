---
author: 'Faiz Unisa Jazadi -- `21/475298/PA/20563`'
date: '2022-09-16'
subtitle: Praktikum Sistem Komputer dan Jaringan
title: Activity 3.1
---

# Soal

Try the following commands, capture your screen for their results, and explain
what these commands are doing:

1.  `uname`
2.  `df`
3.  `hostname`
4.  `hostname -I`

# Jawaban

## `uname`

![](img/a31-uname.png)

Command `uname` digunakan untuk mencetak detil mengenai mesin yang sedang
digunakan dan sistem operasi yang sedang berjalan. Dalam hal ini, outputnya
adalah `Linux` karena sistem operasi yang digunakan adalah Linux.

## `df`

![](img/a31-df.png)

Command `df` digunakan untuk menampilkan daftar filesystem yang sedang dimuat
beserta beberapa detil (termasuk banyak blok, ruang yang digunakan, yang
tersedua, dan mount point).

## `hostname`

![](img/a31-hostname.png)

Command `hostname` digunakan untuk menampilkan nama host dari sistem.

## `hostname -I`

![](img/a31-hostname-i.png)

Command `hostname -I` digunakan untuk menampilkan semua IP address dari host
sistem.
