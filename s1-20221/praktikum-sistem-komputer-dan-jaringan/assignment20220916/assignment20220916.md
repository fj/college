---
author: 'Faiz Unisa Jazadi -- `21/475298/PA/20563`'
date: '2022-09-16'
subtitle: Praktikum Sistem Komputer dan Jaringan
title: Assignment 3
---

# Soal 1

Di folder `home`, buatlah sebuah file teks bernama `myname.txt` dan tulis namamu
di dalamnya. Kemudian, jalankan dan simpan hasil `strace` untuk perintah `copy`
(`cp`) untuk menyalin file `myname.txt` tersebut dari `folder` home ke folder
`Documents`. Simpan hasil `strace` tersebut dalam sebuah file bernama
`copy.log`. Kemudian filter file `copy.log` untuk mencari path `Documents` di
dalamnya. Jelaskan apa yang dilakukan oleh system calls hasil filter tersebut!
Sertakan tangkapan layar proses yang dilakukan.

## Jawaban

![](img/a3-cp-strace.png)

Pada tangkapan layar tersebut, terdapat empat invokasi system call yang
dijelaskan sebagai berikut.

1.  `execve("/usr/bin/cp", ["cp", "myname.txt", ...`

    System call ini digunakan untuk menjalankan program `cp` yang terletak pada
    `/usr/bin/cp` dengan tiga argumen yaitu `cp` (nama program), `myname.txt`
    (source), dan `/home/cat/Documents/myname.txt` (destination). Sebanyak 38
    environment variable juga diteruskan ke program.

2.  `stat("/home/cat/Documents/myname.txt", 0x7ffc0888ed40) ...`

    System call ini bertujuan untuk mendapatkan informasi terhadap suatu file
    (`/home/cat/Documents/myname.txt`). Informasi file tersebut kemudian
    disimpan dalam struct buffer pada alamat `statbuf` (`0x7ffc0888ed40`). Dalam
    hal ini, return value dari system call ini adalah -1 (error) dengan `errno`
    bernilai 2 (`ENOENT`, atau file tidak ditemukan).

3.  `newfstatat(AT_FDCWD, "/home/cat/Documents/myname.txt", ...`

    System call `newfstatat()` adalah wrapper glibc modern untuk `fstatat()`.
    Fungsinya mirip dengan `stat()`, hanya saja `fstatat()` menyediakan
    interface yang lebih general. Dalam kasus ini, `fstatat()` digunakan untuk
    mendapatkan informasi terhadap file `/home/cat/Documents/myname.txt`
    (absolute path, sehingga `dirfd` atau `AT_FDCWD` diabaikan) dan menyimpannya
    dalam struct buffer pada alamat `statbuf` (`0x77fc0888ead0`). Return value
    dari invokasi `newfstatat()` ini sama dengan hasil invokasi `stat()`
    sebelumnya yaitu -1 (dengan `errno` bernilai `2` atau `ENOENT`).

4.  `openat(AT_FDCWD, "/home/cat/Documents/myname.txt", O_WR...`

    System call `openat()` digunakan untuk membuka atau bisa juga untuk membuat
    sebuah file. Dalam kasus ini, `openat()` digunakan untuk membuka file
    `/home/cat/Documents/myname.txt` dengan flag: `O_WRONLY` (hanya menulis
    file), `O_CREAT` (buat file baru jika belum ada), dan `O_EXCL` (pastikan ada
    file baru yang dibuat atau kembalikan error `EEXIST` jika file sudah ada).
    File dibuat dengan mode `0644` (`rw-r--r--`). Hasil invokasi `openat()` ini
    adalah file descriptor dari file yang baru dibuat (`6`). Nilainya tidak nol,
    yang berarti sukses (file berhasil dibuka/dibuat).

# Soal 2

Buatlah sebuah program menggunakan system calls dengan bahasa C untuk menerima
masukan dari keyboard dan menyimpan masukan tersebut ke dalam sebuah file
bernama `output.txt`. Tuliskan source code program tersebut dalam file PDF dan
Sertakan tangkapan layar saat program tersebut dikompilasi dan dijalankan, serta
isi dari file `output.txt`.

## Jawaban

### Source Code

``` {.c}
#include <fcntl.h>
#include <unistd.h>

#define BUFSIZE 20

int main(void)
{
        char buf[BUFSIZE];
        int fd;
        int rb;

        fd = creat("output.txt", 0644);
        rd = read(0, buf, BUFSIZE);
        write(fd, buf, rb);
        close(fd);

        return 0;
}
```

<http://ix.io/4b6J>

### Tangkapan layar

![](img/a3-writefile-output.png)
