#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

struct thread_info {
        pthread_t thread_id;    /* thread ID from pthread_create() */
        int thread_num;         /* application thread # */
        pthread_mutex_t* locks; /* array of locks */
        int* locking_order;     /* order to acquire */
        size_t sz;              /* length of locks */
};

static void* start_thread(void* arg);

int main(void)
{
        struct thread_info* tinfo = calloc(3, sizeof(struct thread_info));
        static pthread_mutex_t locks[3];

        /* simulate deadlock by creating circular wait */
        int locking_order[3][3] = { { 0, 1, 2 }, { 1, 2, 0 }, { 2, 1, 0 } };

        /* create all threads */
        for (int i = 0; i < 3; i++) {
                pthread_mutex_init(&locks[i], NULL);
                tinfo[i].locking_order = locking_order[i];
                tinfo[i].thread_num = i;
                tinfo[i].locks = locks;
                tinfo[i].sz = 3;
                pthread_create(&tinfo[i].thread_id, NULL, start_thread, &tinfo[i]);
        }

        /* join all threads */
        for (int i = 0; i < 3; i++)
                pthread_join(tinfo[i].thread_id, NULL);

        /* deadlock expected, this code region will never execute */
}

static void* start_thread(void* arg)
{
        struct thread_info* tinfo = arg;
        int lock_i;

        /* acquiring locks */
        for (int i = 0; i < tinfo->sz; i++) {
                lock_i = tinfo->locking_order[i];
                printf("Thread %d acquiring lock %d\n", tinfo->thread_num, lock_i);
                pthread_mutex_lock(&(tinfo->locks[lock_i]));
                sleep(1);
        }

        /* releasing locks */
        for (int i = 0; i < tinfo->sz; i++) {
                lock_i = tinfo->locking_order[i];
                printf("Thread %d releasing lock %d\n", tinfo->thread_num, lock_i);
                pthread_mutex_unlock(&(tinfo->locks[lock_i]));
        }

        return (void*)NULL; /* doesn't return a value, no dereferencing */
}
