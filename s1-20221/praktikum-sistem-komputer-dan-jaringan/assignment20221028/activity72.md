---
author: 'Faiz Unisa Jazadi -- `475298`'
date: '2022-10-28'
subtitle: Praktikum Sistem Komputer dan Jaringan
title: Activity 7.2
---

# Answers

Modify the second thread function to have the same order of accessing the
resources as the first thread function.

1.  What is the output of the program?

    ![](img/a72-output.png)

2.  What happens?

    Deadlock no longer occurs. Previously, at the start of both functions,
    `function1` acquires `res_a` and `function2` acquires `res_b`. Then,
    `function1` tries to acquire `res_b` and `function2` tries to acquire
    `res_a`. This means that both functions are in circular wait. By making both
    functions access resources in the same order, no deadlock occured.
