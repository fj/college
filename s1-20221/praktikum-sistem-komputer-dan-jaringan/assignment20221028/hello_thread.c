#include <fcntl.h>
#include <pthread.h>
#include <string.h>
#include <unistd.h>

void* write_to_hello(void* arg)
{
        char* s = (char*)arg;
        size_t slen = strlen(s);
        int fd = creat("hello.txt", 0644);
        write(fd, s, slen);
        close(fd);
        return (void*)NULL;
}

int main(void)
{
        pthread_t t;
        char* s = "Hello Thread";
        pthread_create(&t, NULL, write_to_hello, s);
        pthread_join(t, NULL);
        return 0;
}
