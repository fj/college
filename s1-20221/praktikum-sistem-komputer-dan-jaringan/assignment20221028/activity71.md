---
author: 'Faiz Unisa Jazadi -- `475298`'
date: '2022-10-28'
subtitle: Praktikum Sistem Komputer dan Jaringan
title: Activity 7.1
---

# Answers

Comment the line that calls `pthread_join()` in the `simple_thread.c`, compile,
and run the program.

1.  What is the output of the program now?

    ![](img/a71-output.png)

2.  What happens?

    Because the call to `pthread_join()` is commented out, the main thread no
    longer waits until the thread terminates before continuing with its loop.
    Thus, both loops in both threads seems to be running at the same time.
