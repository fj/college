---
author: 'Faiz Unisa Jazadi -- `475298`'
date: '2022-10-29'
geometry: 'right=2cm,left=2cm'
subtitle: Praktikum Sistem Komputer dan Jaringan
title: Assignment 7
---

# Problem 1

Create a program that sends a string "Hello Thread" to a thread that will store
the string into a file titled "hello.txt".

## Program

``` {.c}
#include <fcntl.h>
#include <pthread.h>
#include <string.h>
#include <unistd.h>

void* write_to_hello(void* arg)
{
        char* s = (char*)arg;
        size_t slen = strlen(s);
        int fd = creat("hello.txt", 0644);
        write(fd, s, slen);
        close(fd);
        return (void*)NULL;
}

int main(void)
{
        pthread_t t;
        char* s = "Hello Thread";
        pthread_create(&t, NULL, write_to_hello, s);
        pthread_join(t, NULL);
        return 0;
}
```

## Output

![](img/a7-p1.png)

# Problem 2

Create a program that simulates a deadlock using three threads.

## Program

``` {.c}
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

struct thread_info {
        pthread_t thread_id;    /* thread ID from pthread_create() */
        int thread_num;         /* application thread # */
        pthread_mutex_t* locks; /* array of locks */
        int* locking_order;     /* order to acquire */
        size_t sz;              /* length of locks */
};

static void* start_thread(void* arg);

int main(void)
{
        struct thread_info* tinfo = calloc(3, sizeof(struct thread_info));
        static pthread_mutex_t locks[3];

        /* simulate deadlock by creating circular wait */
        int locking_order[3][3] = { { 0, 1, 2 }, { 1, 2, 0 }, { 2, 1, 0 } };

        /* create all threads */
        for (int i = 0; i < 3; i++) {
                pthread_mutex_init(&locks[i], NULL);
                tinfo[i].locking_order = locking_order[i];
                tinfo[i].thread_num = i;
                tinfo[i].locks = locks;
                tinfo[i].sz = 3;
                pthread_create(&tinfo[i].thread_id, NULL, start_thread, &tinfo[i]);
        }

        /* join all threads */
        for (int i = 0; i < 3; i++)
                pthread_join(tinfo[i].thread_id, NULL);

        /* deadlock expected, this code region will never execute */
}

static void* start_thread(void* arg)
{
        struct thread_info* tinfo = arg;
        int lock_i;

        /* acquiring locks */
        for (int i = 0; i < tinfo->sz; i++) {
                lock_i = tinfo->locking_order[i];
                printf("Thread %d acquiring lock %d\n", tinfo->thread_num, lock_i);
                pthread_mutex_lock(&(tinfo->locks[lock_i]));
                sleep(1);
        }

        /* releasing locks */
        for (int i = 0; i < tinfo->sz; i++) {
                lock_i = tinfo->locking_order[i];
                printf("Thread %d releasing lock %d\n", tinfo->thread_num, lock_i);
                pthread_mutex_unlock(&(tinfo->locks[lock_i]));
        }

        return (void*)NULL; /* doesn't return a value, no dereferencing */
}
```

## Output

![](img/a7-p2.png)
