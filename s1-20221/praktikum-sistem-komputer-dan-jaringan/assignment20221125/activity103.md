---
author: 'Faiz Unisa Jazadi -- `475298`'
date: '2022-11-25'
subtitle: Praktikum Sistem Komputer dan Jaringan
title: Activity 10.3
---

Compile the changes that you made and run the program.

![](img/a103-output.png)

1.  What is the result?

    **Answer:**

    The program seems to run normally although it tries to access values it has
    freed.

2.  Can the pointer still be used to store and print values? Why?

    **Answer:**

    Yes, it can be used to store and print values because it still points to the
    previously allocated memory with initialized values. It works but it is not
    good because accessing a freed memory address may result in an unexpected
    behavior (the pointed memory address may become overwritten at any time).
