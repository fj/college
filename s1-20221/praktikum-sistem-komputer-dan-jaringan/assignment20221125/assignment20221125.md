---
author: 'Faiz Unisa Jazadi -- `21/475298/PA/20563`'
date: '2022-11-24'
subtitle: Praktikum Sistem Komputer dan Jaringan
title: Assignment 10
---

# Soal 1

Buat sebuah program yang terdapat deklarasi variabel dengan tipe di dalamnya.
Tampilkan alamat memori dari variabel tersebut dan temukan di segmen mana
variabel tersebut disimpan dalam virtual address space. Jelaskan mengapa
variabel tersebut disimpan pada segmen tersebut.

## Program

``` {.c}
#include <stdio.h>
#include <unistd.h>

int main(void)
{
        char var;
        var = '\0';
        printf("program pid: %d\n", getpid());
        printf("var address: %p\n", &var);
        pause(); /* wait for a signal before terminating */
        return 0;
}
```

## Jawaban

Berdasarkan output di bawah, ditemukan bahwa variabel `var` yang dibuat berada
di stack. Variabel `var` dimuat di stack karena variabel `var` adalah variabel
lokal pada fungsi `main()`. Secara konvensi, variabel lokal akan dimuat ke stack
sebelum suatu fungsi dijalankan.

![](img/p1-output.png)

# Soal 2

Buat sebuah program yang menerima sebuah nilai string sebagai input dan
menampilkannya ke layar. Nilai string tersebut harus disimpan di dalam sebuah
array karakter yang diaplikasikan menggunakan fungsi `malloc()`. Pertama,
program tersebut menerima masukan berupa panjang dari string yang akan
dimasukkan. Kemudian, program tersebut menerima masukan string yang akan
ditampilkan. Terakhir, program tersebut menampilkan string yang sudah dimasukkan
sebelumnya. Fungsi `scanf()` dan `printf()` dapat digunakan dalam pembuatan
program ini.

## Program

``` {.c}
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(void)
{
        size_t sz;
        char* buf;
        scanf("%zu", &sz);
        buf = malloc((sz + 1) * (sizeof *buf));
        scanf("%s", buf);
        printf("%s\n", buf);
}
```

## Output

![](img/p2-output.png)
