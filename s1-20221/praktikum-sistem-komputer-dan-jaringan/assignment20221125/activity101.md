---
author: 'Faiz Unisa Jazadi -- `475298`'
date: '2022-11-25'
subtitle: Praktikum Sistem Komputer dan Jaringan
title: Activity 10.1
---

Modify the above program by adding the function "sleep(10000)".
You may need to import the unistd.h library. Then compile the program and run it
in the background. Identify the process id of this program and check the value
of `/proc/<pid>/maps` file.

![](img/a101-output.png)

1.  In what part of the memory is the pointer pointed to?

    **Answer:**

    It points to the **heap** part of the memory.

2.  What does it mean in terms of memory allocation?

    **Answer:**

    Being pointed to the heap part of the memory means that a variable is
    dynamically allocated at runtime (e.g. using `malloc()` family of
    functions). In this case, `ptr` points to a dynamically allocated array of 5
    integers.
