---
author: 'Faiz Unisa Jazadi -- `475298`'
date: '2022-11-25'
subtitle: Praktikum Sistem Komputer dan Jaringan
title: Activity 10.2
---

Modify the `realloc.c` by commenting on the `realloc()` function and its
corresponding message printing. Then compile and run the `realloc.c` again.

![](img/a102-output.png)

1.  What is the result?

    **Answer:**

    The program still runs even though it tries to access the array outside of
    its allocated size. The output has some unexpected values (two last
    iteration).

2.  Why does that happen?

    **Answer:**

    In C, an array is just a contagious region in memory. An array is just a
    pointer that points to the address of the first element in the array. Thus,
    one can actually modify an array at an arbitrary index (even more than the
    intended array size). In the program, `malloc()` is used to allocate an
    array of integers. But, the program also tries to modify more memory that it
    allocates (it tries to modify the value `ptr[6..9]`). This may work but will
    may also result in an unexpected behavior.
