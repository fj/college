#include <stdio.h>
#include <unistd.h>

int main(void)
{
        char var;
        var = '\0';
        printf("program pid: %d\n", getpid());
        printf("var address: %p\n", &var);
        pause(); /* wait for a signal before terminating */
        return 0;
}
