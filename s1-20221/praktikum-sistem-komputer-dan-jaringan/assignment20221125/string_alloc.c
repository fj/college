#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(void)
{
        size_t sz;
        char* buf;
        scanf("%zu", &sz);
        buf = malloc((sz + 1) * (sizeof *buf));
        scanf("%s", buf);
        printf("%s\n", buf);
}
