---
author: 'Faiz Unisa Jazadi -- `475298`'
date: '2022-09-09'
subtitle: Praktikum Sistem Komputer dan Jaringan
title: Activity 2.4
---

# Request dan Response

Request 1:

![](img/a24-req1.png)

Response 1:

![](img/a24-resp1.png)

Request 2:

![](img/a24-req2.png)

# Jawaban

12. What is the server's response (status code and phrase) in response to the
    initial HTTP GET message from your browser?

    Server merespon request pertama dengan `401 Unauthorized`.

13. When your browser's sends the HTTP GET message for the second time, what new
    field is included in the HTTP GET message? Note that the username
    (wireshark-students) and password (network) that you entered are

    Header baru yang terdapat pada request kedua adalah `Authorization`.

    `Authorization: Basic d2lyZXNoYXJrLXN0dWRlbnRzOm5ldHdvcms=`

    Header ini berisi username dan password yang dimasukkan di browser dalam
    bentuk base64 encoded.
