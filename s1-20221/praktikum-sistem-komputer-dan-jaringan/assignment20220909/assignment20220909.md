---
author: 'Faiz Unisa Jazadi -- `475298`'
date: '2022-09-09'
subtitle: Praktikum Sistem Komputer dan Jaringan
title: Assignment 2
---

# Soal 1

Perhatikan RAW data (window terbawah Wireshark) untuk paket yang merupakan
response pada problem no. 3 di Activity 2.1. Dari mana didapatkan nilai 128
bytes untuk konten file HTML tersebut? Jelaskan!

## Jawaban

Nilai 128 bytes didapatkan dari header `Content-Length` pada respon. Berdasarkan
[RFC 9110 (section
8.6)](https://www.rfc-editor.org/rfc/rfc9110.html#section-8.6), header
`Content-Length` didefinisikan sebagai:

> The "Content-Length" header field indicates the associated representation's
> data length as a decimal non-negative integer number of octets. When
> transferring a representation as content, Content-Length refers specifically
> to the amount of data enclosed so that it can be used to delimit framing
> (e.g., Section 6.2 of \[HTTP/1.1\]).

Dapat juga dilihat pada gambar berikut bahwa ukuran payload atau body dari
request tersebut adalah sebesar 128 bytes.

![](img/act2-contentlength.png){height="200px"}

# Soal 2

Lakukan Activity 2.2 dengan browser Chrome. Masukkan URL yang sama beberapa kali
(minimal 2 kali) dengan menekan enter pada browser (bukan reload/F5). Chrome
terkenal dengan sifatnya yang resource-intensive, coba analisis kenapa bisa
seperti itu dilihat dari sisi bagaimana chrome mengirim GET. Lakukan juga
pengujian pada browser lainnya seperti Microsoft Edge, perbedaan apa yang bisa
Anda lihat pada browser-browser tersebut?

## Jawaban

Pada soal ini, dilakukan percobaan dengan browser Google Chrome dan Mozilla
Firefox. Pada masing-masing browser (dengan cache clear), dimuat halaman
sebanyak tiga kali (bukan hard refresh/F5). Didapatkan hasil berikut.

Request GET dari browser Mozilla Firefox adalah sebagai berikut.

![](img/act2-ff-pcap.png)

Request GET dari browser Google Chrome adalah sebagai berikut.

![](img/act2-chrome-pcap.png)

Berdasarkan kedua hasil tersebut, dapat dilihat bahwa browser Google Chrome
melakukan request sebanyak tiga kali sedangkan browser Firefox melakukan request
sebanyak satu kali. Temuan ini mengarah pada dugaan bahwa browser Mozilla
Firefox secara default menyimpan cache untuk suatu halaman dan tidak akan
mengirim request lagi jika halaman tersebut dicoba untuk dimuat lagi (kecuali
dengan hard refresh atau menekan tombol F5). Di sisi lain, browser Google Chrome
selalu mengirim request validasi (disertai header `If-Modified-Since`) tiap kali
pengguna memasukkan URL.

Perilaku Google Chrome yang diuraikan di atas dapat dikaitkan dengan asumsi
bahwa browser Google Chrome terkenal sebagai browser yang resource-intensive.
Dengan selalu mengirimkan request tiap kali user memasukkan suatu URL, maka akan
selalu digunakan lebih banyak resource.

# Soal 3

Secara teori, nilai maksimal packet TCP bisa sampai 64kB. Selidiki kenapa paket
TCP dikirim dalam satuan yang jauh lebih kecil (1500an B)?

## Jawaban

Setiap komponen fisik pada jaringan (yang berbasis frame/packet) mempunyai
batasan ukuran transmisi per packet yang disebut Maximum Transmission Unit
(MTU). Pada jaringan internet, nilai atau standar MTU adalah 1500 (menyesuaikan
dengan MTU ethernet). Packet TCP secara teori bisa mengirim maksimal 64 kB.
Namun, pada kenyataannya, packet TCP dikirimkan pada ukuran di bawah nilai MTU
dengan alasan sebagai berikut.

1.  Jika packet TCP melebihi MTU, maka kemungkinan besar akan terjadi
    fragmentasi pada layer IP. Akibatnya, akan ada overhead yang disebabkan oleh
    perlunya proses reassembly pada layer IP untuk paket-paket yang
    difragmentasi.

2.  Jika packet TCP difragmentasi, maka hanya fragmen pertama yang mempunyai
    header TCP sehingga akan menyulitkan NAT dan perangkat lain yang
    memaanfaatkan header tersebut.

3.  Jika packet TCP difragmentasi, jika salah satu fragmen kehilangan link, maka
    keseluruhan packet dianggap akan dikirim dari awal (tidak efisien). Mengirim
    ulang packet dengan ukuran besar tentu akan menambah beban bandwidth pada
    node-node jaringan. Kebanyakan switch juga menggunakan sistem
    "store-and-forward" sehingga paket berukuran besar kemungkinan akan
    mengganggu paket lain yang mungkin lebih penting.

Berdasar pada uraian di atas, penting agar paket TCP dibuat seukuran dengan MTU
agar tidak terjadi fragmentasi dan TCP dapat menjalankan fungsi-fungsinya (mis.
congestion control) dengan baik.

# Soal 4

Lakukan Activity 2.4 secara mandiri di rumah dan upload hasilnya di Activity
2.4. Pelajari metode encoding Base 64 dan tunjukkan hasil encoding (secara
manual) username dan password berikut:

-   Username: `mahasiswa-ugm`
-   Password: `dike`

Tunjukkan step-step encodingnya secara rinci. Lakukan juga proses decoding pada
data yang sudah di-encoding tersebut. Tunjukkan step-step decodingnya secara
rinci.

## Jawaban

Untuk digunakan dalam Basic HTTP, string username dan password pertama harus
digabungkan dengan karakter ":" ([RFC 7617, hal.
5](https://www.rfc-editor.org/rfc/rfc7617)).

Maka, string yang akan di-encode dengan Base 64 adalah `mahasiswa-ugm:dike`.

### Encoding

1.  String tersebut diubah menjadi bentuk sekuens desimal (berdasarkan kode
    ASCII setiap karakter).

        109 97 104 97 115 105 115 119 97 45 117 103 109 58 100 105 107
        101

2.  Sekuens tersebut kemudian diubah ke bentuk binary 8-bit.

        01101101 01100001 01101000 01100001 01110011 01101001 01110011
        01110111 01100001 00101101 01110101 01100111 01101101 00111010
        01100100 01101001 01101011 01100101

3.  Sekuens binary yang tiap elemen mulanya berukuran 8-bit diubah menjadi 6-bit
    (jika elemen terakhir panjangnya tidak 6, ditambahkan padding `00` di sisi
    kanan).

        011011 010110 000101 101000 011000 010111 001101 101001 011100
        110111 011101 100001 001011 010111 010101 100111 011011 010011
        101001 100100 011010 010110 101101 100101

4.  Masing-masing elemen dalam sekuens kemudian diubah menjadi bentuk desimal.

        27 22 5 40 24 23 13 41 28 55 29 33 11 23 21 39 27 19 41 36 26
        22 45 37

5.  Bentuk desimal tersebut adalah indeks dari tabel karakter pada Base 64 ([RFC
    4648, hal. 5](https://www.ietf.org/rfc/rfc4648.txt)). Sekuens sebelumnya
    kemudian diubah menjadi sekuens karakter sesuai dengan tabel karakter
    Base 64. Jika pada tahap 3 ditambahkan padding sebanyak $n$ kali, tambahkan
    karakter padding juga di sisi kanan sebanyak $n$ kali. Sehingga, didapatkan
    hasil akhir encoding sebagai berikut.

        bWFoYXNpc3dhLXVnbTpkaWtl

### Decoding

1.  String Base 64 diubah ke sekuens desimal berdasarkan tabel karakter Base 64.

         27 22 5 40 24 23 13 41 28 55 29 33 11 23 21 39 27 19 41 36 26
         22 45 37

2.  Sekuens tersebut kemudian diubah ke dalam bentuk binary dengan ukuran 6-bit.
    Jika pada string awal terdapat $n$ karakter padding (umumnya "=") di akhir,
    maka hapus padding `00` sebanyak $n$ kali dari sebelah kanan.

        011011 010110 000101 101000 011000 010111 001101 101001 011100
        110111 011101 100001 001011 010111 010101 100111 011011 010011
        101001 100100 011010 010110 101101 100101

3.  Sekuens tersebut ditata ulang sehingga menjadi sekuens binary berukuran
    8-bit.

        01101101 01100001 01101000 01100001 01110011 01101001 01110011
        01110111 01100001 00101101 01110101 01100111 01101101 00111010
        01100100 01101001 01101011 01100101

4.  Sekuens binary 8-bit kemudian diubah menjadi sekuens bilangan desimal

        109 97 104 97 115 105 115 119 97 45 117 103 109 58 100 105 107
        101

5.  Sekuens tersebut kemudian diubah menjadi string berdasarkan tabel karakter
    ASCII. Didapatkan hasil akhir decoding seperti berikut.

        mahasiswa-ugm:dike
