---
author: 'Faiz Unisa Jazadi -- `475298`'
date: '2022-09-09'
subtitle: Praktikum Sistem Komputer dan Jaringan
title: Activity 2.1
---

# Request dan Response

Request 1:

![](img/a22-req.png)

Response 1:

![](img/a22-resp.png)

Request 2:

![](img/a22-req2.png)

Response 2:

![](img/a22-resp2.png)

# Jawaban

4.  Inspect the contents of the first HTTP GET request from your browser to the
    server. Do you see an "IF-MODIFIED-SINCE" line in the HTTP GET?

    Tidak terlihat header `IF-MODIFIED-SINCE` pada request HTTP GET yang dikirim
    browser.

5.  Inspect the contents of the server response. Did the server explicitly
    return the contents of the file? How can you tell?

    Benar, server mengembalikan konten secara langsung. Hal ini ditunjukkan
    melalui adanya header `Content-Length` yang bernilai tidak nol.

6.  Now inspect the contents of the second HTTP GET request from your browser to
    the server. Do you see an "IF-MODIFIED-SINCE:" line in the HTTP GET? If so,
    what information follows the "IF-MODIFIED-SINCE:" header?

    Terdapat header `IF-MODIFIED-SINCE`. Header tersebut diikuti nilai kapan
    terakhir kali halaman tersebut diunduh oleh browser yaitu
    `Fri, 09 Sep 2022 05:59:01 GMT`.

7.  What is the HTTP status code and phrase returned from the server in response
    to this second HTTP GET? Did the server explicitly return the contents of
    the file? Explain.

    HTTP status code yang di-return pada response kedua adalah
    `304 Not Modified`. Pada respon kedua, tidak diberikan konten halaman secara
    langsung/eksplisit karena file tidak dimodifikasi pada server dan konten
    file terakhir yang ada pada browser masih sama (up-to-date).
