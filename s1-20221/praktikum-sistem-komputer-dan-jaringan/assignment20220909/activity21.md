---
author: 'Faiz Unisa Jazadi -- `475298`'
date: '2022-09-09'
subtitle: Praktikum Sistem Komputer dan Jaringan
title: Activity 2.1
---

# Request dan Response

Request:

![](img/a21-req.png)

Response:

![](img/a21-resp.png)

# Jawaban

Soal 1. What languages (if any) does your browser indicate that it can accept to
the server?

    Bahasa yang diterima oleh browser adalah Bahasa Inggris Amerika Serikat
    (en\_US).

2.  When was the HTML file that you are retrieving last modified at the server?

    File HTML yang diterima terakhir dimodifikasi pada 09 Sep 2022 05:59:01

        Last-Modified: Fri, 09 Sep 2022 05:59:01 GMT

3.  How many bytes of content are being returned to your browser?

    Konten yang diterima oleh browser adalah sebesar 128 bytes.

        Content-Length: 128
