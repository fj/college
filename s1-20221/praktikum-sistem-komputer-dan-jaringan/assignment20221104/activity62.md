---
author: 'Faiz Unisa Jazadi -- `475298`'
date: '2022-11-04'
subtitle: Praktikum Sistem Komputer dan Jaringan
title: Activity 6.2
geometry: margin=1.5cm
---

# Answers

6.  Consider the TCP segment containing the HTTP "POST" as the first segment in
    the data transfer part of the TCP connection.

    -   At what time was the first segment (the one containing the HTTP POST) in
        the data-transfer part of the TCP connection sent?

        **Answer:**

        The segment arrives at Feb 3, 2021 09:43:26.716922000 WIB
        (relative/since first frame: 0.024047000)

        ![](img/a62-first-segment-data.png)

    -   At what time was the ACK for this first data-containing segment
        received?

        **Answer:**

        The segment arrives at Feb 3, 2021 09:43:26.745551000 WIB
        (relative/since first frame: 0.052676000)

        ![](img/a62-ack-first-segment-data.png)

    -   What is the RTT for this first data-containing segment?

        **Answer:**

        $$RTT = 0.052671000 - 0.024047000 = 0.028624$$

    -   What is the RTT value the second data-carrying TCP segment and its ACK?

        **Answer:**

        Second data-carrying TCP segment relative time is 0.024047000 and the
        time of its ACK is 0.052676000.

        $$RTT = 0.052676000 - 0.024047000 = 0.028628$$

    -   What is the EstimatedRTT value after the ACK for the second data-
        carrying segment is received? Assume that in making this calculation
        after the received of the ACK for the second segment, that the initial
        value of EstimatedRTT is equal to the measured RTT for the first
        segment, and then is computed using the EstimatedRTT equation (page 236
        of the attached PDF file in elok), and a value of $\alpha = 0.125$.

        **Answer:**

        ```{=tex}
        \begin{align*}
            \text{EstimatedRTT} &= (1 - \alpha) * \text{EstimatedRTT} + \alpha *
        \text{SampleRTT} \\
                                &= (1 - 0.125) * 0.028624 + 0.125 * 0.028628 \\
                                &= 0.0286245
        \end{align*}
        ```

7.  What is the length (header plus payload) of each of the first four
    data-carrying TCP segments?

    **Answer:**

    Each of the four segments have segment length of 1448 and header length of
    $32$. Hence, the total length is 1480.

    ![](img/a62-p7.png)

8.  Are there any retransmitted segments in the trace file? What did you check
    for (in the trace) in order to answer this question?

    **Answer:**

    There are no retransmitted segments. We know this by checking the sequence
    numbers in the trace file. We can also know this by filtering the trace in
    Wireshark using `tcp.analysis.retransmission`.

    ![](img/a62-p8.png)
