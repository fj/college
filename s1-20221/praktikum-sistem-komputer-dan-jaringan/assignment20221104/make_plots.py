"""
This program is used to create plots for solving the 6th assignment.

The trace file is made using the following tshark command.

tshark \
        -r tcp-wireshark-trace1-1.pcapng \
        -Y "ip.dst==192.168.86.68 and tcp.analysis.ack_rtt" \
        -e tcp.analysis.ack_rtt \
        -T fields > trace_file.txt
"""

import sys

from matplotlib import pyplot as plt


def calc_est_rtt(alpha, est_rtt, sample_rtt):
    return (1 - alpha) * est_rtt + alpha * sample_rtt


def calc_timeout_interval(dev_rtt, est_rtt):
    return 4 * dev_rtt + est_rtt


def calc_dev_rtt(beta, dev_rtt, sample_rtt, est_rtt):
    return (1 - beta) * dev_rtt + beta * abs(sample_rtt - est_rtt)


def invcalc_timeout_interval(timeout_interval, est_rtt):
    return (timeout_interval - est_rtt) / 4


def solve_problem_1(rtt_data):
    est_rtt_data = rtt_data[:1]
    for i in range(1, len(rtt_data)):
        est_rtt_data.append(calc_est_rtt(0.125,
                                         est_rtt_data[i - 1],
                                         rtt_data[i]))
    _, ax = plt.subplots()
    ax.plot(rtt_data, label='RTT')
    ax.plot(est_rtt_data, label='EstimatedRTT')
    ax.legend()
    plt.tight_layout()
    plt.show()


def solve_problem_2(rtt_data):
    timeout_data = [1, 1]
    est_rtt_data = rtt_data[:1]
    dev_rtt_data = []

    # calculate est_rtt
    for i in range(1, len(rtt_data)):
        est_rtt_data.append(calc_est_rtt(0.125,
                                         est_rtt_data[i - 1],
                                         rtt_data[i]))

    # calculate initial dev rtt
    for i, timeout in enumerate(timeout_data):
        dev_rtt_data.append(invcalc_timeout_interval(timeout, est_rtt_data[i]))

    # calculate timeout_interval
    for i in range(2, len(rtt_data)):
        dev_rtt_data.append(calc_dev_rtt(0.25,
                                         dev_rtt_data[i - 1],
                                         rtt_data[i],
                                         est_rtt_data[i]))
        timeout_data.append(calc_timeout_interval(dev_rtt_data[i],
                                                  est_rtt_data[i]))

    _, ax = plt.subplots()
    ax.plot(rtt_data, label="RTT")
    ax.plot(est_rtt_data, label="EstimatedRTT")
    ax.plot(timeout_data, label="TimeoutInterval")
    ax.legend()
    plt.tight_layout()
    plt.show()


def main(trace_filename):
    with open(trace_filename) as tf:
        lines = tf.readlines()[1:]  # skip handshake
    lines = [float(line) for line in lines]

    solve_problem_1(lines)
    solve_problem_2(lines)


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print('usage: {sys.argv[0]} <trace_file.txt>')
        sys.exit(8)
    main(sys.argv[1])
