---
author: 'Faiz Unisa Jazadi -- `475298`'
date: '2022-11-04'
geometry: margin=1.5cm
subtitle: Praktikum Sistem Komputer dan Jaringan
title: Assignment 6
---

# Answers

1.  ~~Kerjakan Activity 6.2. Submit langsung di Activity 6.2~~

2.  Baca cara menghitung Estimated RTT dari materi suplemen yang dishare di elok
    dan tonton video youtubenya. Hitung Estimated RTT hingga segmen TCP terakhir
    yang mengandung data pada tracefile yang dishare di elok. Sama seperti
    Activity 6.2, segmen TCP pertama adalah segmen pertama yang memuat data
    (abaikan segmen-segmen yang terlibat dalam three-way handshake). Plot grafik
    yang menunjukkan hubungan Sample RTT dan Estimated RTT seperti di Figure
    3.32 pada materi suplemen. Gunakan nilai $\alpha=0.125$.

    **Answer:**

    ![](img/a6-rtt-vs-estrtt.svg)

3.  Baca cara menghitung timeout pada materi suplemen dan plot nilai timeoutnya
    pada grafik yang sama dengan grafik dari nomor 2. Analisis grafik yang Anda
    dapatkan dan lihat apakah ada segmen yang terkena timeout berdasarkan
    perhitungan timeout tersebut? Jika ada, segmen/packet nomor berapa saja?
    Asumsikan nilai timeout untuk segmen pertama dan kedua adalah 1s (1.000ms).
    Setelah ACK untuk segmen kedua diterima, nilai timeout dihitung berdasarkan
    rumus pada halaman 237 materi suplemen dan diset sebagai timeout untuk
    segmen ketiga dst. Gunakan nilai $\beta=0.25$.

    **Answer:**

    ![](img/a6-timeout-interval.svg)

    Setelah melakukan inspeksi terhadap grafik, tidak ditemukan adanya paket
    yang terkena timeout karena tidak ditemukan adanya titik potong antara RTT
    dan Timeout Interval.

# Metode Analisa

Analisis dilakukan dengan bantuan script Python dan tool `tshark` (berkas script
Python dilampirkan terpisah).
