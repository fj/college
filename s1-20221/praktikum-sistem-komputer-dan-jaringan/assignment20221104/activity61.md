---
author: 'Faiz Unisa Jazadi -- `475298`'
date: '2022-11-04'
subtitle: Praktikum Sistem Komputer dan Jaringan
title: Activity 6.1
---

# Answers

1.  What is the IP address and TCP port number used by the client computer
    (source) that is transferring the alice.txt file to gaia.cs.umass.edu? To
    answer this question, it's probably easiest to select an HTTP message and
    explore the details of the TCP packet used to carry this HTTP message, using
    the "details of the selected packet header window."

    **Answer:**

    The source IP address is `10.6.142.25` and the source port is 55430.

    ![](img/a61-p1.png)

2.  What is the IP address of gaia.cs.umass.edu? On what port number is it
    sending and receiving TCP segments for this connection?

    **Answer:**

    The destination IP address is `128.119.245.12` and the destination port
    is 80.

3.  What is the sequence number of the TCP SYN segment that is used to initiate
    the TCP connection between the client computer and gaia.cs.umass.edu? (Note:
    this is the "raw" sequence number carried in the TCP segment itself; it is
    NOT the packet \# in the "No." column in the Wireshark window. Remember
    there is no such thing as a "packet number" in TCP or UDP; as you know,
    there are sequence numbers in TCP and that's what we're after here. Also
    note that this is not the relative sequence number with respect to the
    starting sequence number of this TCP session.). What is it in this TCP
    segment that identifies the segment as a SYN segment?

    **Answer:**

    The sequence number of the TCP SYN segment is 3308667591. In the TCP
    segment, the SYN flag set is what identifies it as a TCP SYN segment.

    ![](img/a61-tcp-syn-flag.png)

4.  What is the sequence number of the SYNACK segment sent by gaia.cs.umass.edu
    to the client computer in reply to the SYN? What is it in the segment that
    identifies the segment as a SYNACK segment? What is the value of the
    Acknowledgement field in the SYNACK segment? How did gaia.cs.umass.edu
    determine that value?

    **Answer:**

    The sequence number of the SYNACK segment is 171115360. It is a SYNACK
    segment because the SYN and ACK flag is set. The value of the
    Acknowledgement field in the SYNACK field is 3308667592. The value is
    determined from the last ACK'd segment (segment number = 3308667591)
    incremented by one (it expects the next segment).

    ![](img/a61-synack-segment.png)

5.  What is the sequence number of the TCP segment containing the header of the
    HTTP POST command? Note that in order to find the POST message header,
    you'll need to dig into the packet content field at the bottom of the
    Wireshark window, looking for a segment with the ASCII text "POST" within
    its DATA field 1,2. How many bytes of data are contained in the payload
    (data) field of this TCP segment? Did all of the data in the transferred
    file alice.txt fit into this single segment?

    **Answer:**

    The sequence number of the TCP segment is 3308667592. The TCP payload size
    is 717 bytes. Not all of the data in the transferred file `alice.txt` fit into
    the single segment because the file itself is larger than the payload size
    (it is splitted into multiple segments). There is another segment that
    contains another bytes of the `alice.txt` file.

    ![](img/a61-first-payload-segment.png)
