---
author: 'Faiz Unisa Jazadi -- `475298`'
date: '2022-11-18'
geometry: margin=2cm
subtitle: Praktikum Sistem Komputer dan Jaringan
title: Activity 9.2
---

Compile and run the code list above. Screenshot the result and answer the
following questions.

![](img/a92-output.png)

a.  What is the final value of the shared variable?

    **Answer:**

    The final value is (always) `1`.

b.  Which thread obtains the lock first?

    **Answer:**

    In my case, the second thread obtains the lock first (but it can't be
    determined for other cases).

c.  If the other thread first obtains the lock, what will be the final value of
    the shared variable?

    **Answer:**

    The final value will always be $1$ regardless of the execution order of both
    threads. Initial value of the `shared` variable is $1$ and both threads are
    trying to increment and decrement the variable by $1$. Hence, it does not
    matter which one goes first because $1 - 1 + 1$ will always be $1$.
