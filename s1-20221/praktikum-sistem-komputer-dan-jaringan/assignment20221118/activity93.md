---
author: 'Faiz Unisa Jazadi -- `475298`'
date: '2022-11-18'
geometry: margin=2cm
subtitle: Praktikum Sistem Komputer dan Jaringan
title: Activity 9.3
---

Compile and run the code list above. Screenshot the result and answer the
following questions.

![](img/a93-output.png)

a.  What is the final value of the shared variable?

    **Answer:**

    The final value of the shared variable is (always) 1.

b.  From the final value that you obtained, which thread first obtains access to
    the shared variable?

    **Answer:**

    The first thread is the first thread that obtains access to the shared
    variable.
