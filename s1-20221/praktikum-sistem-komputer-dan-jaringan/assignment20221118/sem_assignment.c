#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <unistd.h>

void* increment();
void* decrement();

int varA = 1;
int varB = 2;
sem_t semaphore;

int main()
{
        sem_init(&semaphore, 0, 1);
        pthread_t threadA, threadB;
        printf("[main thread] initial: a=%d, b=%d\n", varA, varB);
        pthread_create(&threadA, NULL, increment, NULL);
        pthread_create(&threadB, NULL, decrement, NULL);
        pthread_join(threadA, NULL);
        pthread_join(threadB, NULL);
        printf("[main thread] final: a=%d, b=%d\n", varA, varB);
}

void* increment()
{
        printf("[thread-1] decrementing semaphore\n");
        sem_wait(&semaphore);
        printf("[thread-1] semaphore decremented, entering critical section\n");
        printf("[thread-1] before: a=%d, b=%d\n", varA, varB);
        varA += 1;
        varB += 1;
        printf("[thread-1] after: a=%d, b=%d\n", varA, varB);
        sem_post(&semaphore);
        printf("[thread-1] semaphore incremented, exiting critical section\n");
        return (void*)NULL;
}

void* decrement()
{
        printf("[thread-2] decrementing semaphore\n");
        sem_wait(&semaphore);
        printf("[thread-2] semaphore decremented, entering critical section\n");
        printf("[thread-2] before: a=%d, b=%d\n", varA, varB);
        varA -= 1;
        varB -= 1;
        printf("[thread-2] after: a=%d, b=%d\n", varA, varB);
        sem_post(&semaphore);
        printf("[thread-2] semaphore incremented, exiting critical section\n");
        return (void*)NULL;
}
