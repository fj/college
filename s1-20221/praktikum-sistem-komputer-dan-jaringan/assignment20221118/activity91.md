---
author: 'Faiz Unisa Jazadi -- `475298`'
date: '2022-11-18'
geometry: margin=2cm
subtitle: Praktikum Sistem Komputer dan Jaringan
title: Activity 9.1
---

Compile and run the source code listing above and compare your result with your
friends.

Screenshot your result and answer the following questions.

![](img/a91-output.png)

a.  Is the final result of the shared variable similar to your friends?

    **Answer:** No, the final results are different.

b.  Why does this happen?

    **Answer:**

    It happens because there is a race condition that happens between both
    threads. Both threads are trying to modify the `shared` variable and the
    final result depends on which thread updates the `shared` variable last.

c.  Which part of the source code list above is the critical section?

    **Answer:**

    The critical section is a section in the code that tries to modify a shared
    resource. In this case, both the `increment()` and `decrement()` functions
    are the critical sections.
