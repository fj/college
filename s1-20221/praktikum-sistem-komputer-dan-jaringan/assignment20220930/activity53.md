---
author: 'Faiz Unisa Jazadi -- `475298`'
date: '2022-09-30'
subtitle: Praktikum Sistem Komputer dan Jaringan
title: Activity 5.3
---

# Answers

![](img/a5311.png)

11. What is the destination port for the DNS query message? What is the source
    port of the DNS response message?

    The destination port for the DNS query message is 53. The source port of the
    DNS response message is 53.

12. To what IP address is the DNS query message sent? Is this the IP address of
    your default local DNS server?

    The DNS query message was sent to `10.13.10.13` which matches my default
    local DNS server.

    ![](img/a5312.png)

13. Examine the DNS query message. What "Type" of DNS query is it? Does the
    query message contain any "answers"?

    ![](img/a5313.png)

    The DNS query is of `A` type and does not contain any answers.

14. Examine the DNS response message to the query message. How many "questions"
    does this DNS response message contain? How many "answers"?

    ![](img/a5314.png)

    The response message contains 1 question and 1 answer.

Last, let's use `nslookup` to issue a command that will return a type NS DNS
record, Enter the following command: `nslookup --type=NS umass.edu` and then
answer the following questions:

![](img/a5315.png)

15. To what IP address is the DNS query message sent? Is this the IP address of
    your default local DNS server?

    The DNS query message was sent to `10.13.10.13` which matches my default
    local DNS server.

16. Examine the DNS query message. How many questions does the query have? Does
    the query message contain any "answers"?

    It contains 1 question and 0 answer.

17. Examine the DNS response message. How many answers does the response have?
    What information is contained in the answers? How many additional resource
    records are returned? What additional information is included in these
    additional resource records?

    The response message has answers that contains 3 RRs with `type=NS`. The
    response also returned 3 additional resource records with `type=A` (IP
    addresses of the NS records provided in the answer).
