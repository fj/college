---
author: 'Faiz Unisa Jazadi -- `475298`'
date: '2022-09-30'
subtitle: Praktikum Sistem Komputer dan Jaringan
title: Activity 5.1 dan 5.2
---

# Activity 1

1.  Run nslookup to obtain the IP address of the web server for the Indian
    Institute of Technology in Bombay, India: www.iitb.ac.in. What is the IP
    address of www.iitb.ac.in

    ![](img/a511.png)

2.  What is the IP address of the DNS server that provided the answer to your
    nslookup command in question 1 above?

    `10.13.10.13`

3.  Did the answer to your nslookup command in question 1 above come from an
    authoritative or non-authoritative server?

    Non-authoritative server.

4.  Use the nslookup command to determine the name of the authoritative name
    server for the iitb.ac.in domain. What is that name? (If there are more than
    one authoritative servers, what is the name of the first authoritative
    server returned by nslookup)? If you had to find the IP address of that
    authoritative name server, how would you do so?

    ![](img/a514.png)

    The first authoritative server returned by `nslookup` is "dns1.iitb.ac.in".
    I will use the "-type=A" to find the IP address of the authoritative name
    server.

# Activity 2

5.  Locate the first DNS query message resolving the name writing.engr.psu.edu.
    What is the packet number in the trace for the DNS query message? Is this
    query message sent over UDP or TCP?

    ![](img/a525.png)

    The packet number is 4181 and the packet was sent over UDP.

6.  Now locate the corresponding DNS response to the initial DNS query. What is
    the packet number in the trace for the DNS response message? Is this
    response message received via UDP or TCP?

    ![](img/a526.png)

    The packet number is 4182 and the packet was received over UDP.

7.  What is the destination port for the DNS query message? What is the source
    port of the DNS response message?

    The destination port of the DNS query message is 53 and the source port
    is 56165.

8.  To what IP address is the DNS query message sent?

    The DNS query message is sent to `10.13.10.13`.

9.  Examine the DNS query message. How many "questions" does this DNS message
    contain? How many "answers" answers does it contain?

    It contains 1 question and 0 answer.

10. Examine the DNS response message to the initial query message. How many
    "questions" does this DNS message contain? How many "answers" answers does
    it contain?

    It contains 1 question and 2 answers.
