---
author: 'Faiz Unisa Jazadi -- `475298`'
date: '2022-09-30'
subtitle: Praktikum Sistem Komputer dan Jaringan
title: Assignment 5
---

# Soal

Cari bagaimana cara mengoperasikan `nslookup` pada `cmd` Windows (atau OS yang
digunakan PC Anda). Lalu lakukan iterative query secara manual untuk me-resolve
dcse.fmipa.ugm.ac.id sehingga bisa diketahui IP addressnya tanpa mengontak local
DNS server.

Pada iterative query ini, Anda harus memulai dari menanyakan IP address dari
domain "id" (tanpa tanda kutip) kepada root server. Berdasarkan jawaban dari
root server, tanyakan IP address dari domain "ac.id" ke server id dan seterusnya
ke bawah hingga Anda menemukan IP address dari dcse.fmipa.ugm.ac.id.

a.  Tunjukkan step-by-step command apa saja yang Anda masukkan beserta outputnya
    (screenshot)!

b.  Total ada berapa name server yang Anda kunjungi? Tunjukkan pula IP address
    dari server-server tersebut.

# Jawaban

## Step-by-step

1.  Mengakses list dari root DNS server

    Command: `nslookup -type=NS .`

    ![](img/a5-root-dns.png)

2.  Mencari "id." dari salah satu root NS

    Command: `nslookup -type=NS id. m.root-servers.net`

    ![](img/a5-id-query.png)

3.  Mencari "ac.id." dari dari `ns4.apnic.net`

    Command: `nslookup -type=NS ac.id. ns4.apnic.net`

    ![](img/a5-ac-id-query.png)

4.  Mencari "ugm.ac.id." dari `d.dns.id`

    Command: `nslookup -type=NS ugm.ac.id. d.dns.id`

    ![](img/a5-ugm-ac-id.png)

5.  Mencari "fmipa.ugm.ac.id" dari `ns1.ugm.ac.id`

    Command: `nslookup -type=NS fmipa.ugm.ac.id ns1.ugm.ac.id`

    ![](img/a5-fmipa-ugm-query.png)

6.  Mencari "dcse.fmipa.ugm.ac.id" dari `ns1.ugm.ac.id`

    Command: `nslookup -type=NS dcse.fmipa.ugm.ac.id ns1.ugm.ac.id`

    ![](img/a5-dcse-fmipa-ugm-query.png)

## DNS Server yang Dikunjungi

1.  Server DNS lokal: `10.13.10.13`
2.  `m.root-servers.net`: `202.12.27.33`
3.  `ns4.apnic.net`: `202.12.31.53`
4.  `d.dns.id`: `45.126.57.57`
5.  `ns1.ugm.ac.id`: `202.43.92.2`
