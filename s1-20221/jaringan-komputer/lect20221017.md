---
author: Faiz Jazadi
date: '2022-10-17'
numbersections: true
title: Routing in Switched Networks
---

# Prologue

Jika dilihat dari peta jaringan, ada banyak jalur. Routing adalah pemilihan
jalur yang terbaik. Routing memiliki karakteristik:

-   correctness
-   simplicity
-   robustness
-   fairness
-   optimality
-   efficiency

# Performance Criteria

-   Untuk seleksi route
-   Biasanya minimum hop atau "least cost"

# Decision Time and Place

-   Time
    -   Packet or virtual circuit basis
    -   Fixed or dynamically changing
-   Place
    -   Distributed - made by each node
    -   Centralized
    -   Source

# Network Information Source and Update Timing

-   Keputusan routing umumnya dibuat berdasarkan keadaan jaringan (namun tidak
    selalu)
    -   distributed routing
    -   central routing
-   Issues of update timing
    -   when is network info held by nodes updated
    -   fixed - never updated
    -   adaptive - regularly updated

# Routing Strategies - Fixed Routing

-   Menggunakan satu rute permanen dari source ke destination.
-   Ditentukan dengan *least cost algorithm*
-   Route is fixed
    -   Paling tidak hingga topologi berubah
    -   Tidak bisa merespon terhadap perubahan traffic
-   Sederhana, tapi tidak fleksibel

# Routing Strategies - Flooding

-   Packet dikirim ke node ke semua tetangga
-   Eventually multiple copies arrive at destination
-   Tidak perlu informasi tentang jaringan
-   Setiap packet di diberi nomor unik sehingga duplikat bisa di-discard
-   Butuh cara untuk membatasi incessant retransmission:
    -   Node-node bisa mengingat packet yang sudah pernah diteruskan (agar load
        bisa terkontrol)
    -   atau masukkan hop count pada packet.

## Properties of Flooding

-   Mencoba semua rute yang memungkinkan
-   Setidaknya satu paket akan melalui rute dengan minimum hop count
-   Semua node dikunjungi
-   Kekurangan: traffic load yang tinggi

# Routing Strategies - Random Routing

-   Mendapatkan kesederhanaan strategi flooding tapi dengan load yang lebih
    sedikit
-   Menggunakan probabilitas

# Routing Strategies - Adaptive Routing

-   Digunakan oleh hampir semua packet switching networks
-   Keputusan routing berubah seiring dengan perubahan kondisi jaringan (baik
    karena macet atau galat)
-   Memerlukan informasi terkait jaringan
-   Kekurangan:
    -   Keputusan yang dibuat lebih kompleks
    -   Trade-off antara kualitas informasi jaringan dan overhead
    -   Bereaksi terlalu cepat menyebabkan oscillation
    -   Bereaksi terlalu lambat menyebabkan informasi menjadi tidak relevan
