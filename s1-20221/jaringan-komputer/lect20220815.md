---
author: Faiz Unisa Jazadi
date: '2022-08-15'
numbersections: true
title: Introduction to Computer Network
---

# Internet

Jaringan raksasa, menghubungkan banyak device dan banyak jaringan.

Didefinisikan berdasarkan service yang disediakan,

-   Infrastruktur yang menyediakan berbagai layanan pengiriman data (mis. web,
    streaming video, dll.)

-   Menyediakan programming interface untuk distributed applications

# Network edge

-   Host = end system, aplikasi (network app) berjalan pada end system (edge
    dari jaringan internet)

-   Edge devices: laptop, desktop, etc.

# Network core

-   Menghubungkan antar router (interconnected routers).

-   Network of networks

## Packet switches

Packet switches berfungsi meneruskan paket. Dua device umum yang menjalankan
fungsi packet switching:

-   Routers
-   Switches

Packet adalah data. Packet switch adalah yang meneruskan packet.

## Communication links

Fiber optik, kabel tembaga, satelit. Intinya, link yang menghubungkan
perangkat-perangkatnya.

# Network defined

Kumpulan device, router, link, yang di-manage oleh suatu organisasi.

# Protokol

Mengatur cara mengirim/menerima data. Mis. protokol HTTP, TCP, IP, dll.

Protokol adalah aturan menerima/mengirim data yang **disepakati**. Apa yang
dilakukan untuk mengirim atau ketika menerima?

Jaringan/network di-govern oleh protokol.

**tl;dr** Protokol mendefinisikan format, urutan pengiriman dan penerimaan, dan
aksi yang dijalankan ketika message dikirim/diterima.

## Standardisasi Protokol

Lembaga yang mengatur:

-   IETF: Internet Engineering Task Force

-   RFC: Request for Comments

# Network edge (cont..)

-   Host: client dan server, server umumnya berada di data center

-   Access network adalah jaringan yang menghubungkan edge ke router terdekat

-   Access network terdiri dari media fisik (switch/router) dan communication
    link (ethernet/wireless/coax/etc.)

-   Host ke edge router dihubungkan umumnya dengan tiga jenis access network:
    residensial, institusional (kantor/sekolah), mobile (wifi, 4g). Bedanya
    adalah pada transfer rate (mobile $<$ residensial $<$ institusional).

## Access network berbasis kabel

        ISP --kabel--> modem -> splitter -> host

Kabel dipecah-mecah menjadi beberapa frekuensi berbeda (frequency-division
multiplexing).

Kabel sifatnya asymmetric, transfer rate upstream dan downstream berbeda.

Contoh: HFC (Hybrid fiber coax, bagian fiber untuk download, bagian coax untuk
upload).

## Access network berbasis DSL (digital subscriber line)

DSL menggunakan kabel telepon yang sudah ada. Karena kabel telepon dedicated
untuk satu orang, maka cenderung tidak di-share.

DSL terhubung langsung (terintegrasi langsung) ke central office. Karena itu,
semakin jauh ke central office $\rightarrow$ semakin lambat.

## Access network: home network

        End devices -> router (wireless/ethernet) -> modem -> ISP

Beberapa end devices juga umumnya dihubungkan melalui ethernet. Misalnya Gigabit
ethernet yang memiliki bandwidth 1 Gbps (meski bandwidth besar, throughput
seringkali dibatasi oleh ISP).

## Wireless access network (WLAN)

-   WLAN menggunakan base station/access point
-   Distandardisasi IEEE (protokol: 802.11 b/gn/ac)
-   Biasanya digunakan dalam satu gedung (range terbatas 10-100 meter)
-   Transmission rate: 11, 54, 450 Mbps

## Wide area cellular access network

Jangkauan lebih luas. Disediakan oleh operator. Misalnya 4G, dll.

## Access network: enterprise network

-   Digunakan institusi, sekolah, perusahaan

-   Mirip home network, hanya saja devicesnya lebih banyak (banyak end device,
    banyak intermediary device seperti router/switch)

## Access network: data center network

Mengunakan high-bandwidth link (10-100 Gbps) yang menghubungkan ratusan hingga
ribuan server ke internet.

# Host dan Packets

Host mengirim data dalam bentuk packet.

-   Host menerima data dari aplikasi

-   Data dibagi-bagi menjadi bagian-bagian yang kecil (paket) dengan fixed
    length sesuai protokol.

-   Paket diteruskan ke router/switch.

## Terms

-   Link capacity: link transmission rate

-   Delay: waktu transmit paket dari satu tujuan ke tujuan yang lain

# Link: media fisik

-   Bit: merambat di antara pasangan transmitter & receiver

-   Link fisik: media perambatan yang menghubungkan secara fisik

-   Guided media: ethernet, fiber karena terarah secara fisik

-   Unguided media: bebas seperti cellular, radio, wifi

## Twisted pair (TP)

Dua buah kabel tembaga yang diisolasi secara berpasangan.

-   Cat 5: 100 Mbps

-   Cat 6: 1 Gbps

## Kabel coaxial

-   Dua konektor tembaga konsentris

-   Bidirectional

-   Broadband

## Fiber optic

-   Membaca light pulse

-   High speed

-   Lebih resistan terhadap EMI (*electromagnetic interference*) dan
    (*radio-frequency interference*)

## Wireless Radio

-   Sinyal dibawa dalam bentuk gelombang elektromagnetik

-   Tidak menggunakan kabel

-   Dipengaruhi environment: pantulan, cuaca, radiasi

# Network core (cont.)

Terdiri dari router yang saling berhubungan, sudah tidak ada end devices.

## Switch

-   Packet-switching: host memecah data dari aplikasi menjadi packet-packet

-   Network meneruskan packets dari router ke router selanjutnya melewati link
    ke destination

-   Fungsi utama: forwarding dan routing. Router mempunyai routing/forwarding
    table untuk menghubungkan jaringan lokal dengan jaringan luar.

-   Local action: forward ke link yang sudah ada/sesuai (di jaringan yang sama)

-   Global action: menentukan path yang akan dilewati dari source ke destination
    (dengan routing algorithm)

-   Karena setiap routing mempunyai forwarding table, maka dari satu router akan
    terus terhubung ke router lainnya hingga mencapai destinasi akhir (end
    device).

-   Packet switching: tunggu semua packet sampai dulu baru diforward ke router
    selanjutnya (next hop)

-   Ketika melakukan packet switching, dimanfaatkan queue karena router tidak
    bisa menghandle semuanya sekaligus (memorinya terbatas). Ini bisa
    mengakibatkan penurunan throughput.

-   Jika memori pada router full dan buffer juga full, maka tidak ada opsi
    selain men-drop paket.

# Packet transmission protocol

-   UDP: tidak mementingkan reliabilitas data, hal terpenting adalah real-time

-   TCP: memastikan reliabilitas data, data harus diterima utuh
