---
author: 'Faiz Unisa Jazadi -- `21/475298/PA/20563`'
date: '2022-09-30'
numbersections: true
subtitle: Tugas Jaringan Komputer
title: Calculating UDP Checksum
---

# Soal

Tonton video youtube berikut dan hitung nilai UDP checksum untuk kasus yang
ditunjukkan pada menit:detik ke 6:45 pada video tersebut. Namun, ganti konten
data menjadi "Halo" (tanpa tanda petik). Asumsikan data diencode menggunakan
ASCII.

<https://www.youtube.com/watch?v=rYVHBICiiEc>

# Jawaban

## Perhitungan

Berikut adalah perhitungan ke biner untuk header-header UDP.

1.  Source IP

        192      168      0        31
        11000000 10101000 00000000 00011111

    Hasil penjumlahan dua chunk 16-bit dari source IP:

        11000000 10101000
        00000000 00011111
        ----------------- +
        11000000 11000111

2.  Destination IP

        192      168      0        30
        11000000 10101000 00000000 00011110

    \pagebreak
    Hasil penjumlahan dengan jumlahan sebelumnya:

        11000000 11000111    jumlahan sebelumnya
        11000000 10101000    chunk 16-bit pertama
        ----------------- +
        10000001 01101111
        00000000 00000001    carry wrap-around
        ----------------- +
        10000001 01110000
        00000000 00011110    chunk 16-bit kedua
        ----------------- +
        10000001 10001110

3.  All-zeros and 8-bit protocol

        0        17
        00000000 00010001

    Hasil penjumlahan dengan jumlahan sebelumnya:

        10000001 10001110
        00000000 00010001
        ----------------- +
        10000001 10011111

4.  UDP length

        0        12
        00000000 00001100

    Hasil penjumlahan dengan jumlahan sebelumnya:

        10000001 10011111
        00000000 00001100
        ----------------- +
        10000001 10101011

5.  Source port

        0        20
        00000000 00010100

    Hasil penjumlahan dengan jumlahan sebelumnya:

        10000001 10101011
        00000000 00010100
        ----------------- +
        10000001 10111111

6.  Destination port

        0        10
        00000000 00001010

    Hasil penjumlahan dengan jumlahan sebelumnya:

        10000001 10111111
        00000000 00001010
        ----------------- +
        10000001 11001001

7.  UDP length

        0        12
        00000000 00001100

    Hasil penjumlahan dengan jumlahan sebelumnya:

        10000001 11001001
        00000000 00001100
        ----------------- +
        10000001 11010101

8.  Data ("Halo")

        H        a        l        o
        72       97       108      111
        01001000 01100001 01101100 01101111

    Hasil penjumlahan dengan jumlahan sebelumnya:

        10000001 11010101    jumlahan sebelumnya
        01001000 01100001    chunk 16-bit pertama
        ----------------- +
        11001010 00110110
        01101100 01101111    chunk 16-bit kedua
        ----------------- +
        00110110 10100101
        00000000 00000001    carry
        ----------------- +
        00110110 10100110

## Kesimpulan

Hasil penjumlahan terakhir pada bagian di atas kemudian dikomplemen (one's
complement).

**Jadi**, didapatkan checksum dengan nilai:

    11001001 01011001
