---
author: Faiz Jazadi
date: '2022-10-31'
numbersections: true
title: TCP/IP Application Layer
---

# Prologue

Layer application adalah layer yang langsung berhubungan dengan user.

Layering diperlukan karena interoperabilitas suatu sistem penting.

Protokol adalah aturan mengenai mekanisme komunikasi.

# Arsitektur Client-Server

-   Aplikasi mungkin adalah sebuah "requestor" (client) atau suatu "provider"
    (server)
-   Umumnya, lebih banyak client dibandingkan server
-   Tiap server umumnya memiliki kemampuan untuk melayani banyak client

## Client

-   Program yang berjalan pada mesin lokal
-   Meminta layanan dari sebuah server
-   Dijalankan ketika dibutuhkan
-   Berakhir ketika sudah selesai
-   Membuka komunikasi, mengirim request, menerima respon, dan menutup channel

## Server

-   Program berjalan pada mesin
-   Menerima permintaan dari client
-   Selalu berjalan dan tidak berakhir setelah merespon suatu request

# Protokol Umum

## File Transfer Protocol (FTP)

-   Mekanisme standar untuk menyalin file dari satu mesin ke mesin lainnya.
-   Tidak bergantung pada OS tertentu.
-   Satu sesi FTP dijalankan dengan dua koneksi:
    -   Control connection
    -   Data connection

### Koneksi Control

-   Server mengirim passive open ke port 21 (port umum)
-   Client memilih port free untuk membuka koneksi baru
-   Koneksi tetap aktif selama sesi berlangsung

## Simple Mail Transfer Protocol (SMTP)

-   Digunakan untuk mengirimkan email
-   Mendukung:
    -   Mengirimkan pesan tunggal ke satu atau lebih penerima
    -   Mengirimkan pesan yang berisi teks, suara, atau gambar
    -   Mengirim pesan ke pengguna di jaringan di luar internet
-   Port umum adalah port 25.

## Hypertext Transfer Protocol (HTTP)

Digunakan untuk mengakses website.
