---
author: Faiz Jazadi
date: '2022-10-24'
numbersections: true
title: 'Network Core -- Routing'
---

# Algoritma-Algoritma Least Cost

-   Basis untuk keputusan routing
    -   meminimalisasi hop
    -   mempunyai nilai link yang berbanding terbalik dengan kapasitas
-   Mendefinisikan biaya path antara dua node sebagai jumlah dari cost yang
    dilalui
    -   dalam jaringan node-node terkoneksi oleh link bi-directional
    -   tiap link mempunyai cost di tiap arah
-   Untuk setiap pasangan node, temukan path dengan biaya terkecil
-   Alfgoritma alternatif: Dijkstra atau Bellman-Ford

## Dijkstra's Algorithm

-   Temukan shortest path dari sumber node `s` ke semua node lainnya
-   Dilakukan dengan mengembangkan path-path secara berurutan dari panjang path
    yang meningkat.
-   ...

## Algoritma Bellman-Ford

# Extras

Pasti keluar di ujian: Dijkstra.
