---
author: Faiz Jazadi
date: '2022-10-31'
numbersections: true
title: Hough Transform
---

# Prologue

Hough transform digunakan untuk mengekstraksi sebuah tepi tetapi dalam bentuk
line. Mekanismenya menggunakan sebuah transformasi dari suatu nilai ke nilai
yang lain.

-   Suatu metode untuk menemukan shape dalam suatu gambar
-   Shape bisa dideskripsikan dalam bentuk parametrik
-   Digunakan skema voting untuk menentukan parameter yang benar

# Contoh: Line Fitting

-   Many objects characterized by presence of straight lines.
-   Kesulitan:
    -   Clutter (titik mana yang berpasangan dengan baris yang mana?)
    -   Hanya sebagian dari garis yang terdeteksi, beberapa hilang
    -   Noise (mana yang garis lurus mana yang bukan?)

# Voting

-   Tidak memungkinkan untuk memeriksa semua kombinasi dua edge point untuk
    membuat suatu garis (mungkin, tapi mahal).
-   Voting adalah teknik umum: kita membiarkan fitur-fitur memilih model mana
    yang cocok
-   Noise dan clutter akan masuk dalam vote juga, namun biasanya bersifat tidak
    konsisten jika dibandingkan dengan feature lainnya yang "bagus".

# Teknik Hough Transform

-   Hough Transform adalah teknik voting yang bisa digunakan untuk menjawab
    pertanyaan berikut.
    -   Berapa banyak garis yang ada?
    -   Titik mana yang masuk dalam suatu garis?
-   Ide kunci:
    -   Simpan vote untuk setiap garis yang mungkin tentang di garis mana titik
        tersebut.
    -   Cari garis yang punya banyak vote

# Hough Space

Hubungan antara ruang image $(x, y)$ dan Hough $(m, b)$.

-   Garis dalam ruang image terkait dengan suatu titik dalam ruang Hough
-   Untuk pergi dari ruang gambar ke ruang Hough:
    -   diberikan himpunan titik $(x, y)$, temukan semua $(m, b)$ sedemikian
        hingga $y = mx + b$.
-   Ke titik mana $(x_0, y_0)$ dipetakan dalam ruang Hough?
    -   Jawabannya adalah semua penyelesaian dari $b = -x_0 m + y_0$
    -   Ini adalah baris dalam ruang Hough
-   Apa parameter garis untuk garis yang berisi $(x_0, y_0)$ dan $(x_1, y_1)$?
    -   Irisan dari garis $b = -x_0m + y_0$ dan $b = -x_1m + y_1$

# Algoritma Hough

-   Let each edge point in image space vote for a set of possible parameters in
    Hough space
-   Accumulate votes in discrete set of bins; parameters with the most votes
    indicate line in image space.
-   Untuk line detection:
    -   Temukan subset dari $n$ titik pada gambar yang berada di garis lurus
        yang sama.
    -   Tuliskan setiap garis yang dibentuk oleh pasangan titik-titik sebagai
        $y_i = ax_i + b$
    -   Kemudian, plot pada parameter space $b = x_ia + y_i$
    -   Semua titik $(x_i, y_i)$ pada garis yang sama akan memberikan parameter
        space point yang sama
    -   Kuantisasikan parameter space dan berapa banyak tiap titik masuk ke
        dalam garis yang sama

## Polar Representation

Terkadang representasi dengan $(m, b)$ menyebabkan nilai tak hingga, sehingga
digunakan representasi polar.

$$x\cos\theta - y\sin\theta = d$$

Titik pada ruang gambar adalah segmen sinusoid pada ruang Hough.

**Catatan ini belum lengkap dan cukup untuk bisa dipahami.**
