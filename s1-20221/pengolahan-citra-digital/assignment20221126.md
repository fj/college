---
author: 'Faiz Unisa Jazadi -- `21/475298/PA/20563`'
date: '2022-11-26'
subtitle: Pengolahan Citra Digital (PCD)
title: Rangkuman Kuliah Umum
---

# Pembicara 1: Laksono Kurnianggoro

-   Citra digital adalah representasi angka yang divisualisasikan.

    -   Representasi umum: angka menunjukkan intensitas cahaya dan ada tiga
        komponen warna (digabung menjadi satu)

-   Citra digital terdiri dari gambar dan video. Pemrosesan video sama saja
    dengan pemrosesan beberapa gambar (frame-frame dalam video)

-   Thresholding adalah proses segmentasi paling sederhana dengan membagi gambar
    menjadi dua bagian berdasarkan suatu threshold (aplikasinya adalah
    background subtraction)

-   Dalam mengenali objek, diekstrak ciri-ciri yang mencolok (misal ciri bentuk
    untuk mengenali rambu-rambu)

-   Feature engineering adalah rekayasa fitur dari yang didapat pada gambar

-   Fitur diubah menjadi histogram kemudian dimasukkan ke model

-   Classifier yang sering digunakan: SVM, random forest, neural network

-   Machine learning adalah pembelajaran mesin (mesin belajar tanpa harus
    diprogram secara explicit).

-   Supervised learning: komputer belajar memetakan input ke output

-   Object detection adalah mengekstrak fitur dari ROI
-   Metode yang bisa digunakan: OpenCV haarcascade face detector
-   ROI diisolasi kemudian di-preprocess

-   Aplikasi di autonomous car:
    -   Deteksi rambu
    -   Estimasi jarak
    -   Drivable region estiamtion
    -   Pendekatan 3D untuk region estimation
    -   Visual odometry
    -   Driver activity monitoring
    -   Bird Eye View
    -   Passenger counter
    -   Face recognition for boarding gate
    -   Vehicle counting
    -   Illegal parking detection
    -   ANPR

-   Di Nodeflux:
    -   Vehicle counting and classification
    -   Vehicle dwelling
    -   ANPR

# Pembicara 2: Dwisnanto Putro

Secara umum membahas vision system berbasis deep learning pada robot sosial.

-   Machine learning memisahkan feature extraction dan classification
-   Deep learning melakukan keduanya dalam satu network
-   Neural network ada metode untuk memprediksi output yang terdiri dari input,
    hidden, dan output layer
-   Komponen-komponen: perceptron dan backpropagation
-   CNN atau Convolutional Neural Network adalah perkalian kernel dengan gambar,
    dan merupakan operasi spasial (beda dengan network biasa).
-   Aplikasi CNN: Socially Assistive Robotics (SAR)
    -   Topik baru
    -   Membahas interaksi sosial robot dan manusia
-   Komponen SAR:
    -   Sistem robotic: actuator, sensor, itnerface
    -   Human robot interface: ability intelligence, task intelligence
-   SAR dalam robotika digunakan untuk robot industri atau non industrial.
-   SAR mampu mengenali manusia, ekspresinya, dan mampu meng-inference emosi
    manusia
-   DL diimplementasikan pada robot sosial bisa dengan GPU
    -   Bobot dihitung dengan loss function
    -   Weight dimodelkan untuk inferensi
-   Aplikasi:
    -   Person detection
    -   Deteksi wajah efisien pada perangkat biaya rendah
    -   Deteksi emosi wajah
