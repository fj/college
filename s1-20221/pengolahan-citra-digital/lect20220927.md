---
author: 'Faiz Unisa Jazadi -- `21/475298/PA/20563`'
date: '2022-09-27'
numbersections: true
title: Segmentation by Region and Feature
---

# Pendahuluan

Segmentation adalah salah satu tahap yang sangat penting. Biasanya dilakukan
setelah enhancement. Segmentation bertujuan untuk mencoba mendeteksi apakah
objek yang dianalisis berada pada sebuah citra.

1.  Partitioning image into meaningful pieces
2.  Isolating a specific region of interest

Region-based methods: mencoba langsung membentuk region dari suatu pixel dengan
mengamati karakteristik dari pixel-pixel tersebut.

# Region Growing

Diatur suatu initial threshold $T$, dipilih suatu titik initial random $(x, y)$.
Kemudian, dicari tetangga-tetangga titik tersebut secara rekursif iteratif yang
berada di dalam threshold $T$.

Region growing bisa digunakan jika satu objek dan objek lainnya mempunyai batas
yang jelas.

# Watersheds Segmentation

-   Sebuah gambar seperti sebuah lembah
-   Misalnya dilihat dari segi $x$-nya saja, terlihat seperti
-   Seolah-olah air dituangkan dan genangan-genangannya menjadi region-region
    yang berbeda

![](img/w7-watershed-example.png)

# Segmentation as Clustering

-   Segmentasi dengan menganggap satu pixel sebagai satu data untuk kemudian
    diaplikasikan metode clustering terhadapnya. Diasumsikan bahwa pixel yang
    ada pada objek yang sama memiliki fitur yang sama.
-   Pada segmentasi ini, setiap pixel diwujudkan dalam bentuk *feature vector*.
-   Cluster dalam $k$ cluster atau $k$ segmen
    -   Umumnya digunakan K-means clustering
-   Setiap cluster digunakan untuk merepresentasikan suatu region
-   Kelemahan: objek dengan gradasi warna dianggap sebagai region yang berbeda

# Segmentasi Berbasis Fitur

-   Disebut *detection*
-   Menggunakan fitur-fitur objek yang ingin dilokalisasi
    -   Dilakukan terlebih dahulu terhadap sampel objek dengan menggunakan suatu
        metode machine learning
-   Gunakan sliding window untuk menemukan semua kemungkinan objek yang sama
    pada gambar
    -   "Slide" a box around image and classify each image crop inside a box
        whether it contains object of interest or not
    -   Inspect every window with fixed size in different scale,
    -   Extract feature in window,
    -   Classify the window,
    -   Post processing
-   Detection evaluation
    -   IOU (intersection over union): Detection is correct if the intersection
        of the bounding boxes, divided by their union is more than 50%

# Important Note

Video yang diringkas dalam catatan ini sama persis dengan video pada pertemuan
sebelumnya.
