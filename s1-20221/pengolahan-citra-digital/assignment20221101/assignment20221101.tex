\documentclass[conference]{IEEEtran}
\IEEEoverridecommandlockouts
\usepackage{hyperref}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{algorithmic}
\usepackage{graphicx}
\usepackage{textcomp}
\usepackage{xcolor}
\def\BibTeX{{\rm B\kern-.05em{\sc i\kern-.025em b}\kern-.08em
T\kern-.1667em\lower.7ex\hbox{E}\kern-.125emX}}
\hbadness=3000
\graphicspath{{img/}}

\begin{document}

\title{Aplikasi Pengolahan Citra Digital pada Berbagai Kasus}

\author{\IEEEauthorblockN{Faiz Unisa Jazadi}
	\IEEEauthorblockA{\textit{Departemen Ilmu Komputer dan Elektronika} \\
		\textit{Universitas Gadjah Mada}\\
		Yogyakarta, Indonesia \\
		faiz.uni2003@mail.ugm.ac.id}
}

\maketitle

\begin{abstract}
  Artikel ini memuat implementasi pengolahan citra digital dalam menyelesaikan
  masalah-masalah di kehidupan nyata. Di artikel ini, dibahas mengenai beberapa
  studi yang mengaplikasikan pengolahan citra digital. Terdapat tiga kasus yang
  dibahas, di antaranya adalah kasus automatic plate number recognition, kasus
  screening displasia panggul, dan kasus deteksi gumpalan darah pada diagnosa
  penyakit Alzheimer.
\end{abstract}

\begin{IEEEkeywords}
	pengolahan citra digital, aplikasi, image enhancement
\end{IEEEkeywords}

\section{Pendahuluan}

Pengolahan citra digital adalah salah satu bidang studi dalam ilmu komputer
yang memiliki peran penting. Pengolahan citra digital adalah studi mengenai
proses yang dilakukan dalam mengolah citra digital. Pengolahan citra digital
umumnya diperlukan sebagai langkah awal dalam melakukan suatu penelitian.
Beberapa tujuan pengolahan citra digital: enhancement, restorasi, ekstraksi
fitur, dan segmentasi. Beberapa bidang penelitian seperti computer vision dan
pembelajaran mesin yang di dalamnya memroses suatu citra digital umumnya
memiliki proses pengolahan citra digital. Dalam artikel ini, akan dibahas
secara ringkas mengenai beberapa contoh kasus aplikasi pengolahan citra
digital.

\section{Kasus Automatic Plate Number Recognition (ANPR)}

ANPR adalah suatu metode yang digunakan untuk mengekstrak data nomor plat
kendaraan dari suatu citra digital (umumnya didapatkan dari kamera berkecepatan
tinggi dengan pencahayaan yang cukup) tanpa intervensi manusia. ANPR digunakan
dalam banyak hal termasuk mendeteksi tindakan kriminal di jalan raya dan
identifikasi kendaraan parkir. ANPR digunakan dalam hal-hal yang membutuhkan
informasi terkait nomor plat kendaraan. Dalam \cite{adak22}, ANPR dilakukan
melalui pendekatan deep learning yang menggunakan algoritma YOLOv3 (`You Only
Look Once') untuk object detection.  Dibandingkan algoritma lain, algoritma
tersebut tidak hanya mengklasifikasikan objek menjadi suatu kategori, namun
juga bisa mengklasifikasikan beberapa objek dalam suatu citra. Algoritma
tersebut tidak bergantung pada satu neural network, namun hanya mengaplikasikan
satu neural network pada citra. Network yang diaplikasikan akan memecah gambar
menjadi beberapa region dan memprediksikan bounding box serta probabilitas
untuk tiap region. Pada ANPR, region of interest (ROI) adalah plat nomor
kendaraan.

Dalam \cite{adak22}, ANPR dilakukan melalui beberapa proses (Fig.
\ref{fig:anpr-flow}): mendapatkan gambar asli dari kamera, menggunakan model
YOLO untuk mendapatkan ROI, melakukan pre-processing terhadap ROI, dan
memasukkan ke dalam CNN. Salah satu komponen arsitektur program yang disebutkan
adalah tahapan pre-processing. Tahapan tersebut terdiri atas beberapa operasi
manipulasi citra: resize, mengubah ke greyscale, menerapkan bilateral filter,
canny edge detection, menghilangkan garis vertikal dan horizontal, menghapus
blob, mendeteksi kontur untuk karakter dan angka, menggambar bounding box di
sekitar karakter dan angka, dan melakukan cropping terhadap semua kontur yang
ditemukan.

\begin{figure}
	\includegraphics[width=0.4\textwidth]{anpr-flow}
	\centering
	\caption{Diagram alir program ANPR \cite{adak22}}
	\label{fig:anpr-flow}
\end{figure}

Melalui serangkaian proses di atas, didapatkan program ANPR dengan akurasi
mencapai 95\% untuk gambar yang tidak dipengaruhi faktor lingkungan (contoh:
rotasi, multi-line, dan custom font).

\section{Kasus Screening Displasia Panggul (DDH) pada Bayi}

Displasia panggul atau developmental dysplasia of the hip (DDH) adalah kondisi
perkembangan abnormal pada bayi dengan kepala femur berada di posisi yang salah
pada sendi panggul.  DDH adalah salah satu penyebab arthritis prematur terbesar
pada orang-orang di awal masa dewasa. Pada bayi yang baru lahir, kondisi DDH
bervariasi, mulai dari pergeseran kepala femur hingga dislokasi kepala femur
sepenuhnya dari socket.  Relasi ini menunjukkan pentingnya screening DDH pada
bayi. Namun, mengandalkan para ahli untuk melakukan dan melaporkan scan pada
bayi-bayi yang baru lahir adalah upaya yang mahal. Sehingga, hanya mereka yang
memiliki faktor risiko yang akan melewati ultrasound ketika lahir.

Secara klinis, untuk memeriksa DDH, ultrasound lebih sensitif dibandingkan
eksaminasi fisik. Ultrasound juga lebih ideal untuk program-program screening.
Dengan ultrasound, eksaminasi biasanya dilakukan dengan metode Femoral Head
Coverage (FHC). FHC dilakukan dengan menggambar garis lurus dari tepi atas
ilium, kemudian proporsi kepala femur pada kedua sisi diukur (Fig.
\ref{fig:fhc-method}). Keputusan dihasilkan berdasarkan nilai proporsi 50\%
(dinyatakan DDH jika FHC$\leq$50\% dan sehat jika FHC$\geq$50\%). Proporsi
dihitung dengan menggunakan persamaan $\text{FHC\%} = \frac{A}{B}\times100$.

\begin{figure}
	\includegraphics[width=0.4\textwidth]{fhc-method}
	\centering
	\caption{Anotasi ultrasound sendi panggul (kanan) dan metode FHC (kiri) \cite{stamper22}}
	\label{fig:fhc-method}
\end{figure}

Studi \cite{stamper22} mengusulkan penggunaan convolutional neural network
(CNN) untuk melakukan screening DDH menggunakan citra ultrasound secara
otomatis. Di tahapan awal, digunakan multi-class CNN untuk melakukan segmentasi
terhadap struktur anatomi pada citra. FHC kemudian dihitung dengan mencari
titik teratas dan terbawah pada kepala femur (sebelumnya dilakukan pencarian
lokasi kepala femur). Meski terkait erat dengan CNN secara fungsional, salah
satu tahapan pada program screening yang dirancang pada studi \cite{stamper22}
adalah pre-processing. Beberapa tahapan pre-processing diperlukan di antaranya
adalah melakukan cropping dan down sampling. Pada akhirnya, program yang
dirancang mampu mengevaluasi adanya DDH dengan akurasi mencapai 89.8\%.

\section{Kasus Deteksi Gumpalan Darah pada Diagnosa Penyakit Alzheimer}

Keberhasilan dalam mengidentifikasi penghambat pada pembuluh darah adalah suatu
tahapan krusial dalam mendiagnosa penyakit Alzheimer. Blok-blok yang menghambat
bisa diidentifikasi berdasarkan variabel spasial dan time-depth dari citra
Two-Photon Excitation Microscopy (TPEF) terhadap pembuluh darah otak
menggunakan metode machine learning. Studi \cite{ghosh22} mengusulkan beberapa
skema pre-processing untuk meningkatkan performa metode-metode tersebut (Fig.
\ref{fig:alzheimer-mm}).

Pada tahapan image processing, diekstrak region of interest (ROI) dari
frame-frame pada video diekstrak. Kemudian, nilai pixel pada region di luar ROI
diganti dengan nilai lain yang tidak ada pada ROI (agar fokus pada ROI).
Kemudian, untuk mengubah frame-frame video menjadi representasi volumetrik
pembuluh darah, ROI diekstrak dari dari frame-frame dan disusun menjadi stack
dengan kedalaman sehingga membentuk volume 3D. Pada stack tersebut,
diaplikasikan low pass filter (LPF) 3D Gaussian. Kemudian, diaplikasikan juga
Otsu thresholding pada volume 3D tersebut untuk membuat binary mask, yang akan
membentuk representasi point cloud dari pembuluh darah. Digunakan clustering
DBScan untuk menghapus titik-titik pencilan sehingga 3D mask yang dihasilkan
akan bebas noise. Terakhir, mask yang dibuat diaplikasikan ke stacked frames
untuk menghapus region yang bukan pembuluh darah.

Hasil akhir studi \cite{ghosh22} menunjukkan bahwa skema pre-processing yang diusulkan lebih baik dibandingkan solusi pre-processing lain (top 3 pada sebuah private leaderboard).Metode pre-processing yang diusulkan berhasil melampaui performa metode terbaik (berdasarkan leaderboard) lebih dari 4\% dari segi akurasi dan 8\% dari segi Matthews Correlations Coefficient (MCC).

\begin{figure}
	\includegraphics[width=0.4\textwidth]{alzheimer-mm}
	\centering
	\caption{Skema multi-model pre-processing yang diusulkan \cite{ghosh22}}
	\label{fig:alzheimer-mm}
\end{figure}



\bibliographystyle{IEEEtran}
\bibliography{IEEEabrv,references}
\end{document}
