---
author: Faiz Unisa Jazadi
date: '2022-09-20'
numbersections: true
title: Segmentation
---

# Apa itu segmentasi?

-   Partitioning: mencoba memisahkan/mendapatkan bagian-bagian objek pada sebuah
    citra

-   Isolating: mencoba menganalisis objek tertentu pada citra (umumnya
    dihasilkan hanya dua buah region)

    -   Foreground: region of interest
    -   Background: yang bukan region of interest

-   Tujuan segmentasi: mengidentifikasi kelompok pixel yang berada pada objek
    yang sama (grouping nilai pixel)

# Mengapa segmentasi?

-   Mengkuantifikasi properti objek
    -   Berapa ukurannya? Apakah menjadi semakin besar atau semakin kecil?
    -   Analisis statistik
    -   Menghitung banyaknya objek
-   Detection/localization
    -   Di mana kendaraannya? Di mana objeknya?
    -   Apakah kendaraan di dekat manusia?

# Strategi

-   Pixel-based
    -   Klasifikasikan pixel terlebih dahulu, kemudian hasilkan region
-   Region-based
    -   Region bersifat locally homogeneous
    -   Region memenuhi beberapa properti

# Pixel-based Segmentation

-   Basic thresholding: mengklasifikan intensitas pixel menjadi dua kelas
    berdasarkan satu nilai threshold (ditentukan manual)
-   Adaptive thresholding: threshold ditentukan secara otomatis
-   Range thresholding: digunakan probabilitas gauss untuk membagi pixel

## Basic Thresholding

Mengklasifikasikan suatu pixel merupakan foreground atau background dengan
menggunakan suatu nilai threshold. Biasanya pixel-pixel hanya dibagi menjadi
beberapa kasus.

$$
B(x, y) =
\begin{cases}
    255, & I(x, y) \geq T \\
    0, & I(x, y) < T
\end{cases}
$$

$T$ mungkin ditentukan secara manual (histogram optimal dapat ditentukan
menggunakan histogram).

Contoh global thresholding:

![](img/w6-histogram-thresholding.png)

Metode global thresholding tidak selalu yang terbaik, ada juga **local
thresholding** yang menentukan threshold berdasarkan bagian-bagian gambar.

## Adaptive Thresholding

Threshold ditentukan secara adaptif.

-   Salah satu metodenya adalah **Otsu method**, ditemukan oleh Nobuyuki Otsu
-   Cara kerja:
    -   Coba beberapa threshold
    -   Gunakan threshold dengan *within class variance* minimum

## Thresholding by Probability

-   Berguna untuk deteksi objek dengan warna spesifik
    -   Kumpulkan gambar dengan warna tertentu
    -   Hitung rata-rata dan variance dari setiap channel pixel
    -   Hitung probabilitas pixel baru menggunakan probabilitas Gaussian dengan
        training average dan deviasi standar

# Region-based Segmentation

-   Region growing
    -   Mulai dari satu seed point dan periksa point-point tetangga
-   Watershed/flood fill
    -   Terinspirasi dari cara mengisi air
    -   Bayangkan setiap pixel sebagai wadah
-   Segmentation as clustering
    -   Representasikan setiap pixel sebagai satu data
        -   Setiap pixel bisa direpresentasikan sebagai:
            -   Intensitas
            -   Warna
            -   Warna + koordinat
    -   Data tersebut di-cluster menjadi beberapa cluster
        -   Contoh: K-means clustering
