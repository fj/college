---
author: 'Faiz Unisa Jazadi -- `21/475298/PA/20563`'
date: '2022-08-31'
subtitle: Praktikum Basis Data
title: Tugas 1
---

**Deadline:** 7 September 2022 (sebelum praktikum berikutnya)

# Task 1

-   Buatlah sebuah model database dari sistem perpustakaan
-   Tentukan entitas-entitas yang ada didalamnya dan jelaskan atribut-atribut
    penyusun entitas tersebut
-   Tentikan primary key, strong entity atau weak entity, relasi dan
    kardinalitas antara setiap entity
-   Gambarkan model tersebut dalam ER Diagram
-   ERD digambarkan berdasarkan asumsi aturan bisnis yang Anda buat sendiri.
    Jelaskan asumsi tersebut!

## Jawaban

Sistem perpustakaan adalah sistem yang mengelola buku, peminjaman buku,
keanggotaan, dan kunjungan.

### Entitas

-   Anggota (strong entity)

    -   ID anggota (PK)
    -   Nama
    -   Tanggal keanggotaan
    -   Alamat
    -   Nomor identitas

-   Buku (strong entity)

    -   ID buku (PK)
    -   ID kategori (FK)
    -   Judul buku
    -   Penulis
    -   Tahun terbit

-   Kategori (strong entity)

    -   ID kategori
    -   Nama

-   Peminjaman (strong entity)

    -   ID peminjaman (PK)
    -   ID buku (FK)
    -   ID anggota (FK)
    -   Tanggal pengembalian
    -   Status

-   Kunjungan (weak entity)

    -   Nama pengunjung
    -   Alamat
    -   Tanggal kunjungan

### Relasi

-   Anggota $\rightarrow$ peminjaman: one to many
-   Buku $\rightarrow$ peminjaman: one to many
-   Buku $\rightarrow$ kategori: many to one

### Entity Relationship Diagram

![ERD Task 1](task1-erd.png)

### Asumsi Aturan Bisnis

1.  Seseorang bisa menjadi anggota tanpa berkunjung
2.  Buku hanya boleh dipinjam oleh member/anggota perpustakaan
3.  Setiap anggota hanya boleh meminjam banyak buku
4.  Terdapat pengunjung perpustakaan yang hanya berkunjung dan/atau membaca buku
    di tempat

# Task 2

Perusahaan rekaman Aneka Record memutuskan untuk menyimpan semua informasi
mengenai musisi yang mengerjakan albumnya dalam suatu basis data. Pihak
perusahaan menyewa Anda sebagai desainer/perancang basis data. Beberapa
keterangan/deskripsi yang menggambarkan fakta yang ada pada perusahaan tersebut
sebagai berikut.

-   Tiap musisi yang melakukan rekaman di Aneka Record mempunyai SSN, nama,
    alamat dan nomor telepon.
-   Tiap instrumen yang digunakan untuk merekam berbagai macam lagu di Aneka
    Record mempunyai nama (contoh: piano, gitar, bas, sinthesizer, flute) dan
    kunci music (contoh: C, B-flat, E-flat).
-   Tiap album yang dicatat di Aneka Record mempunyai judul rekaman, tanggal
    copyright, format (contoh: KASET, CD, atau VCD) dan sebuah identifikasi
    album.
-   Tiap lagu yang dicatat di Aneka Record mempunyai judul dan pengarang lagu.
-   Tiap musisi mungkin memainkan beberapa instrumen dan tiap instrumen dapat
    dimainkan oleh beberapa musisi.
-   Tiap album mempunyai beberapa lagu di dalamnya, tapi tidak ada lagu yang
    muncul bersamaan dalam satu album.
-   Tiap lagu dibawakan oleh satu atau lebih musisi dan seorang musisi bisa
    membawakan beberapa lagu.
-   Tiap album dibawakan seorang musisi yang berperan sebagai produser. Seorang
    musisi bisa menghasilkan beberapa album.

Buatlah E-R Diagram dari sebuah Sistem Informasi Rekaman pada Perusaaan Aneka
Record tersebut di atas yang menyesuaikan dengan deskripsi kasusnya!

## Jawaban

Berikut adalah E-R diagram untuk Sistem Informasi Rekaman pada Perusahaan Aneka
Record.

![ERD Task 2](task2-erd.png)
