---
author: 'Faiz Unisa Jazadi -- `21/475298/PA/20563`'
date: '2022-11-21'
geometry: margin=1.5cm
pagenumbering: 0
subtitle: Praktikum Basis Data
title: Homework 10
---

# Soal 1

![](img/p1.png)

# Soal 2

![](img/p2.png)

# Soal 3

![](img/p3.png)
