-- Tugas 2 Praktikum Basis Data
-- Faiz Unisa Jazadi
-- 21/475298/PA/20563

-- Hapus jika database sudah pernah ada sebelumnya
DROP DATABASE IF EXISTS `ACADEMIC`;

-- Buatlah suatu database ACADEMIC
CREATE DATABASE IF NOT EXISTS `ACADEMIC`;

USE `ACADEMIC`;

-- Tabel mahasiswa (attribut: NIM, nama_mhsw, jenis_kelamin, tempat_lahir, tanggal_lahir, alamat, telepon)
CREATE TABLE IF NOT EXISTS `mahasiswa` (
        `nim` int NOT NULL AUTO_INCREMENT,
        `nama_mhsw` varchar(100) NOT NULL,
        `jenis_kelamin` varchar(50) NOT NULL,
        `tempat_lahir` varchar(50) NOT NULL,
        `tanggal_lahir` date NOT NULL,
        `alamat` text NOT NULL,
        `telepon` varchar(20) NULL,
        PRIMARY KEY (`nim`)
);

-- Tabel dosen (attribut: NIP, nama_dosen, jenis_kelamin, laboratorium, minat_riset, alamat, telepon)
CREATE TABLE `dosen` (
        `nip` int NOT NULL AUTO_INCREMENT,
        `nama_dosen` varchar(100) CHARACTER SET utf8 NOT NULL,
        `jenis_kelamin` varchar(20) NOT NULL,
        `laboratorium` varchar(50) NOT NULL,
        `minat_riset` varchar(50) NOT NULL,
        `alamat` text NOT NULL,
        `telepon` varchar(20) NOT NULL,
        PRIMARY KEY (`nip`)
);

-- Tabel mata_kuliah (attribut: kode_mk, nama_mk, NIP, sks, hari, jam, kelas, deskripsi)
CREATE TABLE `mata_kuliah` (
        `kode_mk` int NOT NULL AUTO_INCREMENT,
        `nama_mk` varchar(255) NOT NULL,
        `nip` int NOT NULL,
        `sks` int NOT NULL,
        `hari` int NOT NULL,
        `jam` time NOT NULL,
        `kelas` varchar(20) NOT NULL,
        `deskripsi` text DEFAULT NULL,
        PRIMARY KEY (`kode_mk`),
        FOREIGN KEY (`nip`) REFERENCES `dosen` (`nip`)
);

-- Tabel KRS (attribut: ID_KRS, kode_mk, NIM, tahun, semester)
CREATE TABLE `krs` (
        `id` int NOT NULL AUTO_INCREMENT,
        `kode_mk` int NOT NULL,
        `nim` int NOT NULL,
        `tahun` int NOT NULL,
        `semester` int NOT NULL,
        PRIMARY KEY (`id`),
        FOREIGN KEY (`kode_mk`) REFERENCES `mata_kuliah` (`kode_mk`)
);

-- Tampilkan semua tabel yang ada dalam database
SHOW TABLES;

-- Tampilkan struktur setiap tabel dalam database
DESCRIBE `mahasiswa`;
DESCRIBE `dosen`;
DESCRIBE `mata_kuliah`;
DESCRIBE `krs`;

-- Ubah nama tabel "dosen" menjadi "profesor"
RENAME TABLE `dosen` to `profesor`;

-- Ubah nama atribut "nama_dosen" dengan “nama_profesor”
ALTER TABLE `profesor` CHANGE COLUMN `nama_dosen` `nama_profesor` varchar(100) NOT NULL;

-- Tambahkan kolom baru "nama_ibu" setelah atribut tanggal_lahir pada tabel mahasiswa
ALTER TABLE `mahasiswa` ADD COLUMN `nama_ibu` varchar(100) AFTER `tanggal_lahir`;

-- Ubah tipe data "jenis_kelamin" pada tabel mahasiswa menjadi enum {'male', 'female'}
ALTER TABLE `mahasiswa` CHANGE COLUMN `jenis_kelamin` `jenis_kelamin` enum('male', 'female') NOT NULL;

-- Ubah tipe data "telepon" pada tabel mahasiswa menjadi int
ALTER TABLE `mahasiswa` CHANGE COLUMN `telepon` `telepon` int NULL;

-- Hapus kunci utama di atribut "ID_KRS"
ALTER TABLE `krs` CHANGE COLUMN `id` `id` int NOT NULL;
ALTER TABLE `krs` DROP PRIMARY KEY;

-- Tambahkan kembali kunci utama pada atribut "ID_KRS"
ALTER TABLE `krs` ADD CONSTRAINT `pk_krs` PRIMARY KEY (`id`);
ALTER TABLE `krs` CHANGE COLUMN `id` `id` int NOT NULL AUTO_INCREMENT;

-- Hapus atribut "deskripsi" pada tabel mata_kuliah
ALTER TABLE `mata_kuliah` DROP COLUMN `deskripsi`;
