---
author: 'Faiz Unisa Jazadi -- `21/475298/PA/20563`'
date: '2022-09-28'
numbersections: true
title: Data Manipulation Language 3
---

# Tujuan Pembelajaran

1.  Mengetahui sintaks DML pada MySQL seperti `MAX`, `MIN`, `AVG`, `SUM`,
    `COUNT`, `GROUP BY`, `HAVING`
2.  Dapat menggunakan sintaks DML pada MySQL seperti `MAX`, `MIN`, `AVG`, `SUM`,
    `COUNT`, `GROUP BY`, `HAVING`

# Ringkasan

-   `MAX`: nilai maksimal
-   `MIN`: nilai minimal
-   `AVG`: nilai rata-rata
-   `SUM`: nilai jumlahan
-   `COUNT`: nilai kuantitas
-   `GROUP BY`: mengelompokkan berdasarkan suatu group
-   `HAVING`: memfilter grup berdasarkan suatu kondisi
