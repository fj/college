---
author: 'Faiz Unisa Jazadi -- `21/475298/PA/20563`'
date: '2022-09-28'
numbersections: true
subtitle: Praktikum Basis Data
title: Tugas 5
---

# Pembuatan Database

![](img/p0.png){height="450px"}

# Jawaban

1.  Tampilkan kolom city address tanpa duplikat

    ![](img/p1.png){height="150px"}

2.  Tampilkan ipk maksimum mahasiswa dari Jakarta

    ![](img/p2.png){height="150px"}

3.  Tampilkan ipk minimum dari mahasiswa jurusan Komputer

    ![](img/p3.png){height="150px"}

4.  Tampilkan jumlah mahasiswa di departemen Komputer

    ![](img/p4.png){height="150px"}

5.  Tampilkan IPK rata-rata dari mahasiswa jurusan matematika

    ![](img/p5.png){height="150px"}

6.  Tampilkan jumlah data mahasiswa di setiap jurusan

    ![](img/p6.png){height="150px"}

7.  Tampilkan jumlah data mahasiswa dari city yang berbeda

    ![](img/p7.png){height="150px"}

8.  Tampilkan data jumlah kelompok mahasiswa berdasarkan alamat kota dengan usia
    dibawah 20 tahun

    ![](img/p8.png){height="150px"}
