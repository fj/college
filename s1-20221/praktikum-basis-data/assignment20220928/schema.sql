-- Buat database dengan data table student berikut:

DROP DATABASE IF EXISTS school;

CREATE DATABASE school;

USE school;

CREATE TABLE student (
        no INT NOT NULL AUTO_INCREMENT,
        nim CHAR(5) NOT NULL,
        name VARCHAR(100) NOT NULL,
        city_address TEXT NOT NULL,
        age INT NOT NULL,
        ipk FLOAT(2, 1) NOT NULL,
        department VARCHAR(255),
        PRIMARY KEY (no)
);

INSERT INTO student
(nim, name, city_address, age, ipk, department)
VALUES
(12345, 'Adi', 'Jakarta', 17, 2.5, 'Math'),
(12346, 'Ani', 'Yogyakarta', 20, 2.1, 'Math'),
(12347, 'Ari', 'Surabaya', 18, 2.5, 'Computer'),
(12348, 'Ali', 'Banjarmasin', 20, 3.5, 'Computer'),
(12349, 'Abi', 'Medan', 17, 3.7, 'Computer'),
(12350, 'Budi', 'Jakarta', 18, 3.8, 'Computer'),
(12351, 'Boni', 'Yogyakarta', 20, 3.2, 'Computer'),
(12352, 'Bobi', 'Surabaya', 17, 2.7, 'Computer'),
(12353, 'Beni', 'Banjarmasin', 18, 2.3, 'Computer'),
(12354, 'Cepi', 'Jakarta', 20, 2.2, NULL),
(12355, 'Coni', 'Yogyakarta', 22, 2.6, NULL),
(12356, 'Ceki', 'Surabaya', 21, 2.5, 'Math'),
(12357, 'Dodi', 'Jakarta', 20, 3.1, 'Math'),
(12358, 'Didi', 'Yogyakarta', 19, 3.2, 'Physics'),
(12359, 'Deri', 'Surabaya', 19, 3.3, 'Physics'),
(12360, 'Eli', 'Jakarta', 20, 2.9, 'Physics'),
(12361, 'Endah', 'Jakarta', 18, 2.8, 'Physics'),
(12362, 'Feni', 'Jakarta', 17, 2.7, NULL),
(12363, 'Farah', 'Yogyakarta', 18, 3.5, NULL),
(12364, 'Fandi', 'Surabaya', 19, 3.4, NULL);

-- Tampilkan kolom city address tanpa ada nilai duplikat
SELECT DISTINCT city_address FROM student;

-- Tampilkan ipk maksimum mahasiswa dari Jakarta
SELECT MAX(ipk) FROM student WHERE city_address = 'Jakarta';

-- Tampilkan ipk minimum dari mahasiswa jurusan Komputer
SELECT MIN(ipk) FROM student WHERE department = 'Computer';

-- Tampilkan jumlah mahasiswa di departemen Komputer
SELECT COUNT(*) FROM student WHERE department = 'Computer';

-- Tampilkan IPK rata-rata dari mahasiswa jurusan matematika
SELECT AVG(ipk) FROM student WHERE department = 'Math';

-- Tampilkan jumlah data mahasiswa di setiap jurusan
SELECT department, COUNT(department) FROM student
WHERE department IS NOT NULL
GROUP BY department;

-- Tampilkan jumlah data mahasiswa dari city yang berbeda
SELECT city_address, COUNT(city_address) FROM student GROUP BY city_address;

-- Tampilkan data jumlah kelompok mahasiswa berdasarkan alamat kota dengan usia dibawah 20 tahun
SELECT city_address, COUNT(*) FROM student WHERE age < 20 GROUP BY city_address;
