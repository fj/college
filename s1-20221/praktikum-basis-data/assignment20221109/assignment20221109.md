---
author: 'Faiz Unisa Jazadi -- `21/475298/PA/20563`'
date: '2022-11-09'
numbersections: true
subtitle: Praktikum Basis Data
title: Tugas 9
---

# Query

``` {.sql .numberLines}
USE classicmodels;

DROP FUNCTION IF EXISTS GetBulkPrice;
DROP TRIGGER before_product_update;
DROP TRIGGER before_product_delete;
DROP TRIGGER after_product_insert;

DELIMITER $$

/*
1. Buatlah sebuah function untuk mengetahui harga grosir yang
   disesuaikan dengan jumlah barang yang akan dibeli. Semisal barang
   akan dapat diskon 10% apabila beli minimal 1 lusin, dan diskon 15%
   apabila beli minimal 100.
*/

CREATE FUNCTION GetBulkPrice(
  price DECIMAL(10, 2),
  qty INT
) RETURNS DECIMAL(10, 2)
  DETERMINISTIC
BEGIN
  DECLARE bulkPrice DECIMAL(10, 2);

  IF qty >= 100 THEN
    SET bulkPrice = TRUNCATE(price * 0.85, 10);
  ELSEIF qty >= 12 THEN
    SET bulkPrice = TRUNCATE(price * 0.9, 10);
  ELSE
    SET bulkPrice = price;
  END IF;

  RETURN (bulkPrice);
END$$

DELIMITER ;

/*
2. Buatlah sebuah trigger untuk merekam history perubahan product
   serta buatlah tabel baru untuk menyimpan perubahan tersebut.
*/

DROP TABLE IF EXISTS productHistory;
CREATE TABLE productHistory (
  historyId INT NOT NULL AUTO_INCREMENT,
  productCode VARCHAR(15) NOT NULL,
  productName VARCHAR(70) NOT NULL,
  changeTimestamp DATETIME DEFAULT NULL,
  action VARCHAR(50) DEFAULT NULL,
  PRIMARY KEY (historyId)
);

DELIMITER $$

CREATE TRIGGER before_product_update
  BEFORE UPDATE ON products
  FOR EACH ROW
BEGIN
  INSERT INTO productHistory
  SET action = 'update',
    productCode = OLD.productCode,
    productName = OLD.productName,
    changeTimestamp = NOW();
END$$

CREATE TRIGGER before_product_delete
  BEFORE DELETE ON products
  FOR EACH ROW
BEGIN
  INSERT INTO productHistory
  SET action = 'delete',
    productCode = OLD.productCode,
    productName = OLD.productName,
    changeTimestamp = NOW();
END$$

CREATE TRIGGER after_product_insert
  AFTER INSERT ON products
  FOR EACH ROW
BEGIN
  INSERT INTO productHistory
  SET action = 'create',
    productCode = NEW.productCode,
    productName = NEW.productName,
    changeTimestamp = NOW();
END$$
DELIMITER ;

-- demo
SELECT GetBulkPrice(100000, 101);
```

# Output

![](img/a9-output.png)
