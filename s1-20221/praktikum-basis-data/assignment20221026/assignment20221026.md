---
author: 'Faiz Unisa Jazadi -- `21/475298/PA/20563`'
date: '2022-10-26'
numbersections: true
subtitle: Praktikum Basis Data
title: Tugas 7
---

``` {.sql}
DROP DATABASE IF EXISTS homework7;
SOURCE homework7.sql;
USE homework7;


-- 1.  Menampilkan kode barang, nama barang beserta jumlahnya yang
--     berada di lokasi "Laboratorium 2 M Informatika"
SELECT product_code, product_name, total
FROM tb_product
WHERE location_code = (
    SELECT location_code
    FROM tb_location
    WHERE location_name = 'Laboratorium 2 M Informatika'
);
```

![](img/a7-1.png)

``` {.sql}
-- 2.  Menampilkan kode barang, nama barang yang berjenis "Alat-alat
--     elektronik" dan seluruh barang yang ada di "Ruang Dosen M
--     Informatika"
SELECT product_code, product_name
FROM tb_product
WHERE type_code = (
    SELECT type_code
    FROM tb_type
    WHERE type_name = 'Alat-alat elektronik'
) OR location_code = (
    SELECT location_code
    FROM tb_location
    WHERE location_name = 'Ruang Dosen M Informatika'
);
```

![](img/a7-2.png)

``` {.sql}
-- 3.  Menampilkan kode barang, nama barang dan jumlah barang yang
-- merupakan jenis "Alat-alat elektronik" dan "Mebeler" yang terletak
-- di "Laboratorium 2 M Informatika"
SELECT product_code, product_name, total
FROM tb_product
WHERE type_code IN (
    SELECT type_code
    FROM tb_type
    WHERE type_name IN ('Alat-alat elektronik', 'Mebeler')
)
AND location_code = (
    SELECT location_code
    FROM tb_location
    WHERE location_name = 'Laboratorium 2 M Informatika'
);
```

![](img/a7-3.png)

``` {.sql}
-- 4.  Menampilkan semua informasi barang selain jenis "Mebeler" yang
--     berada di "Laboratorium 2 M Informatika"
SELECT * FROM tb_product
WHERE type_code != (
    SELECT type_code
    FROM tb_type
    WHERE type_name = 'Mebeler'
) AND location_code = (
    SELECT location_code
    FROM tb_location
    WHERE location_name = 'Laboratorium 2 M Informatika'
);
```

![](img/a7-4.png)

``` {.sql}
-- 5.  Menambahkan barang dengan nama "Spidol" dengan kode barang
--     "AP005" dan informasi barang lain sama dengan "Papan Tulis"
--     tetapi jumlah barang 2 kali lipat dari jumlah "Papan Tulis"
INSERT INTO tb_product
(type_code, location_code, product_code, product_name, total)
SELECT type_code, location_code, 'AP005', 'Spidol', total * 2
    FROM tb_product
    WHERE product_name = 'Papan Tulis';
```

![](img/a7-5.png)

``` {.sql}
-- 6.  Menambahkan "Kabel HDMI" yang akan di tempatkan di "Ruang Dosen
--     M Informatika" dengan kode barang "EK0017", jenis 'E001', dan
--     berjumlah sama dengan seluruh "LCD Projector"
INSERT INTO `tb_product`
(type_code, location_code, product_code, product_name, total)
VALUES (
    'E001',
    (SELECT location_code FROM tb_location
        WHERE location_name = 'Ruang Dosen M Informatika'),
    'EK0017',
    'Kabel HDMI',
    (SELECT SUM(total) FROM tb_product as t
        WHERE t.product_name = 'LCD Projector')
);
```

![](img/a7-6.png)

``` {.sql}
-- 7.  Mengubah jumlah "Komputer" yang ada di "Laboratorium 1 M
--     Informatika" sesuai dengan jumlah "Meja Komputer" di ruangan
--     tersebut
UPDATE tb_product
SET total = (
    SELECT total FROM tb_product
    WHERE product_name = 'Meja Komputer'
    AND location_code = (
        SELECT location_code FROM tb_location
        WHERE location_name = 'Laboratorium 1 M Informatika'
    )
) WHERE location_code = (
    SELECT location_code FROM tb_location
    WHERE location_name = 'Laboratorium 1 M Informatika'
) AND product_name = 'Komputer';
```

![](img/a7-7.png)

``` {.sql}
-- 8.  Mengubah seluruh jumlah "Kursi Hidrolik" di setiap ruangan
--     dengan jumlah minimum dari "Meja Komputer"
UPDATE tb_product
SET total = (
    SELECT MIN(total) FROM tb_product
    WHERE product_name = 'Meja Komputer'
) WHERE product_name = 'Kursi Hidrolik';
```

![](img/a7-8.png)

``` {.sql}
-- 9.  Menghapus seluruh data barang yang ada di lokasi "Ruang Dosen
--     M Informatika"
DELETE FROM tb_product
WHERE location_code = (
    SELECT location_code FROM tb_location
    WHERE location_name = 'Ruang Dosen M Informatika'
);
```

![](img/a7-9.png)

``` {.sql}
-- 10. Menghapus seluruh data barang di lokasi "Laboratorium 2 M
--     Informatika" yang bukan merupakan jenis "Mebeler"
DELETE FROM tb_product
WHERE location_code = (
    SELECT location_code FROM tb_location
    WHERE location_name = 'Laboratorium 2 M Informatika'
) AND type_code != (
    SELECT type_name FROM tb_type
    WHERE type_name = 'Mebeler'
);
```

![](img/a7-10.png)
