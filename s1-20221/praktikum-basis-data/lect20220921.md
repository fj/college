---
author: Faiz Unisa Jazadi
date: '2022-09-21'
numbersections: true
title: Data Manipulation Language 2
---

# Tujuan Pembelajaran

1.  Mengetahui sintaks DML pada MySQL seperti `SELECT`, `DISTINCT`, `WHERE`,
    `ORDER`, `LIMIT`, `AND`, `OR`, `IN`, `BETWEEN`, `LIKE`, `IS NULL`
2.  Dapat menggunakan sintaks DML pada MySQL seperti `SELECT`, `DISTINCT`,
    `WHERE`, `ORDER`, `LIMIT`, `AND`, `OR`, `IN`, `BETWEEN`, `LIKE`, `IS NULL`

# Ringkasan

1.  `SELECT`: untuk menyeleksi baris tertentu
2.  `DISTINCT`: untuk mengeliminasi baris yang duplikat
3.  `WHERE`: untuk menyeleksi berdasarkan suatu kondisi
4.  `ORDER`: untuk men-spesifikasikan urutan baris
5.  `LIMIT`: untuk membatasi jumlah baris yang diseleksi
6.  `AND`: konjungsi dan
7.  `OR`: konjungsi atau
8.  `IN`: untuk memeriksa apakah suatu nilai ada dalam suatu set
9.  `BETWEEN`: untuk memeriksa apakah suatu nilai ada dalam suatu range
10. `LIKE`: untuk memeriksa apakah suatu nilai cocok dengan suatu pattern
11. `IS NULL`: untuk memeriksa apakah bernilai `NULL`
