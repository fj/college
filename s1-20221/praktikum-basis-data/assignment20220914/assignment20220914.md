---
author: 'Faiz Unisa Jazadi -- `21/475298/PA/20563`'
date: '2022-09-14'
numbersections: true
subtitle: Praktikum Basis Data
title: Tugas 3
---

# Query

``` {.sql}
-- 1) Tuliskan query untuk membuat database store
CREATE DATABASE IF NOT EXISTS store;

USE store;

-- 2) Tuliskan query untuk membuat tabel Customer, tabel Product,
--    dan Tabel Transaction
CREATE TABLE Customer (
        customerNumber VARCHAR(5) NOT NULL,
        customerName VARCHAR(255) NOT NULL,
        city VARCHAR(100),
        PRIMARY KEY (customerNumber)
);

CREATE TABLE Product (
        productNumber VARCHAR(5) NOT NULL,
        productName VARCHAR(100) NOT NULL,
        stock INT(3) DEFAULT 15,
        PRIMARY KEY (productNumber)
);

CREATE TABLE Transaction (
        transactionNumber VARCHAR(5) NOT NULL,
        transactionDate date NOT NULL,
        customerNumber VARCHAR(5) NOT NULL,
        PRIMARY KEY (transactionNumber),
        FOREIGN KEY (customerNumber)
        REFERENCES Customer (customerNumber)
);

-- 3) Tuliskan query untuk memasukkan data pada ketiga tabel
--    sesuai dengan data berikut (ada di modul)
INSERT INTO Customer
(customerNumber, customerName, city) VALUES
('C-003', 'Geneva Electronic', 'Bandung'),
('C-007', 'Gama Store', 'Yogyakarta'),
('C-009', 'Family Mart', 'Medan'),
('C-011', 'Star Store', 'Bandung');

INSERT INTO Product
(productNumber, productName, stock) VALUES
('P-100', 'Monitor 17', 15),
('P-120', 'Keyboard Ps2', 20),
('P-123', 'Mouse Ps2', 15),
('P-125', 'Memory 512', 10);

INSERT INTO Transaction
(transactionNumber, transactionDate, customerNumber) VALUES
('T-001', '2019-10-19', 'C-011'),
('T-002', '2020-01-10', 'C-003'),
('T-003', '2020-08-07', 'C-009'),
('T-004', '2020-09-09', 'C-007');

-- 4) Tampilkan nama city dari Customer secara unik
SELECT DISTINCT city FROM Customer;

-- 5) Tampilkan seluruh data Customer
SELECT * FROM Customer;

-- 6) Tampilkan productName dan stock dari seluruh data product
SELECT productName, stock FROM Product;

-- 7) Tampilkan customerName yang tinggal di kota Bandung
SELECT customerName FROM Customer
WHERE city = 'Bandung';

-- 8) Tampilkan transactionDate yang dilakukan oleh customer yang
--    memiliki customerNumber C-007
SELECT transactionDate FROM Transaction
WHERE customerNumber = 'C-007';

-- 9) Tampilkan seluruh data product yang memiliki stock 15
SELECT * FROM Product WHERE stock = 15;

-- 10) Ubahlah transactionDate dari customer yang memiliki
--     customerNumber C-007 menjadi tanggal hari ini
UPDATE Transaction
SET transactionDate = CURRENT_DATE()
WHERE customerNumber = 'C-007';

-- 11) Ubahlah customerName menjadi HappyFamily Mart dan city menjadi
--     Jakarta untuk customer yang memiliki customerNumber C-009
UPDATE Customer
SET customerName = 'HappyFamily Mart', city = 'Jakarta'
WHERE customerNumber = 'C-009';

-- 12) Tambahkan data product berikut menggunakan nilai DEFAULT
INSERT INTO Product (productNumber, productName) VALUES ('P-130', 'NIC D-Link');

-- 13) Tambahkan data transaction berikut menggunakan tanggal hari ini
--     pada transactionDate
INSERT INTO Transaction
(transactionNumber, transactionDate, customerNumber) VALUES
('T-005', CURRENT_DATE(), 'C-003'),
('T-006', CURRENT_DATE(), 'C-011');

-- 14) Hapuslah 2 data transaction pertama yang diurutkan berdasarkan
--     transactionNumber
DELETE FROM Transaction ORDER BY transactionNumber ASC LIMIT 2;

-- 15) Hapuslah 2 data transaction terakhir yang diurutkan berdasarkan
--     transactionNumber
DELETE FROM Transaction ORDER BY transactionNumber DESC LIMIT 2;
```

# Output

![](img/query-output.png)
