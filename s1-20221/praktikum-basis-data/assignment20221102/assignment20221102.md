---
author: 'Faiz Unisa Jazadi -- `21/475298/PA/20563`'
date: '2022-11-02'
numbersections: true
subtitle: Praktikum Basis Data
title: Tugas 8
---

# Query

```sql
USE classicmodels;

DELIMITER $$

/*
1. Buatlah sebuah prosedur untuk menaikkan harga kategori produk
   tertentu dengan persentase tertentu. Buatlah tabel product dengan
   data yang sesuai untuk menguji prosedur yang telah dibuat. Atau,
   load database classicmodels pada personal machine sehingga dapat
   memiliki akses lengkap. DELIMITER harus diubah sebelum membuat
   prosedur.
*/
CREATE OR REPLACE PROCEDURE RaiseCategoryPrice (
  IN categoryName VARCHAR(50),
  IN percent DECIMAL(4, 2)
)
BEGIN
  UPDATE products
  SET MSRP = TRUNCATE(MSRP * ((100 + percent) / 100), 10)
  WHERE productLine = categoryName;
END$$

/*
2. Buatlah prosedur untuk melaporkan jumlah yang dipesan pada bulan dan
   tahun tertentu untuk customer yang memiliki string karakter tertentu
   pada nama mereka.
*/
CREATE OR REPLACE PROCEDURE SpecificCustTotalOrder (
  IN orderYear INT,
  IN orderMonth INT,
  IN nameChar CHAR(1),
  OUT total INT
)
BEGIN
  SELECT SUM(orderdetails.quantityOrdered)
  INTO total
  FROM orders
  JOIN customers
    ON customers.customerNumber = orders.customerNumber
  JOIN orderdetails
    ON orders.orderNumber = orderdetails.orderNumber
  WHERE MONTH(orderDate) = orderMonth
    AND YEAR(orderDate) = orderYear
    AND LOCATE(nameChar, customerName) > 0;
END$$


/*
3. Buatlah prosedur untuk mengubah batas kredit seluruh *customer* di
   negara tertentu dengan persentase tertentu.
*/
CREATE OR REPLACE PROCEDURE ChangeCountryCustCredLimit (
  IN custCountry VARCHAR(50),
  IN percent DECIMAL(4, 2)
)
BEGIN
  UPDATE customers
  SET creditLimit = TRUNCATE(creditLimit * (percent / 100), 10)
  WHERE country = custCountry;
END$$

DELIMITER ;

-- for testing
CALL RaiseCategoryPrice('Classic Cars', 50);
CALL SpecificCustTotalOrder(2003, 2, 'a', @total);
CALL ChangeCountryCustCredLimit('Russia', 50);
```

# Output

![](img/a8-output.png)
