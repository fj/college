DROP TABLE IF EXISTS menu_history;

CREATE TABLE menu_history (
        Menu_Name VARCHAR(100),
        Price DECIMAL(10, 2),
        Update_Time DATETIME
);

DROP TRIGGER IF EXISTS after_menu_update;

CREATE TRIGGER after_menu_update
AFTER UPDATE ON menu
FOR EACH ROW
        INSERT INTO menu_history
        (Menu_Name, Price, Update_Time)
        VALUES
        (old.Menu_Name, old.Price, NOW());
