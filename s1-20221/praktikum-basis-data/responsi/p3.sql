USE cafe;

SELECT *, (
        SELECT Item
        FROM order_recap
        WHERE Customer_Number = customer.ID_Customer
        ORDER BY COUNT(Item) DESC
        LIMIT 1
) as Freq_Menu 
FROM customer;
