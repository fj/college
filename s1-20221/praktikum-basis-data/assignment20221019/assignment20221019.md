---
author: 'Faiz Unisa Jazadi -- `21/475298/PA/20563`'
date: '2022-10-19'
numbersections: true
subtitle: Praktikum Basis Data
title: Tugas 6
---

``` {.sql}
----------------------
-- Membuat database
----------------------
DROP DATABASE IF EXISTS library;
CREATE DATABASE library;
USE library;

CREATE TABLE Books (
        bookID INT NOT NULL AUTO_INCREMENT,
        bookTitle TEXT NOT NULL,
        authorName VARCHAR(255) NOT NULL,
        borrowedStatus BOOLEAN NOT NULL,
        PRIMARY KEY (bookID)
);

CREATE TABLE `User` (
        userID INT NOT NULL AUTO_INCREMENT,
        userName VARCHAR(100) NOT NULL,
        numberOfBorrowing INT NOT NULL,
        numberOfReturning INT NOT NULL,
        PRIMARY KEY (userID)
);

CREATE TABLE Flow (
        flowID INT NOT NULL AUTO_INCREMENT,
        userIDBorrowing INT NOT NULL,
        bookIDBorrowed INT NOT NULL,
        borrowDate DATE NOT NULL,
        returnDate DATE NOT NULL,
        PRIMARY KEY (flowID),
        FOREIGN KEY (userIDBorrowing)
        REFERENCES `User` (userID)
        ON DELETE CASCADE ON UPDATE CASCADE,
        FOREIGN KEY (bookIDBorrowed)
        REFERENCES Books (BookID)
        ON DELETE CASCADE ON UPDATE CASCADE
);


INSERT INTO Books
(bookTitle, authorName, borrowedStatus)
VALUES
('Intro. to Machine Learning', 'Raditya Rifan', TRUE),
('Advanced Programming', 'John Doe', FALSE),
('Manajemen Perpustakaan', 'Jane Doe', TRUE),
('Manajemen Basis Data', 'Dzikri Rahadian Fudholi', FALSE);

INSERT INTO `User`
(userName, numberOfBorrowing, numberOfReturning)
VALUES
('Rifan Raditya', 4, 3),
('Muhammad John', 1, 1),
('Ronggo Tsani',  2, 1),
('Rifania Putri', 1, 1);

INSERT INTO Flow
(userIDBorrowing, bookIDBorrowed, borrowDate, returnDate)
VALUES
(1, 1, DATE_SUB(CURRENT_DATE(), INTERVAL 1 DAY),
		DATE_ADD(CURRENT_DATE(), INTERVAL 6 DAY)),
(2, 3, DATE_SUB(CURRENT_DATE(), INTERVAL 3 DAY),
		DATE_ADD(CURRENT_DATE(), INTERVAL 4 DAY));
```
``` {.sql}
----------------------------------------------------------
-- 1. Menampilkan semua judul buku yang memiliki status
--    dipinjam dan tanggal peminjamannya kemarin
----------------------------------------------------------
SELECT bookTitle FROM Books
JOIN Flow ON Books.bookID = Flow.bookIDBorrowed
WHERE borrowDate = DATE_SUB(CURRENT_DATE(), INTERVAL 1 DAY)
AND Books.borrowedStatus = TRUE;
```

![](img/a5-1.png)

``` {.sql}
-----------------------------------------------------------------
-- 2. Menampilkan semua judul buku, termasuk buku yang tidak
--    dipinjam dan userID peminjam yang meminjam buku tersebut
-----------------------------------------------------------------

SELECT bookTitle, Flow.userIDBorrowing
FROM Books
LEFT JOIN Flow ON Books.bookID = Flow.bookIDBorrowed;
```

![](img/a5-2.png)

``` {.sql}
-------------------------------------------------------
-- 3. Menampilkan semua buku yang dipinjam dan semua
--    userID, baik dia meminjam atau tidak
-------------------------------------------------------
SELECT Books.bookTitle, `User`.userID
FROM Flow
JOIN Books ON Flow.bookIDBorrowed = Books.bookID
RIGHT JOIN `User` ON `User`.userID = Flow.userIDBorrowing
WHERE Books.borrowedStatus = TRUE or Books.borrowedStatus IS NULL;
```

![](img/a5-3.png)

``` {.sql}
------------------------------------------------------
-- 4. Menggunakan satu query, buatlah daftar semua
--    judul buku dan nama user yang meminjam buku
--    tersebut dan user tersebut telah meminjam lebih
--    dari 3 buku.
------------------------------------------------------

SELECT Books.bookTitle, `User`.userName
FROM Flow
JOIN Books ON Flow.bookIDBorrowed = Books.bookID
JOIN `User` ON `User`.userID = Flow.userIDBorrowing
WHERE `User`.numberOfBorrowing > 3;
```

![](img/a5-4.png)
