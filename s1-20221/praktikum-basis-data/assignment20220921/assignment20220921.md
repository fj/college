---
author: 'Faiz Unisa Jazadi -- `21/475298/PA/20563`'
date: '2022-09-21'
numbersections: true
subtitle: Praktikum Basis Data
title: Tugas 4
---

# Pembuatan Database

![](img/p0.png){height="450px"}

# Jawaban

1.  Tampilkan kolom city address tanpa ada nilai duplikat

    ![](img/p1.png){height="150px"}

2.  Tampilkan data name dari mahasiswa yang memiliki departemen 'Computer'

    ![](img/p2.png){height="200px"}

3.  Tampilkan data nim, name, age dan address yang diurutkan dari mahasiswa yang
    tertua

    ![](img/p3.png){height="200px"}

4.  Tampilkan name dan age dari 3 mahasiswa termuda dari 'Jakarta'

    ![](img/p4.png){height="200px"}

5.  Tampilkan name dan ipk mahasiswa dari 'Jakarta' dengan ipk dibawah 2,5

    ![](img/p5.png){height="200px"}

6.  Tampilkan name mahasiswa mana saja yang dari 'Yogyakarta' atau berusia lebih
    dari 20 tahun

    ![](img/p6.png){height="200px"}

7.  Tampilkan name dan address mahasiswa yang bukan dari 'Jakarta' dan
    'Surabaya'

    ![](img/p7.png){height="200px"}

8.  Tampilkan name, age dan ipk mahasiswa dengan ipk dari 2,5 hingga 3,5

    ![](img/p8.png){height="200px"}

9.  Tunjukkan name mahasiswa yang memiliki huruf a pada namanya

    ![](img/p9.png){height="200px"}

10. Tampilkan NIM mahasiswa dengan data NULL

    ![](img/p10.png){height="200px"}
