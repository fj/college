---
author: Faiz Unisa Jazadi
date: '2022-08-31'
numbersections: true
title: Introduction to ERD
---

# Tujuan pembelajaran

1.  Membuat ERD
2.  Mengimplementasikan ERD dengan RDBMS (database yang digunakan untuk
    praktikum adalah database relasional)
3.  Mengolah basis data dengan Structured Query Language (SQL)

# Pendahuluan

Dalam praktikum ini, digunakan **MySQL**.

Selain MySQL, ada juga OracleDB dan PostgreSQL (perbedaan terletak pada di
sintaksis). Oracle ditargetkan untuk skala bisnis dan sifatnya tidak
open-source. PostgreSQL juga mirip.

Terdapat 11 pertemuan, dengan pertemuan terakhir membahas final project
(presentasi). Terdapat 1 responsi akhir (di minggu terakhir sebelum UAS).

## Penilaian

1.  Quiz: 20% (dibagi 10)
2.  Tugas: 30% (dibagi 10)
3.  Project 20%
4.  Responsi 30%

## Ketentuan

1.  Tidak ada toleransi keterlambatan
2.  Deadline tugas 1 minggu setelah hari praktikum
3.  Quiz sebelum kelas dimulai
4.  Format nama: `[KELAS PRAKTIKUM]-[NOMOR TOPIK]-[NIU]-[NAMA].[ext]` (Example:
    `KOMB-1-299979-Dzikri Rahadian Fudholi.pdf`)

# Entity Relationship

Entity adalah sebuah objek, barang, benda, atau entitas. Entitas mempunyai
data/atribut. Entity relationship artinya dua atau lebih identitas yang
berhubungan.

## Jenis Atribut

-   Simple: hanya data
-   Composite: atributnya bisa dibagi menjadi komponen yang lebih kecil
-   Single-valued: hanya ada satu nilai unik (misal. NIU)
-   Multi-valued: entitas dengan atribut yang bisa berisi nol, satu, atau lebih
    dari satu nomor
-   Derivative: entitas yang atributnya bisa didapat dari kalkulasi atribut lain
    (mis. umur dari tanggal lahir)

## Jenis Relationship

-   One to one: hanya satu entitas A terhubung dengan hanya satu entitas B
-   One to many: suatu entitas A dapat terhubung dengan satu atau lebih entitas
    B
-   Many to one: sama seperti one to many, hanya dibalik (berbeda cara bacanya,
    dan jenis hubungan antara entitasnya)
-   Many to many: satu atau lebih entitas A dapat terhubung dengan satu atau
    lebih entitas B

Jenis relationship tergantung permasalahan dan interpretasi database engineer.

## Key

Setelah mempunyai dua relationship, cara menghubungkannya adalah dengan *key*,

**Contoh** Primary key dari ruangan lab adalah LABKOM2 (maka, sifat PK adalah
unik). Pada spidol, dicatat nama LABKOM2 (sehinga menjadi FK).

-   Primary key: identitas untuk entitas tersebut
-   Foreign key: identitas untuk entitas lain

### Sifat PK

-   Unik
-   Berurut

### Sifat FK

-   Unik
-   Memberi karakteristik relationship

## Diagram (ERD)

Memvisualisasikan hubungan entitas. Ada banyak jenis ERD (**perlu dihafalkan
untuk ujian**).

Tipe entitas:

-   Strong: memiliki primary key
-   Weak: tidak punya primary key

**Best Practice:** Sebisa mungkin, hindari weak entitiy.

### Shapes

-   Kotak: entitas
-   Ellipse: atribut
-   Atribut dengan garis bawah: PK
-   Layang-layang: relasi masing-masing entitas (mis. `is_of`, `based`)
-   Garis: jenis relationship (ditulis seperti weight pada graf namun pada dua
    ujung), untuk one to many ditulis `n` dan `1`.

Masih ada shape yang lain.
