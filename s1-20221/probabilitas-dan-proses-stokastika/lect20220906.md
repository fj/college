---
author: Faiz Unisa Jazadi
date: '2022-09-06'
numbersections: true
title: 'Pertemuan Minggu Ke-4'
---

# Keywords/Outline

Materi untuk dibaca:

1.  PDF
2.  CDF
3.  Gaussian
4.  Expected value
5.  Continuous random variables

# Transcript

Ini juga ada ini yah. Ekspektasi kemudian sudah. Kemudian expected value dari
dan seterusnya. Expected value kemudian untuk continous random itu ehh. Ini
totalnya. Hubhubhub. Ini contoh-contohnya bisa anda baca sendiri.

kemudian families of continuous random variable.

$X$ is a $uniform(a, b)$ random variable if...

Jadi ini yah PDf-nya kemudian.. kalau yang ini yang uniform ya PDF nya kemudian
CDF nya garis lurus gitu ya tapi naik nanti kita liat aja lah kalo ada contohnya
lagi

Nah yang eksponensila contohnya yang tadi itu lho Yang tadi polinomial bukan
eksponensial

Kalo contoh yang uniform itu kalo probabilitasnya mungkin sama ehh nyari
contohnya ehh hhubhubhub

EH jadi e eksponensial kalo ini CDF kalo anda liat kumulatifnya naik cepet
antara $0, 5$. Begitu di atas 5 udah ngeflight banget.

Ini emang contoh seperti ini saya ada data realnya. Dari sebentar...

Bentar yaa..

Nah ini sebentar saya share sebentar ya...

Memang ada data seperti ini... ah ini..

Ini persebaran data jadi ini durasi rata-ratanya...

![](img/w4-fig1.png)

Jadi kebanyakan itu dijawab ya, kemudian itu durasinya ini yang menarik jadi
kepotong di sekitar angka 2000-an. Ini dat areal ini. Jadi ehh ceh data call
keluar dari ruang guru konon sehari ada berapa ribu call ini hehehehe. Tapi saya
minta datanya juga gaboleh ehehehe.

Jadi ini kebanyakan eksponensial. Kebanyakan 10000 sekon ke bawah. Dibagi 60-an.
Kok terlanjur sih hehehehehe.

Nah...

Jadi...

Masa pakai itu.. telepon call last no more than $t$ minutes.

Nah ini kalau dimodelkan $f(t)$ nya. Karena ini $1 - e^{-t}/3$ dengan
$t \geq 0$. Jadi oke.

Kemudian, PDF-nya juga akan banyak di sekitar 0 sampai di sekitar kalau kita
eeh. Dari CDF ini bisa kita turunkan ke PDF nya atau $f(t) = dF_T(t)/dt$.

Hp saya dari 2018 belum ganti, sebelumnya saya pake apa itu lupa lama juga

Orang saya gatau, mungkin gatau lah kalo hp

Mungkin yang ini aja deh, masa pakai elektronik atau apa gitu

Kalau mobil saya juga ehehehehe

Saya juga lama

Hp yang udah 10 tahun lebih msaih saya pegang ya. Tapi orang umum 5 tahun ganti
mobil. Itu masa pakai nya komponen-komponennya setelah 5 tahun banyak minta
jajan. Itu bentuknya eksponensial.

Jadi standardnya ini..
