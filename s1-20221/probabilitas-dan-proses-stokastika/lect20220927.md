---
author: 'Faiz Unisa Jazadi -- `21/475298/PA/20563`'
date: '2022-09-27'
numbersections: true
title: Info UTS
---

# Info

-   UTS dilaksanakan secara open-book.
-   Scope materi yang diujiankan hanya sampai materi expected value (materi
    joint tidak keluar).
    -   Probabilitas
    -   Variabel Random Diskrit
    -   Variabel Random Kontinyu
-   Pastikan tulisan rapi, ada indentasi, teorema diberi highlight.
-   Kebanyakan soal kemungkinan merupakan adaptasi dari soal pada buku.
