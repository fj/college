# college

This repository contains my class notes, exercises, and assignments. I store it
here for easier access and sharing.

## File Formats

The files are mostly written in Pandoc Markdown (compile to PDF using Pandoc for
a better reading experience). Other files like LaTeX files have to be compiled
first before it can be viewed.

## License

Refer to the `LICENSE.md` file.
