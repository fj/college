import java.util.Scanner;

public class ActivityThree {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.print("Enter a: ");
		double a = input.nextDouble();
		System.out.print("Enter b: ");
		double b = input.nextDouble();
		System.out.println("The minimum value is " + getMinValue(a, b));
		System.out.println();

		System.out.println("isSameAbsoluteValue(i, j)");
		System.out.print("Enter i: ");
		int i = input.nextInt();
		System.out.print("Enter j: ");
		int j = input.nextInt();
		System.out.println("The result is " + isSameAbsoluteValue(i, j));
		System.out.println();

		System.out.println("getMessage(Faiz, true)");
		getMessage("Faiz", true);
		System.out.println("getMessage(Faizah, false)");
		getMessage("Faizah", false);

		input.close();
	}

	public static double getMinValue(double a, double b) {
		if (a < b) {
			return a;
		}
		return b;
	}

	public static boolean isSameAbsoluteValue(int i, int j) {
		return Math.abs(i) == Math.abs(j);
	}

	public static boolean getMessage(String name, boolean isKid) {
		String pronoun = isKid ? "Dek" : "Pak";
		System.out.printf("Halo %s %s", pronoun, name);
		return isKid;
	}
}
