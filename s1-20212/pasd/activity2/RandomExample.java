public class RandomExample {
	public static void main(String[] args) {
		// soal 1b (0 - 3)
		int n = (int) (Math.random() * 4);
		System.out.println(n);

		// soal 1c (1 - 6)
		n = (int) (Math.random() * 6) + 1;
		System.out.println(n);

		// soal 1d (2, 4, 6, 8)
		n = ((int) (Math.random() * 4) + 1) * 2;
		System.out.println(n);

		// soal 1e (-5 - 5)
		n = (int) (Math.random() * 11) - 5;
		System.out.println(n);
	}
}
