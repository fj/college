public class MSTPrim {
        public boolean verbose = false;
        int nNode;
        Edge[] edges;
        boolean[] connectedNodes;
        boolean[] selectedEdges;

        public MSTPrim(int nNode, Edge[] edges)
        {
                this.nNode = nNode;
                this.edges = edges;
                connectedNodes = new boolean[nNode];
                selectedEdges = new boolean[edges.length];
        }

        int getMinEdge()
        {
                int id = -1;
                int val = Integer.MAX_VALUE;
                for (int i = 0; i < edges.length; i++) {
                        Edge e = edges[i];
                        if (e.len < val
                            && (!connectedNodes[e.from] ^ !connectedNodes[e.to])) {
                                id = i;
                                val = e.len;
                        }
                }
                return id;
        }

        boolean isConnectedAll()
        {
                for (int i = 0; i < nNode; i++) {
                        if (connectedNodes[i] == false)
                                return false;
                }
                return true;
        }

        public int solve()
        {
                connectedNodes[0] = true;
                while (true) {
                        int id = getMinEdge();
                        if (id < 0)
                                break;
                        selectedEdges[id] = true;
                        int src = edges[id].from;
                        int dst = edges[id].to;

                        connectedNodes[src] = true;
                        connectedNodes[dst] = true;
                }
                if (!isConnectedAll())
                        return -1;

                int ans = 0;
                String s = "";

                for (int i = 0; i < edges.length; i++) {
                        if (selectedEdges[i]) {
                                ans += edges[i].len;
                                if (verbose)
                                        s += edges[i] + " ";
                        }
                }
                if (verbose)
                        System.err.println(s);
                return ans;
        }
}
