import java.util.Scanner;

public class RunMSTPrim {
        public static void main(String[] args)
        {
                Scanner sc = new Scanner(System.in);
                int nNode = sc.nextInt();
                int nEdge = sc.nextInt();
                Edge[] edges = new Edge[nEdge];

                for (int i = 0; i < nEdge; i++) {
                        int from = sc.nextInt();
                        int to = sc.nextInt();
                        int len = sc.nextInt();
                        edges[i] = new Edge(from, to, len);
                }

                MSTPrim mst = new MSTPrim(nNode, edges);
                mst.verbose = true;
                
                System.out.println(mst.solve());
                
                sc.close();
        }
}
