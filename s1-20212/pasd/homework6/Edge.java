public class Edge {
        int from;
        int to;
        int len;

        public Edge(int from, int to, int len)
        {
                this.from = from;
                this.to = to;
                this.len = len;
        }

        public String toString()
        {
                return "#edge:[" + from + "," + to + "," + len + "]";
        }
}
