import java.util.Scanner;

public class RunDijkstra {
    static double[][] map;
    static int src;
    static int dst;

    public static void main(String[] args)
    {
	Scanner sc = new Scanner(System.in);
	System.out.println("masukkan jumlah node");
	int nTown = sc.nextInt();
	map = new double[nTown][nTown];

	System.out.println("Masukkan edge 'a b w\\n'");
	System.out.println("Berhenti dengan memasukkan -1");
	// 0 1 1
	//
	// -1
	while (true) {
	    int a = sc.nextInt();
	    if (a == -1)
		break;
	    int b = sc.nextInt();
	    double w = sc.nextDouble();
	    map[a][b] = w;
	    map[b][a] = w;
	}

	System.out.println("masukkan starting node");
	src = sc.nextInt();
	System.out.println("masukkan destination node");
	dst = sc.nextInt();

	Dijkstra dj = new Dijkstra(map);
	dj.solve(src, dst);
	System.out.println(dj.getDistance(dst));
    }
}
