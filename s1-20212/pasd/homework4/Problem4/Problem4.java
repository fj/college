import java.util.Arrays;
import java.util.Scanner;

public class Problem4 {
	public static void main(String[] args) throws Exception
	{
		// Inisialisasi `Scanner` untuk stdin
		Scanner in = new Scanner(System.in);
		// Inisialisasi `Staqueue` dengan kapasitas sesuai input
		var stq = new Staqueue<Integer>(in.nextInt());

		int choice = 1; // Variabel untuk pilihan menu
		System.out.println("1: enqueue, 2: dequeueFront, 3: dequeueRear");

		// Looping: selama choice valid, lakukan operasi pada `stq` sesuai choice
		while (choice > 0 && choice < 4) {
			choice = in.nextInt();
			switch (choice) {
			case 1:
				stq.enqueue(in.nextInt());
				break;
			case 2:
				stq.dequeueFront();
				break;
			case 3:
				stq.dequeueRear();
			}
			stq.printQueue();
		}
	}
}

/*
 * `Staqueue` adalah implementasi struktur data menyerupai queue
 * yang diubah sehingga operasi dequeue dapat dilakukan dari depan
 * dan belakang. Implementasi ini menggunakan konsep circular array.
 */
class Staqueue<E> {
	private int cap;  // Kapasitas queue
	private int size; // Ukuran queue
	private E[] arr;  // Array untuk isi queue

	private int front; // Index (pointer ke) bagian depan queue
	private int rear;  // Index bagian belakang queue

	@SuppressWarnings("unchecked")
	public Staqueue(int cap)
	{
		// Inisialisasi semua properti
		this.cap = cap;
		// Inisialisasi array untuk queue
		this.arr = (E[]) new Object[cap];

		// Pada kondisi awal (tidak ada elemen
		// pada queue), nilai `this.front` diatur menjadi 0
		// dan nilai `this.rear` diatur menjadi -1.
		this.front = 0;
		this.rear = -1;
		this.size = 0;
	}

	public void enqueue(E j) throws Exception
	{
		// Cek jika queue full
		if (this.isFull()) {
			throw new Exception("Queue full");
		}
		
		// Jika `this.rear` sudah mencapai ujung array,
		if (this.rear == this.cap - 1) {
			// atur `this.rear` menjadi -1 (wrap around)
			this.rear = -1;
		}
		
		// Increment `this.rear` lalu masukkan `j` ke `this.arr[this.rear]`
		this.arr[++this.rear] = j;
		this.size++; // Increment `this.size`
	}

	public E dequeueFront() throws Exception
	{
		// Cek jika queue kosong
		if (this.isEmpty()) {
			throw new Exception("Queue empty");
		}

		// Simpan nilai yang akan di-dequeue, increment
		// nilai `this.front`
		E temp = this.arr[this.front++];

		// Jika `this.front` sudah mencapai ujung array,
		if (this.front == this.cap)
			// atur `this.front` menjadi 0 (wrap around)
			this.front = 0;
		this.size--; // Decrement size
		return temp; // Kembalikan nilai yang sudah di-dequeue
	}

	public E dequeueRear() throws Exception
	{
		// Cek jika queue kosong
		if (this.isEmpty()) {
			throw new Exception("Queue empty");
		}

		// Simpan nilai yang akan di-dequeue
		E temp = this.arr[this.rear--];
		this.size--; // Decrement size

		// Jika `this.rear` sudah mencapai awal array
		// dan queue tidak kosong (terjadi wrap around),
		if (this.rear == -1 && this.size != 0) {
			// atur nilai `this.rear` menjadi `this.cap - 1`
			this.rear = this.cap - 1;
		}
		// Kembalikan nilai yang disimpan pada `temp`
		return temp;
	}

	public boolean isEmpty()
	{
		// Operasi queue dan dequeue akan merefleksikan
		// perubahan banyak elemen pada atribut `this.size`
		// Sehingga, queue dikatakan kosong jika `this.size == 0`
		return (this.size == 0);
	}

	public boolean isFull()
	{
		// Queue dikatakan full apabila memiliki ukuran
		// yang sama besarnya dengan kapasitas queue
		return (this.size == this.cap);
	}

	public void printQueue()
	{
		// Cetak array pada queue dengan menggunakan Arrays.toString
		System.out.print(Arrays.toString(this.arr));
		// Cetak juga nilai front, rear, dan size
		System.out.printf(" f=%d, r=%d, s=%d\n", this.front, this.rear, this.size);
	}
}
