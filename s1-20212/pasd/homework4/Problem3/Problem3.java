import java.util.Arrays;
import java.util.Scanner;

public class Problem3 {
	public static void main(String[] args) throws Exception
	{
		// Inisialisasi Scanner stdin
		var inp = new Scanner(System.in);
		// Inisialisasi `TwinStack` dengan size 2
		var ts = new TwinStack<Integer>(2);

		System.out.println("1: pushA, 2: pushB, 3: popA, 4: popB");
		int choice = 1; // Atur nilai awal `choice` menjadi `1`

		// Looping: selama nilai choice valid
		while (choice > 0 && choice < 5) {
			choice = inp.nextInt();

			// Lakukan setiap operasi sesuai nilai `choice`
			switch (choice) {
			case 1:
				ts.pushA(inp.nextInt());
				break;
			case 2:
				ts.pushB(inp.nextInt());
				break;
			case 3:
				ts.popA();
				break;
			case 4:
				ts.popB();
			}
			ts.print(); // Cetak isi stack
		}

		inp.close(); // Cleanup
	}
}

/* `TwinStack` adalah implementasi struktur data stack yang menggunakan
 * satu array untuk menyimpan dua buah stack.
 * Pada implementasi ini, stack A tumbuh ke arah kanan, sedangkan
 * stack B tumbuh ke arah kiri.
 */
class TwinStack<E> {
	final private int cap; // kapasitas stack
	private E[] arr;       // array untuk stack
	private int[] p;       // array untuk stack pointer
	private int mid;       // nilai tengah (pemisah) kedua stack

	@SuppressWarnings("unchecked")
	TwinStack(int scap)
	{
		// `scap` berisi size awal setiap stack
		this.cap = scap * 2; // Atur nilai `cap` sebesar 2 kali `scap`
		// Inisialisasi array `this.p` dengan nilai -1 dan `this.cap`
		// (pointer awal stack A adalah -1, stack B adalah `this.cap`)
		this.p = new int[] { -1, this.cap };
		this.mid = scap; // Nilai tengah array adalah `scap`
		// Inisialisasi array `this.arr` dengan kapasitas `this.cap`
		this.arr = (E[]) new Object[this.cap];
	}

	private void push(int si, E data) throws Exception
	{
		/*
		 * Method `push` menerima dua argumen yaitu `si` (integer)
		 * dan `data` (E). Argumen `si` merupakan stack index:
		 * 0 untuk stack A, 1 untuk stack B. Argumen `data` berisi
		 * nilai yang di-push.
		 */

		// Atur nilai `inc` menjadi 1 jika `si` bernilai 0, dan
		// bernilai -1 jika `si` bernilai 1
		int inc = 1 + (-2 * si);
		if (isFull(si)) {
			// Jika stack full,
			if (isFull((si + 1) % 2)) {
				// Jika stack yang lain juga full, maka
				// sudah tidak ada ruang untuk push
				throw new Exception("stack is full");
			}
			// Increment atau decrement `this.mid`
			// sesuai nilai `inc`
			this.mid += inc;
		}
		// Increment atau decrement stack pointer sesuai `si`
		// dan nilai `inc`
		this.p[si] += inc;
		// Masukkan nilai `data` ke dalam `arr` sesuai stack pointer
		this.arr[this.p[si]] = data;
	}

	private E pop(int si) throws Exception
	{
		// Method `pop` menerima argumen `si` untuk menentukan
		// stack yang mana yang akan di-pop

		if (isEmpty(si)) {
			// Jika stack kosong, maka operasi pop tidak bisa
			// dilakukan
			throw new Exception("stack is empty");
		}

		// Simpan nilai data yang akan di-pop
		E popped = this.arr[this.p[si]];
		// Increment/decrement stack pointer sesuai `si`
		// (decrement jika `si` bernilai 0)
		this.p[si] += -1 + (2 * si);
		return popped;
	}

	public boolean isEmpty(int si)
	{
		// Stack kosong apabila stack pointer bernilai
		// -1 atau this.cap
		return this.p[si] == -1 || this.p[si] == this.cap;
	}

	public boolean isFull(int si)
	{
		// Stack full apabila stack pointer bernilai
		// this.mid atau this.mid + 1
		return this.p[si] == (this.mid + si - 1);
	}

	// Method public di bawah digunakan untuk mengemas method
	// `push` dan `pop` menjadi `pushA`, `pushB`, `popA`, dan `popB`
	public void pushA(E data) throws Exception
	{
		push(0, data);
	}

	public void pushB(E data) throws Exception
	{
		push(1, data);
	}

	public E popA() throws Exception
	{
		return pop(0);
	}

	public E popB() throws Exception
	{
		return pop(1);
	}

	public void print()
	{
		// Cetak stack dengan `Arrays.toString`
		System.out.println(Arrays.toString(this.arr));
	}
}
