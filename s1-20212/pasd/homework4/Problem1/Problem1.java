import java.util.Arrays;
import java.util.Scanner;

public class Problem1 {
	public static void main(String[] args)
	{
		// Inisiasi Scanner untuk stdin
		var input = new Scanner(System.in);
		// Baca `input` hingga newline
		String word = input.nextLine();
		// Inisiasi Stack of Character dengan panjang `word`
		var stack = new Stack<Character>(word.length());

		// Looping: push setiap huruf pada `word`
		//          ke dalam `stack`
		for (int i = 0; i < word.length(); i++) {
			stack.push(word.charAt(i));
		}

		// Looping: cetak hasil operasi pop pada
		//          stack sebanyak panjang `word`
		//          pada stdout
		for (int i = 0; i < word.length(); i++) {
			System.out.print(stack.pop());
		}

		// Output seharusnya merupakan `word` yang
		// dibalik karena stack bersifat LIFO

		System.out.println();
		input.close(); // close stdin Scanner
	}
}

// Implementasi stack dengan tipe E (generic type)
class Stack<E> {
	private final int cap; // Kapasitas stack
	private E[] arr;       // Array untuk stack
	private int top;       // Pointer untuk stack top

	@SuppressWarnings("unchecked")
	public Stack(int cap)
	{
		// Atur atribut `cap` sesuai dengan argumen `cap`
		this.cap = cap;
		// Inisiasi `arr` sebagai array of `Object` dengan
		// kapasitas `cap`, lalu cast menjadi array of E
		this.arr = (E[]) new Object[cap];
		// Atur nilai awal top menjadi -1
		this.top = -1;
	}

	public void push(E v)
	{
		// Operasi push: increment nilai `this.top` kemudian
		//               atur nilai `arr[this.top]`
		//               menjadi `v`,
		this.arr[++this.top] = v;
	}

	public E pop()
	{
		// Operasi pop: kembalikan nilai `this.arr[this.top]`
		//              kemudian decrement nilai `this.top`
		return this.arr[this.top--];
	}

	public boolean isEmpty()
	{
		// Stack kosong jika `this.top` bernilai -1
		return (this.top == -1);
	}

	public void printStack()
	{
		// Cetak stack dengan menggunakan `Arrays.toString`
		System.out.println(Arrays.toString(this.arr));
	}
}
