import java.util.Arrays;
import java.util.Scanner;

public class Problem2 {
	public static void main(String[] args)
	{
		// Scanner untuk stdin
		var input = new Scanner(System.in);
		// Simpan satu baris di `word`, ubah ke lowercase
		// agar case-insensitive
		String word = input.nextLine().toLowerCase();
		// Inisialisasi stack of Character dengan kapasitas
		// sebanyak panjang `word`

		// Simpan panjang `word` karena banyak digunakan
		int wordLength = word.length();
		var stack = new Stack<Character>(wordLength);

		// Looping: push setiap karakter pada `word`
		//          (bagian setengah akhir) ke dalam `stack`
		for (int i = wordLength / 2; i < wordLength; i++) {
			stack.push(word.charAt(i));
		}

		// Asumsikan `word` adalah palindrome
		boolean isPalindrome = true;

		// Looping: cocokkan word[i...wordLength()/2]
		//          dengan nilai `stack.pop()`
		//          (cocokkan word dari depan dan dari
		//          belakang)
		for (int i = 0; i < (wordLength / 2); i++) {
			if (stack.pop() != word.charAt(i)) {
				// Keluar dari looping jika ada yang
				// tidak cocok, juga atur nilai
				// `isPalindrome` menjadi false
				isPalindrome = false;
				break;
			}
		}

		// Cetak nilai `isPalindrome`
		System.out.printf("is palindrome: %b\n", isPalindrome);
		input.close(); // Cleanup
	}
}

// Implementasi stack
// Penjelasan sama seperti Soal 1
class Stack<E> {
	private final int cap;
	private E[] arr;
	private int top;

	@SuppressWarnings("unchecked")
	public Stack(int cap)
	{
		this.cap = cap;
		arr = (E[]) new Object[cap];
		top = -1;
	}

	public void push(E v)
	{
		arr[++top] = v;
	}

	public E pop()
	{
		return arr[top--];
	}

	public boolean isEmpty()
	{
		return (top == -1);
	}

	public void printStack()
	{
		System.out.println(Arrays.toString(arr));
	}
}
