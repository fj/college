import java.util.Scanner;
import java.io.IOException;

public class LinkList {
	public static void main(String[] args) throws IOException{
		LinkListInit theList1 = new LinkListInit();
		LinkListInit theList2 = new LinkListInit();
		Scanner in = new Scanner(System.in);

		int nodeNum1;
		int nodeNum2;
		int tempNum;

		System.out.print("First list size? ");
		nodeNum1 = in.nextInt();
		for(int i=0; i<nodeNum1; i++){
			System.out.print("Insert number: ");
			tempNum = in.nextInt();
			theList1.insertLast(tempNum);
		}
		theList1.displayList();

		System.out.print("Second list size? ");
		nodeNum2 = in.nextInt();

		for(int i=0; i<nodeNum2; i++){
			System.out.print("Insert number: ");
			tempNum = in.nextInt();

			theList2.insertFirst(tempNum);
		}
		theList2.displayList();

		System.out.println("¥nDeleting first node of first list");
		theList1.deleteFirst();

		theList1.displayList();

		System.out.println("¥nDeleting last node of second list");
		theList2.deleteLast();

		theList2.displayList();
	}
}
