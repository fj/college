package array;

import java.util.Scanner;

class Array {
	public static void main(String[] args) {
		int size;
		Scanner in = new Scanner(System.in);
		System.out.print("Enter the size of the array: ");
		size = in.nextInt();
		int[] arr;
		arr = new int[size];
		for (int j = 0; j < size; j++) {
			System.out.print("Enter number " + j + " :¥t");
			arr[j] = in.nextInt();
		}
		for (int j = 0; j < size; j++) {
			System.out.print(arr[j] + " ");
		}
		System.out.println("");

		int numToReplace;
		int numReplacing;

		System.out.print("What number do you want to replace? ");
		numToReplace = in.nextInt();
		System.out.print("What will be the new number? ");
		numReplacing = in.nextInt();
		for (int j = 0; j < size; j++) {
			if (numToReplace == arr[j]) {
				arr[j] = numReplacing;
			}
		}
		for (int j = 0; j < size; j++) {
			System.out.print(arr[j] + " ");
		}
		System.out.println("");

		in.close();
	}
}
