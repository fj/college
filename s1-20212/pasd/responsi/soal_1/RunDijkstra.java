import java.util.*;

public class RunDijkstra {
	static double[][] map;
	static int src;
	static int dst;
	static int nEdges;

	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		System.out.println("masukkan jumlah node");
		int nTown = sc.nextInt(); //jumlah node
		map = new double[nTown][nTown];

		System.out.println("masukkan starting node");
		src = sc.nextInt();
		System.out.println("masukkan destination node");
		dst = sc.nextInt();

		System.out.println("masukkan banyak edge");
		nEdges = sc.nextInt();
		for (int i = 0; i < nEdges; i++) {
			int a = sc.nextInt();
			if (a == -1)
				break;
			int b = sc.nextInt();
			double w = sc.nextDouble();
			map[a][b] = w;
			map[b][a] = w;
		}

		Dijkstra dj = new Dijkstra(map);
		dj.solve(src, dst);
		System.out.println(dj.getDistance(dst));
		dj.printPath(dst);
	}
}
