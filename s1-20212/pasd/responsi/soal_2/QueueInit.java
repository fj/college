import java.util.Arrays;

public class QueueInit {
    private int maxSize;
    private int[] queueArray;
    private int front;
    private int rear;
    private int nItems;

    public QueueInit(int s)
    { //constructor
        maxSize = s;
        queueArray = new int[maxSize];
        front = 0;
        rear = -1;
        nItems = 0;
    }

    public void enqueue(int j)
    {                            //put item at rear of queue
        if (rear == maxSize - 1) //deal with wraparound
            rear = -1;
        queueArray[++rear] = j; //increment rear and insert item
        nItems++;               //one more item
    }

    public int dequeue()
    {                                 //take item from front of queue
        int temp = queueArray[front]; //get value and increment front
        queueArray[front] = 0;
        front++;
        if (front == maxSize) //deal with wraparound
            front = 0;
        nItems--; //one less item
        return temp;
    }

    public int searchPosition(int x)
    {
        for (int i = 0; i < nItems; i++) {
            int arrIndex = (front + i) % maxSize;
            if (queueArray[arrIndex] == x) {
                return i + 1;
            }
        }
        return -1;
    }

    public boolean isEmpty()
    { //true if queue is empty
        return (nItems == 0);
    }

    public boolean isFull()
    { //true if queue is full
        return (nItems == maxSize);
    }

    public void printQueue()
    {
        System.out.println(Arrays.toString(queueArray));
    }
}
