import java.io.IOException; //exception for I/O
import java.util.Arrays;
import java.util.Scanner; //for input

public class TestQueue {
    public static void main(String[] args) throws IOException
    {
        int queueSize;     //for queue size
        int numTemp;       //for inserted number
        int numChoice = 0; //for command

        Scanner in = new Scanner(System.in); //for input
        System.out.print("Enter queue size: ");
        queueSize = in.nextInt();

        QueueInit theQueue = new QueueInit(queueSize); //set queue

        while (numChoice != 4) {
            System.out.println("\n 1: Enqueue \t 2 : Dequeue \t 3: Search \t 4 : End");
            System.out.print("Enter command: ");
            numChoice = in.nextInt();
            if (numChoice == 1) {
                if (theQueue.isFull())
                    System.out.println("Queue is full");
                else {
                    System.out.print("Enter number: ");
                    numTemp = in.nextInt();
                    theQueue.enqueue(numTemp);
                }
                theQueue.printQueue();
            } else if (numChoice == 2) {
                if (theQueue.isEmpty())
                    System.out.println("Queue is empty");
                else {
                    numTemp = theQueue.dequeue();
                    System.out.println("Dequeue number: " + numTemp);
                }
            
            } else if (numChoice == 3) {
                if (theQueue.isEmpty()) {
                    System.out.println("Queue is empty");
                } else {
                    System.out.print("Search key: ");
                    numTemp = in.nextInt();
                    System.out.println("Queue position: " + theQueue.searchPosition(numTemp));
                }
            } else if (numChoice != 4) {
                System.out.println("Wrong command");
            }
                theQueue.printQueue();
        } //end main()
    }     //end class Queue
}
