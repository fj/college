package rectangle;

public class Cubic extends Rectangle {
	int height;

	Cubic(int width, int length, int height) {
		super(width, length);
		this.height = height;
	}

	Cubic(int size) {
		super(size);
		this.height = size;
	}

	public void printInfo() {
		System.out.printf("width=%d, length=%d, height=%d\n", this.width, this.length, this.height);
	}

	public void setRandomSize(int maxSize) {
		super.setRandomSize(maxSize);
		this.height = (int) (Math.random() * maxSize) + 1;
	}

	public int getArea() {
		int top = super.getArea();
		int left = this.width * this.height;
		int front = this.length * this.height;
		return 2 * (top + left + front);
	}

	public void copy(Cubic b) {
		super.copy(b);
		this.height = b.height;
	}

	public int getVolume() {
		return super.getArea() * this.height;
	}
}
