
package rectangle;

public class CreateCubic {
	public static void main(String[] args) {
		System.out.println("c1 = Cubic(10, 20, 30)");
		var c1 = new Cubic(10, 20, 30);
		System.out.println("c2 = Cubic(25)");
		var c2 = new Cubic(25);

		System.out.println();

		System.out.println("c1.printInfo()");
		c1.printInfo();
		System.out.println("c2.printInfo()");
		c2.printInfo();

		System.out.println();

        System.out.printf("c1.getVolume() -> %d\n", c1.getVolume());
        System.out.printf("c2.getVolume() -> %d\n", c2.getVolume());
		System.out.printf("c1.getArea() -> %d\n", c1.getArea());
		System.out.printf("c2.getArea() -> %d\n", c2.getArea());
		System.out.printf("c1.isLargerThan(c2) -> %b\n", c1.isLargerThan(c2));

		System.out.println();

		System.out.println("c2.setRandomSize(36), then c2.printInfo()");
		c2.setRandomSize(36);
		c2.printInfo();

		System.out.println("c1.copy(c2), then c1.printInfo()");
		c1.copy(c2);
		c1.printInfo();

		System.out.println("c1.rotate(), then c1.printInfo()");
		c1.rotate();
		c1.printInfo();

	}
}
