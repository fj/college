package rectangle;

public class Rectangle {
	int width;
	int length;

	Rectangle(int width, int length) {
		this.width = width;
		this.length = length;
	}

	Rectangle(int size) {
		this(size, size);
	}

	public void printInfo() {
		System.out.printf("width=%d, length=%d\n", this.width, this.length);
	}

	public int getArea() {
		return this.width * this.length;
	}

	public void setRandomSize(int maxSize) {
		this.width = (int) (Math.random() * maxSize) + 1;
		this.length = (int) (Math.random() * maxSize) + 1;
	}

	public boolean isLargerThan(Rectangle r) {
		return this.getArea() > r.getArea();
	}

	public void copy(Rectangle r) {
		this.width = r.width;
		this.length = r.length;
	}

	public void rotate() {
		int tmp = this.width;
		this.width = this.length;
		this.length = tmp;
	}
}
