package rectangle;

public class CreateRectangle {
	public static void main(String[] args) {
		System.out.println("r1 = Rectangle(10, 20)");
		var r1 = new Rectangle(10, 20);
		System.out.println("r2 = Rectangle(25)");
		var r2 = new Rectangle(25);

		System.out.println();

		System.out.println("r1.printInfo()");
		r1.printInfo();
		System.out.println("r2.printInfo()");
		r2.printInfo();

		System.out.println();

		System.out.printf("r1.getArea() -> %d\n", r1.getArea());
		System.out.printf("r2.getArea() -> %d\n", r2.getArea());
		System.out.printf("r1.isLargerThan(r2) -> %b\n", r1.isLargerThan(r2));

		System.out.println();

		System.out.println("r2.setRandomSize(36), then r2.printInfo()");
		r2.setRandomSize(36);
		r2.printInfo();

		System.out.println("r1.copy(r2), then r1.printInfo()");
		r1.copy(r2);
		r1.printInfo();

		System.out.println("r1.rotate(), then r1.printInfo()");
		r1.rotate();
		r1.printInfo();

	}
}
