package circle;

public class Circle {
	private double radius;
	private String color;

	Circle() {
		radius = 1.0;
		color = "red";
	}

	Circle(double r) {
		radius = r;
		color = "red";
	}

	Circle(double r, String c) {
		radius = r;
		color = c;
	}

	public double getRadius() {
		return radius;
	}

	public String getColor() {
		return color;
	}

	public double getArea() {
		return 3.14 * radius * radius;
	}
}
