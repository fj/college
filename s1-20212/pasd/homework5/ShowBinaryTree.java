public class ShowBinaryTree {
	/* 
	 * Method `prettyPrint(node)` di bawah digunakan sebagai wrapper
	 * dari method `prettyPrint(node, prefix, right)`. Method wrapper
	 * ini adalah method yang akan pertama kali dipanggil ketika ingin
	 * melakukan `prettyPrint`
	 */
	public static void prettyPrint(BinaryTreeNode node)
	{
		// Lakukan traversal dari `node.right` dengan prefix "" (level 0)
		if (node.right != null) {
			prettyPrint(node.right, "", true);
		}

		// Cetak label
		System.out.println(node.label);

		// Lakukan traversal dari `node.left` dengan prefix "" (level 0)
		if (node.left != null) {
			prettyPrint(node.left, "", false);
		}
	}

	/* 
	* Method `prettyPrint(node, prefix, right)` di bawah adalah method yang
	* dipanggil oleh wrapper method `prettyPrint(node)`. Method di bawah
	* digunakan untuk mencetak node yang berada di level > 0 dengan rapi.
	*/
	public static void prettyPrint(BinaryTreeNode node, String prefix, boolean right)
	{
		/*
		 * Method ini akan dipanggil secara rekursif dengan tiga argumen:
		 *  `node`  : node yang sedang dijelajahi
		 *  `prefix`: sebuah string yang menjadi prefix (untuk dicetak)
		 *  `right` : nilai boolean untuk membedakan apakah traversal sedang
		 *            dilakukan pada node yang berada di kiri atau kanan
		 *
		 * Adopted from:
		 * https://stackoverflow.com/questions/4965335/how-to-print-binary-
		 * tree-diagram-in-java
		 */

		// Lakukan traversal dari `node.right`
		if (node.right != null) {
			// Lakukan panggilan rekursif, tambahkan juga prefix yang sesuai
			// dengan nilai `right`. Semakin dalam rekursi yang dilakukan,
			// prefix akan semakin panjang.
			prettyPrint(node.right, prefix + (right ? "        " : " |      "), true);
		}

		// Cetak prefix
		System.out.print(prefix);
		// Cetak karakter untuk membuat sudut
		System.out.print(right ? " ." : " '");
		// Cetak garis
		System.out.print("----- ");
		// Cetak label
		System.out.println(node.label);

		// Lakukan traversal dari `node.left` dengan prefix yang sesuai
		if (node.left != null) {
			prettyPrint(node.left, prefix + (right ? " |      " : "        "), false);
		}
	}

	public static void main(String[] args)
	{
		// Buat sebuah tree dengan menggunakan
		// `BinaryTreeNode`
		BinaryTreeNode tree, p, q, r;
		p = new BinaryTreeNode("h", null, null);
		q = new BinaryTreeNode("i", null, null);
		p = new BinaryTreeNode("d", p, q);
		q = new BinaryTreeNode("j", null, null);
		q = new BinaryTreeNode("e", null, q);
		r = new BinaryTreeNode("b", p, q);

		p = new BinaryTreeNode("f", null, null);
		q = new BinaryTreeNode("k", null, null);
		q = new BinaryTreeNode("g", q, null);
		p = new BinaryTreeNode("c", p, q);

		tree = new BinaryTreeNode("a", r, p);
		// Cetak dengan menggunakan method `prettyPrint`
		prettyPrint(tree);
	}
}
