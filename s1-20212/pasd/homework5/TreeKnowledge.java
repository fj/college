import java.util.*;

public class TreeKnowledge {
	Scanner sc;	     // Sebuah Scanner
	BinaryTreeNode root; // Root dari knowledge tree

	public TreeKnowledge(Scanner sc)
	{
		// Atur nilai `this.sc` sesuai argumen constructor
		this.sc = sc;
		// Inisialisasi nilai root dengan sebuah node dengan label
		// "Komputer" (hafalkan "Komputer")
		this.root = new BinaryTreeNode("Komputer", null, null);
	}

	// Method untuk menerima input dan mengembalikan nilai
	// boolean true jika input adalah 'y' dan false jika 'n'
	boolean YorN()
	{
		while (true) {
			String s = sc.next();
			if (s.startsWith("y"))
				return true;
			else if (s.startsWith("n"))
				return false;
		}
	}

	// Method untuk memulai sesi learning interaktif
	public void run()
	{
		// Lakukan infinite loop (loop1)
		while (true) {
			// Atur nilai awal suatu variabel `x` menjadi `root`
			// (mulai traversal dari `root`)
			BinaryTreeNode x = root;

			System.out.println("Pikirkan sebuah benda!");

			// Lakukan infinite loop (loop2)
			while (true) {
				// Jika node yang sedang dijelajahi adalah external node (leaf),
				if (x.left == null && x.right == null) {
					// maka label node tersebut adalah nama benda, kemudian
					// tanyakan secara interaktif apakah benda tersebut sesuai
					// dengan apa yang dipikirkan user
					System.out.printf("Apakah benda tersebut %s (y/n)? ", x.label);

					// Jika sesuai, maka keluar dari loop2
					if (YorN()) {
						break;
					}

					sc.nextLine(); // Flush newline dari pemanggilan `YorN()`

					// Jika tebakan salah, tanyakan jawaban yang benar, lalu
					// simpan sebagai label pada sebuah node (`yNode`)
					System.out.println("Beritau jawabannya");
					BinaryTreeNode yNode = new BinaryTreeNode(sc.nextLine(),
					    null, null);
					// Buat juga node lain (`nNode`) dengan label `x.label`
					BinaryTreeNode nNode = new BinaryTreeNode(x.label,
					    null, null);

					// Input suatu pertanyaan sebagai label baru dari node `x`
					System.out.println("Masukkan pertanyaan yang jika dijawab "
					    + "YES adalah " + yNode.label
					    + " dan jika dijawab NO adalah " + nNode.label);
					x.label = sc.nextLine();
					// Atur `x.left` menjadi `yNode` dan `x.right` menjadi `nNode`
					x.left = yNode;
					x.right = nNode;

					break; // Keluar dari loop2
				}

				// Jika node `x` bukan leaf, maka tanyakan secara interaktif
				// pertanyaan yang tersimpan sebagai label pada node `x`
				System.out.print(x.label + " (y/n)? ");
				// Jelajahi node kiri jika user menjawab `y` dan kanan jika
				// user menjawab `n`
				if (YorN())
					x = x.left;
				else
					x = x.right;
			}

			// Keluar dari infinite loop (loop1) jika user menginputkan n
			System.out.print("Lanjut (y/n)? ");
			if (!YorN())
				break;
		}
		ShowBinaryTree.prettyPrint(root);
	}

	public static void main(String[] args)
	{
		TreeKnowledge tk = new TreeKnowledge(new Scanner(System.in));
		tk.run();
	}
}
