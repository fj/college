public class BinaryTreeNode {
	String label;
	BinaryTreeNode left;
	BinaryTreeNode right;

	public BinaryTreeNode(String label, BinaryTreeNode left, BinaryTreeNode right)
	{
		this.label = label;
		this.left = left;
		this.right = right;
	}

	public String toString()
	{
		return "[" + label + ", " + left + ", " + right + "]";
	}
}
