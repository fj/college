# Group Project ASD (Group 4)
Program ini bertujuan untuk menyelesaikan permasalahan logistik.
Ref: https://maximum-flow-problem.weebly.com/example-1.html

Kalau dipecah-pecah jadi beberapa komponen:
1. Implementasi graf
2. Implementasi algoritma Ford-Fulkerson
3. The `main()` method, yang akan memakai implementasi (1) dan (2)
4. Visualisasi (?)
5. PPT

To-do list:
1. Mapping implementasi (khusus?) seperti apa yang dibutuhkan
2. Bentuk input dan output

Question list (TBD):
1. Apakah boleh pakai builtin types (mis. LinkedList dari java.util)?
2. Bagaimana cara untuk memvisualisasikan graf (GUI/CLI)?

Poin penting:
Implementasi kita semua harus harmonis biar enak hehe
ntar bisa sambil liat2 juga code yg lain di replit
