# pylint: disable=all
import itertools
import sys


def read_data_146():
    data = []

    for a, b, x, y in itertools.product((False, True), repeat=4):
        da = (x and a) or (not x and not y)
        db = (x and b) or (not x and a)

        z = not x and b

        data.append((a, b, x, y, da, db, z))

    return data


def read_data_1410():
    pass


def print_md_table(data):
    for row in data:
        print('|$A$|$B$|$X$|$Y$|$D_A$|$D_B$|$Z$|')
        print('|---|---|---|---|---|---|---|---|')
        print('|'.join(row))


def print_dot(data):
    print("digraph G {")
    edge_list = {}
    data = [(int(c) for c in r) for r in data]
    for row in data:
        a, b, x, y, da, db, z = row
        src = f'{a}{b}'
        dst = f'{da}{db}'
        key = (src, dst)
        label = f'{x}{y}/{z}'
        if edge_list.get(key):
            edge_list[key] += f"\\n{label}"
        else:
            edge_list[key] = label
    for (src, dst), label in edge_list.items():
        print(f'{src} -> {dst} [label="{label}"]')
    print("}")


if __name__ == '__main__':
    output_type = sys.argv[1]
    data = read_data()
    if output_type == 'table':
        print_md_table(data)
    elif output_type == 'dot':
        print_dot(data)
