---
author: 'Faiz Unisa Jazadi -- `21/475298/PA/20563`'
date: 7 Juni 2022
subtitle: Sistem Digital (MII211602)
title: Tugas 2
---

# Soal 4-12

## Soal (a)

![](s2_p412a.svg)

## Soal (b)

![](s2_p412b.svg)

# Soal 4-13

## State equation

Menggunakan K-map, didapatkan state equation berikut.

$$ D_A = \bar{B}X + A\bar{X} $$

$$ D_B = AX + B\bar{X} $$

## Sequential Circuit

![](s2_p413.svg)

# Soal 4-15

## Soal (a): state table

  Present   Next ($x=0$)   Next ($x=1$)   Output
  --------- -------------- -------------- --------
  A         B              D              0
  B         D              C              0
  C         A              F              0
  D         F              C              1
  E         C              E              1
  F         E              F              1

## Soal (b)

### State assignment

We assign states using 3-bit encoding: $A$, $B$, $C$ (last bit as output).

  Symbol   $u$   $v$   $w$ (output)
  -------- ----- ----- --------------
  A        0     0     0
  B        0     1     0
  C        1     0     0
  D        0     0     1
  E        0     1     1
  F        1     0     1

### Encoded state table

  $A$   $B$   $C$   $X$   $D_A$   $D_B$   $D_C$
  ----- ----- ----- ----- ------- ------- -------
  0     0     0     0     0       1       0
  0     0     0     1     0       0       1
  0     0     1     0     1       0       1
  0     0     1     1     1       0       0
  0     1     0     0     0       0       1
  0     1     0     1     1       0       0
  0     1     1     0     1       0       0
  0     1     1     1     0       1       1
  1     0     0     0     0       0       0
  1     0     0     1     1       0       1
  1     0     1     0     0       1       1
  1     0     1     1     1       0       1
  1     1     0     0     d       d       d
  1     1     0     1     d       d       d
  1     1     1     0     d       d       d
  1     1     1     1     d       d       d

## Soal (c): sequential circuit

Dengan menggunakan K-map, diperoleh persamaan untuk $D_A$, $D_B$, dan $D_C$.

-   $D_A = B\bar{C}X + AX + \overline{AB}C + \bar{A}C\bar{X}$

-   $D_B = \overline{ABCX} + BCX$

-   $D_C = \overline{BC}X + \bar{B}C\bar{X} + B\overline{CX} + BCX + AX$

![](s2_p415c.circ.svg)

## Soal (d): one-hot encoding

### Encoded state table

  $A$   $B$   $C$   $D$   $E$   $F$   $X$   $D_A$   $D_B$   $D_C$   $D_D$   $D_E$   $D_F$   $Y$
  ----- ----- ----- ----- ----- ----- ----- ------- ------- ------- ------- ------- ------- -----
  1     0     0     0     0     0     0     0       1       0       0       0       0       0
  1     0     0     0     0     0     1     0       0       0       1       0       0       0
  0     1     0     0     0     0     0     0       0       0       1       0       0       0
  0     1     0     0     0     0     1     0       0       1       0       0       0       0
  0     0     1     0     0     0     0     1       0       0       0       0       0       0
  0     0     1     0     0     0     1     0       0       0       0       0       1       1
  0     0     0     1     0     0     0     0       0       0       0       0       1       1
  0     0     0     1     0     0     1     0       0       1       0       0       0       1
  0     0     0     0     1     0     0     0       0       1       0       0       0       1
  0     0     0     0     1     0     1     0       0       0       0       1       0       1
  0     0     0     0     0     1     0     0       0       0       0       1       0       1
  0     0     0     0     0     1     1     0       0       0       0       0       1       1

Berdasarkan state table, didapatkan persamaaan-persamaan berikut.

-   $D_A = C\bar{X}$
-   $D_B = A\bar{X}$
-   $D_C = (B+D)X + E\bar{X}$
-   $D_D = AX + B\bar{X}$
-   $D_E = EX + F\bar{X}$
-   $D_F = (C+F)X + D\bar{X}$
-   $Y = (D + E + F)$

### Sequential circuit

![](s2_p415d.circ.svg)
