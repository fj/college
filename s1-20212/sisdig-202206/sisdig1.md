---
author: 'Faiz Unisa Jazadi -- `21/475298/PA/20563`'
date: 7 Juni 2022
subtitle: Sistem Digital (MII211602)
title: Tugas 1
---

# Soal 4-4

![](s1_p44.png)

# Soal 4-6

$$D_A = XA + \overline{XY},\  D_B = XB + \bar{X}A,\  Z = \bar{X}B$$

## Logic diagram

![](s1_p46.circ.svg)

## State table

  $A$   $B$   $X$   $Y$   $D_A$   $D_B$   $Z$
  ----- ----- ----- ----- ------- ------- -----
  0     0     0     0     1       0       0
  0     0     0     1     0       0       0
  0     0     1     0     0       0       0
  0     0     1     1     0       0       0
  0     1     0     0     1       0       1
  0     1     0     1     0       0       1
  0     1     1     0     0       1       0
  0     1     1     1     0       1       0
  1     0     0     0     1       1       0
  1     0     0     1     0       1       0
  1     0     1     0     1       0       0
  1     0     1     1     1       0       0
  1     1     0     0     1       1       1
  1     1     0     1     0       1       1
  1     1     1     0     1       1       0
  1     1     1     1     1       1       0

## State diagram

![](s1_p46.svg){width="250px"}

## Mealy or Moore?

Circuit di atas digolongkan ke dalam Mealy machine karena output ($Z$) juga
bergantung pada input ($B$).

# Soal 4-11

## State table

  $A$   $B$   $X$   $Y$   $D_A$   $D_B$   $Z$
  ----- ----- ----- ----- ------- ------- -----
  0     0     0     0     1       0       0
  0     0     0     1     1       1       1
  0     0     1     0     1       1       0
  0     0     1     1     1       1       1
  0     1     0     0     0       1       1
  0     1     0     1     0       0       0
  0     1     1     0     0       0       1
  0     1     1     1     0       0       0
  1     0     0     0     1       1       1
  1     0     0     1     0       1       1
  1     0     1     0     0       1       0
  1     0     1     1     1       0       0
  1     1     0     0     0       0       0
  1     1     0     1     0       1       0
  1     1     1     0     1       0       1
  1     1     1     1     1       1       1

## State diagram

![](s1_p410.svg){width="250px"}

# Soal 4-11

## State table

  $A$   $B$   $X$   $D_A$   $D_B$   $Y$
  ----- ----- ----- ------- ------- -----
  0     0     0     0       1       0
  0     0     1     0       0       1
  0     1     0     1       1       1
  0     1     1     1       0       0
  1     0     0     0       0       1
  1     0     1     0       1       0
  1     1     0     1       0       0
  1     1     1     1       1       1

## State diagram

![](s1_p411.svg){width="250px"}
