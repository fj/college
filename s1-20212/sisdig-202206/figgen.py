# pylint: disable=all
import itertools
import sys


def read_data46():
    data = []

    for a, b, x, y in itertools.product((0, 1), repeat=4):
        da = int((x and a) or (not x and not y))
        db = int((x and b) or (not x and a))

        z = int(not x and b)

        data.append((a, b, x, y, da, db, z))

    return data


def read_data410():
    lda = [1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 1]
    ldb = [0, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 1, 0, 1]
    lz = [0, 1, 0, 1, 1, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 1]
    data = list(zip(lda, ldb, lz))
    for i, (a, b, x, y) in enumerate(itertools.product((0, 1), repeat=4)):
        data[i] = (a, b, x, y, *data[i])
    return data


def read_data411():
    data = []

    for (a, b, x) in itertools.product((0, 1), repeat=3):
        da = b
        db = not (x ^ a)
        y = (x ^ a) ^ b
        da, db, y = int(da), int(db), int(y)
        data.append((a, b, x, da, db, y))

    return data


def print_md_table_x(data):
    print('|$A$|$B$|$X$|$D_A$|$D_B$|$Y$|')
    print('|---|---|---|-----|-----|---|')
    for row in data:
        print('|'.join(str(c) for c in row))


def print_dot_x(data):
    print("digraph G {")
    edge_list = {}
    for row in data:
        a, b, x, da, db, y = row
        src = f'{a}{b}'
        dst = f'{da}{db}'
        key = (src, dst)
        label = f'{x}/{y}'
        if edge_list.get(key):
            edge_list[key] += f"\\n{label}"
        else:
            edge_list[key] = label
    for (src, dst), label in edge_list.items():
        print(f'{src} -> {dst} [label="{label}"]')
    print("}")


def print_md_table_xy(data):
    print('|$A$|$B$|$X$|$Y$|$D_A$|$D_B$|$Z$|')
    print('|---|---|---|---|-----|-----|---|')
    for row in data:
        print('|'.join(str(c) for c in row))


def print_dot_xy(data):
    print("digraph G {")
    edge_list = {}
    for row in data:
        a, b, x, y, da, db, z = row
        src = f'{a}{b}'
        dst = f'{da}{db}'
        key = (src, dst)
        label = f'{x}{y}/{z}'
        if edge_list.get(key):
            edge_list[key] += f"\\n{label}"
        else:
            edge_list[key] = label
    for (src, dst), label in edge_list.items():
        print(f'{src} -> {dst} [label="{label}"]')
    print("}")


if __name__ == '__main__':
    func = locals()[sys.argv[1]]
    arg_func = locals()[sys.argv[2]]

    func(arg_func())
