---
author:
- Faiz Jazadi
date:
- 'June 3rd, 2022'
subtitle: Final Exam Preparation Materials
title: Sistem Digital
---

# Sequential Circuits

## Formal Definition

Sequential circuits are almost the same as combinational circuits, except that
it has a memory component.

-   Combinational circuits: present input $\rightarrow$ present output
-   Sequential circuits: present input + **past output** $\rightarrow$ present
    output

A memory element is required to store the past output(s). A memory component can
be a: flip-flop, latch.

Sequential circuits example: counter

        0 + 1 -> 1
        1 + 1 -> 2
        2 + 1 -> 3
        ^   ^    ^
        |   |    '- present output
        |   '- present input
        '- past output

## SR Latch

SR means:

-   `R` means reset $\rightarrow$ set the output (Q) to 0
-   `S` means set $\rightarrow$ set the output (Q) to 1

Basic storage element is called latch. Two types of SR latches:

-   NOR
-   NAND

A latch, latches `0` or `1`. Similar to door latching, but a latch is quite
complicated because the S and R inputs may change accidentally/incidentally. We
can overcome this problem by utilizing a *clock*.

### NOR SR Latch

        R -- NOR --- Q
          |       ....|
          '_______|__.
           .------' |
        S -- NOR -- Q'

#### NOR truth table

  A   B   Y
  --- --- ---
  0   0   1
  0   1   0
  1   0   0
  1   1   0

#### Possible Cases

-   Case 1 (resetting the memory)

    -   $S = 0$ and $R = 1$, then $Q = 0$ and $\bar{Q} = 1$ We know that $Q = 0$
        because when either inputs are $1$, a NOR gate will always result in
        $0$.

    -   We remove the input, so $S = 0$ and $R = 1$, the previous $Q$ is $0$, so
        $\overline{R+Q} = Q = 1$ (this is called the memory state)

-   Case 2 (setting the memory)

    -   $S = 1$ and $R = 0$, then $\bar{Q} = 0 \rightarrow Q = 1$
    -   Removing the input, we then got $\overline{Q + R} = 1$ (the memory item
        that $Q = 1$ was saved)

-   Case 3 (unexpected behavior)

    -   We set both $S = R = 1$

    -   Depending on which part of the circuit is analyzed first, the value set
        the stored memory can vary, it can either be $1$ or $0$ (unstable)

    -   Due to this behavior, we don't use this case

#### Latch truth table

  S   R   Q   Q    What it does
  --- --- --- ---- ---------------------------------
  1   0   1   0    Setting the memory to $1$
  0   1   0   1    Resetting the memory to $0$
  0   0   m   !m   Loads the stored memory content
  1   1   ?   ?    Unexpected behavior

### NAND SR Latch

The concept of the NAND SR latch is very similar to the NOR latch, except the
swapped placement of the $S$ and the $R$ input. Also, in NAND SR latch, memory
is loaded by setting $S = R = 1$. Setting $S = R = 0$ will raise an unexpected
behavior.

## Clock

Clocking is required to time the cirquit. A clock is actually a signal that goes
from low to high.

      L H L H L
      ---
       |
       T

$T$ (period) is the amount of time from low to high and from high to low (one
cycle).

$$ f = \frac{1}{\text{time period}} $$

To increase speed, we have to increase the frequency, therefore we have to
reduce the time period.

A duty cycle is a fraction of the period where the system is active. A 50% duty
cycle means the clock is high 50% of the time.

## Triggering Methods

There are two types of triggering methods.

-   Level triggering: output changes during high or low level period
-   Edge triggering: output changes during the transition between low to high
    (positive edge triggering) or from high to low (negative edge triggering)

## Achieving Edge Triggering

Edge triggering can be achieved using *differentiator circuit*.

## SR Latch vs Flip-Flop

The difference between both is that latch is level sensitive whereas flip-flop
is edge sensitive.

## Controlled SR Flip-Flop

Basically, there is an introduced clock and two additional NAND gates. For a NOR
SR flip-flop:

      S    ---.
               |!&|--------- S* ....... Q
      CLK  ---'                 .......
             |.......           .......
                     |!&|--- R* ....... !Q
      R -------------'

$S$ and $R$ input will only be fed as their "star" if and only if $CLK$ is HIGH.
Otherwise, it will always be 0, 0 (memory).

This controlled SR flip-flop is often represented using a box with three input
($S$, $R$, $CLK$) and two outputs (Q and $\bar{Q}$). A $>$ symbol in the box
means that the flip-flop is edge-triggered.

## Tables

-   Truth table: $Clk$, $S$, $R$, $Q_{n+1}$

-   Characteristic table: $Q_n$, $S$, $R$, $Q_{n+1}$

-   Excitation table: ${Q_n}$, $Q_{n+1}$, $S$, $R$

## D Flip-Flop

Since we already have a clock, and $S$ and $R$ can either only be $0, 1$ or
$1, 0$ (the inverse of each other), we can simply merge both input pins into
one. With the pins merged, it is a D flip-flop. This type flip-flop is the most
common.

### How to merge the two inputs?

Previously, we have the following setup.

      S --- .....
            .....
      R --- .....

Add an inverter to S and connect that to R.

      D --- .....
        |   .....
        -!- .....

By merging the two pins, the previously unused input set ($S=1, R=1$) is not
possible.

## JK Flip-Flop

JK flip-flop is a modified version of the SR flip-flop. Previously, $S=1, R=1$
input set is unused, but in the JK flip-flop the input set is used to toggle the
next state according to the present state.

Previously in the SR flip-flop, we have $\overline{S C}$ and $\overline{R C}$.
In the JK flip-flop, we added another input to both of the NANDs at the inputs.

$$S^{*} = \overline{S C \bar{Q}}$$ $$R^{*} = \overline{R C Q}$$

### Race condition

By utilizing another input to the two NANDs at the input, we can achieve a way
to toggle the current state. But, when the clock is set to HIGH, both inputs are
also HIGH, with level-triggering, the output will always toggle making a race
condition. Three ways to overcome this problem and achieve toggling:

-   Make $\frac{T}{2} < \text{propagation time}$ (not used, impractical)
-   Use edge triggering
-   Master-slave operation

#### Master-slave operation

To overcome the race condition as mentioned above, we add another flip-flop as a
slave. So, there would be two flip-flops. The first one is master and the next
one is the slave. The clock for the slave flip-flop is the complement of the
clock input for the master. Only one flip-flop is active at any given time.

The effect produced by using the master-slave operation is similar to the
negative edge-triggered flip-flop.

## T Flip-Flop

The T is for toggle. This type of flip-flop can only toggle.

T flip-flop is made by merging two inputs in the JK flip-flop into one pin
($T$).

There is no race condition.
