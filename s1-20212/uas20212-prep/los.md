# LOS List

$$sr = \text{success rate} = \text{confidence} + \text{easing factor}$$

  Course name                          Credit   $sr$
  ------------------------------------ -------- ------
  Organisasi dan Arsitektur Komputer   2 sks    0.3
  Matematika Diskrit                   3 sks    0.4
  Integral dan Persamaan Diferensial   2 sks    0.4
  Algoritma dan Struktur Data          3 sks    0.5
  Sistem Digital                       2 sks    0.7
  Pancasila                            2 sks    0.8
  Kewarganegaraan                      2 sks    0.8
