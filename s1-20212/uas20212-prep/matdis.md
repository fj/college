---
author: 'Faiz Unisa Jazadi -- `21/475298/PA/20563`'
date: 10 Juni 2022
subtitle: Final Exam Preparation Materials
title: Matematika Diskrit
---

# Number theory

## Problem 1

Find the multiplicative inverse of $15$ on $(mod\ 23)$

### Solution

We need to find $x$ such that

$$ x 15 \equiv 1\ (mod\ 23) $$

Since $15$ and $23$ are co-primes ($gcd(15, 23) = 1$) and $23$ is a prime
number, we can use Euler's theorem to find the multiplicative inverse.

```{=tex}
\begin{align*}
y^{\phi(m)}   &\equiv 1\ (mod\ m) \\
15^{\phi(23)} &\equiv 1\ (mod\ 23) \\
15^{22}       &\equiv 1\ (mod\ 23) \\
15(15^{21})   &\equiv 1\ (mod\ 23)
\end{align*}
```
We can see that $x \equiv 15^{21} (mod\ 23)$. Then, using modular
exponentiation, we know that:

```{=tex}
\begin{align*}
15     &\equiv 15  &\ (mod\ 23) \\
15^{2} &\equiv 18  &\ (mod\ 23) \\
15^{4} &\equiv 2   &\ (mod\ 23) \\
15^{8} &\equiv 4   &\ (mod\ 23) \\
15^{16}&\equiv 16  &\ (mod\ 23) \\
15^{21} &\equiv (15^{16}) (15^{4}) (15) &\ (mod\ 23) \\
15^{21} &\equiv 20 &\ (mod\ 23)
\end{align*}
```
$\therefore\ A^{-1} = 20$

## Problem 2

Determine the value of $\phi(4477)$!

### Solution

Bilangan $4477$ dapat dipecah menjadi perkalian tiga bilangan prima seperti
berikut.

$$ 4477 = (11^2)(37) $$

Sesuai Teorema Fermat, $\phi(p^n) = (p^{n-1})p - 1$. Maka,

```{=tex}
\begin{align*}
\phi(4477) & = \phi(11^2)\ \phi(37) \\
           & = (11(10))(36) \\
           & = 3960
\end{align*}
```
# Sum, Product, and Asymptotic

## Problem 3

Determine the closed-form of the sum of the first $n$ terms of:

$$ 1.4 + 2.5 + 3.6 + 4.7 + ... + \text{term}_n $$

### Solution

First, we change the sequence into a summation form, then simplify it.

```{=tex}
\begin{align*}
\sum_{i=1} ^{n} i(i + 3) &= \sum_{i=1} ^{n} i^{2} + 3i \\
                         &= \sum_{i=1} ^{n} i^{2} + 3 \sum_{i=1} ^{n} i
\end{align*}
```
Using the known closed-form of the summations, we can derive te closed-form for
the sequence.

```{=tex}
\begin{align*}
\sum_{i=1} ^{n} i^{2} + 3 \sum_{i=1} ^{n} i
= \frac{n(n+1)(2n+1)}{6} + 3 \left(\frac{n(n+1)}{2}\right)
\end{align*}
```
# Recurrency

## Problem 4

Solve for the following linear recurrence.

$$
f(x) = \begin{cases}
n                            & \text{if } n=1,2,3 \\
6f(n-1) - 12f(n-2) + 8f(n-3) & \text{if } n \geq 4
\end{cases}
$$

### Solution

We know that $f(x)$ is a homogeneous linear recurrence problem since all terms
has $f(x)$. First, we transform the recurrent form into the generic answer form.

```{=tex}
\begin{align*}
f(n) &= 6f(n-1) - 12f(n-2) + 8f(n-3) \\
0 &= f(n) - 6f(n-1) + 12f(n-2) -8f(n-3) \\
\end{align*}
```
Then, we can derive the following characteristic equation and solve for $a$.

```{=tex}
\begin{align*}
a^3 - 6a^2 + 12a - 8 &= 0 \\
(a-2)^3 &= 0 \\
\boxed{\therefore\ a_1, a_2, a_3 = 0} \\
\end{align*}
```
The solution for the linear recurrence must be in a form of:

$$ f(n) = n^0 c_1 a_{1}^{n} + n^1 c_2 a_2^{n} + n^2 c_3 a_3^{n} $$

Plug $a_1$, $a_2$, and $a_3$.

$$ f(n) = c_1 2^n + n c_2 2^n + n^2 c_3 2^n $$

Then, we solve for $c_1$, $c_2$, and $c_3$ using the base cases of the linear
recurrence.

```{=tex}
\begin{align*}
f(1) = 1 &= 2c_1 + 2c_2 + 2c_3 \\
f(2) = 2 &= 4c_1 + 8c_2 + 16c_3 \\
f(3) = 3 &= 8c_1 + 24c_2 + 72c_3
\end{align*}
```
After solving the above linear system of three variables, we get the solution
for the linear recurrence.

$$ c_1 = \frac{3}{8}\,, c_2 = \frac{3}{16}\,, c_3 = - \frac{1}{16} $$

Plug back the constants to the generic answer and $f(x)$ is solved.

# Counting

## Problem 5

Given a multinomial $(2x^3 - 3xy^2 + z^2)^6$. Determine the term and its
coefficient that has $x^{11}$ and $y^4$.


### Solution
