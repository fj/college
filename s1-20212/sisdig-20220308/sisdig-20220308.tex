\documentclass[a4paper]{article}

\usepackage{circuitikz}
\usepackage{amsmath}
\usepackage{amssymb}
\PassOptionsToPackage{hyphens}{url}\usepackage[hidelinks]{hyperref}

\title{Tugas Sistem Digital (MII211602) \\ \textbf{NAND dan NOR sebagai Gerbang Logika Universal}}
\author{Faiz Unisa Jazadi \\ \small{21/475298/PA/20563}}
\date{8 Maret 2022}

\ctikzset{
	logic ports=ieee,
	logic ports/fill=lightgray,
	logic ports/scale=0.8
}


\begin{document}
	\maketitle
	
	Gerbang NAND dan NOR dapat disebut sebagai gerbang logika yang
	bersifat universal karena kombinasi keduanya dapat membentuk gerbang-gerbang
	operasi dasar. Dengan kata lain, kombinasi gerbang NAND atau kombinasi gerbang
	XOR dapat diubah/dikonversi menjadi inverter, gerbang OR, gerbang AND,
	atau gerbang-gerbang operasi dasar lainnya.
	
	Suatu himpunan konektor logis atau operator Boolean disebut sebagai
	\textit{functionally complete} apabila kombinasi anggota-anggotanya dapat
	mengekspresikan seluruh kemungkinan tabel kebenaran. Salah satu contoh himpunan
	tersebut adalah $\{ AND, NOT \}$.
	Gerbang logika universal adalah himpunan tunggal yang bersifat \textit{functionally complete}. Dalam sistem digital, hanya terdapat dua gerbang logika universal,
	yaitu NAND dan NOR. Dalam artikel ini, akan dijabarkan tentang konversi keduanya
	menjadi beberapa gerbang operasi dasar.
	
	\section{Konversi Gerbang NAND} \label{nand-conv}
	Pada bagian ini, akan dijabarkan tentang bagaimana cara membentuk
	gerbang-gerbang operasi dasar dengan mengkombinasikan satu atau
	lebih gerbang NAND.
	
	\subsection{Gerbang NOT} \label{nand-not}
	Gerbang NAND adalah inversi gerbang AND. Jika kedua input pada gerbang NAND disamakan,
	maka nilai kebenarannya akan sama dengan gerbang NOT.
	
	\begin{center}
		\begin{circuitikz}
			\draw
			(1, 0) node[nand port] (NANDa) {};
			
			\draw (0, 0) |- (NANDa.in 1);
			\draw (0, 0) |- (NANDa.in 2);
			
			\draw (0, 0) -- ++(left: 0.5) node[left]{$A$};
			\draw (NANDa.out) -- ++(right: 0.5) node[right]{$\bar{A}$};
		\end{circuitikz}
	\end{center}
	
	\subsection{Gerbang AND} \label{nand-and}
	Gerbang AND dapat dirangkai dengan menginversi gerbang NAND.
	Gerbang NAND diinversi dengan menggunakan gerbang NOT yang sebelumnya
	sudah dirangkai (bagian \ref{nand-not}).
	
	\begin{center}
		\begin{circuitikz}
			\draw
			(1, 0) node[nand port] (NANDa) {}
			(3, 0) node[nand port] (NANDb) {};
			
			\draw (NANDa.out) -| (NANDb.in 1);
			\draw (NANDa.out) -| (NANDb.in 2);
			
			\draw
			(NANDa.in 1) -- ++(left: 0.5) node[left]{$A$}
			(NANDa.in 2) -- ++(left: 0.5) node[left]{$B$}
			(NANDb.out) -- ++(right: 0.5) node[right]{$AB$};
		\end{circuitikz}
	\end{center}
	
	\subsection{Gerbang OR} \label{nand-or}
	Sesuai hukum De Morgan,
	\[
	\overline{A + B} \implies \bar{A} \cdot \bar{B}
	\]
	Maka,
	\[
	A + B \implies \overline{\bar{A} \cdot \bar{B}}
	\]
	
	Dengan menggunakan rangkaian gerbang NOT (bagian \ref{nand-not})
	an AND (bagian \ref{nand-and}) sebelumnya, berikut adalah rangkaian gerbang OR.
	
	\begin{center}
		\begin{circuitikz}
			\draw
			(1, 0) node[nand port] (NANDa) {}
			(1, 2) node[nand port] (NANDb) {}
			(3, 1) node[nand port] (NANDc) {};
			
			\draw
			(0, 0) |- (NANDa.in 1)
			(0, 0) |- (NANDa.in 2)
			(0, 2) |- (NANDb.in 1)
			(0, 2) |- (NANDb.in 2);
			
			\draw
			(0, 0) -- ++(left: 0.5) node[left]{$B$}
			(0, 2) -- ++(left: 0.5) node[left]{$A$};
			
			\draw
			(NANDa.out) -| (NANDc.in 2)
			(NANDb.out) -| (NANDc.in 1);
			
			\draw (NANDc.out) -- ++(right: 0.5) node[right]{$A + B$};
		\end{circuitikz}
	\end{center}
	
	\subsection{Gerbang NOR} \label{nand-nor}
	Gerbang NOR dibuat dengan menambahkan gerbang NOT atau menginversi
	keluaran dari rangkaian OR (bagian \ref{nand-or}).
	
	\begin{center}
		\begin{circuitikz}
			\draw
			(1, 0) node[nand port] (NANDa) {}
			(1, 2) node[nand port] (NANDb) {}
			(3, 1) node[nand port] (NANDc) {}
			(5, 1) node[nand port] (NANDd) {};
			
			\draw
			(0, 0) |- (NANDa.in 1)
			(0, 0) |- (NANDa.in 2)
			(0, 2) |- (NANDb.in 1)
			(0, 2) |- (NANDb.in 2);
			
			\draw
			(0, 0) -- ++(left: 0.5) node[left]{$B$}
			(0, 2) -- ++(left: 0.5) node[left]{$A$};
			
			\draw
			(NANDa.out) -| (NANDc.in 2)
			(NANDb.out) -| (NANDc.in 1)
			(NANDc.out) -| (NANDd.in 1)
			(NANDc.out) -| (NANDd.in 2);
			
			\draw (NANDd.out) -- ++(right: 0.5) node[right]{$\overline{A + B}$};
		\end{circuitikz}
	\end{center}
	
	\subsection{Gerbang XOR} \label{nand-xor}
	Operasi XOR didefinisikan sebagai berikut.
	\[
	A \oplus B = A\bar{B} + \bar{A}B
	\]
	Kemudian, disederhanakan secara aljabar sehingga memaksimalkan penggunaan gerbang NAND.
	\begin{align*}
		A \oplus B & = A\bar{B} + \bar{A}B                                                                          \\
		& = A\bar{A} + A\bar{B} + \bar{A}B + B\bar{B}                                                    \\
		& = A(\bar{A}+\bar{B}) + (\bar{A}+\bar{B})B                                                      \\
		& = AC + CB                                                                & C = \bar{A}+\bar{B} \\
		& = \overline{\overline{AC} \cdot \overline{CB}}                                                 \\
		& = \overline{\overline{A \overline{AB}} \cdot \overline{B \overline{AB}}}
	\end{align*}
	
	Dengan menggunakan rangkaian AND (bagian \ref{nand-and}) dan OR (bagian \ref{nand-or}) yang sudah dibentuk sebelumnya,
	berikut adalah rangkaian untuk gerbang XOR.
	
	\begin{center}
		\begin{circuitikz}
			\draw
			(3, 0.75) node[nand port] (NANDa) {}
			(5, 0) node[nand port] (NANDb) {}
			(5, 1.5) node[nand port] (NANDc) {}
			(7, 0.75) node[nand port] (NANDd) {};
			
			\draw
			(NANDc.in 1) ++(left:3) coordinate (inputA)
			(NANDb.in 2) ++(left:3) coordinate (inputB)
			(inputA) ++(right: 1) coordinate (inputAh)
			(inputB) ++(right: 1) coordinate (inputBh);
			
			\draw
			(inputA) -- (NANDc.in 1)
			(inputB) -- (NANDb.in 2);
			
			\draw
			(NANDa.in 1) -- (inputAh)
			(NANDa.in 2) -- (inputBh);
			
			\draw
			(NANDa.out) -| (NANDb.in 1)
			(NANDa.out) -| (NANDc.in 2)
			(NANDc.out) -| (NANDd.in 1)
			(NANDb.out) -| (NANDd.in 2);
			
			\draw
			(inputA) -- ++ (left: 0) node[left] {$A$}
			(inputB) -- ++ (left: 0) node[left] {$B$}
			(NANDd.out) -- ++ (1, 0) node[right] {$A \oplus B$};
		\end{circuitikz}
	\end{center}
	
	\subsection{Gerbang XNOR} \label{nand-xnor}
	Gerbang XNOR memiliki rangkaian yang cukup mirip dengan gerbang NOR.
	Bagian yang berbeda adalah penambahan rangkaian NOT di keluaran akhir.
	
	\begin{center}
		\begin{circuitikz}
			\draw
			(3, 0.75) node[nand port] (NANDa) {}
			(5, 0) node[nand port] (NANDb) {}
			(5, 1.5) node[nand port] (NANDc) {}
			(7, 0.75) node[nand port] (NANDd) {}
			(9, 0.75) node[nand port] (NANDe) {};
			
			\draw
			(NANDc.in 1) ++(left:3) coordinate (inputA)
			(NANDb.in 2) ++(left:3) coordinate (inputB)
			(inputA) ++(right: 1) coordinate (inputAh)
			(inputB) ++(right: 1) coordinate (inputBh);
			
			\draw
			(inputA) -- (NANDc.in 1)
			(inputB) -- (NANDb.in 2);
			
			\draw
			(NANDa.in 1) -- (inputAh)
			(NANDa.in 2) -- (inputBh);
			
			\draw
			(NANDa.out) -| (NANDb.in 1)
			(NANDa.out) -| (NANDc.in 2)
			(NANDc.out) -| (NANDd.in 1)
			(NANDb.out) -| (NANDd.in 2)
			(NANDd.out) -| (NANDe.in 1)
			(NANDd.out) -| (NANDe.in 2);
			
			\draw
			(inputA) -- ++ (left: 0) node[left] {$A$}
			(inputB) -- ++ (left: 0) node[left] {$B$}
			(NANDe.out) -- ++ (1, 0) node[right] {$\overline{A \oplus B}$};
			
		\end{circuitikz}
	\end{center}
	
	\section{Konversi Gerbang NOR} \label{nor-conv}
	Pada bagian ini, akan dijabarkan tentang bagaimana cara membentuk
	gerbang\-gerbang operasi dasar dengan mengkombinasikan satu atau
	lebih gerbang NOR. Sebenarnya, konversi gerbang NOR memiliki banyak
	kemiripan dengan konversi gerbang NAND. Sebagai contoh, terdapat kesamaan
	struktur rangkaian gerbang AND (bagian \ref{nand-and}) dan gerbang
	OR (bagian \ref{nor-and}).
	
	\subsection{Gerbang NOT} \label{nor-not}
	Gerbang NOR adalah inversi gerbang OR. Jika kedua input pada gerbang NOR disamakan,
	maka nilai kebenarannya akan sama dengan gerbang NOT (bagian \ref{nor-not}).
	
	\begin{center}
		\begin{circuitikz}
			\draw
			(1, 0) node[nor port] (NORa) {};
			
			\draw (0, 0) |- (NORa.in 1);
			\draw (0, 0) |- (NORa.in 2);
			
			\draw (0, 0) -- ++(left: 0.5) node[left]{$A$};
			\draw (NORa.out) -- ++(right: 0.5) node[right]{$\bar{A}$};
		\end{circuitikz}
	\end{center}
	
	\subsection{Gerbang OR} \label{nor-or}
	Gerbang OR dapat dirangkai dengan menginversi gerbang NOR.
	Gerbang NOR diinversi dengan menggunakan gerbang NOT yang sebelumnya
	sudah dirangkai (bagian \ref{nor-not}).
	
	\begin{center}
		\begin{circuitikz}
			\draw
			(1, 0) node[nor port] (NORa) {}
			(3, 0) node[nor port] (NORb) {};
			
			\draw (NORa.out) |- (NORb.in 1);
			\draw (NORa.out) |- (NORb.in 2);
			
			\draw
			(NORa.in 1) -- ++(left: 0.5) node[left]{$A$}
			(NORa.in 2) -- ++(left: 0.5) node[left]{$B$}
			(NORb.out) -- ++(right: 0.5) node[right]{$AB$};
		\end{circuitikz}
	\end{center}
	
	\subsection{Gerbang AND} \label{nor-and}
	Sesuai hukum De Morgan:
	\[
	\overline{AB} \implies \bar{A} + \bar{B}
	\]
	Maka:
	\[
	AB \implies \overline{\bar{A} + \bar{B}}
	\]
	
	Dengan menggunakan rangkaian gerbang NOT (bagian \ref{nor-not}) dan
	OR (bagian \ref{nor-or}) yang sudah dirangkai sebelumnya, berikut
	adalah rangkaian gerbang AND.
	
	\begin{center}
		\begin{circuitikz}
			\draw
			(1, 0) node[nor port] (NORa) {}
			(1, 2) node[nor port] (NORb) {}
			(3, 1) node[nor port] (NORc) {};
			
			\draw
			(0, 0) |- (NORa.in 1)
			(0, 0) |- (NORa.in 2)
			(0, 2) |- (NORb.in 1)
			(0, 2) |- (NORb.in 2);
			
			\draw
			(0, 0) -- ++(left: 0.5) node[left]{$B$}
			(0, 2) -- ++(left: 0.5) node[left]{$A$};
			
			\draw
			(NORa.out) -| (NORc.in 2)
			(NORb.out) -| (NORc.in 1);
			
			\draw (NORc.out) -- ++(right: 0.5) node[right]{$AB$};
		\end{circuitikz}
	\end{center}
	
	\subsection{Gerbang NAND} \label{nor-nand}
	Gerbang NAND dibuat dengan menambahkan inverter pada rangkaian
	gerbang AND (bagian \ref{nor-and}).
	
	\begin{center}
		\begin{circuitikz}
			\draw
			(1, 0) node[nor port] (NORa) {}
			(1, 2) node[nor port] (NORb) {}
			(3, 1) node[nor port] (NORc) {}
			(5, 1) node[nor port] (NORd) {};
			
			\draw
			(0, 0) |- (NORa.in 1)
			(0, 0) |- (NORa.in 2)
			(0, 2) |- (NORb.in 1)
			(0, 2) |- (NORb.in 2);
			
			\draw
			(0, 0) -- ++(left: 0.5) node[left]{$B$}
			(0, 2) -- ++(left: 0.5) node[left]{$A$};
			
			\draw
			(NORa.out) -| (NORc.in 2)
			(NORb.out) -| (NORc.in 1)
			(NORc.out) -| (NORd.in 1)
			(NORc.out) -| (NORd.in 2);
			
			\draw (NORd.out) -- ++(right: 0.5) node[right]{$\overline{AB}$};
		\end{circuitikz}
	\end{center}
	
	\subsection{Gerbang XNOR} \label{nor-xnor}
	Operasi XNOR didefinisikan sebagai berikut.
	\[
	\overline{A \oplus B} = A\bar{B} + \bar{A}B
	\]
	Dengan menggunakan aljabar boolean, operasi XNOR tersebut diubah bentuknya agar
	memaksimalkan penggunaan gerbang NOR.
	
	\begin{align*}
		\overline{A \oplus B} & = \overline{\overline{\bar{A} \oplus \bar{B}}}                                 \\
		& = \overline{A \oplus B}                                                        \\
		& = A \oplus \bar{B}                                                             \\
		& = AB + \overline{A + B}                                                        \\
		& = \overline{\overline{AB} (A + B)}                                             \\
		& = \overline{\overline{A + \overline{A + B}} + \overline{B + \overline{A + B}}}
	\end{align*}
	\\
	Berikut adalah rangkaian gerbang NOR untuk membentuk gerbang XNOR.
	
	\begin{center}
		
		\begin{circuitikz}
			\draw
			(3, 0.75) node[nor port] (NORa) {}
			(5, 0) node[nor port] (NORb) {}
			(5, 1.5) node[nor port] (NORc) {}
			(7, 0.75) node[nor port] (NORd) {};
			
			\draw
			(NORc.in 1) ++(left:3) coordinate (inputA)
			(NORb.in 2) ++(left:3) coordinate (inputB)
			(inputA) ++(right: 1) coordinate (inputAh)
			(inputB) ++(right: 1) coordinate (inputBh);
			
			\draw
			(inputA) -- (NORc.in 1)
			(inputB) -- (NORb.in 2);
			
			\draw
			(NORa.in 1) -- (inputAh)
			(NORa.in 2) -- (inputBh);
			
			\draw
			(NORa.out) -| (NORb.in 1)
			(NORa.out) -| (NORc.in 2)
			(NORc.out) -| (NORd.in 1)
			(NORb.out) -| (NORd.in 2);
			
			\draw
			(inputA) -- ++ (left: 0) node[left] {$A$}
			(inputB) -- ++ (left: 0) node[left] {$B$}
			(NORd.out) -- ++ (1, 0) node[right] {$\overline{A \oplus B}$};
			
		\end{circuitikz}
	\end{center}
	
	\subsection{Gerbang XOR} \label{nor-xor}
	Gerbang XOR dibuat dengan menambahkan inverter pada akhir rangkaian
	XNOR (bagian \ref{nor-xnor}).
	
	\begin{center}
		\begin{circuitikz}
			\draw
			(3, 0.75) node[nor port] (NORa) {}
			(5, 0) node[nor port] (NORb) {}
			(5, 1.5) node[nor port] (NORc) {}
			(7, 0.75) node[nor port] (NORd) {}
			(9, 0.75) node[nor port] (NORe) {};
			
			\draw
			(NORc.in 1) ++(left:3) coordinate (inputA)
			(NORb.in 2) ++(left:3) coordinate (inputB)
			(inputA) ++(right: 1) coordinate (inputAh)
			(inputB) ++(right: 1) coordinate (inputBh);
			
			\draw
			(inputA) -- (NORc.in 1)
			(inputB) -- (NORb.in 2);
			
			\draw
			(NORa.in 1) -- (inputAh)
			(NORa.in 2) -- (inputBh);
			
			\draw
			(NORa.out) -| (NORb.in 1)
			(NORa.out) -| (NORc.in 2)
			(NORc.out) -| (NORd.in 1)
			(NORb.out) -| (NORd.in 2)
			(NORd.out) -| (NORe.in 1)
			(NORd.out) -| (NORe.in 2);
			
			\draw
			(inputA) -- ++ (left: 0) node[left] {$A$}
			(inputB) -- ++ (left: 0) node[left] {$B$}
			(NORe.out) -- ++ (1, 0) node[right] {$A \oplus B$};
			
		\end{circuitikz}
	\end{center}
	
	\section{Penutup}
	Berikut adalah beberapa sumer yang digunakan dalam pembuatan artikel ini.
	\begin{enumerate}
		\item \url{https://en.wikipedia.org/wiki/Functional_completeness}
		\item \url{https://www.learnpick.in/questions/details/6649/why-are-nand-and-nor-gates-called-universal-gates}
		\item \url{https://cs.stackexchange.com/questions/43342/how-to-construct-xor-gate-using-only-4-nand-gate}
		\item \url{https://www.eeweb.com/implementing-logic-functions-using-only-nand-or-nor-gates/}
		\item \url{https://electronics.stackexchange.com/questions/84714/how-to-minimize-the-gates-in-implementation}
		\item \url{https://www.circuitlab.com/editor/#?id=t8pg75}
		\item \url{http://nepsweb.co.uk/docs/gatesNotes.pdf}
		
	\end{enumerate}
	
	\begin{tikzpicture}[x=1pt,y=-1pt,line cap=rect]
		\def\logisimfontA#1{\fontfamily{cmr}{#1}} % Replaced by logisim, original font was "SansSerif"
		\def\logisimfontB#1{\fontfamily{cmtt}{#1}} % Replaced by logisim, original font was "Monospaced"
		\definecolor{custcol_0_0_0}{RGB}{0, 0, 0}
		\definecolor{custcol_ff_ff_ff}{RGB}{255, 255, 255}
		\draw [line width=3.0pt, custcol_0_0_0 ]  (349.0,155.0) -- (369.0,155.0) -- (369.0,15.0) -- (389.0,15.0) ;
		\draw [line width=3.0pt, custcol_0_0_0 ]  (129.0,15.0) -- (69.0,15.0) -- (69.0,175.0) -- (249.0,175.0) ;
		\draw [line width=3.0pt, custcol_0_0_0 ]  (159.0,15.0) -- (189.0,15.0) -- (189.0,125.0) -- (249.0,125.0) ;
		\draw [line width=3.0pt, custcol_0_0_0 ]  (89.0,75.0) -- (89.0,55.0) -- (129.0,55.0) ;
		\draw [line width=3.0pt, custcol_0_0_0 ]  (159.0,55.0) -- (209.0,55.0) -- (209.0,195.0) -- (249.0,195.0) ;
		\fill [line width=3.0pt, custcol_0_0_0]  (69.0,15.0) ellipse (5.0 and 5.0 );
		\fill [line width=3.0pt, custcol_0_0_0]  (89.0,75.0) ellipse (5.0 and 5.0 );
		\draw [line width=3.0pt, custcol_0_0_0 ]  (54.0,75.0) -- (59.0,75.0) -- (89.0,75.0) -- (89.0,145.0) -- (249.0,145.0) ;
		\draw [line width=2.0pt, custcol_0_0_0 ]  (44.0,84.0) -- (54.0,75.0) -- (44.0,66.0) -- (20.0,66.0) -- (20.0,84.0) -- cycle;
		\logisimfontB{\fontsize{12pt}{12pt}\selectfont\node[inner sep=0, outer sep=0, custcol_0_0_0, anchor=base west] at  (25.0,81.0)  {x1};}
		\logisimfontA{\fontsize{16pt}{16pt}\fontseries{bx}\selectfont\node[inner sep=0, outer sep=0, custcol_0_0_0, anchor=base west] at  (5.0,82.0)  {C};}
		\fill [line width=2.0pt, custcol_0_0_0]  (59.0,75.0) ellipse (2.0 and 2.0 );
		\draw [line width=3.0pt, custcol_0_0_0 ]  (54.0,15.0) -- (59.0,15.0) -- (69.0,15.0) ;
		\draw [line width=2.0pt, custcol_0_0_0 ]  (44.0,24.0) -- (54.0,15.0) -- (44.0,6.0) -- (20.0,6.0) -- (20.0,24.0) -- cycle;
		\logisimfontB{\fontsize{12pt}{12pt}\selectfont\node[inner sep=0, outer sep=0, custcol_0_0_0, anchor=base west] at  (25.0,21.0)  {x1};}
		\logisimfontA{\fontsize{16pt}{16pt}\fontseries{bx}\selectfont\node[inner sep=0, outer sep=0, custcol_0_0_0, anchor=base west] at  (5.0,22.0)  {A};}
		\fill [line width=2.0pt, custcol_0_0_0]  (59.0,15.0) ellipse (2.0 and 2.0 );
		\draw [line width=3.0pt, custcol_0_0_0 ]  (54.0,45.0) -- (59.0,45.0) ;
		\draw [line width=2.0pt, custcol_0_0_0 ]  (44.0,54.0) -- (54.0,45.0) -- (44.0,36.0) -- (20.0,36.0) -- (20.0,54.0) -- cycle;
		\logisimfontB{\fontsize{12pt}{12pt}\selectfont\node[inner sep=0, outer sep=0, custcol_0_0_0, anchor=base west] at  (25.0,51.0)  {x1};}
		\logisimfontA{\fontsize{16pt}{16pt}\fontseries{bx}\selectfont\node[inner sep=0, outer sep=0, custcol_0_0_0, anchor=base west] at  (5.0,52.0)  {B};}
		\fill [line width=2.0pt, custcol_0_0_0]  (59.0,45.0) ellipse (2.0 and 2.0 );
		\draw [line width=3.0pt, custcol_0_0_0 ]  (393.0,15.0) -- (390.0,15.0) ;
		\draw [line width=2.0pt, custcol_0_0_0 ]  (419.0,6.0) -- (429.0,15.0) -- (419.0,24.0) -- (395.0,24.0) -- (395.0,6.0) -- cycle;
		\logisimfontB{\fontsize{12pt}{12pt}\selectfont\node[inner sep=0, outer sep=0, custcol_0_0_0, anchor=base west] at  (395.0,21.0)  {x1};}
		\logisimfontA{\fontsize{16pt}{16pt}\fontseries{bx}\selectfont\node[inner sep=0, outer sep=0, custcol_0_0_0, anchor=base west] at  (431.0,22.0)  {Y};}
		\fill [line width=2.0pt, custcol_0_0_0]  (389.0,15.0) ellipse (2.0 and 2.0 );
		\draw [line width=2.0pt, custcol_0_0_0 ]  (149.0,55.0) -- (130.0,48.0) -- (130.0,62.0) -- cycle;
		\draw [line width=2.0pt, custcol_0_0_0]  (154.0,55.0) ellipse (4.5 and 4.5 );
		\fill [line width=2.0pt, custcol_0_0_0]  (159.0,55.0) ellipse (2.0 and 2.0 );
		\fill [line width=2.0pt, custcol_0_0_0]  (129.0,55.0) ellipse (2.0 and 2.0 );
		\draw [line width=2.0pt, custcol_0_0_0 ]  (149.0,15.0) -- (130.0,8.0) -- (130.0,22.0) -- cycle;
		\draw [line width=2.0pt, custcol_0_0_0]  (154.0,15.0) ellipse (4.5 and 4.5 );
		\fill [line width=2.0pt, custcol_0_0_0]  (159.0,15.0) ellipse (2.0 and 2.0 );
		\fill [line width=2.0pt, custcol_0_0_0]  (129.0,15.0) ellipse (2.0 and 2.0 );
		\draw [line width=2.0pt, custcol_0_0_0] (264.0,150.0) arc (90.0:-90.0:15.0 and 15.0 );
		\draw [line width=2.0pt, custcol_0_0_0 ]  (264.0,120.0) -- (250.0,120.0) -- (250.0,150.0) -- (264.0,150.0) ;
		\draw [line width=2.0pt, custcol_0_0_0] (264.0,200.0) arc (90.0:-90.0:15.0 and 15.0 );
		\draw [line width=2.0pt, custcol_0_0_0 ]  (264.0,170.0) -- (250.0,170.0) -- (250.0,200.0) -- (264.0,200.0) ;
		\draw [line width=3.0pt, custcol_0_0_0 ]  (279.0,135.0) -- (299.0,135.0) -- (299.0,145.0) -- (319.0,145.0) -- (321.0,145.0) ;
		\draw [line width=3.0pt, custcol_0_0_0 ]  (279.0,185.0) -- (299.0,185.0) -- (299.0,165.0) -- (319.0,165.0) -- (321.0,165.0) ;
		\draw [line width=2.0pt, custcol_0_0_0 ]  (349.0,155.0) .. controls  (339.0,140.0)  ..  (319.0,140.0) .. controls  (327.0,155.0)  ..  (319.0,170.0) .. controls  (339.0,170.0)  ..  (349.0,155.0) -- cycle ;
	\end{tikzpicture}
	
	
\end{document}