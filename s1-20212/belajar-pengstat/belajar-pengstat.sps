GET DATA
  /TYPE=TXT
  /FILE="/home/cat/Downloads/cereal.csv"
  /ARRANGEMENT=DELIMITED
  /DELCASE=LINE
  /FIRSTCASE=2
  /DELIMITERS=","
  /VARIABLES=
    name A65
    mfr A1
    type A1
    calories F3.0
    protein F1.0
    fat F1.0
    sodium F3.0
    fiber F3.0
    carbo F4.0
    sugars F2.0
    potass F3.0
    vitamins F3.0
    shelf F1.0
    weight F4.0
    cups F4.1
    rating F9.6.
VARIABLE LEVEL name (SCALE).
VARIABLE ALIGNMENT name (RIGHT).
VARIABLE WIDTH name (8).
VARIABLE LEVEL mfr (SCALE).
VARIABLE ALIGNMENT mfr (RIGHT).
VARIABLE WIDTH mfr (8).
VARIABLE LEVEL type (SCALE).
VARIABLE ALIGNMENT type (RIGHT).
VARIABLE WIDTH type (8).

