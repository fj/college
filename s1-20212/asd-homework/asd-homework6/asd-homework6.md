---
author: 'Faiz Unisa Jazadi -- `21/475298/PA/20563`'
date: 5 Juni 2022
subtitle: Praktikum Algoritma dan Struktur Data (MII211204)
title: Homework 6
---

# Soal

1.  Lengkapi kode yang hilang pada method `getMinEdge()` dan beri penjelasan
    tentang bagian yang anda lengkapi tersebut.
2.  Jalankan `RunMSTPrim` pada graf yang sama seperti pada 1.2.2. Tunjukkan
    hasil eksekusi anda. Bandingkan hasil tersebut dengan gambar MST pada 1.2.2.
    Apakah edge-edge yang dipilih untuk MST sama?
3.  Jalankan kode di atas pada graf berikut. Berapakah total bobot MST-nya?
    [link
    graph](https://drive.google.com/drive/folders/1RQh2l4u9T4TV21WUoIW_xR4h1WQeiBnQ?usp=shari%20ng)

# Jawaban

## Soal 1

Kode yang hilang dilengkapi sebagai berikut.

``` {.java}
public class MSTPrim {
    // ... stripped ...
    int getMinEdge()
    {
        int id = -1;
        int val = Integer.MAX_VALUE;
        for (int i = 0; i < edges.length; i++) {
            Edge e = edges[i];
            if (e.len < val && (connectedNodes[e.from]
                                ^ connectedNodes[e.to])) {
                id = i;
                val = e.len;
            }
        }
        return id;
    }
    // ... stripped ...
}
```

Pertama, diinisialisasikan sebuah variabel `int` yaitu `id` dengan nilai awal
`-1`. Nilai `id` adalah nilai index dari edge minimum yang dicari dan akan
dikembalikan oleh method `getMinEdge()`. Dibuat juga sebuah variabel `val`
dengan nilai awal `Integer.MAX_VALUE`. Variabel `val` nantinya akan digunakan
untuk menyimpan bobot edge terkecil yang ada.

Method ini bekerja dengan melakukan perulangan terhadap array `edges` dan
memeriksa apakah ada suatu edge dengan kriteria berikut.

-   Memiliki weight yang kurang dari `val`

-   Menghubungkan satu node pada `connectedNodes` dan satu node di luar
    `connectedNodes`

Pada akhirnya, `id` akan memuat index edge dengan kriteria di atas. Jika nilai
yang dikembalikan (nilai `id`) merupakan bilangan negatif, atau `id` tidak
diperbarui, maka dapat diasumsikan bahwa sudah tidak ada edge yang tidak
terpilih.

## Soal 2

Berikut adalah screenshot output hasil eksekusi.

![](problem2.png)

Berdasarkan output hasil eksekusi tersebut, diperoleh sebuah MST dengan total
bobot 17. Total bobot ini sama dengan yang terdapat pada modul praktikum (sesuai
soal).

## Soal 3

Berikut adalah screenshot output hasil eksekusi.

![](problem3.png)

Berdasarkan output hasil eksekusi tersebut, diperoleh total bobot MST sebesar
429.
