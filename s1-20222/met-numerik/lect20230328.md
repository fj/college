---
author: Faiz Jazadi
date: '2023-03-28'
numbersections: true
subtitle: Interpolasi
title: 'Pertemuan Minggu ke-7'
---

# Interpolasi Spline

Pemenuhan sifat interpolasi spline

-   $f(x)$ kontinyu
-   $f'(x)$ kontinyu
-   $f''(x)$ kontinyu
