import math
from prettytable import PrettyTable


def p517():
    x_0 = 2  # initial guess

    def f(x):
        return 9 * math.pi * x**2 - math.pi * x**3 - 90

    def f_slope(x):  # first derivative of f(x), calculated by hand
        return 18 * math.pi * x - 3 * math.pi * x**2

    return x_0, f, f_slope

def p519():
    x_0 = 1  # initial guess

    def f(x):
        return 5 * x**3 - 15 * x**2 + 16

    def f_slope(x):  # first derivative of f(x), calculated by hand
        return 15 * x**2 - 30 * x

    return x_0, f, f_slope


def compute_newton(x_0, f, f_slope):
    t = PrettyTable()
    t.field_names = ('n', 'x_n', 'f(x_n)')
    t.add_row((0, x_0, f(x_0)))
    x_n = x_0
    for i in range(1, 11):
        x_next = x_n - (f(x_n)/f_slope(x_n))
        t.add_row((i, x_next, format(f(x_next), 'f')))
        x_n = x_next
    print(t)

print('P5.17')
compute_newton(*p517())

print('\nP5.19')
compute_newton(*p519())
