import math


def f(x):
    return 9 * math.pi * x**2 - math.pi * x**3 - 90


xl = 0
xu = 3

print(f'xl = {xl}, f(xl) = {f(xl)}')
print(f'xu = {xu}, f(xu) = {f(xu)}')
print(f'f(xl)f(xu) = {f(xl) * f(xu)}')
assert f(xl) * f(xu) < 0


def xr(l, u):
    return u - ((f(u) * (l - u)) / (f(l) - f(u)))


print(f'iteration-1 xr = {xr(xl, xu)}')
