import math


def f(h):
    return 5 * math.pi * h**3 - 15 * math.pi * h**2 + 16 * math.pi


def fxr(l, u):
    return u - ((f(u) * (l - u)) / (f(l) - f(u)))


def prettified_tprint(*cols, width=8):
    for c in cols:
        if isinstance(c, float):
            c = format(c, 'f')
        print(str(c)[:5].ljust(width), end='')
    print()


def bisect(xl, xu):
    fxlxu = f(xl) * f(xu)
    print(f'verify f(xl)f(xu) < 0: {f(xl)}*{f(xu)} = {fxlxu}')
    assert fxlxu < 0
    prettified_tprint('i', 'xl', 'xu', 'xr', 'er')
    old_xr = None
    for i in range(1, 101):
        xr = fxr(xl, xu)
        are = abs(old_xr - xr)/xr if old_xr else None

        prettified_tprint(i, xl, xu, xr, are)

        if xr * xl < 0:
            xu = xr
        else:
            xl = xr

        old_xr = xr


print('xl = 1, xu = 2')
bisect(1, 2)
print('\nxl = 2, xu = 3')
bisect(2, 3)
