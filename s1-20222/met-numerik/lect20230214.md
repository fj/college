---
author: Faiz Jazadi
date: '2023-02-14'
numbersections: true
subtitle: Pengenalan Metode Numerik
title: Pertemuan 1
---

# Intro

Mengapa belajar metode numerik?

Tidak seluruh model matematika dapat diselesaikan dengan mudah (banyak). Jika
tidak punya rumus explicit, sulit dicari penyelesaiannya. Metode numerik
berfungsi menyelesaikan masalah matematika secara analitis (yang umumnya secara
matematis tidak bisa).

Salah satu contoh metode numerik adalah bracketing (sederhananya adalah teknik
"bagi dua"). Diberikan interval awal, dilakukan searching (mirip binary search)
terus menerus untuk mempersempit interval.

# Acuan Mata Kuliah

Buku. Numerical Methods for Engineers 7th edit, Steven C. Chapra, Raymond P.
Canale.

# Latihan

Soal 5.17 dan 5.19.
