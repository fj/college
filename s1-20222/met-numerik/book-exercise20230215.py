import math

import numpy as np
import matplotlib.pyplot as plt

# PT2.4 (page 119)
# E5.1.1


"""
Problem Statement. Use the graphical approach to determine the drag coeffi cient c
needed for a parachutist of mass m = 68.1 kg to have a velocity of 40 m/s after free-
falling for time t = 10 s. Note: The acceleration due to gravity is 9.81 m/s 2 .
Solution. This problem can be solved by determining the root of Eq. (PT2.4) using the
"""

m = 68.1
v = 40
t = 10
g = 9.81


def f(c):
    return (g * m/c) * (1 - math.e**(-(c/m) * t)) - v


x = np.linspace(1, 20, 100)
y = np.vectorize(f)(x)
plt.plot(x, y, 'r')
plt.plot(x, np.vectorize(lambda y: 0)(x))
plt.show()
