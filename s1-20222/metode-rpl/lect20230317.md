---
author: Faiz Jazadi
date: '2023-03-17'
numbersections: true
subtitle: Pemodelan Sistem
title: 'Pertemuan Minggu ke-5'
---

# System Modeling

Ada beberapa perspektif. Ada bermacam-macam, salah satunya adalah Unified
Modeling Language (UML). Use-case diagram juga salah satunya. System modeling
sekarang menjadi cara merepresentasikan sistem dengan notasi grafis. Tidak hanya
ketika ingin membuat sistem, system modeling juga bisa dilakukan terhadap sistem
yang lama atau sistem yang sudah ada rencananya.

# Perspektif Sistem

-   **Eksternal**, memodelkan lingkungan atau konteks dari sistem (sistem
    posisinya di mana dibandingkan yang lain)
-   **Interaksi**, memodelkan interaksi antara sistem dan lingkungan, atau
    antara komponen dalam sistem
-   **Struktural**, memodelkan organisasi suatu sistem atau struktur data yang
    diproses oleh sistem
-   **Behavioral**, memodelkan perilaku dinamis dari sistem dan bagaimana
    perilaku tersebut merespon ke event-event

# Tipe Diagram UML

-   Activity diagrams
-   Use case diagrams
-   Sequence diagrams
-   Class diagrams
-   State diagrams

# Kegunaan Model Grafis

-   Diskusi
-   Dokumentasi
-   Untuk deskripsi sistem sebagai bahan implementasi (beberapa model bahkan
    bisa langsung digunakan untuk meng-generate source code)

# Context Models

Apa yang masuk dalam sistem dan apa yang tidak masuk dalam sistem.
