---
author: Faiz Jazadi
date: '2023-03-31'
numbersections: true
subtitle: 2021/2022 Genap
title: Jawaban UTS REG B
geometry: margin=1cm
classoptions:
- twocolumns
---

# Soal 1

Empat jenis serangan dan security mechanism dan security service yang sesuai.

1.  Traffic analysis

    adalah serangan dalam bentuk upaya menganalisa dan melihat apa yang ada
    dalam lalu lintas informasi.

    Security mechanism: encipherment, routing control, traffic padding

    Security service: data confidentiality (traffic-flow confidentiality)

2.  Denial-of-Service

    adalah upaya dengan sengaja mencoba untuk membuat suatu sistem tidak
    berfungsi sebagaimana mestinya.

    Security mechanism: routing control, event detection, access control

    Security service: availability

3.  Masquerade

    adalah upaya berpura-pura menjadi suatu entitas.

    Security mechanism: digital signature, data integrity

    Security service: nonrepudiation

4.  Replay

    adalah upaya mengirimkan kembali pesan yang sudah diterima dengan maksud
    mencoba menjalankan permintaan lebih dari satu kali.

# Soal 2

    P = PESANAN SUDAH DIKIRIM DUA BELAS JAM YANG LALU
    K_v = FAIZ UNISA JAZADI
    K_h = [2   -1   3
           5    2   1
           1    3   -3]
    E(K_h, K_v, M) = HillE(K_h, VigenereE(K_v, M))
    D(K_h, K_v, C) = VigenereD(K_v, HillD(K_h, C))

    LUT
    A   B   C   D   E   F   G   H   I   J   K   L   M
    0   1   2   3   4   5   6   7   8   9   10  11  12

    N   O   P   Q   R   S   T   U   V   W   X   Y   Z
    13  14  15  16  17  18  19  20  21  22  23  24  25

    Enkripsi
    PESANAN SUDAH DIKIRIM DUA BELAS JAM YANG LALU
    FAIZUNI SAJAZ ADIFAIZ UNI SAJAZ ADI FAIZ UNIS

    PF = 15 + 5     = 20    U
    EA = 4 + 0      = 4     E
    SI = 18 + 8     = 0     A
    AZ = 0 + 25     = 25    Z
    NU = 13 + 20    = 7     H
    AN =                    N
    NI = 13 + 8     = 21    V
    SS = 18 + 18    = 10    K
    UA =                    U
    DJ = 3 + 9      = 12    M
    AA =                    A
    HZ = 7 + 25     = 6     G
    DA =                    D
    ID = 8 + 3      = 11    L
    KI = 10 + 8     = 18    S
    IF = 8 + 5      = 13    N
    RA =                    R
    II = 8 + 8      = 16    Q
    MZ = 12 + 25    = 11    L
    DU = 3 + 20     = 23    X
    UN = 20 + 13    = 7     H
    AI =                    I
    BS = 1 + 18     = 19    T
    EA =                    E
    LJ = 11 + 9     = 20    U
    AA =                    A
    SZ = 18 + 25    = 17    R
    JA =                    J
    AD =                    D
    MI = 12 + 8     = 20    U
    YF = 24 + 5     = 3     D
    AA =                    A
    NI =                    V
    GZ = 6 + 25     = 5     F
    LU = 11 + 20    = 5     F
    AN =                    N
    LI = 11 + 8     = 19    T
    US = 20 + 18    = 12    M

    C_V = UEAZHNVKUMAGDLSNRQLXHITEUARJDUDAVFFNTM
        = UEA ZHN VKU MAG DLS NRQ LXH ITE UAR JDU DAV FFN TMZ (padding=Z)

    UEA = (20 4 0) * K = ()

# Soal 3

## ECB

    P = 1001 0001 0010 0011
    K = 0001

Enkripsi:

    C_1 = 1001 ^ 0001 = 1000
    C_2 = 0001 ^ 0001 = 0000
    C_3 = 0010 ^ 0001 = 0011
    C_4 = 0011 ^ 0001 = 0010

Dekripsi:

    P_1 = 1000 ^ 0001 = 1001
    P_2 = 0000 ^ 0001 = 0001
    P_3 = 0011 ^ 0001 = 0010
    P_4 = 0010 ^ 0001 = 0011

## CBC

    P = 1001 0001 0010 0011
    K = 0001
    IV = 0101

Enkripsi:

    P_1 ^ IV = 1001
               0101
               ---- ^
               1100
    C_1 = 1100
          0001
          ---- ^
          1101

    P_2 ^ C_1 = 0001
                1101
                ---- ^
                1100
    C_2 = 1100
          0001
          ---- ^
          1101

    P_3 ^ C_2 = 0010
                1101
                ---- ^
                1111
    C_3 = 1111
          0001
          ---- ^
          1110

    P_4 ^ C_3 = 0011
                1110
                ---- ^
                1101
    C_4 = 1101
          0001
          ---- ^
          1100

    C = 1101 1101 1110 1100

Dekripsi:

    P_1 ^ IV = 1101 (C_1)
               0001 (K)
               ---- ^
               1100
    P_1 = 1100
          0101 (IV)
          ---- ^
          1001

    P_2 ^ C_1 = 1101 (C_2)
                0001 (K)
                ---- ^
                1100
    P_2 = 1100
          1101 (C_1)
          ---- ^
          0001

    P_3 ^ C_2 = 1110 (C_3)
                0001 (K)
                ---- ^
                1111
    P_3 = 1111
          1101 (C_2)
          ---- ^
          0010

    P_4 ^ C_3 = 1100 (C_4)
                0001 (K)
                ---- ^
                1101
    P_4 = 1101
          1110 (C_3)
          ---- ^
          0011

    P = 1001 0001 0010 0011
