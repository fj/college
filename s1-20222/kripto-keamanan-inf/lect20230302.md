---
author: Faiz Jazadi
date: '2023-03-02'
numbersections: true
subtitle: Kriptografi Simetris
title: 'Pertemuan Minggu ke-3'
---

# Key Points

-   Disebut juga penyandian konvensional
-   Pengirim dan penerima berbagi kunci yang sama
-   Paling banyak digunakan
    -   Semua algoritma klasik merupakan kriptografi simetris
-   Pendekatan pemecahan algoritma
    -   Kriptanalisis
    -   Brute-force attack
-   Suatu skema enkripsi betul-betul tidak aman jika bisa di-attack hanya dengan
    ciphertext
-   Membahas tentang unconditionally secure
-   Block cipher, stream cipher
