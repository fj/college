# Directory to Course Name Table

| Directory Name      | Course Name                                     |
| :------------------ | :---------------------------------------------- |
| analisis-big-data   | Analisis Big Data                               |
| bahasa-otomata      | Bahasa dan Otomata                              |
| komputasi-awan      | Komputasi Awan                                  |
| kripto-keamanan-inf | Kriptografi dan Keamanan Informasi              |
| met-numerik         | Metode Numerik                                  |
| metode-rpl          | Metode Rekayasa Perangkat Lunak                 |
| pembelajaran-mesin  | Pembelajaran Mesin                              |
| peng-startup-dig    | Pengembangan Startup Digital                    |
| workshop-impl-rpl   | Workshop Implementasi Rancangan Perangkat Lunak |
