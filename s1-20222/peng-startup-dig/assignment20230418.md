---
author: Faiz Jazadi
date: '2023-04-18'
numbersections: true
subtitle: Entrepreneur Aptitude Test
title: Tugas 1
---

# Answer to Questions

1.  Are you more at ease by yourself or with people? Do you draw energy from
    being with other people or being with yourself?

    **Answer:** Yes, I am more at ease by myself than with other people and I
    tend to draw more energy being with other people

2.  How do you process information as you're presented it? Do you take it bit by
    bit and literally? Or do you instinctively add judgement and context to
    information?

    **Answer:** I tend to instinctively add judgement and context to the
    information I present.

3.  Thinking vs Feeling: How do you make decisions? Do you do it purely
    rationally or do you "feel" your decisoins and use empathy?

    **Answer:** I believe I make decisions not purely rationally but rather I
    take into account empathy but in small percentage.

4.  Do you make your decisions quickly and move on, or are you more likely to
    take your time and be ok with leaving your options open.

    **Answer:** Sometimes I make decisions quicly when I am confident enough to
    finalize it. But, most of the time, I leave the decision open for as long as
    possible.

# 16Personalities Test Result

INTJ-T

# Individual Essay

As an individual with INTJ-T personality, I am characterized as introvert,
intuitive, thinking, and judging. The name is "The Architect" which means that I
am someone who likes to design and implement innovative ideas.

I do not fully agree with my test results. The test results also showed numbers
that I believe can also be viewed as the classification confidence. I am
classified 53% as an introvert. I partially believe that I sometimes feel like
an introvert, but not all the time. Sometimes, I enjoy spending time alone to
think and reflect, which allows me to generate creative ideas. On the other
side, I also rely heavily on my intuition when making decisions and enjoy
analyzing complex ideas and concepts. I am goal-oriented and enjoy organizing my
thoughts and ideas in a structured manner, which aligns with the judging aspect
of my personality.

In terms of the entrepreneurial team, it is crucial to have a balance of
personalities to ensure the success of the startup. As an INTJ-T, I would
benefit from having team members who possess different personality traits. Some
of them are:

-   ENTP
-   INFJ
-   ENTJ
-   ISTP

In conclusion, being an INTJ-T personality type has its strengths and
weaknesses, but it is important to recognize that each individual brings their
unique strengths and skills to the table. Building a team with a variety of
personalities and traits can help to ensure a well-rounded and successful
startup.
