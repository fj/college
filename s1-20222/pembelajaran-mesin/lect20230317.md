---
author: Faiz Jazadi
date: '2023-03-17'
numbersections: true
subtitle: Linear Regression with One Variable
title: 'Pertemuan Minggu ke-5'
---

# Key Points

-   Gradient descent, metode yang lebih terstruktur untuk menentukan nilai-nilai
    parameter $\theta_0$ dan $\theta_1$

# Tugas

Saya minta Anda untuk belajar mandiri dari Microsoft Teams tentang Logistic
Regression. Buatlah resume tentang Logistic Regression dalam bentuk doc dan pdf
minimal 3 halaman, kemudian simpan dalam sebuah folder dan langsung saya di-add
untuk dapat mengakses. (Cek PPT di MS Teams)
