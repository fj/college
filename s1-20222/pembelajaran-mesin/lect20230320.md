---
author: Faiz Jazadi
date: '2023-03-20'
numbersections: true
subtitle: Decision Tree
title: 'Pertemuan Minggu ke-6'
---

# Review: Supervised vs Unsupervised

-   Supervised: harus menyiapkan data training (punya label yang mengindikasikan
    kelas) untuk diproses
-   Unsupervised: label data training tidak diketahui

# Review: Permasalahan Prediksi

-   Classification: diskret atau nominal
-   Numeric prediction: memodelkan fungsi bernilai kontinu

# Langkah Klasifikasi

-   Membangun model
    -   mendeskripsikan set dari kelas-kelas yang sudah ditentukan
    -   setiap tupel sudah ditentukan masuk ke kelas mana
    -   tupel-tupel ini digunakan untuk membangun model (kemudian disebut
        training set)
    -   model ini bisa direpresentasikan dalam bentuk aturan klasifikasi, pohon
        keputusan, atau formula matematika
-   Menggunakan mode, untuk mengklasifikasikan objek yang di masa depan atau
    tidak diketahui
    -   Estimate accuracy
        -   Label dibandingkan dengan hasil klasifikasi
        -   Rate akurasi ditentukan dari data mana yang diklasifikasi dengan
            benar
        -   Dilakukan terhadap suatu test set (berbeda dari training set)
    -   Jika akurasi sudah bisa diterima, gunakan model untuk mengklasifikasikan
        data baru

# Proses Membentuk Model

Training set → induction → learn model → deduction → test set
