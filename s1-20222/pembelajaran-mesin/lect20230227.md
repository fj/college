---
author: Faiz Jazadi
date: '2023-02-27'
numbersections: true
subtitle: 'Clustering -- Metode dan Algoritma'
title: Pertemuan 3
---

# Unsupervised Learning

-   Label kelas setiap objek yang tidak diketahui
-   Clustering:
    -   proses pengelompokkan data ke kelas-kelas atau cluster-cluster
    -   sangat mirip dalam satu cluster (high similarity)
    -   sangat berbeda dengan anggota cluster lain (very dissimilar)
-   Clustering → cluster
-   Ukuran kesamaan → distance measure
-   Dapat digunakan untuk
    -   mengidentifikasi dense region dan sparse area
    -   menemukan pola distribusi data secara keseluruhan
    -   menggali hubungan menarik antar atribut data
-   Perlu diingat, ukuran kesamaan tidak selalu ekuivalen dengan ukuran
    ketidaksamaan

# Struktur Data

-   Matrik data (object-by-variable structure)
-   Matrik dissimilaritas

# Clustering Type

## Partitioning

-   Data akan dipartisi sejumlah $k$, di mana setiap partisi menyatakan cluster
    dan $k \leq n$.
-   Metode clustering yang mengklasifikasikan data ke dalam $k$ kelompok, dengan
    ketentuan:
    1.  Setiap kelompok harus terdiri dari setidaknya 1 objek
    2.  Setiap objek harus menjadi anggota tepat 1 kelompok
        -   Metode fuzzy partitioning
    3.  Cluster yang dibentuk dapat mengoptimalkan similarity function

### Prosedur

1.  Terdapat $n$ objek
2.  Tentukan berapa partisinya ($k$)
3.  Menginisialisasi $k$ partisi (diberikan satu perwakilan/pemimpin, sebagai
    acuan untuk menarik anggota-anggota lainnya)
4.  Secara iteratif memperbaiki partisi dengan cara memindahkan objek-objek dari
    suatu kelompok ke kelompok lain

### Algoritma

-   Algoritma $k$-means, setiap cluster dinyatakan sebagai nilai rata-rata
    (mean) objek-objek dalam cluster
-   Algoritma $k$-medoid, setiap cluster dinyatakan oleh suatu objek yang berada
    dekat dengan pusat cluster...

#### $K$-means

-   **Input:** Data dengan $n$ objek dan $k$ serta $i_{max}$ (iterasi maksimal
    opsional)
-   **Output:** himpunan $k$ cluster yang optimal
-   **Metode:**
    -   pilih $k$ objek sebagai pusat cluster awal
    -   menilai kesamaan setiap objek dengan setiap cluster menggunakan fungsi
        kesamaan (misal euclidean distance)
    -   menentukan keanggotaan setiap objek berdasarkan kemiripan (hasil no 2)
    -   memperbarui pusat cluster dengan menghitung mean setiap cluster
    -   ulangi langkah 2 s.d. 4 sampai tidak ditemukan perubahan pusat cluster
        dan anggotanya, atau sampai iterasinya mencapai iterasi maksimal
