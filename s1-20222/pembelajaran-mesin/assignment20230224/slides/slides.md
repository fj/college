---
author: Faiz Unisa Jazadi (21/475298/PA/20563)
colortheme: seahorse
date: '2023-03-01'
filetype: beamer
subtitle: Pembelajaran Mesin (MII212402)
theme: Singapore
title: 'Tugas 1: Visualisasi dan Eksplorasi Data'
---

# Soal

1.  Cari minimal 1 dataset terstruktur dan 1 dataset tidak terstruktur secara
    programatik menggunakan API atau web scraping, jelaskan langkah-langkahnya!
2.  Kemudian, lakukan eksplorasi:
    -   Visualisasi variabel untuk beberapa variabel/feature dengan variabel
        yang sesuai, dan
    -   Visualisasi statistik

# Data Terstruktur

Data yang dipilih untuk digunakan dalam tugas ini adalah data hasil penertiban
minuman keras Satpol PP Jakarta Utara tahun 2017. Data diperoleh melalui API
yang didapat dari layanan [Open Data Jakarta](https://data.jakarta.go.id).

## Akuisisi

```{=tex}
\footnotesize
```
``` {.python}
import pandas as pd
import requests

data = requests.get('https://data.jakarta.go.id/...').json()
df = pd.DataFrame.from_dict(data)
df.head()
```

```{=tex}
\normalsize
```
# Data Terstruktur (cont.)

::: {.columns}
::: {.column}
## Sampel Data

![Hasil `df.head()`](img/structured-head.png)
:::

::: {.column}
## Statistika Deskriptif

          jumlah
  ------- ------------
  count   30.000000
  mean    48.366667
  std     48.057784
  min     2.000000
  25%     9.250000
  50%     33.500000
  75%     64.500000
  max     181.000000
:::
:::

# Data Terstruktur: Penertiban per Kecamatan

``` {.python}
df.groupby(by='wilayah').sum().sort_values(
    'jumlah').plot(kind='barh')
```

![Bar plot penertiban per
kecamatan](img/structured-wilayah-vs-jumlah-bar.png){width="300px"}

# Data Terstruktur: Jenis Minuman

``` {.python}
df.groupby('jenis_minuman').sum().sort_values(
    'jumlah').plot.pie(y='jumlah', legend=None)
```

![Persentase jenis minuman yang
ditertibkan](img/structured-jenis-minuman-pie.png){width="300px"}

# Data Tidak Terstruktur

Data yang dipilih untuk digunakan dalam tugas ini adalah data citra satelit
Himawari-9 B07 untuk Southeast Asia 3 yang diperoleh dari website [Japan
Meteorological
Agency](https://www.data.jma.go.jp/mscweb/data/himawari/list_se3.html).

**Rentang waktu:** 2023-02-28 09:30 UTC to 2023-03-01 09:20 UTC

## Akuisisi

```{=tex}
\footnotesize
```
``` {.python}
sat_url = 'https://www.data.jma.go.jp/.../se3_b07_{}.jpg'
timecodes = map(lambda t: f'{t[0]:02}{t[1]:02}',
                itertools.product(range(24), range(0, 60, 10)))
for timecode in timecodes:
    r = requests.get(sat_url.format(timecode))
    with open(f'sat_data/{timecode}.jpg', 'wb') as f:
        f.write(r.content)
    print(f'collected sat image: {timecode}')
```

```{=tex}
\normalsize
```
# Data Tidak Terstruktur: Histogram (Sampel)

**Pre-processing:** diubah menjadi grayscale

``` {.python}
im = cv2.imread('sat_data/0000.jpg')
im = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
```

::: {.columns}
::: {.column}
![Sampel citra Himawari-9 B07](img/sampel-himawari-9.png)
:::

::: {.column}
![Histogram sampel citra Himawari-9
B07](img/hist-sampel-himawari-9.png){width="200px"}
:::
:::

# Data Tidak Terstruktur: Fitur yang Diekstrak

Dari data tidak terstruktur berupa citra-citra satelit, fitur yang ingin
diekstrak adalah waktu dan *cloudiness* (persentase keberadaan awan).

Metode ekstraksi:

1.  Binary thresholding ($t = 120$, berdasarkan histogram sampel)
2.  Hitung cloudiness berdasarkan frekuensi hasil thresholding (banyak pixel
    yang ada di sisi kanan atau $f(x, y) > t$)
3.  Sesuaikan timestamp dengan waktu asli pengambilan citra

# Data Tidak Terstruktur: Hasil Ekstraksi Fitur

::: {.columns}
::: {.column}
## Sampel Data

``` {.python}
df.head()
```

      time                    cloudiness
  --- --------------------- ------------
  0   2023-02-28 09:30:00       0.144469
  1   2023-02-28 09:40:00       0.151086
  2   2023-02-28 09:50:00       0.159581
  3   2023-02-28 10:00:00       0.169581
  4   2023-02-28 10:10:00       0.181477
:::

::: {.column}
## Statistika Deskriptif

``` {.python}
df.describe()
```

            cloudiness
  ------- ------------
  count            142
  mean        0.222873
  std        0.0600493
  min         0.138577
  25%         0.154424
  50%         0.249913
  75%         0.278256
  max         0.293119
:::
:::

# Data Tidak Terstruktur: Time vs Cloudiness

``` {.python}
df.plot(kind='line', x='time', y='cloudiness')
```

![Time vs cloudiness](img/unstructured-time-vs-cloudiness.png){width="300px"}

# Takeaways

Hal-hal yang dipelajari selama mengerjakan tugas ini.

-   Teknik akuisisi data terstruktur maupun tidak terstruktur secara programatik
-   Metode dalam melakukan visualisasi dan eksplorasi data baik secara statistik
    maupun grafis
-   Proses ekstraksi fitur yang relevan pada data yang tidak terstruktur dan
    cara melakukan pre-processing

# Terima kasih

Beberapa referensi yang membantu dalam mengerjakan tugas ini:

-   <https://data.jakarta.go.id/dataset>
-   <https://www.data.jma.go.jp/mscweb/data/himawari/>
-   <https://github.com/farizdotid/DAFTAR-API-LOKAL-INDONESIA>
-   <https://pandas.pydata.org/Pandas_Cheat_Sheet.pdf>
-   <https://chat.openai.com>
