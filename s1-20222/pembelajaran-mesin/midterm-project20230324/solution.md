---
author: Faiz Unisa Jazadi (21/475298/PA/20563)
colortheme: lily
date: '2023-04-25'
filetype: beamer
subtitle: Pembelajaran Mesin (MII212402)
theme: Warsaw
title: 'Tugas Akhir Pra-UTS: Implementasi Algoritma/Pendekatan Machine Learning'
---

# Dataset

-   Digunakan data "Telco Customer Churn" dari Kaggle.
-   Data berisi informasi customer pada suatu perusahaan telco.
-   Tiap baris merepresentasikan customer dan tiap kolom merepresentasikan
    atribut customer.
-   Dataset ditujukan untuk mengklasifikasikan churning atau terkait retensi
    customer. Dengan kata lain, dataset ditujukan untuk menjawab pertanyaan
    "Apakah customer akan lanjut berlangganan?".

<https://www.kaggle.com/datasets/blastchar/telco-customer-churn>

# Rencana

-   Dataset berisi data customer yang ditujukan untuk digunakan dalam menentukan
    churning
-   Saya berencana menggunakan metode machine learning supervised
    -   decision tree learning (DTL).

# Library dan Utilities

Digunakan bantuan beberapa library dalam tugas ini:

-   `matplotlib`
-   `numpy`
-   `pandas`
-   `scikit-learn`

# Tahap 1: Memuat Dataset

``` {.python}
df = pd.read_csv('dataset.csv')
df.head()
```

![Dataset sample rows](img/dataset-head.png)

# Tahap 2: Data Preprocessing

1.  Hapus kolom yang tidak diperlukan

    ``` {.python}
    df = df.drop(columns='customerID')
    ```

2.  Kelompokkan jenis kolom untuk diperbaiki

    `\footnotesize`{=tex}

    ``` {.python}
    numerical_columns = ['tenure', 'MonthlyCharges',
                         'TotalCharges']
    binary_columns = ['SeniorCitizen', 'Partner',
                      'Dependents', 'PhoneService',
                      'PaperlessBilling', 'Churn']
    ```

--------------------------------------------------------------------------------

3.  Beberapa kolom yang seharusnya numerik tapi malah terbaca sebagai
    kategorikal (dan sebaliknya) juga harus diperbaiki dan disesuaikan.
    Kolom-kolom yang bernilai biner (Ya atau Tidak) juga akan diubah menjadi
    integer.

    `\footnotesize`{=tex}

    ``` {.python}
    df['TotalCharges'] = pd.to_numeric(df['TotalCharges'],
                                       errors='coerce')

    for col_name in binary_columns:
        df[col_name] = df[col_name].map(
                        lambda val: 1 if val == 'Yes' else 0)
    ```

    `\normalsize`{=tex}

4.  Baris-baris yang mempunyai nilai `null` juga akan dihapus karena dinilai
    kurang relevan dan lengkap.

    `\footnotesize`{=tex}

    ``` {.python}
    df = df.dropna()
    ```

# Tahap 3: Persiapan Data untuk DTL

Sebelumnya, selain kolom-kolom yang dikelompokkan sebagai numerik dan biner, ada
juga kolom yang bersifat kategorikal. Agar mudah diproses, suatu kolom
kategorikal akan dipecah menjadi kolom-kolom dummy sebanyak nilai yang mungkin
untuk kolom kategorikal tersebut.

``` {.python}
df_dummy = pd.get_dummies(df)
df_dummy.columns
```

`\footnotesize`{=tex}

    SeniorCitizen, Partner, Dependents, tenure, PhoneService,
    PaperlessBilling, MonthlyCharges, TotalCharges, Churn,
    gender_Female, gender_Male, MultipleLines_No,
    MultipleLines_No phone service, MultipleLines_Yes,
    InternetService_DSL, InternetService_Fiber optic,
    InternetService_No, OnlineSecurity_No,
    OnlineSecurity_No internet service, OnlineSecurity_Yes,
    ... stripped

# Tahap 4: Pemisahan Data Train dan Test

Data training dan testing dipisah dengan rasio 4:1 (25% data testing).

`\footnotesize`{=tex}

``` {.python}
from sklearn.model_selection import train_test_split

feature_cols = [c for c in df_dummy.columns if c != 'Churn']
X = df_dummy[feature_cols]
y = df_dummy['Churn']

(X_train, X_test,
 y_train, y_test) = train_test_split(X, y, test_size=0.25,
                                     random_state=16)
```

# Tahap 5: Training

Digunakan `DecisionTreeClassifier()` dari `scikit-learn`. Training didasarkan
pada GINI index. Dicari model terbaik berdasarkan hyperparameter `max_depth`.

`\footnotesize`{=tex}

``` {.python}
clf = tree.DecisionTreeClassifier(criterion='gini')
clf.fit(X_train, y_train)
initial_depth = clf.tree_.max_depth

best_model = clf
model_score = clf.score(X_test, y_test)
for max_depth in range(2, initial_depth):
    clf = tree.DecisionTreeClassifier(criterion='gini',
                                      max_depth=max_depth)
    clf.fit(X_train, y_train)
    score = clf.score(X_test, y_test)
    if score > model_score:
        model_score = score
        best_model = clf
```

# Tahap 6: Visualisasi Decision Tree

``` {.python}
tree.plot_tree(best_model, filled=True,
               feature_names=X_train.columns);
```

![Hasil Visualisasi](img/dt-visual.png){width="420px"}

# Tahap 7: Evaluasi

Untuk mengukur performa model yang dihasilkan, dilakukan evaluasi. Evaluasi
dilakukan dengan menggunakan `classification_report()` dari `sklearn`.

`\footnotesize`{=tex}

``` {.python}
from sklearn.metrics import classification_report

y_pred = best_model.predict(X_test)
print(classification_report(y_test, y_pred))
```

--------------------------------------------------------------------------------

## Hasil Evaluasi

                  precision    recall  f1-score   support

               0       0.81      0.94      0.87      1282
               1       0.70      0.41      0.51       476

        accuracy                           0.79      1758
       macro avg       0.75      0.67      0.69      1758
    weighted avg       0.78      0.79      0.77      1758

--------------------------------------------------------------------------------

... end of slides, terima kasih!
