---
author: 'Faiz Unisa Jazadi -- `21/475298/PA/20563`'
classoption:
- a4paper
- twocolumn
date: '2023-03-17'
geometry: 'margin=2.5cm'
subtitle: Tugas Individu 3 Pembelajaran Mesin
title: Ringkasan Regresi Logistik
---

# Klasifikasi vs Prediksi

Sebelumnya, pada prediksi, yang diprediksi adalah suatu nilai kontinu, sedangkan
pada klasifikasi yang diprediksi adalah kelas-kelas. Misalkan pada email,
terdapat email bisa diklasifikasikan dalam dua kelas yaitu spam atau tidak spam.
Dalam implementasi, biasanya digunakan integer untuk merepresentasikan dua
kelas. Misal untuk kasus tumor, kelasnya adalah 0 (kelas negatif atau tumor
jinak) dan 1 (kelas positif atau tumor ganas).

$$y \in \{0, 1\}$$

Di sini, kita ingin melihat apakah bisa kita menyelesaikan kasus klasifikasi
dengan regresi linear. Pada contoh tumor di atas, diberikan ukuran tumor, kita
ingin menentukan kelas tumor (ganas atau jinak). Bisa saja digunakan hypothesis
function $h_\theta(x)$ seperti pada regresi linear. Namun, hasilnya tidak selalu
cocok karena yang ingin diprediksi bukan suatu nilai kontinu, tapi suatu kelas.
Oleh karena itu, regresi linear tidak selalu cocok untuk kasus klasifikasi.

Solusi masalah klasifikasi adalah menggunakan suatu threshold. Misal, jika
$h_\theta(x) \geq 0.5$, prediksi $y = 1$ dan jika $h_\theta(x) < 0.5$, prediksi
$y = 0$. Namun, dalam regresi linear, bisa saja $h_\theta(x) < 0$ atau
$h_\theta(x) > 1$. Dalam kasus klasifikasi, kita menginginkan nilai
$0 \leq h_\theta(x) \leq 1$. Regresi logistik digunakan untuk tujuan ini
(mengubah representasi nilai kontinu menjadi suatu representasi probabilitas
"masuk ke kelas yang mana").

# Model Regresi Logistik

Kita menginginkan $0 \leq h_\theta(x) \leq 1$, kita perlu memodifikasi fungsi
hipotesis dengan menambahkan suatu fungsi yang disebut dengan *sigmoid function*
atau *logistic function* yang dilambangkan sebagai $g(z)$.

$$g(z) = \frac{1}{1+e^{-z}}$$

Misalkan, $h_\theta(x) = g(\theta^{T}x)$, maka setelah dimodifikasi akan menjadi
$h_\theta(x) = \frac{1}{1+e^{\theta^Tx}}$.

Jika dibuat *plot*-nya, fungsi $g(z)$ ini akan "dipaksa" untuk berada dalam
selang $[0, 1]$.

# Interpretasi Output Hipotesis

$h_\theta(x)$ adalah estimasi probabilitas $y = 1$ pada input $x$.

Misal (pada kasus klasifikasi tumor), jika

$$x = \begin{bmatrix}
        x_0 \\
        x_1
    \end{bmatrix}
  = \begin{bmatrix}
        1 \\
        \text{tumorSize}
    \end{bmatrix}$$

dan $h_\theta(x) = 0.7$, maka beritahu pasien bahwa terdapat peluang 70% bahwa
tumor bersifat ganas. Dengan kata lain, $h_\theta(x) = P(y = 1 | x; \theta)$
(probabilitas $y = 1$ jika diberikan $x$ dan diparametrisasi oleh $\theta$).

# Regresi Logistik

Misalkan kita ingin memprediksi $y = 1$ jika $h_\theta(x) \geq 0.5$ dan
memprediksi $y = 0$ jika $h_\theta(x) < 0.5$. Sebetulnya, jika digambarkan ($z$
terhadap $g(z)$), $g(z) \geq 0.5$ jika $z > 0$. Atau, untuk $g(z) \geq 0.5$,
nilai $\theta^Tx$ harus $\geq 0$. Dengan kata lain, probabilitas akan ditentukan
dari nilai $\theta^T x$.

# Decision Boundary

Decision boundary adalah suatu garis yang membatasi bidang-bidang untuk tiap
kelas yang berbeda. Misalkan
$h_\theta(x) = g(\theta_0 + \theta_1 x_1 + \theta_2 x_2)$ dan diasumsikan
$\theta_0 = -1$, $\theta_1 = 1$, dan $\theta_2 = 1$. Maka, akan diprediksi
$y = 1$ jika $-3 + x_1 + x_2 \geq 0$ atau ketika $x_1 + x_2 \geq 3$. Decision
boundary adalah ketika $x_1 + x_2 = 3$. Perlu diingat bahwa decision boundary
**tidak selalu linear** (bisa saja berbentuk lingkaran atau tidak beraturan).
Pada akhirnya, decision boundary adalah garis yang memisahkan kelas negatif dan
positif.

# Menentukan Parameter

Dalam menentukan parameter ($\theta$) pada regresi logistik, tahapan yang
digunakan hampir sama dengan kasus regresi linear yaitu dengan menggunakan
*gradient descent*.

# Cost Function pada Regresi Logistik

Misalkan dipunyai training set
$\{(x^{(1)} y^{(1)}), (x^{(2)} y^{(2)}), \dots, (x^{(m)} y^{(m)})\}$.

$$x \in \begin{bmatrix}
    x_0 \\
    x_1 \\
    \cdots \\
    x_n
\end{bmatrix}\quad x_0 = 1, y \in \{0, 1\}$$

Pada regresi linear, fungsi biaya atau cost function dihitung dengan menggunakan
squared error function. Maka, untuk regresi logistik:

$$\text{Cost}(h_\theta(x^{(i)}), y^{(i)}) = \frac{1}{2} (h_\theta(x^{(i)}) -
y^{(i)})^2$$

Jika di-*plot*, fungsi biaya di atas akan terlihat cenderung *wiggly* dan
mempunyai banyak local minima (yang ingin kita cari adalah pada global minima).

$$J(\theta) = \frac{1}{m} \sum_{i = 1}^m \text{Cost}(h_\theta(x^{(i)}), y^{(i)})$$

$$Cost(h_\theta(x), y) = \begin{cases}
    -\log(h_\theta(x)) &\quad \text{jika}\ y = 1 \\
    -\log(1 - h_\theta(x)) &\quad  \text{jika}\ y = 0
\end{cases}$$

**Intuisi:** jika $h_\theta(x) = 0$ (prediksi $P(y = 1 | x; \theta) = 0)$, namun
$y = 1$, algoritma learning akan diberi penalti dengan biaya yang besar (berlaku
sebaliknya).

Fungsi $\text{Cost}(h_\theta(x), y)$ sebenarnya bisa digabungkan menjadi lebih
sederhana.

$$\text{Cost}(h_\theta(x), y) = -y \log(h_\theta(x)) - (1 - y) \log(1 -
h_\theta(x))$$

Sehingga,

$$J(\theta) = - \frac{1}{m}\big[
    \sum_{i = 1}^{m} y^{(i)} \log h_\theta(x^{i}) + (1 - y^{(i)}) \log (1 -
    h_\theta(x^{(i)}))
\big]$$

Untuk fitting parameter $\theta$, cost function diminimalkan untuk mencari cost
function yang optimal atau $\min_{\theta} J(\theta)$. Untuk memprediksi suatu
$x$ yang baru,

$$h_\theta(x) = \frac{1}{1 + e^{-\theta^T x}}$$

**Catatan:** Ingat kembali, $x^{(i)}$ adalah vektor fitur dengan $i$ adalah
indeksnya.

## Gradient Descent

Diinginkan $\min_\theta J(\theta)$:

Repeat {
$$\qquad \theta_j := \theta_j - \alpha \frac{\partial}{\partial \theta_j}
    J(\theta)$$ } (perbarui semua $\theta_j$ terus-menerus)

# Multi-class Classification: One-vs-All

Multi-class berbeda dengan multi-label. Multi-class artinya class-nya lebih dari
satu. Multi-label artinya satu data bisa masuk ke beberapa kelas. Pada
klasifikasi biner, dipisahkan dua kelas berdasarkan decision boundary. Metode
one-vs-all adalah salah satu cara untuk mengklasifikasikan lebih dari dua kelas
dengan membangun beberapa model. Misalkan ada tiga kelas: 1, 2, dan 3. Untuk
bisa mengklasifikasikan kelas-kelas tersebut, dibuat tiga model: 1 vs 23, 2 vs
13, dan 3 vs 12.

$$h_\theta^{(i)}(x) = P(y = i|x; \theta) \quad (i = 1, 2, 3)$$

Latih classifier regresi logistik $h_\theta^{(i)}(x)$ untuk setiap kelas $i$
untuk memprediksi probabilitas $y = i$. Untuk input baru $x$, prediksi dibuat
dengan memilih kelas $i$ yang memaksimalkan $\max_i h_\theta^{(i)}(x)$.
