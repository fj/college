---
author: Faiz Jazadi
date: '2023-03-10'
numbersections: true
subtitle: Density Clustering
title: 'Pertemuan Minggu ke-4'
---

# Kriteria Clustering

1.  Scalability
2.  Mampu menangani berbagai jenis atribut
3.  Dapat menemukan cluster dengan bentuk sembarang
4.  Memerlukan minimal parameter
5.  Mampu menangani data noise
6.  Tidak sensitif terhadap urutan data
7.  Dapat menangani malah data dengan dimensi tinggi

# Data yang Ideal untuk Clustering

Mudah kalau datanya bagus. Namun, data pada kenyataannya tidak selalu mudah.

# Density-Based Clustering Methods

-   Cluster berdasarkan densitas, misalnya dilihat dari kepadatan titik-titik
    yang terhubung atau berdasarkan suatu fungsi kepadatan
-   Fitur:
    -   Mengenali cluster dengan bentuk apapun
    -   Menangani noise
    -   One scan
    -   Need density parameters
-   Studi yang menarik:
    -   DBSCAN
    -   DENCLUE
    -   OPTICS
    -   CLIQUE

## DBSCAN

-   Density = banyak poin dalam radius tertentu $r$ (Eps, $\epsilon$)
-   Suatu titik adalah **core point** jika mempunyai titik lebih dari suatu
    nilai MinPts dalam $\epsilon$
    -   Atau titk-titik yang ada pada interior suatu cluster
-   Border point jika punya titik-titik dalam $\epsilon$ lebih sedikit daripada
    MinPts, tapi dalam ketetanggaan suatu core point
-   Noise point adalah titik yang bukan core pore point atau border point

### Bagaimana clusteringnya?

Ada dua parameter:

-   $\epsilon$: maksimum radius ketetanggaan
-   MinPts: nilai banyak titik minimum dalam suatu ketetanggaan Eps suatu titik
-   Directly density-reachable: suatu point $p$ directly density-reachable dari
    suatu point $q$ jika
    1)  $p$ anggota $N_{\epsilon}(q)$
    2)  core point condition: $|N_{\epsilon}(q)| \geq \text{MinPts}$
-   Density-reachable: suatu $p$ density-reachable (tidak direct) dari $q$ jika
    ada chain of points $p_1, \dots, p_n$ di mana di antara $p_1$ dan $p_n$ ada
    titik yang direcly density-reachable dari $p_i$
-   Density-connected: jika ada point $o$ sedemikian hingga $p$ dan $q$
    sama-sama density-reachable dari $o$ dalam $\epsilon$ dan MinPts

Untuk membentuk suatu cluster:

1.  DBSCAN mengecek tiap titik
2.  Kemudian akan menetapkan status. Jika $p$ adalah core point, maka dia akan
    membentuk cluster atas tetangga-tetangganya. Jika $p$ adalah border point
    tapi tidak ada titik dalam tetangganya yang density-reachable dari $p$, maka
    diabaikan (tidak dibentuk cluster dari $p$), jika ada maka bisa dibentuk
    cluster.

### Kekurangan

-   Ketika ada density yang bervariasi
-   Data yang berdimensi tinggi

# Ukuran Validitas Cluster

-   External index, ada informasi label sebelumnya
-   Internal index, tanpa melihat informasi dari luar (hanya melihat isi
    cluster), contoh DV Index, Silhouette Index
    -   Cluster cohesion
    -   Cluster separation
-   Relative index, membandingkan dua skema cluster berbeda, contoh SSE atau
    entropi
