---
author: Faiz Jazadi
date: '2023-02-24'
numbersections: true
subtitle: 'Data Visualization & Exploration'
title: Pertemuan 2
---

# Visualisasi Variabel

-   **Pie chart**, untuk menunjukkan seberapa banyak setiap jenis kategori dalam
    dataset berbanding dengan keseluruhan
-   **Bar chart**, untuk membandingkan data kategorikal, hanya saja tidak dalam
    bentuk persen
-   **Line graph**, untuk menunjukkan bagaimana kemajuan data selama beberapa
    periode
    -   Biasanya kita mengilustrasikan dua garis untuk menggambarkan dua data
        (data aktual dan data prediksi)
-   **Scatter plot**, untuk menunjukkan distribusi dari suatu dataset (banyak
    juga digunakan untuk melihat natural cluster)
    -   Contoh: visualisasi data yang terkait dengan perbedaan lemon dan lime
        berdasarkan karateristik fisiologis
-   **Heatmap**, jenis visualisasi yang menggunakan kode warna untuk mewakili
    nilai atau kepadatan relatif data di seluruh permukaan. Warna ini kemudian
    digunakan untuk memeriksa data secara visual guna menemukan kelompok dengan
    nilai serupa dan mendeteksi tren dalam data.
-   **Histogram**, cukup penting untuk memahami distribusi pada data

# Tugas 1

**Deadline:** Kamis, 1 Maret 2023 23:59

-   Mencari minimal 1 dataset terstruktur dan 1 dataset tidak terstruktur secara
    programatik menggunakan API atau web scraping
-   Jelaskan langkah-langkahnya
-   Kemudian lakukan eksplorasi:
    -   Visualisasi variabel untuk beberapa variabel/feature dengan variabel
        yang sesuai, dan
    -   Visualisasi statistik
