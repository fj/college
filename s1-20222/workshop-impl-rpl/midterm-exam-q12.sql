-- vim:sw=2:sts=2:ts=2:et:
DROP DATABASE IF EXISTS wrplmte;
CREATE DATABASE wrplmte;
USE wrplmte;

-- Tabel Kategori
CREATE TABLE Kategori (
  id INT NOT NULL AUTO_INCREMENT,
  nama VARCHAR(100) NOT NULL,
  PRIMARY KEY (id)
);

INSERT INTO Kategori (nama)
VALUES
('Outdoor & Adventure'),
('Home & Decor'),
('Health & Wellness'),
('Fashion & Accessories'),
('Toys & Games'),
('Beauty & Personal Care'),
('Sports & Fitness'),
('Electronics & Gadgets'),
('Pet Supplies'),
('Arts & Crafts'),
('Food & Beverage'),
('Books & Magazines'),
('Travel & Leisure'),
('Office & Stationery'),
('Music & Entertainment'),
('Baby & Kids'),
('Automotive & Motorcycles'),
('DIY & Tools'),
('Party & Events'),
('Vintage & Collectibles');


-- Tabel Produk
CREATE TABLE Produk (
  id INT PRIMARY KEY AUTO_INCREMENT,
  nama VARCHAR(255),
  harga DECIMAL(10,2),
  stok INT
);

INSERT INTO Produk (id, nama, harga, stok) VALUES
(1, 'T-Shirt', 10.50, 100),
(2, 'Jeans', 25.75, 50),
(3, 'Sneakers', 60.00, 25),
(4, 'Watch', 120.99, 10),
(5, 'Backpack', 35.00, 75),
(6, 'Hoodie', 45.25, 80),
(7, 'Sunglasses', 20.99, 30),
(8, 'Phone Case', 12.00, 150),
(9, 'Headphones', 70.50, 20),
(10, 'Jacket', 95.00, 15);


-- Tabel Pembelian      
CREATE TABLE PembelianProduk (
  id INT PRIMARY KEY AUTO_INCREMENT,
  id_produk INT,
  jumlah INT,
  harga DECIMAL(10, 2)
);

INSERT INTO PembelianProduk
(id_produk, harga, jumlah)
VALUES
(1, 10.50, 50),
(2, 25.75, 25),
(3, 60.00, 7),
(4, 120.99, 10),
(5, 35.00, 5),
(6, 45.25, 8),
(7, 20.99, 15),
(8, 12.00, 13),
(9, 70.50, 17),
(10, 95.00, 18);


-- Tabel Pelanggan
CREATE TABLE Pelanggan (
  id INT PRIMARY KEY,
  email VARCHAR(255),
  password VARCHAR(255),
  nama VARCHAR(255),
  no_tlp VARCHAR(20)
);

INSERT INTO Pelanggan (id, email, password, nama, no_tlp) VALUES
(1, 'john@example.com', 'password1', 'John Doe', '555-1234'),
(2, 'jane@example.com', 'password2', 'Jane Smith', '555-5678'),
(3, 'bob@example.com', 'password3', 'Bob Johnson', '555-2468'),
(4, 'amy@example.com', 'password4', 'Amy Brown', '555-9876'),
(5, 'adam@example.com', 'password5', 'Adam Davis', '555-6543'),
(6, 'lisa@example.com', 'password6', 'Lisa Taylor', '555-7890'),
(7, 'mike@example.com', 'password7', 'Mike Lee', '555-1357'),
(8, 'jill@example.com', 'password8', 'Jill Scott', '555-8642'),
(9, 'ted@example.com', 'password9', 'Ted Anderson', '555-3197'),
(10, 'sara@example.com', 'password10', 'Sara Green', '555-4268'),
(11, 'dave@example.com', 'password11', 'Dave Wilson', '555-7529'),
(12, 'jessica@example.com', 'password12', 'Jessica Brown', '555-2836'),
(13, 'steve@example.com', 'password13', 'Steve Miller', '555-6425'),
(14, 'emily@example.com', 'password14', 'Emily Young', '555-8793'),
(15, 'chris@example.com', 'password15', 'Chris Martin', '555-3654');


-- Tabel Pembayaran
CREATE TABLE Pembayaran (
  id INT PRIMARY KEY,
  nama VARCHAR(255),
  jumlah INT,
  bukti VARCHAR(255),
  tanggal DATE
);

INSERT INTO Pembayaran (id, nama, jumlah, bukti, tanggal) VALUES
(1, 'John Doe', 200, 'bukti_1.jpg', '2023-04-01'),
(2, 'Jane Smith', 100, 'bukti_2.jpg', '2023-04-02'),
(3, 'Bob Johnson', 150, 'bukti_3.jpg', '2023-04-03'),
(4, 'Amy Brown', 75, 'bukti_4.jpg', '2023-04-04'),
(5, 'Adam Davis', 50, 'bukti_5.jpg', '2023-04-05'),
(6, 'Lisa Taylor', 225, 'bukti_6.jpg', '2023-04-06'),
(7, 'Mike Lee', 125, 'bukti_7.jpg', '2023-04-07'),
(8, 'Jill Scott', 80, 'bukti_8.jpg', '2023-04-08'),
(9, 'Ted Anderson', 100, 'bukti_9.jpg', '2023-04-09'),
(10, 'Sara Green', 150, 'bukti_10.jpg', '2023-04-10'),
(11, 'Dave Wilson', 50, 'bukti_11.jpg', '2023-04-11'),
(12, 'Jessica Brown', 200, 'bukti_12.jpg', '2023-04-12'),
(13, 'Steve Miller', 90, 'bukti_13.jpg', '2023-04-13'),
(14, 'Emily Young', 120, 'bukti_14.jpg', '2023-04-14'),
(15, 'Chris Martin', 75, 'bukti_15.jpg', '2023-04-15');




-- STORED PROCEDURES
DELIMITER $$
CREATE PROCEDURE DisplayAllProducts()
BEGIN
    SELECT * FROM Produk;
END$$

CREATE PROCEDURE AddNewProduct(
    IN nama VARCHAR(50),
    IN harga DECIMAL(10, 2),
    IN stok INT
)
BEGIN
    INSERT INTO Produk (nama, harga, stok)
    VALUES (nama, harga, stok);
END$$
DELIMITER ;

CALL AddNewProduct('Product SQ23', 1999, 10);
SELECT * FROM Produk WHERE nama = 'Product SQ23';
