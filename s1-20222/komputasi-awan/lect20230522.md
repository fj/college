---
author: Faiz Jazadi
date: 2023-05-22
numbersections: true
subtitle: Service Level Agreement
title: Pertemuan Minggu ke 13?
---

# Definisi SLA

-   Kontrak formal antara dua pihak
-   Pihak pertama adalah penyedia layanan
-   Pihak kedua adalah pelanggan
-   SLA mendefinisikan layanan yang diberikan
-   SLA mendefinisikan kualitas layanan
-   SLA mendefinisikan kewajiban penyedia layanan
-   SLA mendefinisikan kewajiban pelanggan
-   SLA mendefinisikan kompensasi jika layanan tidak sesuai
-   SLA mendefinisikan batas waktu untuk kompensasi

# Pentingnya SLA

Bagi kedua belah pihak, SLA adalah informasi formal tentang provider.

-   Informasi yang lenkap tentang layanan dan tipe layanan
-   Memuat tujuan dan acuan layanan berdasarkan kepentingan bisnis
-   Memuat tujuan dan acuan layanan berdasarkan kepentingan teknis
-   Pedoman bagi konsumen

# Level SLA

-   Customer-based
    -   mengacu pada kebutuhan dan harapan konsumen
-   Service-based
    -   mengacu pada atribut dan karakter layanan
-   Multi-level
    -   SLA berjenjang
    -   SLA di tingkat IaaS, di tingkat PaaS, di tingkat SaaS
    -   Metrik/standar kinerja menyesuaikan tingkat

# Muatan SLA

> Fungsional itu bukan di SLA

-   Kebutuhan non-fungsional
-   Service Level Objective (SLO)
-   Key Performance Indicators (KPI)
-   Recovery Time Objective (RTO)
-   Metric: recovery time, resolution time, response time, availability,
    reliability, accountability, warranties

# SLO

Target kinerja yang diharapkan untuk sebuah layanan.

Keypoint:

-   Definisi kinerja
-   Informasi harapan
-   Pengukuran dan monitoring
-   Pemenuhan kebutuhan/bisnis
-   Kebijakan dan perbaikan
-   Evaluasi dan peningkatan
-   Kontrak kesepakatan

# KPI

Layanan dikatakan berhasil apabila ...

# Tugas Diskusi

-   Cari contoh SLA di cloud
-   Eksplorasi SLA yang Anda temukan
-   Uraikan SLA yang ditemukan
