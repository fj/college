---
author: Faiz Jazadi
date: '2023-03-06'
numbersections: true
subtitle: Keamanan Cloud
title: 'Pertemuan ke-4'
---

# Rationale

-   Lost of control, infrastrukturnya bukan milik kita (pakai rangka sendiri aja
    tidak yakin, apalagi menitipkan di rangka orang/cloud) dan berbagi bakai
    dengan orang lain
-   Lack of trust:
-   Multi-tenancy

## Scope

**Khususnya di IaaS, di atasnya menyesuaikan**

Ranah provider biasanya ada di sisi hardware.

Ranah pelanggan ada di atas ranah hypervisor.

-   User
-   Firewall
-   Aplikasi dan perangkat klien
-   Virtual machine
-   Otentikasi
-   Patch and backup

# Keamanan Informasi

-   **C**onfidentiality/kerahasiaan, aman dari mata yang mengintip
-   Authenticity, data dari sumber yang dikenal
-   **I**ntegrity, data belum dirusak/dimodifikasi
-   Non-repudiasi, atau ada jaminan terhadap pengingkaran

## Apa yang diamankan?

-   Akses kontrol, "akses dan modifikasi pengguna istimewa"
-   Keamanan jangka panjang
-   (sistem kripto tidak memberikan jaminan jangka panjang)

# Cara Mengamankan

## Enkripsi

Simetris, PKC, HMAC, signature, TLS/SSL.

Dengan enkripsi, data hanya bisa diamankan saat transit dan at rest.

Ada juga homomorphic encryption, untuk mengamankan data at rest (bcrypt?).

# Isu Pengambilan Informasi Private

-   PIR/SPIR
-   Pencarian data dienkripsi
-   Secure anonymous database search (SADS)
-   Tidak mudah

# Isu Data Remanence

> Representasi sisa data (residu) setelah pembersihan

-   Bagaimana memmbersihkan residu data di cloud?
    -   risiko di semua tingkatan (SaaS, PaaS, dan IaaS)
-   Penghapusan aman
    -   Enkripsi data di awan
    -   Penghapusan data = penghancuran kunci

# Administrasi Terpisah

-   Pelanggan cloud kehilangan kontrol atas perhitungan dan datanya
-   Bagaimana jika sesuatu yang tidak beres?
-   Tanggung jawab manajemen: dobago
-   Siapa yang harus mengatasi masalah?

# Tamper-Evident Log

Catatan yang kalau berubah kalau datanya diubah sehingga tidak bisa di-tamper.

# Manfaat Cloud Akuntabel

-   Insentif pelanggan
-   Insentif provider

**Masih banyak lagi bagian lain namun tidak dicatat.**
