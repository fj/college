---
author: Faiz Jazadi
date: '2023-02-20'
numbersections: true
subtitle: Cloud Computing
title: Pertemuan 2
---

# Cloud

-   Internet
-   Lokasi logik secara infrastruktur
-   Penyederhanaan kompleksitas internet
-   Batasan:
    -   Infrastruktur: terhubung ke internet dengan
    -   Teknlogi: TCP/IP
    -   Tata kelola: mungkin terhubung ke internet, tapi tidak berada di cloud
        (tidak publik?)

# Cloud Computing

-   Semua aktivitas di komputer adalah aktivitas komputasi
-   Maka, semua aktivitas komputer yang disediakan di internet adalah cloud
    computing

Apakah komputasi awan seluas ini?

## Grid & Clustering

-   Multi factual, single view
    -   Banyak objek komputasi berkumpul, di-abstraksi sehingga nampak seperti
        komputer tunggal
-   Virtualisasi, membagi-bagi secara virtual sesuai kebutuhan konsumsi/aplikasi

## Pembatasan Cloud Computing

Grid & clustering + virtualisasi $\rightarrow$ cloud computing

> Dikumpul-kumpulkan jadi satu, terus dipecah-pecah lagi. Misalnya dua objek
> komputasi, 10 + 10 = 20 unit komputasi. Ternyata, rata-rata konsumsi adalah
> sebesar 2 unit komputasi. Maka, dengan virtualisasi, bisa dipecah-pecah
> menjadi 10 komputer yang bisa disewakan terpisah.

Menurut Gartner, 2008

> a style of computing in which **scalable** and **elastic** IT-enabled
> capabilities are delivered as **a service** to external customers using
> Internet technologies.

Menurut Thomas Earl, et al, 2013

> a specialized form of **distributed** computing that introduce utilization
> models for **remotely provisioning scalable** and **measured** resources.

Dari perspektif konsumen

> menggunakan sarana prasarana komputasi pihak lain (HW/SW) guna mengelola data,
> infrormasi dan proses bisnis kita.

## Elastic dan Scalable

Dikatakan *elastic* jika memiliki karakteristik berikut:

-   luwes,
-   penyesuaian kapasitas komputasi, dan
-   sesuai kebutuhan dan kemampuan.

Dikatakan *scalable* jika memiliki karakteristik berikut:

-   mampu menambah/mengurangi sumberdaya, baik secara
    -   vertikal, atau perubahan sumberdaya objek tunggal, maupun
    -   horizontal, atau perubahan kuantitas objek.

## Daya Tarik

-   Tinggal pakai tanpa repot mengadakan
-   Memungkinkan mendapat layanan komputasi yang besar
-   Membuka kesempatan bagi start-up
-   Hemat biaya
-   Fleksibilitas dan skalabilitas

## Keuntungan

-   Mengubah biaya investasi menjadi biaya operasional (CapEx to OpEx)
-   Benefit from massive economies of scale
-   Tidak perlu menebak-nebak kebutuhan kapasitas
-   Meningkatakan speed and agility
-   Tidak perlu menghabiskan uang untuk menjalankan dan merawat data center
-   Waktu server deployment yang cepat

## Karakteristik

-   On-demand
-   Multitenance
-   Measured usage
-   Ubiquitous access
-   Elasticity
-   Resiliency

## Model

-   Service
    -   SaaS (less control)
        -   Contoh: GMail, Google Docs, Google Sheets
    -   PaaS (higher control)
        -   Mirip SaaS, namun lebih di aspek pengembangan aplikasi
        -   Menyediakan development core yang bisa dikembangkan lebih lanjut
            oleh pelanggan sesuai kebutuhan
        -   Contoh: Firebase, AppWrite
    -   IaaS (much higher control)
        -   Menyediakan layanan infrastruktur
        -   Berbasis layanan tinggal pakai
        -   Dynamic scalability
        -   Umumnya berbagi pakai dengan pelanggan lain
-   Deployment
    -   Private: aktif untuk satu pelanggan, mahal
    -   Public/community: berbagi pakai
    -   Hybrid: gabungan

# Tugas

Buat artikel di Wiki (eLOK) tentang cloud computing, boleh apa saja selama ada
di domain cloud computing.
